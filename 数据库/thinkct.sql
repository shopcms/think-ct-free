-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2021-08-18 21:07:30
-- 服务器版本： 5.6.50-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suzhizhan`
--

-- --------------------------------------------------------

--
-- 表的结构 `shig_ad`
--

CREATE TABLE IF NOT EXISTS `shig_ad` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `type1` char(50) DEFAULT NULL,
  `jpggif` char(50) DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `adbh` char(30) DEFAULT NULL,
  `txt` text,
  `sj` datetime DEFAULT NULL,
  `dqsj` datetime DEFAULT NULL,
  `uip` char(30) DEFAULT NULL,
  `aurl` varchar(230) DEFAULT NULL,
  `sm` varchar(230) DEFAULT NULL,
  `tel` char(50) DEFAULT NULL,
  `xh` int(10) DEFAULT NULL,
  `other1` varchar(230) DEFAULT NULL,
  `aw` int(10) DEFAULT NULL,
  `ah` int(10) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shig_ad`
--

INSERT INTO `shig_ad` (`id`, `bh`, `type1`, `jpggif`, `tit`, `adbh`, `txt`, `sj`, `dqsj`, `uip`, `aurl`, `sm`, `tel`, `xh`, `other1`, `aw`, `ah`) VALUES
(83, '1575904757', '文字', NULL, '小锋资源网', 'AD001', NULL, '2019-12-09 23:19:17', NULL, '106.112.82.216', 'http://xiaofengzyw.cn/', '首页友情链接', '', 13, '', NULL, NULL),
(84, '1575905241', '文字', NULL, '码发布', 'AD001', NULL, '2019-12-09 23:27:21', NULL, '106.112.82.216', 'http://www.mafabu.com', '首页友情链接-*', '', 2, '', NULL, NULL),
(85, '1575987767', '文字', NULL, '无忧源码', 'AD001', NULL, '2019-12-10 22:22:47', NULL, '221.192.179.236', 'https://www.ymwy.net/', '首页友情链接-*', '', 3, '', NULL, NULL),
(86, '1576338521', '文字', NULL, '科雅资源网', 'AD001', NULL, '2019-12-14 23:48:41', NULL, '221.192.179.202', 'http://www.clo19.cn', '首页友情链接-*', '', 4, '', NULL, NULL),
(87, '1576338587', '文字', NULL, '淘码库', 'AD001', NULL, '2019-12-14 23:49:47', NULL, '221.192.179.202', 'http://www.taomaku.com/', '首页友情链接-*', '', 5, '', NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_ads`
--

CREATE TABLE IF NOT EXISTS `shop_ads` (
  `id` int(11) NOT NULL,
  `bh` char(20) DEFAULT NULL,
  `type1` int(2) DEFAULT NULL,
  `type2` int(2) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `sm` varchar(250) DEFAULT '',
  `tit` char(50) DEFAULT NULL,
  `xh` int(2) DEFAULT NULL,
  `day` int(3) DEFAULT NULL,
  `jifen` int(10) DEFAULT '0',
  `adnum` int(2) DEFAULT NULL,
  `type` char(20) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_ads`
--

INSERT INTO `shop_ads` (`id`, `bh`, `type1`, `type2`, `sj`, `money`, `sm`, `tit`, `xh`, `day`, `jifen`, `adnum`, `type`) VALUES
(1, '10444504', 1, 1, '2014-02-12 03:28:10', '0.00', '首页限时特价倒计时广告位，按天计算', '限时特价广告推广', 1, 0, 0, 8, 'code'),
(2, '104040', 1, 2, '2014-02-12 03:27:08', '0.00', '源码集市列表上方横排推荐广告位，按天计算', '列表推荐广告推广', 2, 0, 0, 12, 'code'),
(3, '1111', 2, 1, '2014-02-12 04:35:31', '10.00', '', '', 1, 1, 100, NULL, NULL),
(5, '1392212513', 2, 1, '2014-02-12 21:41:53', '27.00', '', '', 2, 3, 270, NULL, NULL),
(6, '1392212547', 2, 1, '2014-02-12 21:42:27', '50.00', '', '', 3, 7, 500, NULL, NULL),
(7, '1392212859', 2, 2, '2014-02-12 21:47:39', '20.00', '', '', 1, 3, 200, NULL, NULL),
(8, '1392213026', 2, 2, '2014-02-12 21:50:26', '40.00', '', '', 2, 7, 400, NULL, NULL),
(9, '1392213164', 2, 2, '2014-02-12 21:52:44', '60.00', '', '', 3, 14, 600, NULL, NULL),
(10, '1392213276', 2, 2, '2014-02-12 21:54:36', '100.00', '', '', 4, 30, 1000, NULL, NULL),
(11, '1392227697', 2, 1, '2014-02-13 01:54:57', '200.00', '', '', 4, 30, 2000, NULL, NULL),
(12, '1440326929', 1, 3, '2015-08-23 18:48:49', '0.00', '暂只适合有出售源码的店铺', '首页推荐源码店', 3, NULL, 0, 9, 'sell'),
(13, '1440327255', 1, 4, '2015-08-23 18:54:15', '0.00', '购买后可设置自定义标签链接', '首页推荐米铺', 4, NULL, 0, 9, 'sell'),
(14, '1440327298', 1, 5, '2015-08-23 18:54:58', '0.00', '购买后可设置自定义标签链接', '首页推荐服务商', 5, NULL, 0, 3, 'sell'),
(15, '1440327654', 1, 6, '2015-08-23 19:00:54', '0.00', '可以让网站出售信息排在网站交易首页最前列', '网站信息置顶', 6, NULL, 0, 10, 'web'),
(16, '1440327777', 1, 7, '2015-08-23 19:02:57', '0.00', '可以让域名出售信息排在域名交易首页最前列', '域名信息置顶', 7, NULL, 0, 10, 'domain'),
(17, '1440327807', 1, 8, '2015-08-23 19:03:27', '0.00', '可以让任务信息排在任务大厅首页最前列', '任务信息置顶', 8, NULL, 0, 10, 'task'),
(18, '1440327907', 1, 9, '2015-08-23 19:05:07', '0.00', '暂只适合有出售源码的店铺', '商家专区热荐商家', 9, NULL, 0, 6, 'sell'),
(19, '1440327994', 2, 3, '2015-08-23 19:06:34', '10.00', '', '', 1, 1, 100, NULL, 'sell'),
(20, '1440328647', 2, 3, '2015-08-23 19:17:27', '27.00', '', '', 2, 3, 270, NULL, 'sell'),
(21, '1440328984', 2, 3, '2015-08-23 19:23:04', '50.00', '', '', 3, 7, 500, NULL, 'sell'),
(22, '1440329073', 2, 3, '2015-08-23 19:24:33', '180.00', '', '', 4, 30, 1800, NULL, 'sell'),
(23, '1440389227', 2, 4, '2015-08-24 12:07:07', '4.00', '', '', 1, 1, 40, NULL, 'sell'),
(24, '1440389270', 2, 4, '2015-08-24 12:07:50', '10.00', '', '', 2, 3, 100, NULL, 'sell'),
(25, '1440389284', 2, 4, '2015-08-24 12:08:04', '16.00', '', '', 3, 7, 160, NULL, 'sell'),
(26, '1440389364', 2, 4, '2015-08-24 12:09:24', '50.00', '', '', 4, 30, 500, NULL, 'sell'),
(27, '1440389385', 2, 5, '2015-08-24 12:09:45', '4.00', '', '', 1, 1, 40, NULL, 'sell'),
(28, '1440389391', 2, 5, '2015-08-24 12:09:51', '10.00', '', '', 2, 3, 100, NULL, 'sell'),
(29, '1440389396', 2, 5, '2015-08-24 12:09:56', '16.00', '', '', 3, 7, 160, NULL, 'sell'),
(30, '1440389403', 2, 5, '2015-08-24 12:10:03', '50.00', '', '', 4, 30, 500, NULL, 'sell'),
(31, '1440389600', 2, 9, '2015-08-24 12:13:20', '5.00', '', '', 1, 1, 50, NULL, 'sell'),
(32, '1440389612', 2, 9, '2015-08-24 12:13:32', '12.00', '', '', 2, 3, 120, NULL, 'sell'),
(33, '1440389623', 2, 9, '2015-08-24 12:13:43', '20.00', '', '', 3, 7, 200, NULL, 'sell'),
(34, '1440389833', 2, 9, '2015-08-24 12:17:13', '70.00', '', '', 4, 30, 700, NULL, 'sell'),
(35, '1440421054', 2, 6, '2015-08-24 20:57:34', '100.00', '', '', 1, 1, 100, NULL, 'web'),
(36, '1440421066', 2, 6, '2015-08-24 20:57:46', '27.00', '', '', 2, 3, 270, NULL, 'web'),
(37, '1440421082', 2, 7, '2015-08-24 20:58:02', '27.00', '', '', 1, 3, 270, NULL, 'domain'),
(38, '1440421094', 2, 7, '2015-08-24 20:58:14', '50.00', '', '', 2, 7, 500, NULL, 'domain'),
(39, '1440421119', 2, 7, '2015-08-24 20:58:39', '200.00', '', '', 3, 30, 2000, NULL, 'domain');

-- --------------------------------------------------------

--
-- 表的结构 `shop_announce_log`
--

CREATE TABLE IF NOT EXISTS `shop_announce_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '公告ID',
  `create_at` int(11) NOT NULL DEFAULT '0' COMMENT '阅读时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_anquan`
--

CREATE TABLE IF NOT EXISTS `shop_anquan` (
  `id` int(10) NOT NULL,
  `ubh` char(20) DEFAULT NULL,
  `uip` char(50) DEFAULT NULL,
  `type` char(50) DEFAULT NULL,
  `way` char(10) DEFAULT '0',
  `sj` datetime DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `pc` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_app_menu`
--

CREATE TABLE IF NOT EXISTS `shop_app_menu` (
  `id` int(11) NOT NULL,
  `function_id` int(11) NOT NULL COMMENT '唯一值',
  `menu` text NOT NULL COMMENT '菜单项'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='客户端菜单';

--
-- 转存表中的数据 `shop_app_menu`
--

INSERT INTO `shop_app_menu` (`id`, `function_id`, `menu`) VALUES
(1, 0, '{"title":"\\u5546\\u54c1\\u5206\\u7c7b","img_url":"http://api.faka.zuy.cn/static/upload/5b5ad8346b8f7/5b5ad8346b933.png","function_id":"0","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.goods.GoodsGenreActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"ClassifyListViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/goodsCategory/lists/lists"}'),
(2, 1, '{"title":"\\u6dfb\\u52a0\\u5546\\u54c1","img_url":"http://api.faka.zuy.cn/static/upload/5b5ad8d9671f5/5b5ad8d96722e.png","function_id":"1","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.goods.AddGoodsActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"AddGoodsViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/goods/add/add"}'),
(3, 2, '{"title":"\\u5546\\u54c1\\u5217\\u8868","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5ad920eed40\\/5b5ad920eed7a.png","function_id":"2","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.goods.GoodsListActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"GoodsListViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/goods/lists/lists"}'),
(4, 3, '{"title":"\\u5361\\u5bc6\\u5217\\u8868","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5ada3c8c52f\\/5b5ada3c8c568.png","function_id":"5","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.CardListActivity","iOS_ViewType":"1","iOS_sotryBoard":"Cards","iOS_ViewController":"CardListViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/goodsCard/lists/lists"}'),
(5, 4, '{"title":"\\u5e97\\u94fa\\u94fe\\u63a5","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5adc07170e2\\/5b5adc071711c.png","function_id":"3","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.goods.StoreLinksActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"StoreLinkViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/user/link/link"}'),
(6, 5, '{"title":"\\u6dfb\\u52a0\\u5361\\u5bc6","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5adc6b692fb\\/5b5adc6b69335.png","function_id":"4","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.CardAddActivity","iOS_ViewType":"1","iOS_sotryBoard":"Cards","iOS_ViewController":"AddCardsViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/goodsCard/edit/edit"}'),
(7, 6, '{"title":"\\u8ba2\\u5355\\u5217\\u8868","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5adce9bdd2e\\/5b5adce9bdd68.png","function_id":"6","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.order.OrderListActivity","iOS_ViewType":"1","iOS_sotryBoard":"Order","iOS_ViewController":"OrderListViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/order/lists/lists"}'),
(8, 7, '{"title":"\\u4f18\\u60e0\\u5238","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5add2b1ed3c\\/5b5add2b1ed75.png","function_id":"7","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.CouponsActivity","iOS_ViewType":"1","iOS_sotryBoard":"Coupon","iOS_ViewController":"CouponListViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/goodsCoupon/lists/lists"}'),
(9, 8, '{"title":"\\u63d0\\u73b0\\u7ba1\\u7406","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5add6aafc89\\/5b5add6aafcc3.png","function_id":"8","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.ApplyMoneyListActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"EnchashmentListViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/user/cashList/cashList"}'),
(10, 9, '{"title":"\\u4ed8\\u6b3e\\u65b9\\u5f0f","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5addb00d1b5\\/5b5addb00d1ee.png","function_id":"9","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.PayWayActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"PayTypeListViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/pay/lists/lists"}'),
(11, 10, '{"title":"\\u5361\\u5bc6\\u56de\\u6536","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5addcf3fab3\\/5b5addcf3faec.png","function_id":"10","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.RecycleBinActivity","iOS_ViewType":"1","iOS_sotryBoard":"Cards","iOS_ViewController":"CardRecycleViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/goodsCard/trash/trash"}'),
(12, 11, '{"title":"\\u6536\\u76ca\\u5206\\u6790","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5addf6a6548\\/5b5addf6a6581.png","function_id":"11","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.IncomeAnalysisActivity","iOS_ViewType":"1","iOS_sotryBoard":"Order","iOS_ViewController":"InComeAnalyzeViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/user/static/statics"}'),
(13, 12, '{"title":"\\u6e20\\u9053\\u5206\\u6790","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5ade8db3465\\/5b5ade8db349e.png","function_id":"12","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.ChannelAnalysisActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"ChannelAnalysisViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/pay/detail/detail"}'),
(16, 14, '{"title":"\\u767b\\u5f55\\u65e5\\u5fd7","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5b4e6037138\\/5b5b4e6037171.png","function_id":"14","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.LoginDiaryActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"LoginDiaryViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/user/loginLogs/loginLogs"}'),
(17, 15, '{"title":"\\u63a8\\u5e7f\\u7ba1\\u7406","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5bccc8c9960\\/5b5bccc8c99a0.png","function_id":"15","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.SpreadManageActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"PromoteViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/spread/lists/lists"}'),
(18, 16, '{"title":"\\u6295\\u8bc9\\u7ba1\\u7406","img_url":"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5bce9b6e4ed\\/5b5bce9b6e526.png","function_id":"16","sort":0,"is_show":true,"function_links":"cc.zuy.faka_android.ui.activity.menu.ComplainActivity","iOS_ViewType":"1","iOS_sotryBoard":"Home","iOS_ViewController":"ComplainViewController","android_Ver":"1.0","iOS_Ver":"1.0","wxapp_page":"/pages/complaint/lists/lists"}');

-- --------------------------------------------------------

--
-- 表的结构 `shop_app_version`
--

CREATE TABLE IF NOT EXISTS `shop_app_version` (
  `id` int(11) NOT NULL,
  `platform` enum('android','ios') NOT NULL COMMENT '平台',
  `package` varchar(255) NOT NULL COMMENT '安装包下载地址',
  `create_at` int(10) NOT NULL COMMENT '发布时间',
  `version` varchar(255) NOT NULL COMMENT '安装包版本',
  `remark` text NOT NULL COMMENT '更新说明',
  `create_ip` varchar(255) NOT NULL COMMENT '上传 IP'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端版本';

-- --------------------------------------------------------

--
-- 表的结构 `shop_article`
--

CREATE TABLE IF NOT EXISTS `shop_article` (
  `id` int(10) unsigned NOT NULL,
  `cate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '',
  `title_img` varchar(255) NOT NULL DEFAULT '' COMMENT '标题图',
  `description` text NOT NULL COMMENT '文章描述',
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `views` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '浏览量',
  `create_at` int(10) unsigned NOT NULL,
  `update_at` int(10) unsigned NOT NULL DEFAULT '0',
  `is_system` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1表示系统调用到的页面，禁止删除',
  `top` int(10) NOT NULL DEFAULT '0' COMMENT '置顶时间',
  `hotspot` int(11) NOT NULL COMMENT '热点推荐'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_article`
--

INSERT INTO `shop_article` (`id`, `cate_id`, `title`, `title_img`, `description`, `content`, `status`, `views`, `create_at`, `update_at`, `is_system`, `top`, `hotspot`) VALUES
(1, 3, '禁用chrome http自动跳转到https', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/89f9ee156c5d81af/2bb47aa385b8d904.png', 'https失效后，打开http链接会自动跳转到https，进行如下操作即可修复', '&lt;p&gt;https失效后，打开http链接会自动跳转到https，进行如下操作即可修复&lt;br /&gt;\r\n1. 地址栏中输入 chrome://net-internals/#hsts&lt;br /&gt;\r\n2. 在 Delete domain security policies 中输入项目的域名，并 Delete 删除&lt;br /&gt;\r\n3. 可以在 Query domain 测试是否删除成功 这里如果还是不行， 请清除浏览器缓存！&lt;/p&gt;\r\n', 1, 2, 1608992670, 0, 0, 0, 1),
(2, 3, 'PHP错误代码500 快速定位方法', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/55750758b8b501c3/2f18fbdb72a44daa.png', '好多人都知道PHP500是没有错误页面跟错误提示的。', '&lt;p&gt;好多人都知道PHP500是没有错误页面跟错误提示的。&lt;/p&gt;\r\n\r\n&lt;p&gt;直接就崩溃了。&lt;/p&gt;\r\n\r\n&lt;p&gt;下面代码放在程序最开始。&lt;/p&gt;\r\n\r\n&lt;p&gt;可以快速定位错误文件、行数、原因。&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img border=&quot;0&quot; src=&quot;http://suzhizhan.oss-cn-beijing.aliyuncs.com/55750758b8b501c3/2f18fbdb72a44daa.png&quot; style=&quot;max-width:500px&quot; title=&quot;image&quot; /&gt;&lt;/p&gt;\r\n', 1, 1, 1608992677, 0, 0, 0, 0),
(3, 3, '易语言解析网页内容替换换行符问题', '', '易语言解析网页内容的时候#换行符分割有时候是没有效果的~我们可以直接用字符数来替换', '&lt;p&gt;易语言解析网页内容的时候&lt;/p&gt;\r\n\r\n&lt;p&gt;#换行符分割有时候是没有效果的~&lt;/p&gt;\r\n\r\n&lt;p&gt;我们可以直接用字符数来替换&lt;/p&gt;\r\n\r\n&lt;p&gt;分割文本 (局_结果, 字符 (10), )&lt;/p&gt;\r\n\r\n&lt;p&gt;或者&lt;/p&gt;\r\n\r\n&lt;p&gt;分割文本 (局_结果, 字符 (13), )&lt;/p&gt;\r\n', 1, 1, 1608992366, 0, 0, 0, 0),
(4, 3, '卸载阿里云腾讯云服务器监控系统详情教程', '', '众所周知，阿里云、腾讯云的服务器都自带监控（AliYunDun/阿里云盾/安骑士教程），大家都不想自己的所作所为都被监控着，比如我在上面安装SS服务，一旦云监控查到，会被警告，很麻烦，我们总想着自己买的东西能够完全自己做主，所以我们需要让他的监控失效，卸载监控！', '&lt;p&gt;众所周知，阿里云、腾讯云的服务器都自带监控（AliYunDun/阿里云盾/安骑士教程），大家都不想自己的所作所为都被监控着，比如我在上面安装SS服务，一旦云监控查到，会被警告，很麻烦，我们总想着自己买的东西能够完全自己做主，所以我们需要让他的监控失效，卸载监控！&lt;/p&gt;\r\n', 1, 1, 1608992458, 0, 0, 1608992426, 0),
(5, 3, '新增内容板块，到底是用二级域名还是二级目录?', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/db3d8b35fc06f49b/b06490affdbffcce.jpg', '二级域名:需要创建二级域名，设置DNS，修改A记录，指定IP等操作。具体建网站或者编辑网页时，需要对不同二级域名下的内容分开存放。相当于一个独立的网站。如，http://blog.dsj365.cn', '&lt;p&gt;当网站内容想要继续扩展，建立新频道的时候，基于seo方面的因素，如新增关键词，又或是给主站带来的收录量，权重等影响，影响大家对于二级域名还是二级目录的选择。&amp;nbsp;&lt;br /&gt;\r\n&lt;br /&gt;\r\n首先，我们来看看什么是二级域名，什么是二级目录？&lt;br /&gt;\r\n&lt;br /&gt;\r\n1：二级域名:需要创建二级域名，设置DNS，修改A记录，指定IP等操作。具体建网站或者编辑网页时，需要对不同二级域名下的内容分开存放。相当于一个独立的网站。如，http://blog.dsj365.cn&lt;br /&gt;\r\n&lt;br /&gt;\r\n2：二级目录，就是子目录，继承在主站目录下的，相当于网站的跟目录在建立一个文件夹存放网站。如，http://www.dsj365.cn/blog/&lt;br /&gt;\r\n&lt;br /&gt;\r\n其次，让我们通过各种比较来分析二级目录和二级域名的优势和劣势：&lt;br /&gt;\r\n&lt;br /&gt;\r\n1.二级域名用来优化的关键词的话，有优势。&lt;br /&gt;\r\n&lt;br /&gt;\r\n2.二级目录的形式，多少会继承首页的权重，增加网站收录量。&lt;br /&gt;\r\n&lt;br /&gt;\r\n3.二级域名，在搜索引擎看来是一个新的独立网站。网站权重、PR值之类的，要重头培养。倒不如用新域名做个新站更好。&lt;br /&gt;\r\n&lt;br /&gt;\r\n4.二级目录当面临高难度的关键词时候，比起二级域名优势不明显。&lt;br /&gt;\r\n&lt;br /&gt;\r\n5.二级域名作为独立网站容易和其他网站做友情链接。&lt;br /&gt;\r\n&lt;br /&gt;\r\n6.二级目录虽然比较难做友情链接，但也可以通过与对方网站做交叉连接得以解决。&lt;br /&gt;\r\n&lt;br /&gt;\r\n最后，总结一下二级域名和二级目录。&lt;br /&gt;\r\n&lt;br /&gt;\r\n什么时候使用二级目录？&lt;br /&gt;\r\n&lt;br /&gt;\r\n做网站（尤其是中小型网站），可以靠多个二级目录来壮大网站。如果开设的二级域名太多，会削弱主站的优势。&lt;br /&gt;\r\n&lt;br /&gt;\r\n所以我认为中小型网站（包括博客）尽可能使用二级目录的形式更好。前提是网站的内容和主站内容差不多。如果相差甚远，就应该考虑使用二级域名了。&lt;br /&gt;\r\n&lt;br /&gt;\r\n什么时候使用二级域名？&lt;br /&gt;\r\n大型门户网站、行业门户、垂直类门户等信息量巨大的网站，可以选择把信息量大的频道设置为二级域名。或者你的新开设的频道和主站不搭边，可以考虑二级域名。如果是地域型公司，也可以考虑按地区设置二级域名。 [来源:dsj365.cn]&lt;/p&gt;\r\n', 1, 5, 1608992604, 0, 0, 1608992585, 0),
(6, 3, 'nginx反向代理http和https共同使用 双存在', '', 'nginx反向代理http和https共同使用 双存在', '&lt;p&gt;今天发现了一个问题，就是在反代过程中，https跟http只能单一反代！&lt;/p&gt;\r\n\r\n&lt;p&gt;不能自适应协议，也不支持协议变量，各种百度啊，两个钟头，测试了各种，都不适用宝塔，&lt;/p&gt;\r\n\r\n&lt;p&gt;第一种就是建两个server，80与443端口分开。&lt;/p&gt;\r\n\r\n&lt;p&gt;不过这样代码比较长，不利于维护，第二种，加一个判断，比较简单，宝塔需要手动修改，不能傻瓜设置！&lt;/p&gt;\r\n\r\n&lt;p&gt;我下面只贴主要代码！&lt;/p&gt;\r\n\r\n&lt;p&gt;第一种&lt;/p&gt;\r\n', 1, 1, 1608992654, 0, 0, 0, 0),
(7, 3, '闭站对网站有什么影响？备案如何做到不关站', '', '闭站对网站有什么影响？备案如何做到不关站', '&lt;p&gt;关闭网站对 SEO 优化可以说是致命的打击，以及对用户体验都是非常严重的影响，特别是对于已经上线正式运营的站点，有时候可能因为各种原因，甚至有时候就是任性，想要把备案改一改，换一换，或者注销体验一下，子凡当年就因为备案号连续不上后注销了在备案，这就是任性。&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img border=&quot;0&quot; src=&quot;http://suzhizhan.oss-cn-beijing.aliyuncs.com/87c3186ac4ba4546/614839189f2c5759.png&quot; style=&quot;max-width:500px&quot; title=&quot;image&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;虽然现在百度也都提供了闭站工具，可以保留在搜索引擎的索引库，将对搜索引擎的影响算是降到了最低最低，而这并不是完美的解决办法，就像曾经泪雪博客都上线三四年了，当时每天高的时候每天还是会有几百个 IP 通过搜过关键词来到，所以我要的结果肯定不是关站点备案，白白的浪费这些流量，虽然泪雪博客本身也并不盈利，但是关注子凡的都知道，我有开发一些 WordPress 主题和插件在泪雪建站上出售，失去博客的导流，可能就没有转化，生活费是不是就没有了呢？&lt;/p&gt;\r\n\r\n&lt;p&gt;其实网上关于网站域名备案不管站点的方法很多，让人有点眼花缭乱，甚至可以说是混乱，但是也不可否认其确实可以加以利用，可以很好的实现不闭站就能成功的备案。&lt;/p&gt;\r\n\r\n&lt;p&gt;首先子凡有必要告诉大家的是，目前国内正规的运营商，只要是国内的服务器或者主机，也就是说服务器 IP 地址是属于国内的，只要域名未成功备案，都是不能被解析的，简而言之就是只要域名没备案，或者当备案被注销时，域名的解析是会被服务商强制停止绑定，跳转或者出现相关的提示信息，从而不能正常的访问。&lt;/p&gt;\r\n\r\n&lt;p&gt;那么这时候想要网站被继续运行的方法就是将站点数据迁移到非国内大陆地区，港澳台及国外任何地方都行，然后再将域名解析过去，DNS 缓存更新后就可以正常被访问，一般情况下只要备案不是被管局直接注销，相信我们都是可以做到无断开的迁移，用户访问是感觉不到的，当然搜索引擎一般会在 24 小时内就能发现这个网站的 IP 地址变更了，但是说真的，IP 地址的变化对网站 SEO 优化的影响几乎可以忽略不计，只要你不是天天换的那种就行了。&lt;/p&gt;\r\n\r\n&lt;p&gt;最后就还剩下一个非常重要的步骤，当网站搬迁到国外服务器正常运行后，我们需呀将网站的首页，做成一个提示页面，如果你是一个爱美的人，那么还可以设置一下，否则直接就显示几个大字，例如：网站正在备案，暂时无法访问。等类似的提醒，基本就可以继续备案操作了，这是子凡认为在最大程度上将网站的影响降到最低，而且能够保证网站备案的正常有效进行。&lt;/p&gt;\r\n\r\n&lt;p&gt;当然网上的一些方法就更为大胆，可以碰碰运气，也就是说什么都不做，因为工信部的人不一定会打开，还有的方法就是通过判断工信部审核地区，将审核地区的 IP 地址都给屏蔽了，让他们不能访问就可以了。&lt;/p&gt;\r\n\r\n&lt;p&gt;子凡就给大家分享到这里吧，都是一些经验之谈和个人认为比较好的观点方式方法，或许你并不一定现在就会遇到这样的情况，不过难免以后会遇到，总之当作是一个了解或者参考总会是不错的，当然如果你还有其它更奇葩的方法，记得留言告诉子凡，哈哈哈。&lt;/p&gt;\r\n', 1, 1, 1608992757, 0, 0, 0, 0),
(8, 3, '浏览器关闭(电脑管家提醒您)报毒拦截页面', '', '浏览器关闭(电脑管家提醒您)报毒拦截页面', '&lt;p&gt;装电脑管家的用户，在打开一个报毒的网站的时候总会提醒，可麻烦了！&lt;/p&gt;\r\n\r\n&lt;p&gt;而且有的时候还会出问题的，要多麻烦有多麻烦！&lt;/p&gt;\r\n\r\n&lt;p&gt;经过查看后是电脑管家的问题，于是找了半天，&lt;/p&gt;\r\n\r\n&lt;p&gt;设置，浏览器保护都没有！&lt;/p&gt;\r\n\r\n&lt;p&gt;后来发现在实时防护里面！&lt;/p&gt;\r\n\r\n&lt;p&gt;直接将网页防火墙关闭就好了，永远不会提醒了！&lt;/p&gt;\r\n', 1, 1, 1608992958, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_article_category`
--

CREATE TABLE IF NOT EXISTS `shop_article_category` (
  `id` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(1024) NOT NULL,
  `name` varchar(30) NOT NULL DEFAULT '',
  `alias` varchar(30) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `create_at` int(10) unsigned NOT NULL,
  `update_at` int(10) unsigned DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_article_category`
--

INSERT INTO `shop_article_category` (`id`, `pid`, `path`, `name`, `alias`, `remark`, `status`, `create_at`, `update_at`) VALUES
(1, 0, '0', '新闻资讯', 'xinwen', '新闻资讯', 1, 1608988207, 0),
(2, 0, '0', '原创文章', 'yuanchuang', '原创文章', 1, 1608988234, 0),
(3, 0, '0', '源码行情', 'hangqing', '源码行情', 1, 1608988250, 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_auto_unfreeze`
--

CREATE TABLE IF NOT EXISTS `shop_auto_unfreeze` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `money` decimal(10,3) unsigned NOT NULL DEFAULT '0.000' COMMENT '冻结金额',
  `unfreeze_time` int(11) NOT NULL DEFAULT '0' COMMENT '解冻时间',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `trade_no` varchar(255) NOT NULL DEFAULT '0' COMMENT '冻结资金来源订单号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '冻结资金记录状态，1：可用，-1：不可用（订单申诉中等情况）'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='订单金额T+1日自动解冻表';

--
-- 转存表中的数据 `shop_auto_unfreeze`
--

INSERT INTO `shop_auto_unfreeze` (`id`, `user_id`, `money`, `unfreeze_time`, `created_at`, `trade_no`, `status`) VALUES
(1, 10001, '0.090', 1539014400, 1538990812, 'T1810081726376105', 1),
(2, 10001, '0.090', 1568390400, 1568359416, 'T190913152320100086', 1);

-- --------------------------------------------------------

--
-- 表的结构 `shop_bid`
--

CREATE TABLE IF NOT EXISTS `shop_bid` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `ubh` char(50) DEFAULT NULL,
  `uip` char(50) DEFAULT NULL,
  `taskbh` char(50) DEFAULT NULL,
  `money` int(10) DEFAULT NULL,
  `cyc` int(3) DEFAULT NULL,
  `txt` text,
  `sj` datetime DEFAULT NULL,
  `uptime` datetime DEFAULT NULL,
  `hides` char(50) DEFAULT NULL,
  `zt` int(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_brand`
--

CREATE TABLE IF NOT EXISTS `shop_brand` (
  `id` int(11) NOT NULL COMMENT 'id',
  `sj` datetime NOT NULL COMMENT '时间',
  `uip` varchar(200) NOT NULL COMMENT 'IP',
  `ubh` varchar(200) NOT NULL COMMENT '用户编号',
  `typeid` varchar(11) NOT NULL COMMENT '分类',
  `company` varchar(200) NOT NULL COMMENT '归属',
  `tit` varchar(200) NOT NULL COMMENT '标题',
  `website` varchar(200) NOT NULL COMMENT '主页',
  `url` varchar(200) NOT NULL COMMENT 'URL',
  `desc` varchar(200) NOT NULL COMMENT '介绍',
  `txt` text NOT NULL COMMENT '内容',
  `bh` varchar(20) NOT NULL COMMENT '编号',
  `psort` varchar(200) NOT NULL COMMENT '大图',
  `zt` int(11) NOT NULL COMMENT '状态',
  `lastgx` datetime NOT NULL COMMENT '更新数据',
  `zan` int(11) NOT NULL DEFAULT '0' COMMENT '点赞'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_buy`
--

CREATE TABLE IF NOT EXISTS `shop_buy` (
  `bh` char(50) NOT NULL,
  `tx` char(200) DEFAULT '//suzhizhan.oss-cn-beijing.aliyuncs.com/images/none.jpg',
  `name` char(50) DEFAULT NULL,
  `contact` char(20) DEFAULT NULL,
  `wechat` varchar(100) NOT NULL,
  `zt` int(10) DEFAULT '1',
  `ztsm` char(50) DEFAULT NULL,
  `rev_m` int(50) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_buy`
--

INSERT INTO `shop_buy` (`bh`, `tx`, `name`, `contact`, `wechat`, `zt`, `ztsm`, `rev_m`) VALUES
('u1629290430', '//suzhizhan.oss-cn-beijing.aliyuncs.com/images/none.jpg', '时光网络', '66983239', '', 1, NULL, 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_cart`
--

CREATE TABLE IF NOT EXISTS `shop_cart` (
  `id` int(10) unsigned NOT NULL,
  `bh` char(20) DEFAULT NULL,
  `codebh` char(20) DEFAULT NULL,
  `ubh` char(20) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `jf` int(50) DEFAULT '0',
  `type` char(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_cash`
--

CREATE TABLE IF NOT EXISTS `shop_cash` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `bh` varchar(100) NOT NULL COMMENT '订单编号',
  `ubh` varchar(100) NOT NULL COMMENT '用户编号',
  `type` varchar(10) NOT NULL COMMENT '收款产品类型 1支付宝 2微信',
  `type_name` varchar(100) NOT NULL COMMENT '类型名称',
  `collect_info` varchar(1024) NOT NULL DEFAULT '' COMMENT '提现信息',
  `truename` varchar(100) NOT NULL COMMENT '收款姓名',
  `bankid` varchar(100) NOT NULL COMMENT '收款账户',
  `money` decimal(10,2) unsigned NOT NULL COMMENT '提现金额',
  `fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `actual_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '实际到账',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态 0审核中 1审核通过 2审核未通过',
  `create_at` int(10) unsigned NOT NULL COMMENT '创建时间',
  `complete_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '完成时间',
  `collect_img` tinytext COMMENT '收款二维码',
  `auto_cash` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 表示自动提现',
  `bank_name` varchar(50) NOT NULL DEFAULT '' COMMENT '银行名称',
  `bank_branch` varchar(150) NOT NULL DEFAULT '' COMMENT '开户地址',
  `bank_card` varchar(50) NOT NULL DEFAULT '' COMMENT '银行卡号',
  `realname` varchar(50) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `idcard_number` varchar(50) NOT NULL DEFAULT '' COMMENT '身份证号码',
  `orderid` varchar(50) NOT NULL DEFAULT '' COMMENT '订单号',
  `account` int(11) NOT NULL DEFAULT '0' COMMENT '代付账号',
  `daifu_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '代付状态（0，未申请，1，已申请）'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_cashed`
--

CREATE TABLE IF NOT EXISTS `shop_cashed` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `ubh` char(50) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `bankid` char(50) DEFAULT NULL COMMENT '提现账号',
  `truename` char(50) DEFAULT NULL COMMENT '提现姓名',
  `zt` int(11) DEFAULT NULL COMMENT '提现状态',
  `sm` char(50) DEFAULT NULL,
  `type` char(50) DEFAULT NULL COMMENT '提现类型',
  `fee` decimal(10,2) NOT NULL COMMENT '手续费',
  `actual_money` decimal(10,2) NOT NULL COMMENT '实际到账',
  `complete_sj` datetime NOT NULL COMMENT '处理时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_cash_channel`
--

CREATE TABLE IF NOT EXISTS `shop_cash_channel` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '代付渠道名',
  `code` varchar(255) NOT NULL COMMENT '代付渠道代码',
  `account_fields` text NOT NULL COMMENT '代付所需的字段',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1支付宝，2微信，3银行',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '渠道状态，0关闭，1开启'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_cash_channel`
--

INSERT INTO `shop_cash_channel` (`id`, `title`, `code`, `account_fields`, `type`, `status`) VALUES
(1, '千游银行卡代付', 'qianyoupay', 'appKey:appKey|screctKey:screctKey', 3, 0),
(2, '千游支付宝代付', 'QianyouAlipay', 'appKey:appKey|screctKey:screctKey', 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_cash_channel_account`
--

CREATE TABLE IF NOT EXISTS `shop_cash_channel_account` (
  `id` int(10) unsigned NOT NULL,
  `channel_id` int(10) unsigned NOT NULL COMMENT '渠道ID',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '账户名',
  `params` text NOT NULL COMMENT '参数',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1启用 0禁用'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_channel`
--

CREATE TABLE IF NOT EXISTS `shop_channel` (
  `id` int(11) unsigned NOT NULL COMMENT '通道ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '通道名称',
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '通道代码',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1 开启 0 关闭',
  `mch_id` varchar(50) NOT NULL DEFAULT '' COMMENT '商户号',
  `signkey` varchar(255) NOT NULL DEFAULT '' COMMENT '签名KEY',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT 'APPID(账号)',
  `appsecret` varchar(50) NOT NULL DEFAULT '' COMMENT 'APPSECRET(密钥)',
  `gateway` varchar(255) NOT NULL DEFAULT '' COMMENT '网关地址',
  `return_url` varchar(255) NOT NULL DEFAULT '' COMMENT '页面通知（优先级）',
  `notify_url` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器通知（优先级）',
  `lowrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '充值费率',
  `highrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '封顶费率',
  `costrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '成本费率',
  `accounting_date` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '结算时间 1、D0 2、D1 3、T0 4、T1',
  `max_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔限额 0为最高不限额',
  `min_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单笔限额',
  `limit_time` varchar(19) NOT NULL DEFAULT '' COMMENT '限时',
  `account_fields` varchar(1024) NOT NULL DEFAULT '' COMMENT '账户字段',
  `polling` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '接口模式 0单独 1轮询',
  `account_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '账号ID',
  `weight` text COMMENT '权重',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '支付类型 1 微信扫码 2 微信公众号 3 支付宝扫码 4 支付宝手机 5 网银支付 6',
  `show_name` varchar(255) NOT NULL DEFAULT '' COMMENT '前台展示名称',
  `is_available` tinyint(4) NOT NULL DEFAULT '0' COMMENT '接口可用 0通用 1手机 2电脑',
  `default_fields` varchar(1024) NOT NULL DEFAULT '' COMMENT '字段默认值',
  `applyurl` varchar(255) NOT NULL DEFAULT '' COMMENT '申请地址',
  `is_install` tinyint(4) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '渠道排序'
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8 COMMENT='支付供应商';

--
-- 转存表中的数据 `shop_channel`
--

INSERT INTO `shop_channel` (`id`, `title`, `code`, `status`, `mch_id`, `signkey`, `appid`, `appsecret`, `gateway`, `return_url`, `notify_url`, `lowrate`, `highrate`, `costrate`, `accounting_date`, `max_money`, `min_money`, `limit_time`, `account_fields`, `polling`, `account_id`, `weight`, `updatetime`, `paytype`, `show_name`, `is_available`, `default_fields`, `applyurl`, `is_install`, `sort`) VALUES
(58, '支付宝扫码', 'AlipayScan', 1, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', 'alipay_public_key:alipay_public_key|merchant_private_key:merchant_private_key|app_id:app_id|防封域名（可选）:refer', 0, 4, '[]', 0, 3, '', 2, 'charset=UTF-8', 'https://open.alipay.com/platform/homeRoleSelection.htm', 1, 0),
(59, '支付宝H5', 'AlipayWap', 1, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', 'alipay_public_key:alipay_public_key|merchant_private_key:merchant_private_key|app_id:app_id|防封域名（可选）:refer', 0, 5, '[]', 0, 4, '', 1, 'charset=UTF-8', 'https://open.alipay.com/platform/homeRoleSelection.htm', 1, 0),
(60, '点缀微信扫码', 'DzWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 1, '', 0, '', '', 0, 0),
(61, '点缀支付宝扫码', 'DzAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 3, '', 0, '', '', 0, 0),
(62, '点缀微信公众号', 'DzWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|是否使用自有公众号（0否，1是）:usepublic|防封域名（可选）:refer', 0, 0, '[]', 0, 2, '', 0, '', '', 0, 0),
(63, '15173Wap微信支付', 'Lh15173WapPay', 0, '', '', '', '', '', '', '', '0.0100', '0.0100', '0.0100', 1, '0.00', '0.00', '', 'bargainor_id:bargainor_id|key:key|防封域名（可选）:refer', 0, 16, '[]', 0, 18, '', 1, '', '', 0, 0),
(64, '拉卡微信支付', 'LkWxPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'userid:userid|userkey:userkey|防封域名（可选）:refer', 0, 17, '[]', 0, 19, '', 0, '', '', 0, 0),
(65, '拉卡微信H5支付', 'LkWxH5Pay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'userid:userid|userkey:userkey|防封域名（可选）:refer', 0, 18, '[]', 0, 20, '', 0, '', '', 0, 0),
(66, '拉卡支付宝扫码支付', 'LkAlipayScanPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'userid:userid|userkey:userkey|防封域名（可选）:refer', 0, 19, '[]', 0, 21, '', 0, '', '', 0, 0),
(71, '微信官方H5支付', 'WxpayH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|mch_id:mch_id|signkey:signkey|防封域名（可选）:refer', 0, 0, '[]', 0, 27, '', 0, '', '', 0, 0),
(72, '微信扫码', 'WxpayScan', 0, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', 'appid:appid|mch_id:mch_id|signkey:signkey|notify_url:notify_url|防封域名（可选）:refer', 0, 6, '[]', 0, 1, '', 0, '', '', 0, 0),
(73, '12kaQQ钱包扫码', 'Ka12QqNative', 0, '', '', '', '', '', '', '', '0.0300', '0.0500', '0.0200', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 30, '12kaQQ钱包扫码', 1, '', '', 0, 0),
(74, '12kaQQ钱包wap', 'Ka12QqWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 31, '12kaQQ钱包wap', 2, '', '', 0, 0),
(75, '12ka网银快捷', 'Ka12QuickBank', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 32, '12ka网银快捷', 1, '', '', 0, 0),
(76, '12卡网银快捷WAP', 'Ka12QuickWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 33, '12卡网银快捷WAP', 1, '', '', 0, 0),
(77, '12ka支付宝扫码', 'Ka12AlipayScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 34, '12ka支付宝扫码', 2, '', '', 0, 0),
(78, '12ka支付宝wap', 'Ka12AlipayWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 35, '12ka支付宝wap', 0, '', '', 0, 0),
(79, '12ka微信扫码', 'Ka12WxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 36, '12ka微信扫码', 2, '', '', 0, 0),
(80, '12ka微信wap', 'Ka12WxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 37, '12ka微信wap', 2, '', '', 0, 0),
(81, '15173QQ扫码支付', 'Lh15173QqPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'bargainor_id:bargainor_id|key:key|防封域名（可选）:refer', 0, 0, '', 0, 39, '15173QQ扫码支付', 0, '', '', 0, 0),
(82, '码支付微信扫码', 'CodePayWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '码支付ID:id|通信秘钥:key|防封域名（可选）:refer', 0, 0, '', 0, 38, '码支付微信扫码', 2, '', '', 0, 0),
(83, '码支付qq扫码', 'CodePayQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '码支付ID:id|通信秘钥:key|防封域名（可选）:refer', 0, 0, '', 0, 40, '码支付qq扫码', 0, '', '', 0, 0),
(84, '码支付支付宝扫码', 'CodePayAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '码支付ID:id|通信秘钥:key|防封域名（可选）:refer', 0, 0, '', 0, 41, '码支付支付宝扫码', 0, '', '', 0, 0),
(85, '点缀QQ扫码', 'DzQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 8, '点缀支付PC', 1, '', '', 0, 0),
(86, '黔贵金服支付宝扫码', 'QgjfAlipayScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 51, '黔贵金服支付宝扫码', 2, '', '', 0, 0),
(87, '黔贵金服微信扫码', 'QgjfWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 52, '黔贵金服微信扫码', 2, '', '', 0, 0),
(88, '黔贵金服QQ钱包扫码', 'QgjfQqNative', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 53, '黔贵金服QQ钱包扫码', 2, '', '', 0, 0),
(89, '黔贵金服公众号', 'QgjfWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 54, '黔贵金服公众号', 2, '', '', 0, 0),
(90, '黔贵金服支付宝Wap', 'QgjfAlipayWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 55, '黔贵金服支付宝WAP', 2, '', '', 0, 0),
(91, '黔贵金服微信WAP', 'QgjfWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 56, '黔贵金服微信WAP', 2, '', '', 0, 0),
(92, '点缀支付宝即时到账', 'DzAliToPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 57, '点缀支付宝即时到账', 1, '', '', 0, 0),
(93, '点缀支付京东钱包扫码', 'DzJdScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 58, '点缀支付京东钱包扫码', 2, '', '', 0, 0),
(94, '掌灵付微信H5', 'ZlfWxH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 59, '掌灵付微信H5', 0, '', '', 0, 0),
(95, '掌灵付京东H5', 'ZlfJdH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 60, '掌灵付京东H5', 0, '', '', 0, 0),
(96, '掌灵付京东扫码', 'ZlfJdScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 89, '掌灵付京东扫码', 2, '', '', 0, 0),
(97, '掌灵付微信扫码', 'ZlfWxScan', 0, '', '', '', '', '', '', '', '0.0350', '0.0000', '0.0100', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer|防封域名（可选）:refer', 0, 0, '[]', 0, 47, '掌灵付微信扫码', 2, '', '', 0, 0),
(98, '掌灵付QQ钱包扫码', 'ZlfQqScan', 0, '', '', '', '', '', '', '', '0.0350', '0.0000', '0.0100', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer|防封域名（可选）:refer', 0, 0, '[]', 0, 48, '掌灵付QQ扫码', 0, '', '', 0, 0),
(99, 'QQ钱包扫码（官方）', 'QqNative', 0, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0060', 1, '0.00', '0.00', '', 'mch_id:mch_id|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 8, '官方QQ扫码', 0, '', '', 0, 0),
(104, '海鸟微信公众号', 'HnPayWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid| 商户号:merchant|商户密钥:key|是否使用自有公众号（0否，1是）:publicappid|防封域名（可选）:refer', 0, 0, '[]', 0, 67, '海鸟微信公众号', 0, '', '', 0, 0),
(105, '海鸟微信扫码', 'HnPayWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 68, '海鸟微信扫码', 0, '', '', 0, 0),
(106, '海鸟微信H5', 'HnPayWxH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 69, '海鸟微信H5', 0, '', '', 0, 0),
(107, '海鸟微信qq扫码', 'HnPayQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 70, '海鸟微信qq扫码', 0, '', '', 0, 0),
(108, '海鸟支付宝扫码', 'HnPayAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 71, '海鸟支付宝扫码', 0, '', '', 0, 0),
(109, '完美数卡微信扫码', 'WmskWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 78, '完美数卡微信扫码', 0, '', '', 0, 0),
(110, '完美数卡支付宝扫码', 'WmskAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 79, '完美数卡支付宝扫码', 0, '', '', 0, 0),
(111, '完美数卡QQ扫码', 'WmskQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 80, '完美数卡QQ扫码', 0, '', '', 0, 0),
(112, '完美数卡QQWap', 'WmskQqWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 81, '完美数卡QQWap', 0, '', '', 0, 0),
(113, '完美数卡微信Wap', 'WmskWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 82, '完美数卡微信Wap', 0, '', '', 0, 0),
(114, '完美数卡支付宝Wap', 'WmskAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 83, '完美数卡支付宝Wap', 0, '', '', 0, 0),
(115, '掌灵付支付宝扫码', 'ZlfAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 87, '掌灵付支付宝扫码', 0, '', '', 0, 0),
(116, 'QPay支付宝', 'QPayAli', 0, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', '网关:gateway|用户id:uid|token:token|防封域名（可选）:refer', 0, 7, '[]', 0, 14, '', 0, '', '', 0, 0),
(117, 'QPay微信', 'QPayWx', 0, '', '', '', '', '', '', '', '0.0100', '0.0000', '0.0010', 1, '0.00', '0.00', '', '网关:gateway|用户id:uid|token:token|防封域名（可选）:refer', 0, 0, '[]', 0, 15, '', 0, 'gateway=https://pay.qpayapi.com', '', 0, 0),
(119, '蜂鸟支付宝扫码支付', 'FnAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|防封域名（可选）:refer', 0, 0, '[]', 0, 42, '', 2, '', '', 0, 0),
(120, '蜂鸟支付宝WAP支付', 'FnAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|防封域名（可选）:refer', 0, 0, '[]', 0, 43, '', 1, '', '', 0, 0),
(121, '蜂鸟微信扫码支付', 'FnWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|sub_appid:sub_appid|防封域名（可选）:refer', 0, 0, '[]', 0, 44, '', 2, '', '', 0, 0),
(122, '蜂鸟微信公众号支付', 'FnWxJspay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|wx_appid:wx_appid|wx_secret:wx_secret|防封域名（可选）:refer', 0, 0, '[]', 0, 45, '', 1, '', '', 0, 0),
(123, '蜂鸟QQ钱包扫码支付', 'FnQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|防封域名（可选）:refer', 0, 0, '[]', 0, 46, '', 2, '', '', 0, 0),
(127, '汉口银行微信扫码', 'HkyhWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 99, '', 0, '', '', 0, 0),
(128, '汉口银行微信wap', 'HkyhWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 100, '', 0, '', '', 0, 0),
(129, '汉口银行支付宝扫码', 'HkyhAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 101, '', 0, '', '', 0, 0),
(130, '汉口银行支付宝Wap', 'HkyhAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 102, '', 0, '', '', 0, 0),
(134, '平安付支付宝扫码', 'PafbAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|门店:store|防封域名（可选）:refer', 0, 0, '[]', 0, 106, '', 1, '', '', 0, 0),
(135, '平安付支付宝wap', 'PafbAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|门店:store|防封域名（可选）:refer', 0, 0, '[]', 0, 107, '', 0, '', '', 0, 0),
(136, '平安付微信wap', 'PafbWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|门店:store|防封域名（可选）:refer', 0, 0, '[]', 0, 108, '', 0, '', '', 0, 0),
(137, '网商银行微信', 'WsyhWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '机构AppId:Appid|机构号:IsvOrgId|浙商私钥:private_key|浙商公钥:public_key|商户号:merchantid|清算方式:settleType|服务商类型（咨询上游，不确定填03）:ProviderType|防封域名（可选）:refer', 0, 0, '[]', 0, 109, '', 0, '', '', 0, 0),
(138, '网商银行支付宝', 'WsyhAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '机构AppId:Appid|机构号:IsvOrgId|浙商私钥:private_key|浙商公钥:public_key|商户号:merchantid|清算方式:settleType|服务商类型（咨询上游，不确定填03）:ProviderType|防封域名（可选）:refer', 0, 0, '[]', 0, 110, '', 0, '', '', 0, 0),
(139, '牛支付支付宝扫码', 'NZFAliqrcode', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:memberid|appid:appid|app 密钥:key|子商户 id(没有请留空):submchid|防封域名（可选）:refer|防封域名（可选）:refer', 0, 0, '[]', 0, 88, '牛支付支付宝扫码', 0, '', '', 0, 0),
(154, 'PayApi支付宝', 'PayapiAli', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:uid|秘钥:key', 0, 0, '[]', 0, 113, '', 0, '', '', 0, 0),
(155, 'PayApi微信', 'PayapiWx', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:uid|秘钥:key', 0, 0, '[]', 0, 114, '', 0, '', '', 0, 0),
(165, '易云微信WAP', 'YiyunWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:userid|商户密钥:userkey|防封域名（可选）:refer', 0, 0, '[]', 0, 118, '易云微信WAP', 0, '', '', 0, 0),
(166, '易云微信公众号', 'YiyunWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:userid|商户密钥:userkey|防封域名（可选）:refer', 0, 0, '[]', 0, 119, '易云微信公众号', 0, '', '', 0, 0),
(171, '深度支付宝扫码', 'ShenduAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 124, '深度支付宝扫码', 0, '', '', 0, 0),
(172, '深度支付宝服务窗支付', 'ShenduAliJspay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 125, '深度支付宝服务窗支付', 0, '', '', 0, 0),
(173, '深度微信扫码', 'ShenduWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 126, '深度微信扫码', 0, '', '', 0, 0),
(174, '深度微信公众号', 'ShenduWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 127, '深度微信公众号', 0, '', '', 0, 0),
(176, 'i优支付支付宝', 'EpayAli', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'i优商户Id:pid|i优商户密钥:key', 0, 0, '[]', 0, 129, '支付宝', 0, '', 'http://www.8iu.cn/', 0, 0),
(177, 'i优支付微信', 'EpayWx', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'i优商户Id:pid|i优商户密钥:key', 0, 0, '[]', 0, 130, '微信支付', 0, '', 'http://www.8iu.cn/', 0, 0),
(178, 'i优支付QQ钱包', 'EpayQq', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'i优商户Id:pid|i优商户密钥:key', 0, 0, '[]', 0, 131, 'QQ钱包支付', 0, '', 'http://www.8iu.cn/', 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_channel_account`
--

CREATE TABLE IF NOT EXISTS `shop_channel_account` (
  `id` int(10) unsigned NOT NULL,
  `channel_id` int(10) unsigned NOT NULL COMMENT '渠道ID',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '账户名',
  `params` text NOT NULL COMMENT '参数',
  `max_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '下线额度',
  `cur_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '当前额度',
  `limit_time` varchar(19) NOT NULL DEFAULT '' COMMENT '限时',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1启用 0禁用',
  `lowrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '充值费率',
  `highrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '封顶费率',
  `costrate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '成本费率',
  `rate_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '费率设置 0 继承接口  1单独设置'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_channel_account`
--

INSERT INTO `shop_channel_account` (`id`, `channel_id`, `name`, `params`, `max_money`, `cur_money`, `limit_time`, `status`, `lowrate`, `highrate`, `costrate`, `rate_type`) VALUES
(1, 139, '演示账号', '{"memberid":"2","appid":"12340004","key":"0e0a47c54df24e3015eb78475bfae915","submchid":"","refer":""}', '0.00', '0.00', '', 1, '0.0000', '0.0000', '0.0000', 0),
(3, 176, '10001', '{"pid":"10001","key":"4qGG8r1ddq5h5LlLRJZRYhyDdXOaqLXH"}', '0.00', '0.00', '', 1, '0.0000', '0.0000', '0.0000', 0),
(4, 177, '10001', '{"pid":"10001","key":"4qGG8r1ddq5h5LlLRJZRYhyDdXOaqLXH"}', '0.00', '0.00', '', 1, '0.0000', '0.0000', '0.0000', 0),
(5, 58, '111', '{"alipay_public_key":"1","merchant_private_key":"1","app_id":"1","refer":"1"}', '0.00', '0.00', '', 1, '0.0000', '0.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_code`
--

CREATE TABLE IF NOT EXISTS `shop_code` (
  `id` int(11) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(100) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `typeid` int(10) DEFAULT NULL,
  `tit` varchar(200) DEFAULT NULL,
  `ckey` varchar(200) DEFAULT NULL,
  `cdes` varchar(250) DEFAULT NULL,
  `txt` text,
  `menus` varchar(100) NOT NULL COMMENT '分类',
  `brand` varchar(100) NOT NULL COMMENT '品牌',
  `lang` varchar(100) NOT NULL COMMENT '语言',
  `sql1` varchar(100) NOT NULL COMMENT '数据库',
  `encrypt` varchar(100) NOT NULL COMMENT '源文件',
  `auth` varchar(100) NOT NULL COMMENT '授权',
  `spec` varchar(100) NOT NULL COMMENT '规格',
  `app` varchar(500) DEFAULT NULL COMMENT 'app',
  `ifaz` varchar(100) NOT NULL COMMENT '安装判断',
  `azmoney` int(10) NOT NULL COMMENT '安装费',
  `psort` varchar(2005) NOT NULL COMMENT '演示大图',
  `azsxid` varchar(200) NOT NULL,
  `rewrite` varchar(100) NOT NULL COMMENT '伪静态',
  `azbz` varchar(200) NOT NULL COMMENT '备注',
  `jfio` tinyint(1) DEFAULT '0',
  `jfnum` float(10,1) DEFAULT NULL,
  `hfl` int(10) DEFAULT '0',
  `sydx` varchar(200) DEFAULT NULL,
  `djl` int(10) DEFAULT '0',
  `money` int(10) DEFAULT '0',
  `bb` char(20) DEFAULT NULL,
  `lastgx` datetime DEFAULT NULL,
  `ysweb` varchar(200) DEFAULT NULL,
  `sizes` float DEFAULT NULL,
  `zt` int(10) DEFAULT NULL,
  `ztsm` varchar(200) DEFAULT NULL,
  `ubh` char(50) DEFAULT NULL,
  `iftest` int(10) DEFAULT NULL,
  `xsnum` int(10) DEFAULT NULL,
  `iftuan` int(10) DEFAULT NULL,
  `iftj` int(10) DEFAULT NULL,
  `tuanendsj` datetime DEFAULT NULL,
  `tuantit` varchar(200) DEFAULT NULL,
  `codeqx` varchar(200) DEFAULT NULL,
  `ifwp` int(10) DEFAULT NULL,
  `wpurl` varchar(200) DEFAULT NULL,
  `wppwd` char(50) DEFAULT NULL,
  `listzd` int(10) DEFAULT NULL,
  `temid` int(50) DEFAULT NULL,
  `btg` varchar(200) DEFAULT NULL,
  `tj` int(10) NOT NULL COMMENT '推荐商品',
  `show` int(11) NOT NULL DEFAULT '1' COMMENT '展示方式'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_code`
--

INSERT INTO `shop_code` (`id`, `sj`, `uip`, `bh`, `typeid`, `tit`, `ckey`, `cdes`, `txt`, `menus`, `brand`, `lang`, `sql1`, `encrypt`, `auth`, `spec`, `app`, `ifaz`, `azmoney`, `psort`, `azsxid`, `rewrite`, `azbz`, `jfio`, `jfnum`, `hfl`, `sydx`, `djl`, `money`, `bb`, `lastgx`, `ysweb`, `sizes`, `zt`, `ztsm`, `ubh`, `iftest`, `xsnum`, `iftuan`, `iftj`, `tuanendsj`, `tuantit`, `codeqx`, `ifwp`, `wpurl`, `wppwd`, `listzd`, `temid`, `btg`, `tj`, `show`) VALUES
(1, '2021-08-18 20:45:58', '', '162929068919', NULL, '源码交易商城平台虚拟交易系统ThinkCt商家版V6 网站源码虚拟交易', '', '源码交易商城平台虚拟交易系统ThinkCt商家版V6 网站源码虚拟交易', '&lt;h1&gt;源码交易商城系统&lt;/h1&gt;&lt;h2&gt;虚拟商品可自动发货&lt;/h2&gt;&lt;h3&gt;商城后台设置了网盘下载、网站下载、卡密发货等自动发货形式，卖家只需要将商品编辑好，当有客户购买时，就能实现自动发货，省去时时盯着电脑的精力。&lt;/h3&gt;&lt;h2&gt;支持虚拟商品在线交易&lt;/h2&gt;&lt;h3&gt;如果您商城销售的商品是实物类（如服装等），可以设置发货形式为实物快递。卖家还能自定义设置全国的邮费模板，用户下单时，自动计算运。&lt;/h3&gt;&lt;h2&gt;补丁在线升级，高效方便&lt;/h2&gt;&lt;h3&gt;我们商城源码每天都在开发新补丁，很多升级内容来自用户的实际运营经验反馈，补丁发布后，网站后台会有提醒，站长输入管理员密码，就可以在线直接升级，享受更新补丁效果。&lt;/h3&gt;&lt;p&gt;系统开发文档：&lt;a href=&quot;https://www.kancloud.cn/thinkct/shop/2277127&quot; target=&quot;_blank&quot;&gt;https://www.kancloud.cn/thinkct/shop/2277127&lt;/a&gt;&lt;/p&gt;&lt;p&gt;更新日记&lt;/p&gt;&lt;p&gt;v6.0.5版本更新内容&lt;/p&gt;&lt;p&gt;1.优化任务大厅相关功能上线&lt;/p&gt;&lt;p&gt;2.优化商品管理列表数据&lt;/p&gt;&lt;p&gt;3.优化首页响应加载速度&lt;/p&gt;&lt;p&gt;4.修复压缩模式运行下订单详情布局错乱问题&lt;/p&gt;&lt;p&gt;5.优化介绍模板相关权限&lt;/p&gt;&lt;p&gt;6.后台增加搜索记录管理&lt;/p&gt;&lt;p&gt;7.后台增加自助推广管理&lt;/p&gt;&lt;p&gt;8.增加执行清理用户缓存数据任务脚本&lt;/p&gt;&lt;p&gt;9.增加百度主动收录API提交&lt;/p&gt;&lt;p&gt;10.重写订单数据增加订单模型（重大升级-效果更佳）&lt;/p&gt;&lt;p&gt;11.修复绑定QQ注册报错问题&lt;/p&gt;&lt;p&gt;12.重写优化商品管理列表数据&lt;/p&gt;&lt;p&gt;13.重写优化订单交易列表数据&lt;/p&gt;&lt;p&gt;14.修复买家菜单不显示建立自助交易问题&lt;/p&gt;&lt;p&gt;15.新增发起追加订单&lt;/p&gt;&lt;p&gt;16.优化自助，任务，服务，网站，域名订单订单交易&lt;/p&gt;&lt;p&gt;17.重写优化收银台增加多类型付款&lt;/p&gt;&lt;p&gt;18.后台增加频道类型管理&lt;/p&gt;&lt;p&gt;19.优化收银台相关功能和界面&lt;/p&gt;&lt;p&gt;v6.0.4版本更新内容&lt;/p&gt;&lt;p&gt;修复&lt;/p&gt;&lt;p&gt;1.修复购物车商品下架显示异常问题&lt;/p&gt;&lt;p&gt;2.修复商品收藏显示异常问题&lt;/p&gt;&lt;p&gt;3.修复qq快捷登录异常问题&lt;/p&gt;&lt;p&gt;4.修复店铺推广点击无反应问题&lt;/p&gt;&lt;p&gt;5.修复商家店铺源码，服务报错信息&lt;/p&gt;&lt;p&gt;6.修复商品管理搜索异常信息&lt;/p&gt;&lt;p&gt;7.修复网站提示页面异常信息&lt;/p&gt;&lt;p&gt;8.修复订单相关操作异常问题&lt;/p&gt;&lt;p&gt;9.修复订单不自动执行收货问题&lt;/p&gt;&lt;p&gt;10.优化首页整体布局&lt;/p&gt;&lt;p&gt;11.新增安全验证提交拦截&lt;/p&gt;&lt;p&gt;12.新增首页轮播图片管理&lt;/p&gt;&lt;p&gt;13.新增商品安全交易风险提示&lt;/p&gt;&lt;p&gt;14.新增用户管理操作详情&lt;/p&gt;&lt;p&gt;v6.0.3版本更新内容&lt;/p&gt;&lt;p&gt;1.修复商品介绍&amp;nbsp;字符集显示异常问题&lt;/p&gt;&lt;p&gt;2.修复注册验证邮箱验证码问题&lt;/p&gt;&lt;p&gt;3.修复普通会员商家越权问题&lt;/p&gt;&lt;p&gt;4.修复联网实名认证相关配置报错问题&lt;/p&gt;&lt;p&gt;5.新增京东万象短信接口&lt;/p&gt;&lt;p&gt;6.升级基础框架thinkphp版本为6.0.8&lt;/p&gt;&lt;p&gt;v6.0.2版本更新内容&lt;/p&gt;&lt;p&gt;1.修复网站参数配置，网站副标题，关键字，网站描述更新不了问题&lt;/p&gt;&lt;p&gt;2.修复新用户注册邮箱验证问题&lt;/p&gt;&lt;p&gt;3.修复手机认证，邮箱认证&amp;nbsp;出现网络异常&amp;nbsp;请重试&lt;/p&gt;&lt;p&gt;4.修复任务大厅详情页面提示&amp;nbsp;出现网络异常&amp;nbsp;请重试&lt;/p&gt;&lt;p&gt;5.修复QQ快捷登录问题&lt;/p&gt;&lt;p&gt;6.修复在线充值余额不增加问题&lt;/p&gt;&lt;p&gt;7.增加升级后自动清理运行缓存&lt;/p&gt;&lt;p&gt;v6.0.1版本更新内容&lt;/p&gt;&lt;p&gt;1.修复qq登录头像不显示问题&lt;/p&gt;&lt;p&gt;2.修复部分商品缩略图不显示问题，默认使用演示大图&lt;/p&gt;&lt;p&gt;3.修复第三方登录，谷歌浏览器无法登录问题&lt;/p&gt;&lt;p&gt;4.修复订单详情页面安装要求显示错误问题&lt;/p&gt;&lt;p&gt;5.修复网站需求修改问题&lt;/p&gt;&lt;p&gt;6.修复右侧导航浏览记录不显示问题&lt;/p&gt;&lt;p&gt;7.修改右侧导航购物车列表报错问题&lt;/p&gt;&lt;p&gt;8.修复右侧导航缓存展现模式&lt;/p&gt;&lt;p&gt;9.修复右侧导航消息无法清空问题&lt;/p&gt;&lt;p&gt;10.修复右侧导航购物车无法清空问题&lt;/p&gt;&lt;p&gt;11.优化系统目录构造更简洁更强大&lt;/p&gt;&lt;p&gt;12.优化系统违禁词增强，更好的屏蔽违规内容&lt;/p&gt;&lt;p&gt;13.新增后台管理会员中心操作记录&lt;/p&gt;&lt;p&gt;14.新增关键词自动生成功能，有利于seo优化&lt;/p&gt;&lt;p&gt;15.新增搜索查询模式增加强化模糊查询&lt;/p&gt;&lt;p&gt;16.新增后台搜索关键词统计列表&lt;/p&gt;&lt;p&gt;18.新增后台第三方绑定账号列表&lt;/p&gt;&lt;p&gt;19.新增后台收货时间自定义&lt;/p&gt;&lt;p&gt;20.新增后台退款时间自定义&lt;/p&gt;&lt;p&gt;21.新增后台最低金额自定义&lt;/p&gt;&lt;p&gt;22.新增后商品发布审核规则&lt;/p&gt;&lt;p&gt;23.新增关于我们总共10个页面&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;24.新增后台首页显示列表可控选项&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '9', '25', '35', '39', '200', '204', '207', '211|214', '0', 0, '162929151114.png,162929151096.png,162929150868.png,162929150729.png,162929150554.png,162929149481.png,162929148887.png,162929147955.png,162929147114.png', '216|217|219|220|222|223|224|226|227', '229', 'ThinkCT官网', 0, 0.0, 0, NULL, 9, 2180, NULL, '2021-08-18 20:45:58', '', 21, 1, NULL, 'u1629290430', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', '', NULL, 0, NULL, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `shop_complaint`
--

CREATE TABLE IF NOT EXISTS `shop_complaint` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `trade_no` char(50) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `qq` varchar(15) NOT NULL DEFAULT '',
  `mobile` varchar(15) NOT NULL DEFAULT '',
  `desc` varchar(1000) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0待处理 1已处理',
  `admin_read` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理员查看状态',
  `create_at` int(10) unsigned NOT NULL,
  `create_ip` varchar(15) NOT NULL DEFAULT '',
  `pwd` char(10) NOT NULL DEFAULT '123456' COMMENT '投诉单查询密码',
  `result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '申诉结果',
  `expire_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '申诉过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_complaint_message`
--

CREATE TABLE IF NOT EXISTS `shop_complaint_message` (
  `id` int(10) unsigned NOT NULL,
  `trade_no` varchar(255) NOT NULL DEFAULT '0' COMMENT '投诉所属订单',
  `from` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送人，0为管理员发送的消息',
  `content` varchar(1024) NOT NULL DEFAULT '' COMMENT '对话内容',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0未读  1已读',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间',
  `content_type` varchar(255) NOT NULL DEFAULT '0' COMMENT '投诉内容类型：0：文本消息，1：图片消息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投诉会话信息';

-- --------------------------------------------------------

--
-- 表的结构 `shop_custom`
--

CREATE TABLE IF NOT EXISTS `shop_custom` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `ddbh` char(50) DEFAULT NULL,
  `fqbh` char(50) DEFAULT NULL,
  `jsbh` char(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `htime` datetime DEFAULT NULL,
  `ptime` datetime DEFAULT NULL,
  `etime` datetime DEFAULT NULL,
  `txt` text,
  `txtzt` char(50) DEFAULT '0',
  `txttype` char(50) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `cyc` int(11) DEFAULT '0',
  `cyczt` char(50) DEFAULT NULL,
  `zt` char(50) DEFAULT NULL,
  `sm` varchar(250) DEFAULT NULL,
  `dqmoney` decimal(10,2) DEFAULT '0.00',
  `info` varchar(800) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_data_report`
--

CREATE TABLE IF NOT EXISTS `shop_data_report` (
  `id` int(11) NOT NULL,
  `sj` datetime NOT NULL COMMENT '时间',
  `uip` varchar(100) NOT NULL COMMENT 'IP',
  `bh` varchar(30) NOT NULL COMMENT '编号',
  `ubh` varchar(30) DEFAULT NULL COMMENT '用户编号',
  `reason` varchar(30) NOT NULL COMMENT '举报类型',
  `txt` text NOT NULL COMMENT '内容',
  `qq` varchar(30) NOT NULL COMMENT '联系qq',
  `email` varchar(30) NOT NULL COMMENT '联系邮箱',
  `good` varchar(30) NOT NULL COMMENT '商品类型',
  `number` varchar(30) NOT NULL COMMENT '商品bh',
  `status` int(11) NOT NULL COMMENT '审核状态'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_data_report`
--

INSERT INTO `shop_data_report` (`id`, `sj`, `uip`, `bh`, `ubh`, `reason`, `txt`, `qq`, `email`, `good`, `number`, `status`) VALUES
(1, '2021-01-20 00:12:16', '127.0.0.1', '159615973985', NULL, '描述不符', '测试商品举报测试商品举报测试商品举报', '66983239', '66983239@qq.com', 'code', '1551021388-1', 0),
(2, '2021-01-21 23:43:11', '127.0.0.1', '159615973985', 'u1569916994', '出售禁品', '测试举报网站测试举报网站测试举报网站测试举报网站', '1231231312', '12312321@qq.com', 'web', '158600932551', 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_demand`
--

CREATE TABLE IF NOT EXISTS `shop_demand` (
  `id` int(11) NOT NULL,
  `ubh` char(50) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `txt` text,
  `money` char(50) DEFAULT NULL,
  `bh` char(50) DEFAULT NULL,
  `menus` varchar(200) DEFAULT NULL,
  `typeid2` int(11) NOT NULL COMMENT '二级菜单id',
  `djl` int(10) DEFAULT '2',
  `etime` datetime DEFAULT NULL,
  `uip` char(50) DEFAULT NULL,
  `zt` int(11) DEFAULT '1',
  `hfl` int(10) DEFAULT '0',
  `lastgx` datetime DEFAULT NULL,
  `type` char(10) DEFAULT NULL,
  `mail` int(1) DEFAULT '0',
  `filebh` varchar(250) DEFAULT NULL,
  `qqtype` int(1) DEFAULT NULL,
  `phonetype` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_dingdang`
--

CREATE TABLE IF NOT EXISTS `shop_dingdang` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `ubh` char(50) DEFAULT NULL,
  `ddbh` char(50) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `uip` char(30) DEFAULT NULL,
  `money1` decimal(10,2) DEFAULT NULL,
  `money2` decimal(10,2) DEFAULT NULL,
  `info` text,
  `alipayzt` char(30) DEFAULT NULL,
  `type` char(10) DEFAULT NULL,
  `ddzt` char(30) DEFAULT NULL,
  `bz` text,
  `paymoney` decimal(9,2) DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_domain`
--

CREATE TABLE IF NOT EXISTS `shop_domain` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `ubh` char(50) DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `menus` varchar(250) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `lastgx` datetime DEFAULT NULL,
  `djl` int(11) DEFAULT '1',
  `zt` int(1) DEFAULT '1',
  `txt` mediumtext,
  `hfl` int(11) DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `money` float DEFAULT '0',
  `domain_type` varchar(100) NOT NULL COMMENT '域名类型',
  `suffix` varchar(100) NOT NULL COMMENT '域名后缀',
  `registrar` varchar(100) NOT NULL COMMENT '注册商',
  `attribute` varchar(100) NOT NULL COMMENT '附带属性'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_down`
--

CREATE TABLE IF NOT EXISTS `shop_down` (
  `id` int(11) NOT NULL,
  `codebh` char(20) DEFAULT NULL,
  `buy` char(20) DEFAULT NULL,
  `sell` char(20) DEFAULT NULL,
  `fqbh` char(20) DEFAULT NULL,
  `ddbh` char(20) DEFAULT NULL,
  `fees` char(10) DEFAULT '0',
  `money1` decimal(10,2) DEFAULT NULL,
  `money2` decimal(10,2) DEFAULT NULL,
  `backmoney` varchar(11) NOT NULL DEFAULT '0' COMMENT '部分退款',
  `jf` int(50) DEFAULT '0',
  `azmoney` int(10) NOT NULL,
  `sj` datetime DEFAULT NULL,
  `etime` datetime DEFAULT NULL,
  `tit` varchar(100) DEFAULT NULL,
  `mid` int(10) DEFAULT NULL,
  `type` char(10) DEFAULT NULL,
  `menu` varchar(800) DEFAULT NULL,
  `ddzt` char(10) DEFAULT NULL,
  `tzzt` tinyint(10) DEFAULT '0',
  `bz` varchar(250) DEFAULT NULL,
  `adminbz` text,
  `ifyc` int(10) DEFAULT '0',
  `rev_zt` int(1) DEFAULT '0',
  `service` int(11) NOT NULL COMMENT '客服介入',
  `confirm` int(11) NOT NULL COMMENT '判断收货'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_email_code`
--

CREATE TABLE IF NOT EXISTS `shop_email_code` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `screen` varchar(30) NOT NULL DEFAULT '' COMMENT '场景',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0：未使用 1：已使用',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_email_code`
--

INSERT INTO `shop_email_code` (`id`, `email`, `screen`, `code`, `status`, `create_at`) VALUES
(1, '66983239@qq.com', '', '745358', 0, 1599291632),
(2, '66983239@qq.com', '发布成功！！', '524364', 0, 1599291681),
(3, '66983239@qq.com', '发布成功！！', '235300', 0, 1599292049),
(4, '66983239@qq.com', '发布成功！！', '483988', 0, 1599292396),
(5, '66983239@qq.com', '发布成功！！', '960378', 0, 1599292869),
(6, '66983239@qq.com', '发布成功！！', '4979', 0, 1599293278),
(7, '66983239@qq.com', '发布成功！！', '177109', 0, 1599293647),
(8, '66983239@qq.com', '发布成功！！', '735086', 0, 1599294043),
(9, '66983239@qq.com', '发布成功！！', '371693', 0, 1599295039),
(10, '66983239@qq.com', '发布成功！！', '979341', 0, 1599295393),
(11, '66983239@qq.com', '发布成功！！', '729836', 0, 1599295814),
(12, '66983239@qq.com', '发布成功！！', '442414', 0, 1599305602),
(13, '66983239@qq.com', '发布成功！！', '657040', 0, 1599306184),
(14, '66983239@qq.com', '发布成功！！', '878769', 0, 1599306903),
(15, '2126171616@qq.com', '发布成功！！', '135254', 0, 1600610703),
(16, '1006727758@qq.com', '发布成功！！', '741812', 0, 1600854665),
(17, '291047352@qq.com', '发布成功！！', '603201', 0, 1600869661),
(18, '2136258233@qq.com', '发布成功！！', '679380', 0, 1601039356),
(19, '2136258233@qq.com', '发布成功！！', '228385', 0, 1601039773),
(20, 'w18148120849@163.com', '发布成功！！', '161885', 0, 1601170292),
(21, 'wowsososo@gmail.com', '发布成功！！', '430709', 0, 1601282054),
(22, '778251855@qq.com', '发布成功！！', '827994', 0, 1601375569),
(23, '2011529990@qq.com', '发布成功！！', '369865', 0, 1601475921),
(24, '2257695072@qq.com', '发布成功！！', '730895', 0, 1601714314),
(25, '2257695072@qq.com', '发布成功！！', '908440', 0, 1601714620),
(26, '10096270@qq.com', '发布成功！！', '597526', 0, 1601723009),
(27, '85022061@qq.com', '发布成功！！', '931067', 0, 1601791056),
(28, '1240706243@qq.com', '发布成功！！', '920352', 0, 1601881377),
(29, '1240706243@qq.com', '发布成功！！', '355415', 0, 1601881683),
(30, '3024322505@qq.com', '发布成功！！', '556100', 0, 1602051678),
(31, '751862168@qq.com', '发布成功！！', '414200', 0, 1602306612),
(32, '751862168@qq.com', '发布成功！！', '271952', 0, 1602307400),
(33, '751862168@qq.com', '发布成功！！', '139348', 0, 1602311246),
(34, '97518802@qq.com', '发布成功！！', '753215', 0, 1602311387),
(35, '807792985@qq.com', '发布成功！！', '229816', 0, 1602328408),
(36, '1003123026@qq.com', '发布成功！！', '498736', 0, 1602411610),
(37, '2573704202@qq.com', '发布成功！！', '935161', 0, 1602508899),
(38, '1123291181@qq.com', '发布成功！！', '673234', 0, 1602611611),
(39, '2471236724@qq.com', '发布成功！！', '474766', 0, 1602742768),
(40, '3245744065@qq.com', '发布成功！！', '187777', 0, 1602774553),
(41, '1197464153@qq.com', '发布成功！！', '898606', 0, 1602916774),
(42, 'email@thinkct.net', '发布成功！！', '971793', 0, 1603182353),
(43, 'bananafishyo@163.com', '发布成功！！', '484106', 0, 1603188042),
(44, '2125475929@qq.com', '发布成功！！', '900348', 0, 1603250922),
(45, '495446244@qq.com', '发布成功！！', '673200', 0, 1603414184),
(46, '79432303@qq.com', '发布成功！！', '681419', 0, 1603509918),
(47, 'sobaobao@163.com', '发布成功！！', '437858', 0, 1603515803),
(48, '1423630274@qq.com', '发布成功！！', '990749', 0, 1603527207),
(49, 'liceen@qq.com', '发布成功！！', '451781', 0, 1603532629),
(50, '1446232079@qq.com', '发布成功！！', '305949', 0, 1603698846),
(51, '50329467@qq.com', '发布成功！！', '927157', 0, 1603853290),
(52, '15455@163.com', '发布成功！！', '861072', 0, 1603867112),
(53, '389780281@qq.com', '发布成功！！', '889050', 0, 1603892917),
(54, '8635815@qq.com', '发布成功！！', '588595', 0, 1603949140),
(55, '2577642388@qq.com', '发布成功！！', '735088', 0, 1603967994),
(56, '739914664@qq.com', '发布成功！！', '798626', 0, 1603980653),
(57, '392540408@qq.com', '发布成功！！', '385832', 0, 1604114175),
(58, '1991336263@qq.com', '发布成功！！', '417364', 0, 1604129953),
(59, '1592404889@qq.com', '发布成功！！', '763436', 0, 1604202722),
(60, 'none.0123.0123@gmail.com', '发布成功！！', '442782', 0, 1604281425),
(61, '443536954@qq.com', '发布成功！！', '477521', 0, 1604288259),
(62, '1160817226@qq.com', '发布成功！！', '871886', 0, 1604399026),
(63, '2230159541@qq.com', '发布成功！！', '826643', 0, 1604538977),
(64, '7237372@qq.com', '发布成功！！', '700175', 0, 1604561655),
(65, 'ljpzsh@126.com', '发布成功！！', '325274', 0, 1604570301),
(66, 'a99kj@vip.qq.com', '发布成功！！', '499182', 0, 1604716589),
(67, '1529981@qq.com', '发布成功！！', '519111', 0, 1604756826),
(68, '421381570@qq.com', '发布成功！！', '126738', 0, 1604874324),
(69, '3176221827@qq.com', '发布成功！！', '774751', 0, 1605078221),
(70, '2337079415@qq.com', '发布成功！！', '174592', 0, 1605097881),
(71, '5431081@qq.com', '发布成功！！', '614707', 0, 1605099617),
(72, '2632994426@qq.COM', '发布成功！！', '345481', 0, 1605156150),
(73, 'hanhaihuai@163.com', '发布成功！！', '827347', 0, 1605159988),
(74, '577420565@qq.com', '发布成功！！', '396260', 0, 1605358497),
(75, '2064125152@qq.com', '发布成功！！', '304283', 0, 1605432101),
(76, '276175012@qq.com', '发布成功！！', '574562', 0, 1605490259),
(77, '3336717377@qq.com', '发布成功！！', '800824', 0, 1605720177),
(78, '2183787716@qq.com', '发布成功！！', '664511', 0, 1605772227),
(79, '569811456@qq.com', '发布成功！！', '802972', 0, 1605786039),
(80, '94587262@qq.com', '发布成功！！', '551598', 0, 1605856830),
(81, '1786955275@qq.com', '发布成功！！', '468161', 0, 1605888094),
(82, '596349081@qq.com', '发布成功！！', '879526', 0, 1606094515),
(83, '2220332929@qq.com', '发布成功！！', '616701', 0, 1606138334),
(84, 'q54321@vip.qq.com', '发布成功！！', '115821', 0, 1606249715),
(85, '2505788821@qq.com', '发布成功！！', '316949', 0, 1606296411),
(86, '5133248@qq.com', '发布成功！！', '889540', 0, 1606347908),
(87, 'tdshetuan@163.com', '发布成功！！', '123421', 0, 1606379724),
(88, '2485781@qq.com', '发布成功！！', '855018', 0, 1606582526),
(89, '807792985@qq.com', '发布成功！！', '594814', 0, 1606711810),
(90, '934190039@qq.com', '发布成功！！', '772488', 0, 1606812757),
(91, '21407048@qq.com', '发布成功！！', '961065', 0, 1606813530),
(92, '1786955275@qq.com', '发布成功！！', '805104', 0, 1606824540),
(93, '66983239@qq.com', '发布成功！！', '950170', 0, 1615055813);

-- --------------------------------------------------------

--
-- 表的结构 `shop_fav`
--

CREATE TABLE IF NOT EXISTS `shop_fav` (
  `id` int(11) NOT NULL,
  `bh` char(20) DEFAULT NULL,
  `favbh` char(20) DEFAULT NULL,
  `ubh` char(20) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `type` char(10) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_file`
--

CREATE TABLE IF NOT EXISTS `shop_file` (
  `id` int(10) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(100) NOT NULL,
  `ubh` varchar(100) NOT NULL,
  `bh` varchar(100) NOT NULL,
  `admin` int(10) NOT NULL,
  `type` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `tmp_name` varchar(100) NOT NULL,
  `url` varchar(1000) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_file`
--

INSERT INTO `shop_file` (`id`, `sj`, `uip`, `ubh`, `bh`, `admin`, `type`, `name`, `tmp_name`, `url`) VALUES
(1, '2021-08-18 20:55:39', '101.16.180.123', 'u1629290430', '162929068919', 1, 'code', '1.jpg', '162929133947.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d0348bb56a/611d0348bb5bb.jpg'),
(2, '2021-08-18 20:57:51', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '1.png', '162929147114.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03cc6b10e/611d03cc6b14c.png'),
(3, '2021-08-18 20:57:59', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '2.png', '162929147955.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03d10ab7b/611d03d10abba.png'),
(4, '2021-08-18 20:58:08', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '3.png', '162929148887.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03d85fa6d/611d03d85faac.png'),
(5, '2021-08-18 20:58:14', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '4.png', '162929149481.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03e0c1c9d/611d03e0c1cdb.png'),
(6, '2021-08-18 20:58:25', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '5.png', '162929150554.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03e6880b1/611d03e6886ef.png'),
(7, '2021-08-18 20:58:27', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '6.jpg', '162929150729.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03f220401/611d03f220442.jpg'),
(8, '2021-08-18 20:58:28', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '7.jpg', '162929150868.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03f3b57eb/611d03f3b5829.jpg'),
(9, '2021-08-18 20:58:30', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '8.jpg', '162929151096.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03f49984e/611d03f49988c.jpg'),
(10, '2021-08-18 20:58:31', '101.16.180.123', 'u1629290430', '162929068919', 2, 'code', '9.jpg', '162929151114.png', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/file/611d03f6a6e20/611d03f6a6e63.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `shop_gonggao`
--

CREATE TABLE IF NOT EXISTS `shop_gonggao` (
  `id` int(11) NOT NULL,
  `tit` varchar(220) DEFAULT NULL,
  `desc` varchar(500) NOT NULL COMMENT '描述',
  `txt` text,
  `sj` datetime DEFAULT NULL,
  `fl` char(50) DEFAULT NULL,
  `djl` int(10) DEFAULT '0',
  `bh` char(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_goods`
--

CREATE TABLE IF NOT EXISTS `shop_goods` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `cate_id` int(10) unsigned NOT NULL,
  `theme` varchar(15) NOT NULL DEFAULT 'default' COMMENT '主题',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `name` varchar(500) NOT NULL DEFAULT '',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `cost_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '成本价',
  `wholesale_discount` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '批发优惠',
  `wholesale_discount_list` text NOT NULL COMMENT '批发价',
  `limit_quantity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '起购数量',
  `inventory_notify` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '库存预警 0表示不报警',
  `inventory_notify_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '库存预警通知方式 1站内信 2邮件',
  `coupon_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '优惠券 0不支持 1支持',
  `sold_notify` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '售出通知',
  `take_card_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '取卡密码 0关闭 1必填 2选填',
  `visit_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '访问密码',
  `visit_password` varchar(30) NOT NULL DEFAULT '' COMMENT '访问密码',
  `contact_limit` enum('mobile','email','qq','any','default') NOT NULL DEFAULT 'default',
  `content` varchar(200) NOT NULL DEFAULT '' COMMENT '商品说明',
  `remark` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0下架 1上架',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0',
  `is_freeze` tinyint(4) DEFAULT '0',
  `sms_payer` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '短信付费方：0买家 1商户',
  `delete_at` int(11) DEFAULT NULL COMMENT '删除标记'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_goods`
--

INSERT INTO `shop_goods` (`id`, `user_id`, `cate_id`, `theme`, `sort`, `name`, `price`, `cost_price`, `wholesale_discount`, `wholesale_discount_list`, `limit_quantity`, `inventory_notify`, `inventory_notify_type`, `coupon_type`, `sold_notify`, `take_card_type`, `visit_type`, `visit_password`, `contact_limit`, `content`, `remark`, `status`, `create_at`, `is_freeze`, `sms_payer`, `delete_at`) VALUES
(1, 10001, 1, 'chiji', 0, '绝地求生卡密', '0.10', '0.00', 0, '[]', 1, 0, 1, 0, 0, 0, 0, '', 'default', '这个自动发卡系统的商品说明', '这个自动发卡系统的使用说明', 1, 1538990072, 0, 0, 0),
(2, 10001, 1, 'wangzhe', 0, '王者卡密', '0.10', '0.00', 0, '[]', 1, 0, 1, 0, 0, 0, 0, '', 'default', '这个是自动发卡系统王者卡密的卡密说明', '这个是自动发卡系统王者卡密的使用说明', 1, 1538990308, 0, 0, 0),
(3, 10001, 1, 'yinyangshi', 0, '阴阳师卡密', '0.10', '0.00', 0, '[]', 1, 0, 1, 0, 0, 0, 0, '', 'default', '这个是阴阳师卡密商品说明', '这个是阴阳师卡密使用说明', 1, 1538990374, 0, 0, 0),
(4, 10001, 1, 'default', 0, '23213', '22.00', '0.00', 0, '[]', 1, 0, 1, 0, 0, 0, 0, '', 'default', '', '', 0, 1592575170, 0, 0, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_goods_card`
--

CREATE TABLE IF NOT EXISTS `shop_goods_card` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `goods_id` int(10) unsigned NOT NULL,
  `number` varchar(500) NOT NULL DEFAULT '',
  `secret` varchar(500) NOT NULL DEFAULT '',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '0不可用 1可用 2已使用',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0',
  `delete_at` int(11) DEFAULT NULL COMMENT '删除标记',
  `sell_time` int(11) DEFAULT NULL COMMENT '售出时间'
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_goods_card`
--

INSERT INTO `shop_goods_card` (`id`, `user_id`, `goods_id`, `number`, `secret`, `status`, `create_at`, `delete_at`, `sell_time`) VALUES
(1, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 2, 1538990090, 0, 1538990812),
(2, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 2, 1538990090, 0, 1568359416),
(3, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990090, 0, 0),
(4, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990090, 0, 0),
(5, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990090, 0, 0),
(6, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990090, 0, 0),
(7, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990090, 0, 0),
(8, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990090, 0, 0),
(9, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990090, 0, 0),
(10, 10001, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB', 1, 1538990090, 0, 0),
(11, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(12, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(13, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(14, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(15, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(16, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(17, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(18, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(19, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(20, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(21, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(22, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(23, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(24, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(25, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(26, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(27, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(28, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(29, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(30, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBBAAAAAAAAAAA', 1, 1538990394, 0, 0),
(31, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(32, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(33, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(34, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(35, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(36, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(37, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(38, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(39, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(40, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990394, 0, 0),
(41, 10001, 2, 'AAAAAAAAAAA', 'BBBBBBBBBBBB', 1, 1538990394, 0, 0),
(42, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(43, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(44, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(45, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(46, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(47, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(48, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(49, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(50, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(51, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(52, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(53, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(54, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(55, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(56, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(57, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(58, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(59, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(60, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(61, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(62, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(63, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(64, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(65, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(66, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(67, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(68, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(69, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(70, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(71, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(72, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(73, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(74, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(75, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(76, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(77, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(78, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(79, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(80, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(81, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(82, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(83, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(84, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(85, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(86, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(87, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(88, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(89, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(90, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(91, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(92, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(93, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(94, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(95, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(96, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(97, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(98, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(99, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(100, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(101, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(102, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(103, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(104, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(105, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(106, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(107, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(108, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(109, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(110, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(111, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(112, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1, 1538990414, 0, 0),
(113, 10001, 3, 'AAAAAAAAAAA', 'BBBBBBBBBBBB', 1, 1538990414, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_goods_category`
--

CREATE TABLE IF NOT EXISTS `shop_goods_category` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `sort` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `create_at` int(10) unsigned NOT NULL,
  `theme` varchar(15) NOT NULL DEFAULT 'default' COMMENT '主题'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_goods_category`
--

INSERT INTO `shop_goods_category` (`id`, `user_id`, `name`, `sort`, `status`, `create_at`, `theme`) VALUES
(1, 10001, '游戏卡密', 0, 1, 1538989994, 'default');

-- --------------------------------------------------------

--
-- 表的结构 `shop_goods_coupon`
--

CREATE TABLE IF NOT EXISTS `shop_goods_coupon` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `cate_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '全部',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '类型 1、元  100、%',
  `amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `code` varchar(255) NOT NULL DEFAULT '',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1未使用 2已用',
  `expire_at` int(10) unsigned NOT NULL,
  `create_at` int(10) unsigned NOT NULL,
  `delete_at` int(11) DEFAULT NULL COMMENT '删除标记'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_help`
--

CREATE TABLE IF NOT EXISTS `shop_help` (
  `id` int(11) NOT NULL,
  `ty1id` int(10) DEFAULT NULL,
  `ty2id` int(10) DEFAULT NULL,
  `tit` varchar(220) DEFAULT NULL,
  `txt` text,
  `xh` int(10) DEFAULT NULL,
  `sj` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_helptype`
--

CREATE TABLE IF NOT EXISTS `shop_helptype` (
  `id` int(11) NOT NULL,
  `sj` datetime DEFAULT NULL,
  `admin` int(10) DEFAULT NULL,
  `name1` char(150) DEFAULT NULL,
  `name2` char(50) DEFAULT NULL,
  `xh` int(10) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_helptype`
--

INSERT INTO `shop_helptype` (`id`, `sj`, `admin`, `name1`, `name2`, `xh`) VALUES
(1, '2013-09-27 17:39:13', 1, '买家帮助', NULL, 2),
(2, '2013-09-27 18:37:10', 2, '买家帮助', '如何购买', 2),
(3, '2014-03-17 05:19:11', 1, '商家帮助', NULL, 3),
(4, '2014-03-17 05:23:23', 2, '商家帮助', '如何设置自动发货', 5),
(5, '2014-03-17 05:19:24', 1, '支付说明', NULL, 4),
(6, '2014-03-17 05:19:24', 2, '支付说明', '积分说明', 1),
(7, '2014-03-17 05:18:19', 1, '新手上路', NULL, 1),
(8, '2014-10-08 22:40:10', 1, '关于商业源码', NULL, 5),
(9, '2014-03-17 05:20:05', 1, '入驻帮助', NULL, 6),
(10, '2014-03-17 05:20:17', 2, '新手上路', '注册与安全登录', 1),
(11, '2014-03-17 05:20:30', 2, '入驻帮助', '入驻相关问题', 1),
(12, '2014-10-08 22:40:10', 2, '关于商业源码', '管理团队', 1),
(13, '2014-10-08 22:40:10', 2, '关于商业源码', '合作伙伴', 2),
(14, '2014-10-08 22:40:10', 2, '关于商业源码', '大事件', 3),
(15, '2014-10-08 22:40:10', 2, '关于商业源码', '平台资质', 4),
(16, '2014-10-08 22:40:10', 2, '关于商业源码', '平台荣誉', 5),
(17, '2014-03-17 05:21:25', 2, '支付说明', '如何支付', 2),
(18, '2014-03-17 05:21:30', 2, '支付说明', '安全保障', 3),
(19, '2014-03-17 05:21:36', 2, '支付说明', '关于预付款', 4),
(20, '2014-03-17 05:21:41', 2, '支付说明', '常见支付问题', 5),
(21, '2014-03-20 00:22:32', 2, '商家帮助', '禁售产品及发布规则', 1),
(22, '2014-03-17 05:22:18', 2, '商家帮助', '入驻流程', 2),
(23, '2014-03-17 05:22:24', 2, '商家帮助', '商家协议', 3),
(24, '2014-03-17 05:22:30', 2, '商家帮助', '如何设置关键词', 4),
(25, '2014-03-17 05:22:44', 2, '商家帮助', '商家常见问题', 6),
(26, '2014-03-17 05:23:42', 2, '买家帮助', '平台优势', 1),
(27, '2014-03-17 05:23:57', 2, '买家帮助', '怎么付款', 3),
(28, '2014-03-17 05:24:11', 2, '买家帮助', '如何退款', 4),
(29, '2014-03-17 05:24:16', 2, '买家帮助', '买家常见问题', 5),
(30, '2014-03-17 05:24:33', 2, '新手上路', '购物流程', 2),
(31, '2014-03-17 05:24:38', 2, '新手上路', '搜索窍门', 3),
(32, '2014-03-17 05:24:43', 2, '新手上路', '关于商家', 4),
(33, '2014-03-17 05:24:49', 2, '新手上路', '新手常见问题', 5);

-- --------------------------------------------------------

--
-- 表的结构 `shop_invite_code`
--

CREATE TABLE IF NOT EXISTS `shop_invite_code` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT '所有者ID',
  `code` char(32) NOT NULL DEFAULT '' COMMENT '邀请码',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '使用状态 0未使用 1已使用',
  `invite_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '受邀用户ID',
  `invite_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '邀请时间',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `expire_at` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_jifen`
--

CREATE TABLE IF NOT EXISTS `shop_jifen` (
  `id` int(11) NOT NULL,
  `bh` char(20) DEFAULT NULL,
  `ubh` char(20) DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `jf` int(10) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `uip` char(20) DEFAULT NULL,
  `pro` char(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_jifen_cms`
--

CREATE TABLE IF NOT EXISTS `shop_jifen_cms` (
  `id` int(10) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(100) NOT NULL,
  `bh` varchar(30) NOT NULL,
  `tit` varchar(100) NOT NULL,
  `ckey` varchar(100) NOT NULL,
  `cdes` varchar(100) NOT NULL,
  `money` varchar(30) NOT NULL,
  `jifen` int(100) NOT NULL,
  `txt` text NOT NULL,
  `tp` varchar(200) NOT NULL,
  `xsnum` int(1) NOT NULL,
  `zt` int(10) NOT NULL,
  `lastgx` datetime NOT NULL COMMENT '更新时间'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_jifen_cms`
--

INSERT INTO `shop_jifen_cms` (`id`, `sj`, `uip`, `bh`, `tit`, `ckey`, `cdes`, `money`, `jifen`, `txt`, `tp`, `xsnum`, `zt`, `lastgx`) VALUES
(1, '2020-03-12 00:00:00', '127.0.0.1', '153649198154', '广告插队卡', '类型:,价格:-1.00,流量: IP,权重: BR,网站出售,网站交易', '广告插队卡,互站交易平台,网站交易', '1', 10, '广告插队卡，可以提升广告排队位置！', '//suzhizhan.oss-cn-beijing.aliyuncs.com/upload/article/fengmian/158400143327.jpg', 1, 1, '0000-00-00 00:00:00'),
(2, '2020-02-25 02:05:21', '127.0.0.1', '153649198221', '测试', '', '', '2', 20, '2222', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/article/fengmian/158400143327.jpg', 0, 0, '0000-00-00 00:00:00'),
(3, '2020-02-25 02:05:21', '127.0.0.1', '153649198121', '店铺改名卡', '', '', '99', 999, '仅适用于平台【商家】店铺名称修改使用。\r\n（为防止不良商家频繁修改店名洗白，每半年限兑换1张改名卡）\r\n\r\n', '//suzhizhan.oss-cn-beijing.aliyuncs.com/upload/article/fengmian/158400826357.jpg', 3, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `shop_link`
--

CREATE TABLE IF NOT EXISTS `shop_link` (
  `id` int(10) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `zt` int(10) NOT NULL DEFAULT '1',
  `type` varchar(100) NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_link`
--

INSERT INTO `shop_link` (`id`, `sj`, `uip`, `name`, `url`, `logo`, `zt`, `type`, `sort`) VALUES
(1, '0000-00-00 00:00:00', '', '码发布', 'https://www.mafabu.com/', '#', 1, 'link', 0),
(2, '0000-00-00 00:00:00', '', 'ThinkCT官网', 'https://www.thinkct.net/', '#', 1, 'link', 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_link_link`
--

CREATE TABLE IF NOT EXISTS `shop_link_link` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `relation_type` varchar(20) NOT NULL DEFAULT '',
  `relation_id` int(10) unsigned NOT NULL DEFAULT '0',
  `token` char(16) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `short_url` varchar(30) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `create_at` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_link_link`
--

INSERT INTO `shop_link_link` (`id`, `user_id`, `relation_type`, `relation_id`, `token`, `url`, `short_url`, `status`, `create_at`) VALUES
(2, 10001, 'goods', 1, '4A9BAC76', '', 'http://t.cn/EhdF9gu', 1, 1538990191),
(3, 10001, 'goods', 2, '9512A536', '', 'http://t.cn/EhdsVgM', 1, 1538990308),
(4, 10001, 'goods', 3, '410BB69A', '', 'http://t.cn/EhdsnoQ', 1, 1538990374),
(5, 10001, 'user', 10001, '3320B5AF', '', 'http://t.cn/EhgZPLY', 1, 1538991202),
(6, 10001, 'goods', 4, 'BCAA6599', '', '', 1, 1592575170);

-- --------------------------------------------------------

--
-- 表的结构 `shop_log`
--

CREATE TABLE IF NOT EXISTS `shop_log` (
  `id` int(10) unsigned NOT NULL,
  `business_type` varchar(20) NOT NULL DEFAULT '' COMMENT '业务类型',
  `content` text NOT NULL COMMENT '内容',
  `ua` varchar(255) NOT NULL DEFAULT '',
  `uri` varchar(255) NOT NULL DEFAULT '',
  `create_at` int(10) unsigned NOT NULL COMMENT '记录时间',
  `create_ip` varchar(15) NOT NULL DEFAULT '' COMMENT 'ip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_merchant_log`
--

CREATE TABLE IF NOT EXISTS `shop_merchant_log` (
  `id` bigint(20) unsigned NOT NULL,
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `node` char(200) NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '操作人用户ID',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `action` varchar(200) NOT NULL DEFAULT '' COMMENT '操作行为',
  `content` text NOT NULL COMMENT '操作内容描述',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作日志表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_message`
--

CREATE TABLE IF NOT EXISTS `shop_message` (
  `id` int(10) NOT NULL,
  `ubh` varchar(100) NOT NULL,
  `sj` datetime NOT NULL,
  `tit` varchar(50) NOT NULL,
  `txt` varchar(200) NOT NULL,
  `url` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `zt` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_message_message`
--

CREATE TABLE IF NOT EXISTS `shop_message_message` (
  `id` int(10) unsigned NOT NULL,
  `from_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0为管理员',
  `to_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(60) NOT NULL DEFAULT '',
  `content` varchar(1024) NOT NULL DEFAULT '',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0未读  1已读',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_migrations`
--

CREATE TABLE IF NOT EXISTS `shop_migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `shop_migrations`
--

INSERT INTO `shop_migrations` (`id`, `migration`, `batch`) VALUES
(53, '20180428_alter_migrations_table.sql', 0),
(54, '20180428_create_foo_table.sql', 0),
(55, '20180428_drop_foo_table.sql', 0),
(56, '20180502_create_foo_table.sql', 1),
(57, '20180502_drop_foo_table.sql', 2),
(58, '20180507_alter_table.sql', 3),
(59, '20180509_alter_order_table_goods_table.sql', 3),
(60, '20180512_alter_user_money_log_table.sql', 4),
(61, '20180512_alter_user_table.sql', 4),
(62, '20180513_add_auto_unfreeze_table.sql', 4),
(63, '20180513_alter_user_table.sql', 5),
(64, '20180513alter_goods_card_table.sql', 5),
(65, '20180515_alter_auto_unfreeze_table.sql', 5),
(66, '20180523alter_order_card_table.sql', 6);

-- --------------------------------------------------------

--
-- 表的结构 `shop_moneyback`
--

CREATE TABLE IF NOT EXISTS `shop_moneyback` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `money1` decimal(10,2) DEFAULT '0.00',
  `sj` datetime DEFAULT NULL,
  `oksj` datetime DEFAULT NULL,
  `sell` char(20) DEFAULT NULL,
  `buy` char(20) DEFAULT NULL,
  `codebh` char(50) DEFAULT NULL,
  `tkly` text,
  `cljg` text,
  `zt` int(10) DEFAULT NULL,
  `jjsj` datetime DEFAULT '0000-00-00 00:00:00',
  `money2` decimal(10,2) DEFAULT NULL,
  `ddbh` char(50) DEFAULT NULL,
  `jf` int(50) DEFAULT '0',
  `type` char(10) DEFAULT NULL,
  `waiver` int(11) NOT NULL COMMENT '消保免赔:0启用1禁用',
  `waiver_sj` datetime NOT NULL COMMENT '消保免赔时间',
  `waiver_oksj` datetime NOT NULL COMMENT '消保免赔结束时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_moneydb`
--

CREATE TABLE IF NOT EXISTS `shop_moneydb` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `ddbh` char(20) DEFAULT NULL,
  `codebh` char(50) DEFAULT NULL,
  `money1` decimal(10,2) DEFAULT NULL,
  `money2` decimal(10,2) DEFAULT '0.00',
  `jf` int(50) DEFAULT '0',
  `buy` char(20) DEFAULT NULL,
  `sell` char(20) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `oksj` datetime DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `type` char(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_moneyrecord`
--

CREATE TABLE IF NOT EXISTS `shop_moneyrecord` (
  `id` int(11) NOT NULL,
  `bh` char(20) DEFAULT NULL,
  `ubh` char(20) DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `moneynum` decimal(10,2) DEFAULT '0.00',
  `sj` datetime DEFAULT NULL,
  `uip` char(20) DEFAULT NULL,
  `pro` char(20) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_moneyrecord`
--

INSERT INTO `shop_moneyrecord` (`id`, `bh`, `ubh`, `tit`, `moneynum`, `sj`, `uip`, `pro`) VALUES
(1, '1629291579', 'u1629290430', '1000', '1000.00', '2021-08-18 20:59:39', '101.16.180.123', NULL),
(2, '1629291595', 'u1629290430', '购买推广广告', '-200.00', '2021-08-18 20:59:55', '101.16.180.123', '1'),
(3, '1629291598', 'u1629290430', '购买推广广告', '-100.00', '2021-08-18 20:59:58', '101.16.180.123', '1'),
(4, '1629291610', 'u1629290430', '购买推广广告', '-180.00', '2021-08-18 21:00:10', '101.16.180.123', '1'),
(5, '1629291614', 'u1629290430', '购买推广广告', '-70.00', '2021-08-18 21:00:14', '101.16.180.123', '1'),
(6, '1629291619', 'u1629290430', '购买推广广告', '-50.00', '2021-08-18 21:00:19', '101.16.180.123', '1'),
(7, '1629291624', 'u1629290430', '购买推广广告', '-50.00', '2021-08-18 21:00:24', '101.16.180.123', '1'),
(8, '1629291628', 'u1629290430', '购买推广广告', '-180.00', '2021-08-18 21:00:28', '101.16.180.123', '1');

-- --------------------------------------------------------

--
-- 表的结构 `shop_nav`
--

CREATE TABLE IF NOT EXISTS `shop_nav` (
  `id` bigint(20) unsigned NOT NULL,
  `pid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `node` varchar(200) NOT NULL DEFAULT '' COMMENT '节点代码',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `class` varchar(100) NOT NULL COMMENT '样式',
  `url` varchar(400) NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '菜单排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `create_by` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='前台导航表';

--
-- 转存表中的数据 `shop_nav`
--

INSERT INTO `shop_nav` (`id`, `pid`, `title`, `node`, `icon`, `class`, `url`, `params`, `target`, `sort`, `status`, `create_by`, `create_at`) VALUES
(1, 0, '首页', '', '', 'bold', '/', '', '0', 6, 0, 0, '2018-03-23 01:20:50'),
(2, 0, '网站寄售', '', '', 'bold', '/web/', '', '0', 3, 1, 0, '2018-03-23 01:21:11'),
(3, 0, '服务市场', '', '', 'bold', '/serve/', '', '0', 4, 1, 0, '2018-03-23 01:21:35'),
(4, 0, '源码集市', '', '', 'bold', '/code/', '', '0', 5, 1, 0, '2018-03-23 01:22:09'),
(6, 0, '任务大厅', '', '', '', '/task/', '', '0', 1, 1, 0, '2018-05-10 04:24:37'),
(7, 0, '域名交易', '', '', '', '/domain/', '', '0', 2, 1, 0, '2018-06-21 02:53:00');

-- --------------------------------------------------------

--
-- 表的结构 `shop_oauth`
--

CREATE TABLE IF NOT EXISTS `shop_oauth` (
  `id` int(10) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(50) NOT NULL,
  `ubh` varchar(100) NOT NULL,
  `name` varchar(30) NOT NULL,
  `code` varchar(100) NOT NULL,
  `tx` varchar(100) NOT NULL,
  `login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(30) NOT NULL,
  `status` int(11) NOT NULL COMMENT '状态'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_order`
--

CREATE TABLE IF NOT EXISTS `shop_order` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `goods_id` int(10) unsigned NOT NULL,
  `trade_no` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(60) NOT NULL DEFAULT '' COMMENT '流水号',
  `paytype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '支付渠道',
  `channel_account_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '渠道账号',
  `pay_url` varchar(10240) NOT NULL DEFAULT '' COMMENT '付款地址',
  `pay_content_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付参数类型 1：二维码 2：跳转链接 3：表单',
  `goods_name` varchar(500) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品单价',
  `goods_cost_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '成本价',
  `quantity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `coupon_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否使用优惠券',
  `coupon_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '优惠券ID',
  `coupon_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '优惠价格',
  `total_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '总价（买家实付款）',
  `total_cost_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '总成本价',
  `sold_notify` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '售出通知（买家）',
  `take_card_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要取卡密码',
  `take_card_password` varchar(20) NOT NULL DEFAULT '' COMMENT '取卡密码',
  `contact` varchar(20) NOT NULL DEFAULT '' COMMENT '联系方式',
  `email_notify` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否邮件通知',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '邮箱号',
  `sms_notify` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否短信通知',
  `rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '手续费率',
  `fee` decimal(10,3) unsigned NOT NULL DEFAULT '0.000' COMMENT '手续费',
  `agent_rate` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '代理费率',
  `agent_fee` decimal(10,3) unsigned NOT NULL DEFAULT '0.000' COMMENT '代理佣金',
  `sms_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态 0未支付 1已支付 2已关闭',
  `is_freeze` tinyint(4) NOT NULL DEFAULT '0',
  `create_at` int(10) unsigned NOT NULL COMMENT '创建时间',
  `create_ip` varchar(15) NOT NULL DEFAULT '',
  `success_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '支付成功时间',
  `first_query` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单第一次查询无需验证码',
  `sms_payer` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '短信付费方：0买家 1商户',
  `total_product_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品总价（不含短信费）',
  `sendout` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '已发货数量',
  `fee_payer` tinyint(4) NOT NULL DEFAULT '1' COMMENT '订单手续费支付方，1：商家承担，2买家承担',
  `settlement_type` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '结算方式，1:T1结算，0:T0结算',
  `finally_money` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '商户订单最终收入（已扣除短信费，手续费）'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_order`
--

INSERT INTO `shop_order` (`id`, `user_id`, `goods_id`, `trade_no`, `transaction_id`, `paytype`, `channel_id`, `channel_account_id`, `pay_url`, `pay_content_type`, `goods_name`, `goods_price`, `goods_cost_price`, `quantity`, `coupon_type`, `coupon_id`, `coupon_price`, `total_price`, `total_cost_price`, `sold_notify`, `take_card_type`, `take_card_password`, `contact`, `email_notify`, `email`, `sms_notify`, `rate`, `fee`, `agent_rate`, `agent_fee`, `sms_price`, `status`, `is_freeze`, `create_at`, `create_ip`, `success_at`, `first_query`, `sms_payer`, `total_product_price`, `sendout`, `fee_payer`, `settlement_type`, `finally_money`) VALUES
(1, 10001, 1, 'T1810081726376105', '', 88, 139, 1, '<form id=''pay_form'' class="form-inline" method="post" action="https://api.tianniu.cc/pay/index"><input type="hidden" name="pay_memberid" value="2"><input type="hidden" name="pay_appid" value="12340004"><input type="hidden" name="pay_submchid" value="0"><input type="hidden" name="pay_orderid" value="T1810081726376105"><input type="hidden" name="pay_applydate" value="2018-10-08 17:26:37"><input type="hidden" name="pay_bankcode" value="Aliqrcode"><input type="hidden" name="pay_notifyurl" value="https://ka.tianniu.cc/pay/notify/NZFAliqrcode"><input type="hidden" name="pay_callbackurl" value="https://ka.tianniu.cc/pay/page/NZFAliqrcode"><input type="hidden" name="pay_amount" value="0.10"><input type="hidden" name="pay_md5sign" value="663739E9035E4E5C33B21C3DE38807DF"><input type="hidden" name="pay_productname" value="投诉QQ：12344321 订单：T1810081726376105"><input type="hidden" name="pay_productdesc" value="投诉QQ：12344321 订单：T1810081726376105"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '18620528362', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 1, 0, 1538990796, '61.242.114.84', 1538990812, 1, 0, '0.10', 1, 1, 1, '0.0000'),
(2, 10001, 2, 'T190911235313100030', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190911235313100030"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190911235313100030"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="effcba8af670113bf7efcf77881e2648"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '王者卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '53432453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217193, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(3, 10001, 2, 'T190911235555100012', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190911235555100012"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190911235555100012"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="ff108fdd556d4847b3e3ed31516b5211"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '王者卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '53432453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217355, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(4, 10001, 2, 'T190911235734100075', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190911235734100075"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190911235734100075"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="589d42404606a89efb941d69a30a6ffb"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '王者卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '53432453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217454, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(5, 10001, 2, 'T190912000335100036', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190912000335100036"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190912000335100036"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="bf0d67b4fd42d6a26c18a83e8fdd2ff4"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '王者卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '53432453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217815, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(6, 10001, 2, 'T190912000340100041', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190912000340100041"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190912000340100041"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="5bbb04c3e15da2f0bd662be07ec616ec"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '王者卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '53432453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217820, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(7, 10001, 2, 'T190912000347100086', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190912000347100086"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190912000347100086"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="ef699fc75233c68734858c6c0b2a3c98"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '王者卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '53432453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217826, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(8, 10001, 2, 'T190912000404100016', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190912000404100016"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190912000404100016"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="2f4298a4066e72f9b3251ddf8d46d90c"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '王者卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '53432453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217843, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(9, 10001, 1, 'T190912000440100031', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190912000440100031"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190912000440100031"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="4f911bf4130965be57692c0eb883c290"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '42544532', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217880, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(10, 10001, 1, 'T190912000445100057', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190912000445100057"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190912000445100057"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="8cb03a1de2082fab1f3bd0ddc6a42f11"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '42544532', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217885, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(11, 10001, 1, 'T190912000453100098', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190912000453100098"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190912000453100098"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="8d91a253fe4178597b01034115864105"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '42544532', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568217893, '60.180.237.51', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(12, 10001, 1, 'T190913144108100092', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913144108100092"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913144108100092"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="78662b266b87ede68efd47e6722ddf66"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '3.43.4.45', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568356868, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(13, 10001, 1, 'T190913144127100065', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913144127100065"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913144127100065"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="74935d10cfed3beabe8e57279352aafc"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '3.43.4.45', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568356887, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(14, 10001, 1, 'T190913151130100034', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913151130100034"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913151130100034"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="aec65ec98667944fe20dcc34871e8111"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '45345345', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568358690, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(15, 10001, 1, 'T190913151258100075', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913151258100075"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913151258100075"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="ea4b3fbfb1aa53f264fa3436ad1002f2"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '45345345', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568358778, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(16, 10001, 1, 'T190913151309100050', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913151309100050"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913151309100050"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="aeeb748a545e04425d430a41375d1631"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '45345345', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568358789, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(17, 10001, 1, 'T190913151325100085', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913151325100085"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913151325100085"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="2297c5277dc7368313d3dc8c09e77094"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '412442342', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568358805, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(18, 10001, 1, 'T190913151331100085', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913151331100085"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913151331100085"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="e9b6b847d8279b89e4427de4abe4634f"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '412442342', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568358810, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(19, 10001, 1, 'T190913151944100032', '', 129, 176, 2, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="1341479"><input type="hidden" name="type" value="alipay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayAli"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayAli"><input type="hidden" name="out_trade_no" value="T190913151944100032"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913151944100032"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="412699a4fd4dd6f1f1132daca0ff9b9d"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '456343453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 0, 0, 1568359184, '60.180.234.125', 0, 0, 0, '0.10', 0, 1, 1, '0.0000'),
(22, 10001, 1, 'T190913152320100086', '', 130, 177, 4, '<form id=''pay_form'' class="form-inline" method="post" action="http://www.8iu.cn/submit.php"><input type="hidden" name="pid" value="10001"><input type="hidden" name="type" value="wxpay"><input type="hidden" name="notify_url" value="http://cee.8iu.cn/pay/notify/EpayWx"><input type="hidden" name="return_url" value="http://cee.8iu.cn/pay/page/EpayWx"><input type="hidden" name="out_trade_no" value="T190913152320100086"><input type="hidden" name="name" value="投诉QQ：12344321 订单：T190913152320100086"><input type="hidden" name="money" value="0.10"><input type="hidden" name="sign" value="392b27fd286cb68552b8fd25cd391dc0"><input type="hidden" name="sign_type" value="MD5"></form><script>document.forms[''pay_form''].submit();</script>', 3, '绝地求生卡密', '0.10', '0.00', 1, 0, 0, '0.00', '0.10', '0.00', 0, 0, '', '456343453', 0, '', 0, '0.0000', '0.010', '0.0000', '0.000', '0.00', 1, 0, 1568359400, '60.180.234.125', 1568359416, 1, 0, '0.10', 1, 1, 1, '0.0900');

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_card`
--

CREATE TABLE IF NOT EXISTS `shop_order_card` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `number` varchar(500) NOT NULL DEFAULT '',
  `secret` varchar(500) NOT NULL DEFAULT '',
  `card_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_order_card`
--

INSERT INTO `shop_order_card` (`id`, `order_id`, `number`, `secret`, `card_id`) VALUES
(1, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 1),
(2, 22, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\r', 2);

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_dispute`
--

CREATE TABLE IF NOT EXISTS `shop_order_dispute` (
  `id` int(11) NOT NULL,
  `sj` datetime NOT NULL COMMENT '时间',
  `uip` varchar(100) NOT NULL COMMENT 'uip',
  `bh` varchar(30) NOT NULL COMMENT '编号',
  `ddbh` varchar(30) NOT NULL COMMENT '订单编号',
  `ubh` varchar(30) NOT NULL COMMENT '用户编号',
  `role` varchar(20) NOT NULL COMMENT '角色',
  `txt` text NOT NULL COMMENT '内容',
  `backmoney` decimal(10,2) NOT NULL COMMENT '部分退款'
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='订单纠纷';

--
-- 转存表中的数据 `shop_order_dispute`
--

INSERT INTO `shop_order_dispute` (`id`, `sj`, `uip`, `bh`, `ddbh`, `ubh`, `role`, `txt`, `backmoney`) VALUES
(1, '2020-12-17 16:10:03', '127.0.0.1', '1608192603', '160818949077', 'u1569891600', 'buy', '测试退款补充测试退款补充测试退款补充', '0.00'),
(2, '2020-12-17 16:15:50', '127.0.0.1', '1608192950', '160818949077', 'u1569891600', 'buy', '测试123123123测试123123123测试123123123', '0.00'),
(3, '2020-12-17 16:16:10', '127.0.0.1', '1608192970', '160818949077', 'u1569891600', 'buy', '测试123123123测试123123123测试123123123', '0.00'),
(4, '2020-12-17 16:19:00', '127.0.0.1', '1608193140', '160818949077', 'u1569891600', 'buy', '测试部分退款101测试部分退款101', '0.00'),
(5, '2020-12-17 16:38:52', '127.0.0.1', '1608194332', '160818949077', 'u1569891600', 'buy', '部分退款1213123部分退款1213123', '0.00'),
(6, '2020-12-17 16:40:45', '127.0.0.1', '1608194445', '160818949077', 'u1569891600', 'buy', '部分退款部分退款部分退款部分退款', '0.00'),
(7, '2020-12-17 16:42:42', '127.0.0.1', '1608194562', '160818949077', 'u1569891600', 'buy', '222222222222222222222222', '0.00'),
(8, '2020-12-17 16:48:51', '127.0.0.1', '1608194931', '160818949077', 'u1569891600', 'buy', '测试退款测试退款测试退款测试退款测试退款测试退款', '0.00'),
(9, '2020-12-17 16:50:08', '127.0.0.1', '1608195008', '160818949077', 'u1569891600', 'buy', '测试退款测试退款测试退款测试退款测试退款', '0.00'),
(10, '2020-12-17 17:23:20', '127.0.0.1', '1608197000', '160818949077', 'u1573740831', 'sell', '测试12312312测试12312312测试12312312测试12312312', '0.00'),
(11, '2020-12-17 17:23:55', '127.0.0.1', '1608197035', '160818949077', 'u1573740831', 'sell', '测试1231312312测试1231312312测试1231312312', '0.00'),
(12, '2020-12-20 21:10:21', '127.0.0.1', '1608469821', '160846861645', '', 'message', '申请平台人工客服介入该笔交易纠纷。', '0.00'),
(13, '2020-12-20 21:14:18', '127.0.0.1', '1608470058', '160846861645', 'u1569891600', 'message', '申请码发布人工客服介入该笔交易纠纷。', '0.00'),
(14, '2020-12-20 21:20:22', '127.0.0.1', '1608470422', '160846861645', 'u1569891600', 'message', '申请码发布人工客服介入该笔交易纠纷。', '0.00'),
(15, '2020-12-20 22:16:31', '127.0.0.1', '1608473791', '160847048997', 'u1569891600', 'buy', '测试退款新版测试退款新版', '0.00'),
(16, '2020-12-20 22:18:02', '127.0.0.1', '1608473882', '160847048997', 'u1569891600', 'buy', '测试退款123123测试退款123123测试退款123123', '0.00'),
(17, '2020-12-20 22:21:40', '127.0.0.1', '1608474100', '160847399634', 'u1569891600', 'buy', '测试退款测试退款测试退款测试退款', '0.00'),
(18, '2020-12-20 22:25:03', '127.0.0.1', '1608474303', '160847427599', 'u1569891600', 'buy', '测试退款测试退款测试退款测试退款', '0.00'),
(19, '2020-12-20 22:26:31', '127.0.0.1', '1608474391', '160847432731', 'u1569891600', 'buy', '【质量缺陷】测试退款12312312测试退款12312312测试退款12312312', '0.00'),
(20, '2020-12-20 22:30:41', '127.0.0.1', '1608474641', '160847432731', 'u1569891600', 'buy', '测试退款理由22221213测试退款理由22221213测试退款理由22221213', '0.00'),
(21, '2020-12-20 22:46:55', '127.0.0.1', '1608475615', '160847558214', 'u1569891600', 'buy', '【网盘失效】我要申请退款12313我要申请退款12313', '0.00'),
(22, '2020-12-20 22:47:08', '127.0.0.1', '1608475628', '160847558214', 'u1569891600', 'buy', '申请退款补充123123', '0.00'),
(23, '2020-12-20 22:52:47', '127.0.0.1', '1608475967', '160847558214', 'u1569916994', 'sell', '测试一下 拒绝退款123', '0.00'),
(24, '2020-12-20 22:53:12', '127.0.0.1', '1608475992', '160847558214', 'u1569916994', 'sell', '测试123123123123', '0.00'),
(25, '2020-12-20 22:54:13', '127.0.0.1', '1608476053', '160847558214', 'u1569891600', 'message', '申请码发布人工客服介入该笔交易纠纷。', '0.00'),
(26, '2020-12-20 23:23:11', '127.0.0.1', '1608477791', '160847558214', 'u1569891600', 'buy', '测试12312313测试12312313测试12312313', '0.00'),
(27, '2020-12-20 23:23:57', '127.0.0.1', '1608477837', '160847558214', 'u1569891600', 'buy', '测试测试测试测试测试测试', '0.00'),
(28, '2020-12-20 23:25:25', '127.0.0.1', '1608477925', '160847558214', 'u1569891600', 'buy', '测试退款理由测试退款理由测试退款理由', '0.00'),
(29, '2020-12-20 23:26:21', '127.0.0.1', '1608477981', '160847558214', 'u1569916994', 'sell', '有点意思哈哈哈有点意思哈哈哈有点意思哈哈哈', '0.00'),
(30, '2020-12-20 23:37:06', '127.0.0.1', '1608478626', '160847558214', 'u1569891600', 'buy', '测试123123123测试123123123测试123123123', '0.00'),
(31, '2020-12-20 23:37:45', '127.0.0.1', '1608478665', '160847558214', 'u1569891600', 'buy', '测试21312321322222', '0.00'),
(32, '2020-12-20 23:45:05', '127.0.0.1', '1608479105', '160847558214', 'u1569891600', 'buy', '测试23123123测试23123123测试23123123', '0.00'),
(33, '2020-12-21 10:17:58', '127.0.0.1', '1608517078', '160851599163', 'u1569891600', 'buy', '【货不对版】若 对方 在【2天12小时32分6秒】内未申诉或申诉理由不成立，交易自动完成，款项将转入 您 的账户。若 对方 在【2天12小时32分6秒】内未申诉或申诉理由不成立，交易自动完成，款项将转入 您 的账户。', '0.00'),
(34, '2020-12-21 10:18:11', '127.0.0.1', '1608517091', '160851599163', 'u1569891600', 'message', '申请码发布人工客服介入该笔交易纠纷。', '0.00'),
(35, '2020-12-21 10:19:00', '127.0.0.1', '1608517140', '160851599163', 'u1569891600', 'message', '申请码发布人工客服介入该笔交易纠纷。', '0.00'),
(36, '2020-12-21 10:25:40', '127.0.0.1', '1608517540', '160851599163', 'u1569891600', 'buy', '测试123123123', '0.00'),
(37, '2020-12-21 10:30:42', '127.0.0.1', '1608517842', '160851599163', 'u1569916994', 'sell', '测试测试测试测试测试测试测试测试测试', '0.00'),
(38, '2020-12-21 10:30:58', '127.0.0.1', '1608517858', '160851599163', 'u1569916994', 'sell', '测试测试测试测试测试测试测试测试测试', '0.00'),
(39, '2020-12-21 10:31:13', '127.0.0.1', '1608517873', '160851599163', 'u1569916994', 'sell', '测试测试测试测试测试测试测试测试测试', '0.00'),
(40, '2020-12-31 20:41:01', '127.0.0.1', '1609418461', '160941211996', 'u1569916994', 'buy', '【货不对版】买错买错买错买错买错买错买错', '10.00'),
(41, '2021-01-02 16:47:23', '127.0.0.1', '1609577243', '160941211996', 'u1569916994', 'buy', '测试一下测试一下测试一下测试一下', '10.00'),
(42, '2021-01-02 16:48:03', '127.0.0.1', '1609577283', '160941211996', 'u1569916994', 'buy', '测试一下3测试一下3测试一下3测试一下3', '10.00'),
(43, '2021-01-02 16:48:46', '127.0.0.1', '1609577326', '160941211996', 'u1569916994', 'buy', '测试测试测试测试测试', '15.00'),
(44, '2021-01-02 19:56:31', '127.0.0.1', '1609588591', '160941211996', 'u1569891600', 'sell', '测试拒绝退款测试拒绝退款测试拒绝退款', '0.00'),
(45, '2021-01-02 20:38:07', '127.0.0.1', '1609591087', '160941211996', 'u1569916994', 'message', '申请码发布人工客服介入该笔交易纠纷。', '0.00'),
(46, '2021-01-02 21:06:47', '127.0.0.1', '1609592807', '160958805453', 'u1569891600', 'buy', '【货不对版】测试退款测试退款测试退款测试退款测试退款', '0.00');

-- --------------------------------------------------------

--
-- 表的结构 `shop_pay_type`
--

CREATE TABLE IF NOT EXISTS `shop_pay_type` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(100) NOT NULL COMMENT '支付名',
  `product_id` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付类型',
  `logo` varchar(100) NOT NULL DEFAULT '' COMMENT 'logo',
  `ico` varchar(100) NOT NULL DEFAULT '' COMMENT '图标',
  `is_mobile` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否手机支付',
  `is_form_data` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否 form 提交',
  `target` tinyint(3) NOT NULL DEFAULT '0' COMMENT '银行支付使用，未知作用',
  `sub_lists` text COMMENT '银行支付使用，指定支持的银行列表'
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COMMENT='支付类型表';

--
-- 转存表中的数据 `shop_pay_type`
--

INSERT INTO `shop_pay_type` (`id`, `name`, `product_id`, `logo`, `ico`, `is_mobile`, `is_form_data`, `target`, `sub_lists`) VALUES
(1, '微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(2, '微信H5', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(3, '支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(4, '支付宝H5', 1, 'icon_zfb', 'icon_zfb', 1, 0, 0, ''),
(5, '网银跳转', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(6, '网银直连', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(7, '百度钱包', 5, 'baidu_logo', 'baidu_ico', 0, 0, 0, ''),
(8, 'QQ钱包扫码', 3, 'qqrcode', 'qqrcode', 0, 0, 0, ''),
(9, '京东钱包', 4, 'jd_logo', 'jd_ico', 0, 0, 0, ''),
(10, 'QQ钱包H5', 3, 'icon_qq', 'icon_qq', 1, 0, 0, ''),
(11, '支付宝PC', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(12, '微信刷卡', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(13, '支付宝刷卡', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(14, '支付宝免签', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(15, '微信免签', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(16, '微极速微信支付', 2, 'icon_wx', 'icon_wx', 1, 1, 0, ''),
(17, '15173PC微信支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(18, '15173Wap微信支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(19, '拉卡微信扫码支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(20, '拉卡微信h5支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(21, '拉卡支付宝扫码支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(22, '快接微信扫码支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(23, '快接微信H5支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(24, '快接支付宝扫码支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(25, '快接支付宝扫码支付(线上二维码)', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(26, '快接支付宝H5支付', 1, 'icon_zfb', 'icon_zfb', 1, 0, 0, ''),
(27, '微信官方H5支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(28, '拉卡QQ支付', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(29, '拉卡银联快捷', 6, 'icon_bank', 'icon_bank', 0, 0, 0, '{"1":{"name":"\\u4e2d\\u56fd\\u5de5\\u5546\\u94f6\\u884c","logo":"icon_bank","ico":"icon_bank","is_mobile":0,"target":0,"is_form_data":0,"code":10001},"2":{"name":"\\u4e2d\\u56fd\\u519c\\u4e1a\\u94f6\\u884c","logo":"icon_zgnyyh","ico":"icon_zgnyyh","is_mobile":0,"target":0,"is_form_data":0,"code":10002}}'),
(30, '12kaQQ钱包扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(31, '12kaQQWap', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(32, '12ka网银快捷', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(33, '12ka网银Wap', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(34, '12ka支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(35, '12ka支付宝wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(36, '12ka微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(37, '12ka微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(38, '码支付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(39, '15173Qq扫码支付', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(40, '码支付QQ扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(41, '码支付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(42, '蜂鸟支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(43, '蜂鸟支付宝WAP', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(44, '蜂鸟微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(45, '蜂鸟微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(46, '蜂鸟QQ钱包扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(47, '掌灵付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(48, '掌灵付QQ扫码', 3, 'qqrcode', 'qqrcode', 0, 1, 0, ''),
(49, '掌灵付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(50, '掌灵付微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(51, '黔贵金服支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(52, '黔贵金服微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(53, '黔贵金服QQ扫码', 3, 'qqrcode', 'qqrcode', 0, 1, 0, ''),
(54, '黔贵金服微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(55, '黔贵金服支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(56, '黔贵金服微信WAP', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(57, '点缀支付宝即时到账', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(58, '点缀支付京东钱包扫码', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(59, '掌灵付微信H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(60, '掌灵付京东H5', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(61, '点缀微信 H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(62, '优畅上海微信 H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(63, '优畅上海微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(64, '优畅上海微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(65, 'Topay微信', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(66, 'Topay 支付宝', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(67, '海鸟微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(68, '海鸟微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(69, '海鸟微信 H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(70, '海鸟 QQ 扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(71, '海鸟支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(72, '淘米支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(73, '淘米微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(74, '淘米QQ扫码', 3, 'qqrcode', 'qqrcode', 0, 1, 0, ''),
(75, '淘米微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(76, '淘米支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(77, '淘米微信WAP', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(78, '完美数卡微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(79, '完美数卡支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(80, '完美数卡QQ扫码', 3, 'icon_qq', 'icon_qq', 0, 1, 0, ''),
(81, '完美数卡QQWap', 3, 'icon_qq', 'icon_qq', 0, 1, 0, ''),
(82, '完美数卡微信Wap', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(83, '完美数卡支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(84, '支付宝免签', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(85, 'qq免签', 3, 'icon_qq', 'icon_qq', 0, 1, 0, ''),
(86, '微信免签', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(87, '掌灵付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(88, '牛支付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(89, '掌灵付京东扫码', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(90, '优畅上海支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(91, '优畅上海支付宝 Wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(92, '威富通支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(93, '威富通支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(94, '威富通微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(95, '威富通京东扫码', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(96, '威富通微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(97, '威富通微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(98, '汉口银行微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(99, '汉口银行微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(100, '汉口银行微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(101, '汉口银行支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(102, '汉口银行支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(103, '优畅上海QQ扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(104, '优畅上海QQWap', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(105, '平安付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(106, '平安付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(107, '平安付支付宝wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(108, '平安付微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(109, '网商银行微信', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(110, '网商银行支付宝', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(111, '吉易支付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(112, '吉易支付微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(113, 'PayApi支付宝', 2, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(114, 'PayApi微信', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(115, '易云支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(116, '易云支付宝WAP', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(117, '易云微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(118, '易云微信WAP', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(119, '易云微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(120, '恒隆支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(121, '恒隆支付宝WAP', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(122, '恒隆微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(123, '恒隆微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(124, '深度支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(125, '深度支付宝服务窗支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(126, '深度微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(127, '深度微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(128, '聚合支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(129, '彩虹易支付支付宝', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(130, '彩虹易支付微信', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(131, '彩虹易支付QQ钱包', 3, 'icon_qq', 'icon_qq', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `shop_plugin`
--

CREATE TABLE IF NOT EXISTS `shop_plugin` (
  `id` int(11) NOT NULL,
  `sj` datetime NOT NULL COMMENT '时间',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `show_name` varchar(100) NOT NULL COMMENT '备注标记',
  `code` varchar(100) NOT NULL COMMENT '插件代码',
  `is_available` int(11) NOT NULL COMMENT '插件可用',
  `is_install` int(11) NOT NULL COMMENT '判断是否安装',
  `applyurl` varchar(200) NOT NULL COMMENT '申请地址',
  `account_fields` text NOT NULL COMMENT '账户字段',
  `type` varchar(100) NOT NULL COMMENT '类型',
  `author` varchar(30) NOT NULL COMMENT '作者',
  `status` int(11) NOT NULL COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='插件列表';

--
-- 转存表中的数据 `shop_plugin`
--

INSERT INTO `shop_plugin` (`id`, `sj`, `name`, `show_name`, `code`, `is_available`, `is_install`, `applyurl`, `account_fields`, `type`, `author`, `status`) VALUES
(1, '2020-12-21 06:17:17', '免签约易支付', '第三方免签约支付', 'epay', 0, 1, 'http://pay.mafabu.com/', '接口地址:apiurl|用户ID:partner|私有秘钥:key', 'pay', '官方', 1),
(2, '2020-12-21 06:17:17', '支付宝电脑支付', '官方支付宝通道', 'alipaypc', 0, 1, '', '应用ID:app_id|商户私钥:merchant_private_key|支付宝公钥:alipay_public_key', 'pay', '官方', 1),
(3, '2020-12-21 06:17:17', 'QQ快捷登录', '官方QQ快捷登录', 'qqoauth', 0, 1, 'https://connect.qq.com/', '应用ID:appid|商户私钥:appkey|回调域名:domain|回调地址:callback', 'oauth', '官方', 1),
(4, '2020-12-21 06:17:17', '支付宝当面付', '官方支付宝当面付', 'alipaydmf', 0, 1, '', '应用ID:appid|商户私钥:private_key|支付宝公钥:alipay_public_key', 'pay', '官方', 1),
(5, '2020-12-21 06:17:17', '微信扫码支付', '官方微信扫码付支付', 'wechatpay', 0, 1, '', '公众号ID:GetAppId|商户ID:GetMerchantId|商户私钥:GetKey|应用秘钥:GetAppSecret', 'pay', '官方', 0),
(6, '2020-12-21 06:17:17', 'QQ钱包扫码支付', '官方QQ钱包扫码支付', 'qpay', 0, 1, '', '应用ID:appid|应用公钥:appkey', 'pay', '官方', 0);

-- --------------------------------------------------------

--
-- 表的结构 `shop_plugin_account`
--

CREATE TABLE IF NOT EXISTS `shop_plugin_account` (
  `id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL COMMENT '插件id',
  `name` varchar(100) NOT NULL COMMENT '用户名',
  `code` varchar(100) NOT NULL COMMENT '代码',
  `params` text NOT NULL COMMENT '参数',
  `rate_type` int(11) NOT NULL COMMENT '费率设置 0 继承接口  1单独设置',
  `lowrate` varchar(11) NOT NULL COMMENT '充值费率',
  `status` int(11) NOT NULL COMMENT '状态 1启用 0禁用'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='插件账号';

-- --------------------------------------------------------

--
-- 表的结构 `shop_product`
--

CREATE TABLE IF NOT EXISTS `shop_product` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '通道名称',
  `code` varchar(50) NOT NULL COMMENT '通道代码',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1 开启 0 关闭',
  `polling` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '接口模式 0单独 1轮询',
  `channel_id` int(10) unsigned NOT NULL,
  `weight` text NOT NULL COMMENT '权重',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '支付类型 1 微信扫码 2 微信公众号 3 支付宝扫码 4 支付宝手机 5 网银支付 6'
) ENGINE=InnoDB AUTO_INCREMENT=908 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_product`
--

INSERT INTO `shop_product` (`id`, `title`, `code`, `status`, `polling`, `channel_id`, `weight`, `paytype`) VALUES
(901, '微信公众号', 'WXJSAPI', 0, 0, 0, '[]', 2),
(902, '微信扫码支付', 'WXSCAN', 0, 0, 0, '[]', 1),
(903, '支付宝扫码支付', 'ALISCAN', 0, 0, 14, '[]', 3),
(904, '支付宝手机', 'ALIWAP', 0, 0, 15, '[]', 4),
(907, '网银支付', 'DBANK', 0, 0, 0, '[]', 5);

-- --------------------------------------------------------

--
-- 表的结构 `shop_publicity`
--

CREATE TABLE IF NOT EXISTS `shop_publicity` (
  `id` int(10) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(100) NOT NULL,
  `bh` varchar(30) NOT NULL,
  `tit` varchar(100) NOT NULL,
  `txt` text NOT NULL,
  `stopinfo` varchar(200) NOT NULL,
  `filter` varchar(100) NOT NULL COMMENT '类型'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_rate_group`
--

CREATE TABLE IF NOT EXISTS `shop_rate_group` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(20) NOT NULL COMMENT '分组名',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `delete_at` int(10) unsigned DEFAULT NULL COMMENT '删除时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='费率分组表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_rate_group_rule`
--

CREATE TABLE IF NOT EXISTS `shop_rate_group_rule` (
  `id` int(11) unsigned NOT NULL,
  `group_id` int(11) NOT NULL COMMENT '分组 ID',
  `channel_id` int(11) NOT NULL COMMENT '渠道 ID',
  `rate` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '渠道费率',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1：开启 0：关闭'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分组费率规则';

-- --------------------------------------------------------

--
-- 表的结构 `shop_rate_group_user`
--

CREATE TABLE IF NOT EXISTS `shop_rate_group_user` (
  `id` int(11) unsigned NOT NULL,
  `group_id` int(11) NOT NULL COMMENT '分组 ID',
  `user_id` int(11) NOT NULL COMMENT '用户 ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分组内用户表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_reviews`
--

CREATE TABLE IF NOT EXISTS `shop_reviews` (
  `id` int(10) NOT NULL,
  `ddbh` char(20) DEFAULT NULL,
  `pro` char(20) DEFAULT NULL,
  `type` char(10) DEFAULT NULL,
  `sell` char(20) DEFAULT NULL,
  `buy` char(20) DEFAULT NULL,
  `sellcom` varchar(250) DEFAULT NULL,
  `buycom` varchar(250) DEFAULT NULL,
  `sellimp` char(50) DEFAULT NULL,
  `buyimp` char(50) DEFAULT NULL,
  `Attitude` int(1) DEFAULT NULL,
  `efficiency` int(1) DEFAULT NULL,
  `quality` int(1) DEFAULT NULL,
  `buytisp` int(1) DEFAULT '0',
  `buytime` datetime DEFAULT NULL,
  `selltisp` int(1) DEFAULT '0',
  `selltime` datetime DEFAULT NULL,
  `zjcom` varchar(250) DEFAULT NULL,
  `zjtime` datetime DEFAULT NULL,
  `hfcom` varchar(600) DEFAULT NULL,
  `hftime` datetime NOT NULL COMMENT '回复时间',
  `zt` int(1) DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=12018 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_reviews`
--

INSERT INTO `shop_reviews` (`id`, `ddbh`, `pro`, `type`, `sell`, `buy`, `sellcom`, `buycom`, `sellimp`, `buyimp`, `Attitude`, `efficiency`, `quality`, `buytisp`, `buytime`, `selltisp`, `selltime`, `zjcom`, `zjtime`, `hfcom`, `hftime`, `zt`) VALUES
(11717, '145939460515', '145939406013', 'code', 'u1459359290', 'u1459394473', '谢谢买家的支持！！', '卖家很好！服务不错', '2', '222', 5, 5, 5, 0, '2016-03-31 17:24:47', 0, '2016-03-31 17:41:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11718, '145944039122', '145941875387', 'code', 'u1459359290', 'u1459425550', '感谢对我们公司的支持！！', '不错人脉管家！！新版', '2', '2', 5, 5, 5, 0, '2016-04-01 00:10:40', 0, '2016-04-01 00:11:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11719, '145955833258', '145941741411', 'code', 'u1459359290', 'u1459425550', '好评！', '好评！', '2', '2', 5, 5, 5, 1, '2016-04-02 09:03:14', 0, '2016-04-02 10:06:27', NULL, NULL, '<buy>2016-04-02 10:06:44-xx-谢谢！！！！！！！！！！！！！！</buy>', '0000-00-00 00:00:00', 1),
(11720, '145956195223', '145941875387', 'code', 'u1459359290', 'u1459394473', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-02 09:57:40', 0, '2016-04-02 10:06:08', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11721, '145955908918', '145944339262', 'code', 'u1459359290', 'u1459425550', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-07 22:47:45', 0, '2016-04-02 10:06:20', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11722, '145957750246', '145944363898', 'code', 'u1459359290', 'u1459394473', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-02 15:09:28', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11723, '145955908918', '145944339262', 'code', 'u1459359290', 'u1459425550', NULL, '好评！', '2', '2', 5, 5, 5, 0, '2016-04-07 22:47:45', 0, '2016-04-05 09:46:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11724, '145957750246', '145944363898', 'code', 'u1459359290', 'u1459394473', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-05 14:38:38', 0, '2016-04-05 14:38:38', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11725, '146004000129', '145941741411', 'code', 'u1459359290', 'u1459425550', '很好！！！！！！！！！！！！！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-07 22:47:24', 0, '2016-04-07 23:23:55', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11726, '145958013535', '145944712945', 'code', 'u1459359290', 'u1459425550', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-07 22:47:35', 0, '2016-04-16 15:19:35', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11727, '146010363256', '145944363898', 'code', 'u1459359290', 'u1460101467', '谢谢', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-09 11:55:04', 0, '2016-04-09 12:05:22', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11728, '146004683276', '145941834382', 'code', 'u1459359290', 'u1459425550', '不错！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-09 13:03:49', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11729, '146004661760', '145944317643', 'code', 'u1459359290', 'u1459425550', '谢谢', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-09 13:04:08', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11730, '146021998519', '145941741411', 'code', 'u1459359290', 'u1460181025', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-10 00:48:30', 0, '2016-04-16 15:18:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11731, '145958013535', '145944712945', 'code', 'u1459359290', 'u1459425550', '好评！', NULL, '2', '2', 5, 5, 5, 0, '2016-04-10 22:41:06', 0, '2016-04-16 15:19:35', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11732, '146004661760', '145944317643', 'code', 'u1459359290', 'u1459425550', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-11 00:35:29', 0, '2016-04-11 00:35:29', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11733, '146004683276', '145941834382', 'code', 'u1459359290', 'u1459425550', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-11 00:35:52', 0, '2016-04-11 00:35:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11734, '145948805367', '145941686758', 'code', 'u1459359290', 'u1459487854', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-11 13:21:00', 0, '2016-04-11 13:21:00', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11735, '146021998519', '145941741411', 'code', 'u1459359290', 'u1460181025', '好评！', NULL, '2', '2', 5, 5, 5, 0, '2016-04-13 00:48:17', 0, '2016-04-16 15:18:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11736, '146054931842', '145944712945', 'code', 'u1459359290', 'u1460181025', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-16 15:15:39', 0, '2016-04-16 15:18:24', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11737, '146051976173', '146021316424', 'code', 'u1459359290', 'u1460181025', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-16 15:16:56', 0, '2016-04-16 15:18:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11738, '146021969345', '146021316424', 'code', 'u1459359290', 'u1460181025', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-16 15:17:15', 0, '2016-04-16 15:18:48', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11739, '146009232163', '145944712945', 'code', 'u1459359290', 'u1459394473', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-16 15:18:55', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11740, '146004490881', '145944427545', 'code', 'u1459359290', 'u1459394473', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-16 15:19:03', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11741, '146004336660', '145944692117', 'code', 'u1459359290', 'u1459394473', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-16 15:19:11', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11742, '146004323339', '145944692117', 'code', 'u1459359290', 'u1459394473', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-16 15:19:18', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11743, '146000056176', '145944363898', 'code', 'u1459359290', 'u1459394473', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-16 15:19:27', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11744, '146004323339', '145944692117', 'code', 'u1459359290', 'u1459394473', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-16 17:28:06', 0, '2016-04-16 17:28:06', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11745, '146000056176', '145944363898', 'code', 'u1459359290', 'u1459394473', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-17 11:42:45', 0, '2016-04-17 11:42:45', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11746, '146079354641', '145941875387', 'code', 'u1459359290', 'u1460793477', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-04-17 16:11:00', 0, '2016-04-24 21:15:04', '好评!!!!!!!!!!!!!!!!!!!!!!!!', '2016-04-17 16:11:15', NULL, '0000-00-00 00:00:00', 1),
(11747, '146004336660', '145944692117', 'code', 'u1459359290', 'u1459394473', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-17 23:37:39', 0, '2016-04-17 23:37:39', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11748, '146004490881', '145944427545', 'code', 'u1459359290', 'u1459394473', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-18 00:01:52', 0, '2016-04-18 00:01:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11749, '146009232163', '145944712945', 'code', 'u1459359290', 'u1459394473', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-18 13:12:04', 0, '2016-04-18 13:12:04', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11750, '146079354641', '145941875387', 'code', 'u1459359290', 'u1460793477', '好评！', NULL, '2', '2', 5, 5, 5, 0, '2016-04-20 16:10:33', 0, '2016-04-24 21:15:04', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11751, '146079799687', '145944363898', 'code', 'u1459359290', 'u1460797970', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-20 17:13:38', 0, '2016-04-20 17:13:38', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11752, '146079784961', '145941686758', 'code', 'u1459359290', 'u1460793977', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-20 17:16:00', 0, '2016-04-20 17:16:00', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11753, '146080020243', '145944427545', 'code', 'u1459359290', 'u1460181025', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-20 17:51:57', 0, '2016-04-20 17:51:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11754, '146080676059', '145944427545', 'code', 'u1459359290', 'u1460220656', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-20 19:39:44', 0, '2016-04-20 19:39:44', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11755, '146081448810', '145941686758', 'code', 'u1459359290', 'u1460814231', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-20 21:48:40', 0, '2016-04-20 21:48:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11756, '146044632197', '145941686758', 'code', 'u1459359290', 'u1460445408', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-21 15:22:48', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11757, '146039016330', '145941741411', 'code', 'u1459359290', 'u1460220656', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-21 23:56:14', 0, '2016-04-21 23:56:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11758, '146044632197', '145941686758', 'code', 'u1459359290', 'u1460445408', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-22 15:32:35', 0, '2016-04-22 15:32:35', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11759, '146129189358', '146128992090', 'code', 'u1459359290', 'u1461290319', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-24 21:14:43', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11760, '146129166436', '146129036725', 'code', 'u1459359290', 'u1461290319', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-24 21:14:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11761, '146129166436', '146129036725', 'code', 'u1459359290', 'u1461290319', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-25 10:22:14', 0, '2016-04-25 10:22:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11762, '146129189358', '146128992090', 'code', 'u1459359290', 'u1461290319', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-04-25 10:25:13', 0, '2016-04-25 10:25:13', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11763, '146177571437', '146158073518', 'code', 'u1459359290', 'u1461773782', '感谢你对我们公司的支持！谢谢', '好评！', '2', '2', 5, 5, 5, 0, '2016-05-02 11:11:12', 0, '2016-04-29 10:04:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11764, '146177400997', '146121301865', 'code', 'u1459359290', 'u1461773782', '感谢你对我们公司的支持！谢谢', '好评！', '2', '2', 5, 5, 5, 0, '2016-05-02 11:11:04', 0, '2016-04-29 10:04:39', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11765, '146177298440', '146121301865', 'code', 'u1459359290', 'u1461772131', '感谢你对我们公司的支持！谢谢', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-29 10:04:48', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11766, '146176010045', '145985434769', 'code', 'u1459359290', 'u1461759934', '感谢你对我们公司的支持！谢谢', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-29 10:04:54', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11767, '146175774534', '146138867081', 'code', 'u1459359290', 'u1461756192', '感谢你对我们公司的支持！谢谢', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-04-29 10:05:00', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11768, '146181657277', '146121301865', 'code', 'u1459359290', 'u1461816301', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-05-02 11:28:00', 0, '2016-04-29 16:16:59', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11769, '146175774534', '146138867081', 'code', 'u1459359290', 'u1461756192', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-01 20:16:46', 0, '2016-05-01 20:16:46', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11770, '146176010045', '145985434769', 'code', 'u1459359290', 'u1461759934', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-01 20:28:39', 0, '2016-05-01 20:28:39', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11771, '146177298440', '146121301865', 'code', 'u1459359290', 'u1461772131', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-02 00:03:24', 0, '2016-05-02 00:03:24', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11772, '146177400997', '146121301865', 'code', 'u1459359290', 'u1461773782', NULL, '好评！', '2', '2', 5, 5, 5, 0, '2016-05-02 11:11:04', 0, '2016-05-02 00:20:34', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11773, '146177571437', '146158073518', 'code', 'u1459359290', 'u1461773782', NULL, '好评！', '2', '2', 5, 5, 5, 0, '2016-05-02 11:11:12', 0, '2016-05-02 00:48:39', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11774, '146201529389', '146181585995', 'code', 'u1459359290', 'u1462015223', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-05-02 11:08:08', 0, '2016-05-02 11:09:29', '24小时没有处理，自动默认好评！', '2016-05-02 11:08:43', NULL, '0000-00-00 00:00:00', 1),
(11775, '146206980228', '146177584658', 'code', 'u1459359290', 'u1462069477', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-05-02 11:09:23', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11776, '146191694776', '146181276862', 'code', 'u1459359290', 'u1461916901', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-05-02 11:09:35', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11777, '146215771599', '145944363898', 'code', 'u1459359290', 'u1462157412', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-05-03 15:26:38', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11778, '146209715018', '146177584658', 'code', 'u1459359290', 'u1461816301', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-05-03 15:26:44', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11779, '146191694776', '146181276862', 'code', 'u1459359290', 'u1461916901', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-03 16:03:33', 0, '2016-05-03 16:03:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11780, '146206980228', '146177584658', 'code', 'u1459359290', 'u1462069477', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-05 10:30:09', 0, '2016-05-05 10:30:09', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11781, '146209715018', '146177584658', 'code', 'u1459359290', 'u1461816301', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-05 18:06:02', 0, '2016-05-05 18:06:02', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11782, '146215771599', '145944363898', 'code', 'u1459359290', 'u1462157412', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-06 10:56:17', 0, '2016-05-06 10:56:17', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11783, '146225187395', '146225116294', 'code', 'u1459359290', 'u1462251803', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-07 13:05:35', 0, '2016-05-07 13:05:35', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11784, '146226038891', '146138867081', 'code', 'u1459359290', 'u1462259962', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-07 15:26:33', 0, '2016-05-07 15:26:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11785, '146203441045', '146158073518', 'code', 'u1459359290', 'u1462031924', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-08 00:40:25', 0, '2016-05-08 00:40:25', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11786, '146293493258', '146261678999', 'code', 'u1459359290', 'u1462934579', '好评！', '很好很不错，一模一样的！真是赚了', '2', '2', 5, 5, 5, 0, '2016-05-11 10:51:56', 0, '2016-05-11 10:55:22', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11787, '146259963728', '146138867081', 'code', 'u1459359290', 'u1462599399', '好评！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2016-05-11 10:55:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11788, '146259963728', '146138867081', 'code', 'u1459359290', 'u1462599399', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-11 13:42:04', 0, '2016-05-11 13:42:04', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11789, '146303402926', '146138867081', 'code', 'u1459359290', 'u1462950314', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-05-15 07:57:09', 0, '2016-05-13 23:22:11', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11790, '146315196669', '146121301865', 'code', 'u1459359290', 'u1462950314', '好评！', '亲自测试，可以用，老板服务太好了', '2', '2', 5, 5, 5, 0, '2016-05-15 07:56:54', 0, '2016-05-15 07:50:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11791, '146294126841', '146177584658', 'code', 'u1459359290', 'u1462941204', '好评！', '好评！', '2', '2', 5, 5, 5, 0, '2016-05-15 07:58:57', 0, '2016-05-15 07:51:00', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11792, '146320396992', '146297608765', 'code', 'u1459359290', 'u1463203848', NULL, '很便宜，赞！！跟演示一样', NULL, '2', 5, 5, 5, 0, '2016-05-15 07:53:41', 0, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11793, '146320396992', '146297608765', 'code', 'u1459359290', 'u1463203848', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-18 07:52:52', 0, '2016-05-18 07:52:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11794, '146323944692', '145944483744', 'code', 'u1459359290', 'u1463238889', NULL, NULL, '2', '2', 5, 5, 5, 0, '2016-05-18 07:51:52', 0, '2016-05-18 07:51:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11795, '156991712567', '156989787976', 'code', 'u1569891600', 'u1569916994', '999999999999999999999', '不错 哈哈不错 哈哈不错 哈哈', '2', '2', 5, 5, 5, 0, '2019-10-01 17:00:38', 0, '2019-10-01 17:09:11', '不错 哈哈不错 哈哈不错 哈哈', '2019-10-01 17:00:47', NULL, '0000-00-00 00:00:00', 1),
(11796, '1569919870182', '1569919870182', 'task', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-10-12 17:37:43', 0, '2019-10-12 17:37:43', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11797, '157374061779', '157373033385', 'code', 'u1569891600', 'u1569916994', NULL, '', NULL, '2', 5, 5, 5, 0, '2019-11-14 22:11:55', 0, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11798, '157374061779', '157373033385', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-17 22:11:27', 0, '2019-11-17 22:11:27', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11799, '157409215391', '157355409347', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-22 23:59:38', 0, '2019-11-22 23:59:38', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11800, '1574092837017', '157365956126', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-23 00:00:37', 0, '2019-11-23 00:00:37', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11801, '1574092837119', '157373033385', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-23 00:00:37', 0, '2019-11-23 00:00:37', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11802, '157409345366', '157373033385', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-23 00:14:54', 0, '2019-11-23 00:14:54', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11803, '1574106018046', '157373033385', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-23 03:40:18', 0, '2019-11-23 03:40:18', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11804, '1574106018139', '157355739066', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-23 03:40:18', 0, '2019-11-23 03:40:18', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11805, '157435285782', '1539264032-1', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-26 00:15:51', 0, '2019-11-26 00:15:51', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11806, '157450203243', '1539264032-1', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-11-27 17:40:32', 0, '2019-11-27 17:40:32', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11807, '157537560958', '156943109649', 'code', 'u1569891600', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-12-07 20:20:09', 0, '2019-12-07 20:20:09', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11808, '157590391141', '157590377523', 'code', 'u1569891600', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-12-13 23:05:11', 0, '2019-12-13 23:05:11', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11809, '157593585278', '1543998576-1', 'code', 'u1569891600', 'u1575935660', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-12-14 07:57:32', 0, '2019-12-14 07:57:32', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11810, '157607600258', '1560742214-56', 'code', 'u1569916994', 'u1575788484', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-12-20 19:52:28', 0, '2019-12-20 19:52:28', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11811, '157639596351', '157632911161', 'code', 'u1569916994', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-12-19 15:46:03', 0, '2019-12-19 15:46:03', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11812, '157685165073', '157655405849', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-12-24 22:20:50', 0, '2019-12-24 22:20:50', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11813, '157727178983', '1542356648-1', 'code', 'u1569891600', 'u1575788484', NULL, NULL, '2', '2', 5, 5, 5, 0, '2019-12-29 19:03:09', 0, '2019-12-29 19:03:09', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11814, '157789951176', '157788022464', 'code', 'u1569891600', 'u1577899235', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-01-06 01:25:27', 0, '2020-01-06 01:25:27', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11815, '157806163653', '1546077373-1', 'code', 'u1569891600', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-01-07 13:57:55', 0, '2020-01-07 13:57:55', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11816, '157806206089', '157180326962', 'code', 'u1569891600', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-01-07 22:34:20', 0, '2020-01-07 22:34:20', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11817, '157813059396', '1557065224-1', 'code', 'u1569891600', 'u1575907797', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-01-08 17:36:33', 0, '2020-01-08 17:36:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11818, '157821601951', '156839036922', 'code', 'u1569891600', 'u1578211903', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-01-09 17:20:19', 0, '2020-01-09 17:20:19', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11819, '157841760873', '1555833211-538', 'code', 'u1569891600', 'u1577187160', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-01-12 01:20:08', 0, '2020-01-12 01:20:08', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11820, '158018800295', '1554647390-520', 'code', 'u1569891600', 'u1580183773', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-02-01 13:06:42', 0, '2020-02-01 13:06:42', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11821, '158036194723', '156786650075', 'code', 'u1569891600', 'u1580361336', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-02-03 13:25:52', 0, '2020-02-03 13:25:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11822, '158146001661', '1539881342-1', 'code', 'u1569891600', 'u1581396218', NULL, '好评！', NULL, '2', 5, 5, 5, 0, '2020-02-12 06:28:49', 0, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11823, '158146001661', '1539881342-1', 'code', 'u1569891600', 'u1581396218', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-02-15 06:28:35', 0, '2020-02-15 06:28:35', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11824, '158191081724', '1543920029-1', 'code', 'u1569891600', 'u1581910679', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-02-21 11:40:27', 0, '2020-02-21 11:40:27', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11825, '158239186172', '156724687798', 'code', 'u1569891600', 'u1578034632', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-02-26 01:18:50', 0, '2020-02-26 01:18:50', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11826, '158229999159', '1541505661-24', 'code', 'u1569891600', 'u1582299609', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-02-28 00:04:38', 0, '2020-02-28 00:04:38', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11827, '158281572028', '1549776162-1', 'code', 'u1569891600', 'u1582815617', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-01 23:02:28', 0, '2020-03-01 23:02:28', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11828, '158298591177', '157788022464', 'code', 'u1569891600', 'u1582985737', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-04 22:18:41', 0, '2020-03-04 22:18:41', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11829, '158364857517', '1561431899-56', 'code', 'u1569891600', 'u1582562596', '感谢您的购买！！！', NULL, '0', NULL, NULL, NULL, NULL, 0, NULL, 0, '2020-03-11 23:54:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11830, '158364857517', '1561431899-56', 'code', 'u1569891600', 'u1582562596', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-12 14:23:45', 0, '2020-03-12 14:23:45', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11831, '158425046722', '1561894781-380', 'code', 'u1569891600', 'u1584247599', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-19 13:34:31', 0, '2020-03-19 13:34:31', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11832, '158448495598', '1541505661-24', 'code', 'u1569891600', 'u1584484740', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-22 06:43:42', 0, '2020-03-22 06:43:42', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11833, '158452122523', '1560080969-585', 'code', 'u1569891600', 'u1584521182', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-22 16:48:08', 0, '2020-03-22 16:48:08', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11834, '158462546589', '1556668858-257', 'code', 'u1569891600', 'u1584625382', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-22 21:45:36', 0, '2020-03-22 21:45:36', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11835, '158476025516', '1539268842-1', 'code', 'u1569891600', 'u1584760227', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-25 11:11:14', 0, '2020-03-25 11:11:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11836, '158505135938', '157951565752', 'code', 'u1569891600', 'u1585051212', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-28 20:02:47', 0, '2020-03-28 20:02:47', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11837, '158506392294', '158226226727', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-28 23:32:07', 0, '2020-03-28 23:32:07', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11838, '158489563747', '158489388834', 'code', 'u1569891600', 'u1584884030', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-30 00:38:02', 0, '2020-03-30 00:38:02', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11839, '158522503992', '156514791593', 'code', 'u1569891600', 'u1585220563', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-30 20:18:43', 0, '2020-03-30 20:18:43', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11840, '158523283149', '1561015633-585', 'code', 'u1569891600', 'u1585220563', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-30 22:28:24', 0, '2020-03-30 22:28:24', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11841, '158531084994', '156905936433', 'code', 'u1569891600', 'u1585310794', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-03-31 20:07:57', 0, '2020-03-31 20:07:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11842, '158523573667', '158394348091', 'code', 'u1569891600', 'u1585220563', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-03 17:21:14', 0, '2020-04-03 17:21:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11843, '158555023385', '1551021388-1', 'code', 'u1569891600', 'u1585543498', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-03 14:39:59', 0, '2020-04-03 14:39:59', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11844, '158558360389', '158558337059', 'code', 'u1569891600', 'u1585552706', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-03 23:53:30', 0, '2020-04-03 23:53:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11845, '158563745086', '156515750429', 'code', 'u1569891600', 'u1585637297', NULL, NULL, '2', '23', 5, 5, 5, 0, '2020-04-04 14:51:16', 0, '2020-04-04 14:51:16', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11846, '1585672457014', '158567157884', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '22', 5, 5, 5, 0, '2020-04-05 00:34:17', 0, '2020-04-05 00:34:17', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11847, '1585672457154', '156948858083', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-05 00:34:17', 0, '2020-04-05 00:34:17', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11848, '158571873192', '157848981494', 'code', 'u1569891600', 'u1585583285', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-05 13:25:40', 0, '2020-04-05 13:25:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11849, '158625949611', '1555086323-28', 'code', 'u1569891600', 'u1586253962', NULL, '简单说一下，卖家和站长是一个人，源码不是展示页内容，实际不符，有诸多bug，完全是问题源码，与其沟通爱答不理，声称，可帮助搭建，另外收费用，不是差几十块钱，是服务都没有一个好的态度！！', NULL, '2', 1, 1, 1, 0, '2020-04-09 13:19:04', 0, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11850, '1586181440025', '158428789182', 'code', 'u1569891600', 'u1586180297', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-10 21:57:20', 0, '2020-04-10 21:57:20', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11851, '1586181440137', '156839109466', 'code', 'u1569891600', 'u1586180297', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-10 21:57:20', 0, '2020-04-10 21:57:20', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11852, '158624641637', '1546870627-1', 'code', 'u1569891600', 'u1586245535', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-11 16:00:26', 0, '2020-04-11 16:00:26', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11853, '158627659734', '158505488481', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-12 00:23:21', 0, '2020-04-12 00:23:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11854, '158625949611', '1555086323-28', 'code', 'u1569891600', 'u1586253962', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-11 19:45:40', 0, '2020-04-11 19:45:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11855, '158631006196', '158627651493', 'serve', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-12 11:59:51', 0, '2020-04-12 11:59:51', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11856, '158660655473', '156527949258', 'code', 'u1569891600', 'u1578211903', '可以！！', NULL, '0', NULL, NULL, NULL, NULL, 0, NULL, 0, '2020-04-14 01:13:42', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11857, '158660655473', '156527949258', 'code', 'u1569891600', 'u1578211903', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-15 20:02:42', 0, '2020-04-15 20:02:42', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11858, '158727187949', '1561645309125', 'code', 'u1569891600', 'u1587266167', '可以！！！！', NULL, '0', NULL, NULL, NULL, NULL, 0, NULL, 0, '2020-04-21 22:38:56', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11859, '158727187949', '1561645309125', 'code', 'u1569891600', 'u1587266167', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-23 12:51:31', 0, '2020-04-23 12:51:31', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11860, '158752468738', '157951565752', 'code', 'u1569891600', 'u1587524546', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-26 11:06:57', 0, '2020-04-26 11:06:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11861, '158761453411', '157898863659', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-27 12:02:36', 0, '2020-04-27 12:02:36', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11862, '158761469763', '157898863659', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-27 12:05:48', 0, '2020-04-27 12:05:48', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11863, '158779381337', '158558337059', 'code', 'u1569891600', 'u1587793544', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-29 13:52:52', 0, '2020-04-29 13:52:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11864, '158789452497', '156514772756', 'code', 'u1569891600', 'u1587851162', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-04-30 17:49:10', 0, '2020-04-30 17:49:10', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11865, '158798255624', '1556350249-257', 'code', 'u1569891600', 'u1587982451', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-01 18:16:58', 0, '2020-05-01 18:16:58', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11866, '158805860456', '157632442476', 'code', 'u1569916994', 'u1573834598', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-02 15:29:19', 0, '2020-05-02 15:29:19', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11867, '158805912447', '157632442476', 'code', 'u1569916994', 'u1573834598', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-02 15:32:42', 0, '2020-05-02 15:32:42', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11868, '158805926673', '157632442476', 'code', 'u1569916994', 'u1573834598', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-02 15:35:09', 0, '2020-05-02 15:35:09', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11869, '158806042584', '157606651873', 'code', 'u1569916994', 'u1588054566', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-02 15:53:53', 0, '2020-05-02 15:53:53', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11870, '158795484866', '156420233271', 'code', 'u1569891600', 'u1587954742', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-04 16:14:06', 0, '2020-05-04 16:14:06', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11871, '158816521087', '157621097019', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-03 21:00:17', 0, '2020-05-03 21:00:17', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11872, '158817303693', '158411603934', 'code', 'u1569891600', 'u1588171076', NULL, NULL, '2', '222', 5, 5, 5, 0, '2020-05-03 23:11:39', 0, '2020-05-03 23:11:39', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11873, '158822803664', '158399718616', 'code', 'u1569891600', 'u1573740831', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-04 14:27:21', 0, '2020-05-04 14:27:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11874, '158811624399', '158426625295', 'code', 'u1569891600', 'u1573740831', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-06 07:24:13', 0, '2020-05-06 07:24:13', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11875, '158821657622', '157945121394', 'code', 'u1569891600', 'u1587203792', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-07 09:22:11', 0, '2020-05-07 09:22:11', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11876, '158822893492', '157633906268', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-06 14:43:21', 0, '2020-05-06 14:43:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11877, '158842578565', '158055167331', 'code', 'u1569891600', 'u1588425681', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-06 21:23:34', 0, '2020-05-06 21:23:34', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11878, '158893378428', '158892844833', 'code', 'u1569891600', 'u1573740831', '可以111111', '好使，卖家很耐心教我', '0', '2', 5, 5, 5, 0, '2020-05-09 01:02:51', 0, '2020-05-09 17:31:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11879, '158882280658', '157951565752', 'code', 'u1569891600', 'u1588822775', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-11 11:40:25', 0, '2020-05-11 11:40:25', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11880, '158891614821', '157655763279', 'code', 'u1575907797', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-12 13:35:55', 0, '2020-05-12 13:35:55', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11881, '158895720187', '1551021388-1', 'code', 'u1569891600', 'u1573740831', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-13 01:00:15', 0, '2020-05-13 01:00:15', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11882, '158895728462', '158858803872', 'code', 'u1569891600', 'u1573740831', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-13 01:01:30', 0, '2020-05-13 01:01:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11883, '158908834263', '157898844564', 'code', 'u1569891600', 'u1589088305', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-14 13:25:58', 0, '2020-05-14 13:25:58', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11884, '158910579729', '158910547743', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-14 18:16:47', 0, '2020-05-14 18:16:47', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11885, '158911579374', '156406320145', 'code', 'u1569891600', 'u1589115713', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-14 21:03:19', 0, '2020-05-14 21:03:19', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11886, '158917384376', '156765502218', 'code', 'u1569891600', 'u1589173774', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-15 13:11:34', 0, '2020-05-15 13:11:34', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11887, '158936544048', '156845756877', 'code', 'u1569891600', 'u1589233222', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-17 18:24:10', 0, '2020-05-17 18:24:10', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11888, '158951525888', '156943051555', 'code', 'u1569891600', 'u1589173411', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-19 12:03:21', 0, '2020-05-19 12:03:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11892, '159016589824', '1551021388-1', 'code', 'u1569891600', 'u1573834598', NULL, '可以哒源码非常完整！！！', NULL, '2', 5, 5, 5, 0, '2020-05-23 00:56:05', 0, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11893, '158961322296', '158909924558', 'code', 'u1569916994', 'u1589610515', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-19 16:22:34', 1, '2020-05-19 16:22:34', '追加评论！！！！！！！', '2020-05-24 20:35:37', NULL, '0000-00-00 00:00:00', 1),
(11894, '158946772183', '156681125185', 'code', 'u1569891600', 'u1573834598', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-23 17:28:33', 0, '2020-05-23 17:28:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11895, '159015528969', '1560742214-56', 'code', 'u1569916994', 'u1590154897', '好评！！！', NULL, '2', NULL, NULL, NULL, NULL, 0, NULL, 0, '2020-05-24 18:44:02', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11896, '158996668722', '158892844833', 'code', 'u1569891600', 'u1589966640', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-24 17:25:57', 0, '2020-05-24 17:25:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11897, '159016589824', '1551021388-1', 'code', 'u1569891600', 'u1573834598', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-26 00:49:08', 0, '2020-05-26 00:49:08', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11898, '159015528969', '1560742214-56', 'code', 'u1569916994', 'u1590154897', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-26 21:48:49', 0, '2020-05-26 21:48:49', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11899, '159038023532', '158910529843', 'code', 'u1569916994', 'u1590243179', NULL, '好评！', NULL, '2', 1, 2, 1, 0, '2020-05-28 01:33:38', 0, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11900, '159054843189', '1540227405-1', 'code', 'u1569891600', 'u1590548211', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-31 11:00:56', 0, '2020-05-31 11:00:56', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11901, '159099447751', '158386124866', 'code', 'u1569891600', 'u1590994316', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-05 15:00:26', 0, '2020-06-05 15:00:26', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11902, '159117355086', '156845756877', 'code', 'u1569891600', 'u1591173527', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-07 16:39:25', 0, '2020-06-07 16:39:25', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11903, '159126317252', '156501455978', 'code', 'u1569891600', 'u1591262906', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-08 17:33:43', 0, '2020-06-08 17:33:43', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11904, '159134448453', '157849066123', 'code', 'u1569891600', 'u1573835031', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-08 16:08:57', 0, '2020-06-08 16:08:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11905, '159038023532', '158910529843', 'code', 'u1569916994', 'u1590243179', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-29 12:17:44', 0, '2020-05-29 12:17:44', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11906, '159050514396', '158935836037', 'code', 'u1589233222', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-30 22:59:10', 0, '2020-05-30 22:59:10', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11907, '159050527545', '158909994259', 'code', 'u1573740831', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-05-30 23:01:22', 0, '2020-05-30 23:01:22', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11908, '159142435495', '156845756877', 'code', 'u1569891600', 'u1573835031', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-09 14:19:32', 0, '2020-06-09 14:19:32', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11909, '159153497478', '156515750429', 'code', 'u1569891600', 'u1591527416', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-11 21:03:02', 0, '2020-06-11 21:03:02', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11910, '159159062591', '159158305824', 'code', 'u1569891600', 'u1579510693', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-12 12:31:14', 0, '2020-06-12 12:31:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11911, '159160356144', '1552152737-1', 'code', 'u1569891600', 'u1591603499', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-12 16:06:44', 0, '2020-06-12 16:06:44', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11912, '1591845207147', '1539881242-1', 'code', 'u1569891600', 'u1582562596', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-15 11:13:27', 0, '2020-06-15 11:13:27', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11913, '159211334448', '158394348091', 'code', 'u1569891600', 'u1592113009', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-18 13:42:33', 0, '2020-06-18 13:42:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11914, '159254578276', '1539268842-1', 'code', 'u1569891600', 'u1592545607', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-23 13:49:58', 0, '2020-06-23 13:49:58', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11915, '159264928477', '1555833211-538', 'code', 'u1569891600', 'u1586398858', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-24 18:34:52', 0, '2020-06-24 18:34:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11916, '159264941582', '1548912289-257', 'code', 'u1569891600', 'u1586398858', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-24 18:38:52', 0, '2020-06-24 18:38:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11917, '159265291381', '156683860394', 'code', 'u1569891600', 'u1592652746', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-24 19:35:42', 0, '2020-06-24 19:35:42', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11918, '159274217553', '1554031050-1', 'code', 'u1569891600', 'u1592663100', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-25 20:24:41', 0, '2020-06-25 20:24:41', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11919, '159299815373', '1554031050-1', 'code', 'u1569891600', 'u1592998103', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-28 19:29:57', 0, '2020-06-28 19:29:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11920, '159316836141', '1544774181-1', 'code', 'u1569891600', 'u1593168329', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-30 18:46:32', 0, '2020-06-30 18:46:32', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11921, '159317278136', '156751188476', 'code', 'u1569891600', 'u1593172756', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-30 20:01:19', 0, '2020-06-30 20:01:19', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11922, '159325987253', '156515750429', 'code', 'u1569891600', 'u1593259725', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-01 20:11:23', 0, '2020-07-01 20:11:23', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11923, '159325989061', '156475026786', 'serve', 'u1569891600', 'u1593259725', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-01 20:18:54', 0, '2020-07-01 20:18:54', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11924, '159341157631', '1551611281-372', 'code', 'u1569891600', 'u1593411375', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-03 14:23:20', 0, '2020-07-03 14:23:20', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11925, '159351406565', '156681125185', 'code', 'u1569891600', 'u1593513915', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-04 18:48:16', 0, '2020-07-04 18:48:16', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11926, '159360611145', '156681125185', 'code', 'u1569891600', 'u1593605942', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-05 20:21:57', 0, '2020-07-05 20:21:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11927, '159402538813', '159378228332', 'code', 'u1569891600', 'u1594025348', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-10 16:50:13', 0, '2020-07-10 16:50:13', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11928, '159437780448', '1552152737-1', 'code', 'u1569891600', 'u1594377739', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-14 18:43:29', 0, '2020-07-14 18:43:29', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11929, '159453462185', '1555079620-1', 'code', 'u1569891600', 'u1594534551', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-16 14:17:41', 0, '2020-07-16 14:17:41', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11930, '159488940058', '156515750429', 'code', 'u1569891600', 'u1594889359', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-20 16:50:36', 0, '2020-07-20 16:50:36', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11931, '159509670849', '1553724318-366', 'code', 'u1569891600', 'u1595096596', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-22 02:28:07', 0, '2020-07-22 02:28:07', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11932, '159516475042', '156462911966', 'code', 'u1569891600', 'u1595164663', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-23 21:20:04', 0, '2020-07-23 21:20:04', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11933, '159531258237', '1539268842-1', 'code', 'u1569891600', 'u1595312556', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-25 14:23:28', 0, '2020-07-25 14:23:28', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11934, '159608702916', '159031003273', 'code', 'u1569891600', 'u1595992383', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-03 13:30:45', 0, '2020-08-03 13:30:45', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11935, '159618587972', '158935461036', 'code', 'u1589233222', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-04 16:58:06', 0, '2020-08-04 16:58:06', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11936, '159671460913', '159102264086', 'code', 'u1569891600', 'u1596714350', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-10 19:52:05', 0, '2020-08-10 19:52:05', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11937, '159599244248', '159178325335', 'code', 'u1569916994', 'u1595992383', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-07 00:59:42', 0, '2020-08-07 00:59:42', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11938, '159600391619', '1560742214-56', 'code', 'u1569916994', 'u1596003893', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-07 01:00:16', 0, '2020-08-07 01:00:16', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11939, '159621311986', '157606651873', 'code', 'u1569916994', 'u1596212972', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-05 00:32:21', 0, '2020-08-05 00:32:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11940, '159693174937', '158892844833', 'code', 'u1569891600', 'u1596931712', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-13 08:09:32', 0, '2020-08-13 08:09:32', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11941, '159710807097', '1560080969-585', 'code', 'u1569891600', 'u1597108030', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-15 09:08:02', 0, '2020-08-15 09:08:02', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11942, '159711036162', '157831972871', 'code', 'u1569891600', 'u1597110303', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-15 09:46:44', 0, '2020-08-15 09:46:44', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11943, '159742577516', '1539268842-1', 'code', 'u1569891600', 'u1597425733', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-19 01:23:17', 0, '2020-08-19 01:23:17', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11944, '159765213187', '158273486377', 'code', 'u1569891600', 'u1597651264', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-24 16:15:54', 0, '2020-08-24 16:15:54', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11945, '159765465823', '1558918575-56', 'code', 'u1569891600', 'u1597651264', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-24 16:57:45', 0, '2020-08-24 16:57:45', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11946, '159766088536', '1557742320-1', 'code', 'u1569891600', 'u1597660838', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-25 13:30:49', 0, '2020-08-25 13:30:49', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11947, '1598245671054', '156526490425', 'code', 'u1569891600', 'u1597296342', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-28 13:07:51', 0, '2020-08-28 13:07:51', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11948, '1598245672140', '159214016643', 'code', 'u1569891600', 'u1597296342', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-28 13:07:52', 0, '2020-08-28 13:07:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11949, '159840823982', '1554362402-56', 'code', 'u1569891600', 'u1598407931', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-02 10:17:38', 0, '2020-09-02 10:17:38', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11950, '159862909236', '158226226727', 'code', 'u1569891600', 'u1598629047', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-01 23:39:00', 0, '2020-09-01 23:39:00', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11951, '1591845207071', '159127410961', 'code', 'u1573835031', 'u1582562596', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-15 11:13:27', 0, '2020-06-15 11:13:27', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11952, '159167193854', '158935741666', 'code', 'u1589233222', 'u1591671743', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-06-13 11:24:13', 0, '2020-06-13 11:24:13', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11953, '159886077036', '1541092404-1', 'code', 'u1569891600', 'u1598860725', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-04 15:59:52', 0, '2020-09-04 15:59:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11954, '159886077036', '1541092404-1', 'code', 'u1569891600', 'u1598860725', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-04 15:59:52', 0, '2020-09-04 15:59:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11955, '159912439979', '1551619889-382', 'code', 'u1569891600', 'u1599124364', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-07 17:14:37', 0, '2020-09-07 17:14:37', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11956, '159941670182', '1557579790-18', 'code', 'u1569891600', 'u1599416583', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-11 02:25:53', 0, '2020-09-11 02:25:53', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11957, '159946403447', '156845756877', 'code', 'u1569891600', 'u1599463820', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-11 15:34:09', 0, '2020-09-11 15:34:09', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11958, '159948527282', '158505488481', 'code', 'u1569891600', 'u1599484964', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-11 21:28:23', 0, '2020-09-11 21:28:23', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11959, '159948527282', '158505488481', 'code', 'u1569891600', 'u1599484964', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-11 21:28:23', 0, '2020-09-11 21:28:23', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11960, '159971624458', '158055167331', 'code', 'u1569891600', 'u1599463820', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-14 13:37:44', 0, '2020-09-14 13:37:44', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11961, '159971641711', '1561015633-585', 'code', 'u1569891600', 'u1599714581', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-16 14:52:47', 0, '2020-09-16 14:52:47', NULL, NULL, NULL, '0000-00-00 00:00:00', 1);
INSERT INTO `shop_reviews` (`id`, `ddbh`, `pro`, `type`, `sell`, `buy`, `sellcom`, `buycom`, `sellimp`, `buyimp`, `Attitude`, `efficiency`, `quality`, `buytisp`, `buytime`, `selltisp`, `selltime`, `zjcom`, `zjtime`, `hfcom`, `hftime`, `zt`) VALUES
(11962, '159991541317', '158892844833', 'code', 'u1569891600', 'u1599911436', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-16 21:01:59', 0, '2020-09-16 21:01:59', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11963, '160016721452', '156594897036', 'code', 'u1569891600', 'u1600167187', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-19 18:54:40', 0, '2020-09-19 18:54:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11964, '160052820728', '1546438500-56', 'code', 'u1569891600', 'u1600527763', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-23 23:10:13', 0, '2020-09-23 23:10:13', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11965, '160053771561', '1539268842-1', 'code', 'u1569891600', 'u1600537125', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-24 01:49:47', 0, '2020-09-24 01:49:47', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11966, '160095109141', '158935331595', 'code', 'u1589233222', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-28 20:38:21', 0, '2020-09-28 20:38:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11967, '160136704776', '1539278331-1', 'code', 'u1569891600', 'u1574831757', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-02 16:14:21', 0, '2020-10-02 16:14:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11968, '160128216777', '156943051555', 'code', 'u1569891600', 'u1601281935', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-02 17:48:11', 0, '2020-10-02 17:48:11', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11969, '160137564233', '1539278331-1', 'code', 'u1569891600', 'u1601375503', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-03 18:34:21', 0, '2020-10-03 18:34:21', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11970, '160158060946', '156724687798', 'code', 'u1569891600', 'u1601475898', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-06 03:30:16', 0, '2020-10-06 03:30:16', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11971, '160188175558', '1552152737-1', 'code', 'u1569891600', 'u1601881284', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-09 15:09:31', 0, '2020-10-09 15:09:31', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11972, '160205484166', '160205429358', 'code', 'u1569891600', 'u1600518201', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-11 15:14:33', 0, '2020-10-11 15:14:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11973, '160206624616', '158935493618', 'code', 'u1589233222', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-11 18:24:10', 0, '2020-10-11 18:24:10', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11974, '160250924183', '160231163842', 'code', 'u1569891600', 'u1602508789', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-16 21:28:30', 0, '2020-10-16 21:28:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11975, '160281105135', '1558199407-56', 'code', 'u1569891600', 'u1589115713', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-19 09:32:22', 0, '2020-10-19 09:32:22', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11976, '160281205791', '1554461095-466', 'code', 'u1569891600', 'u1589115713', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-19 09:45:06', 0, '2020-10-19 09:45:06', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11977, '160281283531', '156672411171', 'code', 'u1569891600', 'u1589115713', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-19 09:48:10', 0, '2020-10-19 09:48:10', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11978, '1602813266050', '1559557654-585', 'code', 'u1569891600', 'u1589115713', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-19 10:06:35', 0, '2020-10-19 10:06:35', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11979, '160281330896', '1553169141-466', 'code', 'u1569891600', 'u1589115713', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-19 10:06:26', 0, '2020-10-19 10:06:26', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11980, '160041772467', '1557740174-1', 'code', 'u1569891600', 'u1600415787', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-22 16:29:29', 0, '2020-09-22 16:29:29', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11981, '160272884031', '1542355705-1', 'code', 'u1569891600', 'u1569916994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-22 10:27:24', 0, '2020-10-22 10:27:24', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11982, '160318288241', '160165778683', 'code', 'u1569891600', 'u1603182353', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-24 16:38:52', 0, '2020-10-24 16:38:52', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11983, '160318990242', '1560080969-585', 'code', 'u1569891600', 'u1603188042', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-24 18:32:30', 0, '2020-10-24 18:32:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11984, '160341422124', '1553954540-1', 'code', 'u1569891600', 'u1603414184', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-27 08:51:30', 0, '2020-10-27 08:51:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11985, '160352728945', '1554053170-56', 'code', 'u1569891600', 'u1603527207', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-28 16:14:57', 0, '2020-10-28 16:14:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11986, '160353269884', '156943051555', 'code', 'u1569891600', 'u1603532629', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-28 17:45:32', 0, '2020-10-28 17:45:32', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11987, '159731174312', '1560740218-585', 'code', 'u1569916994', 'u1597238485', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-08-17 17:43:09', 0, '2020-08-17 17:43:09', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11988, '159920699991', '157606651873', 'code', 'u1569916994', 'u1599206906', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-08 16:10:56', 0, '2020-09-08 16:10:56', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11989, '160350994999', '158908720992', 'code', 'u1573834598', 'u1603509918', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-28 11:26:11', 0, '2020-10-28 11:26:11', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11990, '159885196313', '158908719137', 'code', 'u1573834598', 'u1598851904', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-04 13:33:16', 0, '2020-09-04 13:33:16', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11991, '159895317281', '158908720992', 'code', 'u1573834598', 'u1598952912', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-05 17:39:53', 0, '2020-09-05 17:39:53', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11992, '160385333023', '156943051555', 'code', 'u1569891600', 'u1603853290', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-01 10:49:18', 0, '2020-11-01 10:49:18', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11993, '160396818415', '1541092404-1', 'code', 'u1569891600', 'u1603967994', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-01 20:41:56', 0, '2020-11-01 20:41:56', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11994, '160413016577', '1539268842-1', 'code', 'u1569891600', 'u1604129953', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-04 15:42:58', 0, '2020-11-04 15:42:58', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11995, '160457050597', '1539269720-1', 'code', 'u1569891600', 'u1604570301', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-09 18:01:57', 0, '2020-11-09 18:01:57', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11996, '160487434913', '156515838457', 'code', 'u1569891600', 'u1604874324', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-12 06:26:46', 0, '2020-11-12 06:26:46', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11997, '160516001552', '156472029628', 'code', 'u1569891600', 'u1605159988', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-16 13:47:49', 0, '2020-11-16 13:47:49', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11998, '160570849151', '160549139368', 'code', 'u1569891600', 'u1596212972', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-22 22:08:40', 0, '2020-11-22 22:08:40', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(11999, '160515660563', '158908716777', 'code', 'u1573834598', 'u1605156150', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-15 12:53:14', 0, '2020-11-15 12:53:14', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12000, '160625012594', '156343993573', 'code', 'u1569891600', 'u1606249715', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-29 04:35:49', 0, '2020-11-29 04:35:49', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12001, '160629670733', '1558918575-56', 'code', 'u1569891600', 'u1606296411', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-29 17:32:11', 0, '2020-11-29 17:32:11', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12002, '160635013498', '1555833211-538', 'code', 'u1569891600', 'u1606347908', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-11-29 09:49:24', 0, '2020-11-29 09:49:24', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12003, '160077750629', '1546538060-155', 'code', 'u1569891600', 'u1600777474', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-09-30 13:59:23', 0, '2020-09-30 13:59:23', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12004, '160125472399', '156594897036', 'code', 'u1569891600', 'u1601168375', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-10-06 17:55:07', 0, '2020-10-06 17:55:07', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12005, '160658260832', '158055167331', 'code', 'u1569891600', 'u1606582526', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-06 09:22:55', 0, '2020-12-06 09:22:55', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12006, '160689167055', '1558918575-56', 'code', 'u1569891600', 'u1577187160', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-06 14:53:10', 0, '2020-12-06 14:53:10', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12007, '160549503131', '159143072239', 'code', 'u1569891600', 'u1605490259', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-09 23:16:30', 0, '2020-12-09 23:16:30', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12008, '160671435749', '158428789182', 'code', 'u1569891600', 'u1602328289', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-12 09:18:06', 0, '2020-12-12 09:18:06', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12009, '159377235071', '158993595248', 'code', 'u1573740831', 'u1593772201', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-07-06 19:58:56', 0, '2020-07-06 19:58:56', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12010, '160792504018', '158909928043', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-18 13:55:58', 0, '2020-12-18 13:55:58', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12011, '160792567794', '159133124427', 'code', 'u1573835031', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-18 14:01:24', 0, '2020-12-18 14:01:24', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12012, '160792677583', '158988793533', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-18 14:19:41', 0, '2020-12-18 14:19:41', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12013, '160818949077', '158909988046', 'code', 'u1573740831', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-23 17:20:33', 0, '2020-12-23 17:20:33', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12014, '160847558214', '159176476561', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-26 22:48:17', 0, '2020-12-26 22:48:17', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12015, '160851599163', '159178024168', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-27 10:31:01', 0, '2020-12-27 10:31:01', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12016, '160851435341', '159178024168', 'code', 'u1569916994', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2020-12-28 09:32:38', 0, '2020-12-28 09:32:38', NULL, NULL, NULL, '0000-00-00 00:00:00', 1),
(12017, '161513227755', '158796038053', 'domain', 'u1587955576', 'u1569891600', NULL, NULL, '2', '2', 5, 5, 5, 0, '2021-03-11 23:51:24', 0, '2021-03-11 23:51:24', NULL, NULL, NULL, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- 表的结构 `shop_sell`
--

CREATE TABLE IF NOT EXISTS `shop_sell` (
  `id` int(10) NOT NULL,
  `bh` char(20) DEFAULT NULL,
  `contact` text,
  `sj` datetime DEFAULT NULL,
  `tx` char(200) DEFAULT NULL,
  `name` char(20) DEFAULT NULL,
  `wechat` char(50) DEFAULT NULL,
  `notice` varchar(250) DEFAULT NULL,
  `skill` varchar(250) DEFAULT NULL,
  `address` varchar(100) NOT NULL DEFAULT '中国地区' COMMENT '所在地区',
  `s_phone` int(10) NOT NULL COMMENT '客服电话',
  `s_watermark` varchar(100) NOT NULL COMMENT '大图水印',
  `bannar` varchar(100) NOT NULL COMMENT '招牌图',
  `zhuti` varchar(100) NOT NULL COMMENT '主题',
  `bg_type` varchar(100) NOT NULL DEFAULT 'no' COMMENT '配色',
  `bg_color_input` varchar(100) NOT NULL COMMENT '自定义颜色',
  `bond` varchar(100) NOT NULL COMMENT '保证金',
  `bond_zt` int(11) NOT NULL COMMENT '状态：0未缴纳1正常2限制',
  `bond_auto` varchar(100) NOT NULL DEFAULT '-1' COMMENT '缴纳方式',
  `bond_automoney` varchar(100) NOT NULL DEFAULT '300' COMMENT '基础额度',
  `bond_cmoney` varchar(100) NOT NULL COMMENT '基础额度填写',
  `bond_method` varchar(100) NOT NULL DEFAULT '-1' COMMENT '赔付方式',
  `shig_style` varchar(250) DEFAULT NULL,
  `sale_n` int(8) DEFAULT '0',
  `goods_n` int(11) NOT NULL COMMENT '商品数量',
  `goods_count` varchar(100) DEFAULT '0' COMMENT '商品数量统计',
  `back_n` int(8) DEFAULT '0',
  `rev_m` int(8) DEFAULT '0',
  `nav` varchar(500) DEFAULT NULL,
  `shopkey` varchar(100) DEFAULT NULL,
  `sale_m` decimal(10,2) DEFAULT '0.00',
  `score` char(100) DEFAULT NULL,
  `temid` int(10) DEFAULT NULL,
  `stisp` int(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_sell`
--

INSERT INTO `shop_sell` (`id`, `bh`, `contact`, `sj`, `tx`, `name`, `wechat`, `notice`, `skill`, `address`, `s_phone`, `s_watermark`, `bannar`, `zhuti`, `bg_type`, `bg_color_input`, `bond`, `bond_zt`, `bond_auto`, `bond_automoney`, `bond_cmoney`, `bond_method`, `shig_style`, `sale_n`, `goods_n`, `goods_count`, `back_n`, `rev_m`, `nav`, `shopkey`, `sale_m`, `score`, `temid`, `stisp`) VALUES
(1, 'u1629290430', '<qq-i>66983239</qq-i>', '2021-08-18 20:43:28', '//suzhizhan.oss-cn-beijing.aliyuncs.com/images/none.jpg', '时光网络', NULL, NULL, NULL, '-', 0, '', '', '', 'no', '', '', 0, '-1', '300', '', '-1', NULL, 0, 1, '{"code":1,"serve":0,"web":0,"domain":0,"total":1}', 0, 0, NULL, NULL, '0.00', '0|5.00|5.00|5.00|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0', NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_sell_nav`
--

CREATE TABLE IF NOT EXISTS `shop_sell_nav` (
  `id` int(10) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(100) NOT NULL,
  `ubh` varchar(30) NOT NULL,
  `tit` varchar(30) NOT NULL,
  `link` varchar(100) NOT NULL,
  `px` int(10) NOT NULL,
  `zt` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_sell_slide`
--

CREATE TABLE IF NOT EXISTS `shop_sell_slide` (
  `id` int(11) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(200) NOT NULL,
  `ubh` varchar(100) NOT NULL,
  `link` varchar(200) NOT NULL,
  `file` varchar(200) NOT NULL,
  `px` int(10) NOT NULL,
  `zt` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_serve`
--

CREATE TABLE IF NOT EXISTS `shop_serve` (
  `id` int(10) NOT NULL,
  `bh` varchar(50) NOT NULL,
  `ubh` varchar(100) NOT NULL,
  `sj` datetime NOT NULL,
  `lastgx` datetime NOT NULL,
  `uip` char(50) NOT NULL,
  `menus` varchar(30) NOT NULL,
  `zt` int(10) NOT NULL,
  `tit` varchar(200) NOT NULL,
  `ckey` varchar(200) NOT NULL COMMENT '关键词',
  `cdes` varchar(200) NOT NULL,
  `txt` text NOT NULL,
  `xsnum` int(10) NOT NULL,
  `money` float NOT NULL,
  `tmoney` int(10) NOT NULL,
  `tysx` varchar(200) NOT NULL,
  `piece` int(10) NOT NULL,
  `moneydes` varchar(1000) NOT NULL,
  `allmoney` varchar(1000) NOT NULL,
  `pf1` varchar(50) NOT NULL,
  `pf2` varchar(50) NOT NULL,
  `pf3` varchar(50) NOT NULL,
  `beizhu` varchar(100) DEFAULT NULL,
  `ifxj` int(10) NOT NULL DEFAULT '0',
  `psort` varchar(200) NOT NULL,
  `djl` int(11) NOT NULL,
  `tj` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_sign`
--

CREATE TABLE IF NOT EXISTS `shop_sign` (
  `uip` varchar(50) DEFAULT NULL,
  `ubh` char(50) NOT NULL,
  `lasttime` date DEFAULT NULL,
  `qcount` int(10) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_sms_code`
--

CREATE TABLE IF NOT EXISTS `shop_sms_code` (
  `id` int(10) unsigned NOT NULL,
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `screen` varchar(30) NOT NULL DEFAULT '' COMMENT '场景',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0：未使用 1：已使用',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_ip` varchar(255) NOT NULL DEFAULT '' COMMENT '请求 ip'
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_sms_code`
--

INSERT INTO `shop_sms_code` (`id`, `mobile`, `screen`, `code`, `status`, `create_at`, `create_ip`) VALUES
(1, '15116911539', '手机验证码是:4543', '931037', 0, 1599200297, '127.0.0.1'),
(2, '15116911539', '您的验证码为：1428，该验证码5分钟内有效，请勿泄露他人。', '700591', 0, 1599200414, '127.0.0.1'),
(3, '15116911539', '您的验证码为：1243，该验证码5分钟内有效，请勿泄露他人。', '225890', 0, 1599200523, '127.0.0.1'),
(4, '15116911539', '5473', '5473', 0, 1599201543, '127.0.0.1'),
(5, '15116911539', '7573', '7573', 0, 1599225853, '127.0.0.1'),
(6, '15116811528', '您的验证码为：5504，该验证码5分钟内有效，请勿泄露他人。', '5504', 0, 1599298524, '127.0.0.1'),
(7, '15116811528', '您的验证码为：8260，该验证码5分钟内有效，请勿泄露他人。', '8260', 0, 1599298842, '127.0.0.1'),
(8, '15116911539', '您的验证码为：9953，该验证码5分钟内有效，请勿泄露他人。', '9953', 0, 1600157897, '127.0.0.1'),
(9, '18148120849', '您的验证码为：1609，该验证码5分钟内有效，请勿泄露他人。', '1609', 0, 1601254707, '171.223.184.105'),
(10, '18148120849', '您的验证码为：2769，该验证码5分钟内有效，请勿泄露他人。', '2769', 0, 1601275113, '171.223.184.105'),
(11, '18148120849', '您的验证码为：6577，该验证码5分钟内有效，请勿泄露他人。', '6577', 0, 1601278735, '171.223.184.105'),
(12, '17347269069', '您的验证码为：3808，该验证码5分钟内有效，请勿泄露他人。', '3808', 0, 1601481114, '58.47.163.8'),
(13, '17347269069', '您的验证码为：6029，该验证码5分钟内有效，请勿泄露他人。', '6029', 0, 1601481206, '58.47.163.8'),
(14, '18350553996', '您的验证码为：7650，该验证码5分钟内有效，请勿泄露他人。', '7650', 0, 1601714637, '112.47.152.208'),
(15, '15734556784', '您的验证码为：4145，该验证码5分钟内有效，请勿泄露他人。', '4145', 0, 1602311553, '42.184.187.240'),
(16, '13834745712', '您的验证码为：9920，该验证码5分钟内有效，请勿泄露他人。', '9920', 0, 1603170900, '59.49.20.36'),
(17, '17060291893', '您的验证码为：1164，该验证码5分钟内有效，请勿泄露他人。', '1164', 0, 1603197223, '103.208.32.212'),
(18, '17060291893', '您的验证码为：3667，该验证码5分钟内有效，请勿泄露他人。', '3667', 0, 1603197452, '103.208.32.212'),
(19, '15610243082', '您的验证码为：1918，该验证码5分钟内有效，请勿泄露他人。', '1918', 0, 1603867279, '122.4.197.7'),
(20, '13800138000', 'delete_order', '121151', 0, 1603878435, '222.93.27.76'),
(21, '15877555807', '您的验证码为：8329，该验证码5分钟内有效，请勿泄露他人。', '8329', 0, 1603949215, '219.145.30.102'),
(22, '15666681179', '您的验证码为：4430，该验证码5分钟内有效，请勿泄露他人。', '4430', 0, 1604230447, '218.57.124.153'),
(23, '15666681179', '您的验证码为：6503，该验证码5分钟内有效，请勿泄露他人。', '6503', 0, 1604236683, '218.57.124.153'),
(24, '15666681179', '您的验证码为：8175，该验证码5分钟内有效，请勿泄露他人。', '8175', 0, 1604280525, '218.57.124.153'),
(25, '15666681179', '您的验证码为：2894，该验证码5分钟内有效，请勿泄露他人。', '2894', 0, 1604287176, '218.57.124.153'),
(26, '18666109714', '您的验证码为：4505，该验证码5分钟内有效，请勿泄露他人。', '4505', 0, 1604539006, '175.5.49.201'),
(27, '15666681179', '您的验证码为：8153，该验证码5分钟内有效，请勿泄露他人。', '8153', 0, 1604560885, '218.57.124.153'),
(28, '15666681179', '您的验证码为：6145，该验证码5分钟内有效，请勿泄露他人。', '6145', 0, 1604630501, '218.57.124.153'),
(29, '18979063899', '您的验证码为：9430，该验证码5分钟内有效，请勿泄露他人。', '9430', 0, 1604757180, '115.151.134.144'),
(30, '15610243082', '您的验证码为：6003，该验证码5分钟内有效，请勿泄露他人。', '6003', 0, 1605060142, '122.4.197.7'),
(31, '18537062468', '您的验证码为：2977，该验证码5分钟内有效，请勿泄露他人。', '2977', 0, 1605708450, '123.13.226.49'),
(32, '17520257639', '您的验证码为：4480，该验证码5分钟内有效，请勿泄露他人。', '4480', 0, 1605772308, '175.5.121.239'),
(33, '18801733576', '您的验证码为：7488，该验证码5分钟内有效，请勿泄露他人。', '7488', 0, 1606095424, '14.204.75.185'),
(34, '18801733576', '您的验证码为：5443，该验证码5分钟内有效，请勿泄露他人。', '5443', 0, 1606095548, '14.204.75.185'),
(35, '18801733576', '您的验证码为：6611，该验证码5分钟内有效，请勿泄露他人。', '6611', 0, 1606122253, '117.136.72.28'),
(36, '13063663676', '您的验证码为：8418，该验证码5分钟内有效，请勿泄露他人。', '8418', 0, 1606138405, '180.114.207.23'),
(37, '13770315241', '您的验证码为：5532，该验证码5分钟内有效，请勿泄露他人。', '5532', 0, 1606198625, '218.94.33.122'),
(38, '13770315241', '您的验证码为：5960，该验证码5分钟内有效，请勿泄露他人。', '5960', 0, 1606198866, '218.94.33.122'),
(39, '15089864997', '您的验证码为：3760，该验证码5分钟内有效，请勿泄露他人。', '3760', 0, 1606249820, '120.239.105.142'),
(40, '18801733576', '您的验证码为：3318，该验证码5分钟内有效，请勿泄露他人。', '3318', 0, 1606354394, '14.204.75.185'),
(41, '17700709399', '您的验证码为：2167，该验证码5分钟内有效，请勿泄露他人。', '2167', 0, 1606379908, '58.47.125.99'),
(42, '18801733576', '您的验证码为：8890，该验证码5分钟内有效，请勿泄露他人。', '8890', 0, 1606390724, '14.204.75.185'),
(43, '18801733576', '您的验证码为：7861，该验证码5分钟内有效，请勿泄露他人。', '7861', 0, 1606390898, '14.204.75.185'),
(44, '18801733576', '您的验证码为：3274，该验证码5分钟内有效，请勿泄露他人。', '3274', 0, 1606390997, '14.204.75.185'),
(45, '18801733576', '您的验证码为：7830，该验证码5分钟内有效，请勿泄露他人。', '7830', 0, 1606532759, '14.204.75.185'),
(46, '18801733576', '您的验证码为：6145，该验证码5分钟内有效，请勿泄露他人。', '6145', 0, 1606704990, '14.204.75.185'),
(47, '13818888888', '您的验证码为：5469，该验证码5分钟内有效，请勿泄露他人。', '5469', 0, 1606711756, '183.227.148.116'),
(48, '13452059555', '您的验证码为：8386，该验证码5分钟内有效，请勿泄露他人。', '8386', 0, 1606711857, '183.227.148.116'),
(49, '13818888888', '您的验证码为：3885，该验证码5分钟内有效，请勿泄露他人。', '3885', 0, 1606712058, '183.227.148.116'),
(50, '13818888888', '您的验证码为：6898，该验证码5分钟内有效，请勿泄露他人。', '6898', 0, 1606712547, '183.227.148.116'),
(51, '15666681179', '您的验证码为：3576，该验证码5分钟内有效，请勿泄露他人。', '3576', 0, 1606718545, '112.225.74.184'),
(52, '18801733576', '您的验证码为：2653，该验证码5分钟内有效，请勿泄露他人。', '2653', 0, 1606805848, '14.204.75.186'),
(53, '15269713270', '您的验证码为：5102，该验证码5分钟内有效，请勿泄露他人。', '5102', 0, 1606812797, '112.235.140.182'),
(54, '15638945505', '您的验证码为：6590，该验证码5分钟内有效，请勿泄露他人。', '6590', 0, 1606813574, '1.194.65.36'),
(55, '17607624485', '您的验证码为：8249，该验证码5分钟内有效，请勿泄露他人。', '8249', 0, 1606824657, '223.74.227.42'),
(56, '17607624485', '您的验证码为：5070，该验证码5分钟内有效，请勿泄露他人。', '5070', 0, 1606824796, '223.74.227.42');

-- --------------------------------------------------------

--
-- 表的结构 `shop_soukey`
--

CREATE TABLE IF NOT EXISTS `shop_soukey` (
  `id` int(11) NOT NULL,
  `skey` char(50) CHARACTER SET gb2312 DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `airtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `type` char(50) CHARACTER SET gb2312 DEFAULT NULL,
  `endip` char(50) CHARACTER SET gb2312 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_auth`
--

CREATE TABLE IF NOT EXISTS `shop_system_auth` (
  `id` bigint(20) unsigned NOT NULL,
  `title` varchar(20) NOT NULL COMMENT '权限名称',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(1:禁用,2:启用)',
  `sort` smallint(6) unsigned DEFAULT '0' COMMENT '排序权重',
  `desc` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_by` bigint(11) unsigned DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='系统权限表';

--
-- 转存表中的数据 `shop_system_auth`
--

INSERT INTO `shop_system_auth` (`id`, `title`, `status`, `sort`, `desc`, `create_by`, `create_at`) VALUES
(3, '超级管理员', 1, 0, '超级管理员', 0, '2018-04-10 19:24:49');

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_auth_node`
--

CREATE TABLE IF NOT EXISTS `shop_system_auth_node` (
  `auth` bigint(20) unsigned DEFAULT NULL COMMENT '角色ID',
  `node` varchar(200) DEFAULT NULL COMMENT '节点路径'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与节点关系表';

--
-- 转存表中的数据 `shop_system_auth_node`
--

INSERT INTO `shop_system_auth_node` (`auth`, `node`) VALUES
(3, 'admin'),
(3, 'admin/config'),
(3, 'admin/config/index'),
(3, 'admin/config/file'),
(3, 'admin/log'),
(3, 'admin/log/index'),
(3, 'admin/log/del'),
(3, 'admin/menu'),
(3, 'admin/menu/index'),
(3, 'admin/menu/add'),
(3, 'admin/menu/edit'),
(3, 'admin/menu/del'),
(3, 'admin/menu/forbid'),
(3, 'admin/menu/resume'),
(3, 'admin/mlog'),
(3, 'admin/mlog/index'),
(3, 'admin/mlog/del'),
(3, 'admin/nav'),
(3, 'admin/nav/index'),
(3, 'admin/nav/add'),
(3, 'admin/nav/edit'),
(3, 'admin/nav/del'),
(3, 'admin/nav/forbid'),
(3, 'admin/nav/resume'),
(3, 'admin/user'),
(3, 'admin/user/index'),
(3, 'admin/user/auth'),
(3, 'admin/user/add'),
(3, 'admin/user/edit'),
(3, 'admin/user/del'),
(3, 'admin/user/forbid'),
(3, 'admin/user/resume'),
(3, 'manage'),
(3, 'manage/article'),
(3, 'manage/article/index'),
(3, 'manage/article/add'),
(3, 'manage/article/edit'),
(3, 'manage/article/change_status'),
(3, 'manage/article/del'),
(3, 'manage/articlecategory'),
(3, 'manage/articlecategory/index'),
(3, 'manage/articlecategory/add'),
(3, 'manage/articlecategory/edit'),
(3, 'manage/articlecategory/change_status'),
(3, 'manage/articlecategory/del'),
(3, 'manage/backup'),
(3, 'manage/backup/index'),
(3, 'manage/backup/tablist'),
(3, 'manage/backup/backall'),
(3, 'manage/backup/backtables'),
(3, 'manage/backup/recover'),
(3, 'manage/backup/deletebak'),
(3, 'manage/backup/downloadbak'),
(3, 'manage/cash'),
(3, 'manage/cash/index'),
(3, 'manage/cash/config'),
(3, 'manage/cash/detail'),
(3, 'manage/channel'),
(3, 'manage/channel/index'),
(3, 'manage/channel/add'),
(3, 'manage/channel/edit'),
(3, 'manage/channel/del'),
(3, 'manage/channel/change_status'),
(3, 'manage/channel/getlistbypaytype'),
(3, 'manage/channelaccount'),
(3, 'manage/channelaccount/index'),
(3, 'manage/channelaccount/add'),
(3, 'manage/channelaccount/edit'),
(3, 'manage/channelaccount/del'),
(3, 'manage/channelaccount/clear'),
(3, 'manage/channelaccount/change_status'),
(3, 'manage/channelaccount/getfields'),
(3, 'manage/complaint'),
(3, 'manage/complaint/index'),
(3, 'manage/complaint/change_status'),
(3, 'manage/complaint/change_admin_read'),
(3, 'manage/complaint/del'),
(3, 'manage/email'),
(3, 'manage/email/index'),
(3, 'manage/email/test'),
(3, 'manage/goods'),
(3, 'manage/goods/index'),
(3, 'manage/goods/change_status'),
(3, 'manage/index'),
(3, 'manage/index/main'),
(3, 'manage/invitecode'),
(3, 'manage/invitecode/index'),
(3, 'manage/invitecode/add'),
(3, 'manage/invitecode/del'),
(3, 'manage/invitecode/delnum'),
(3, 'manage/log'),
(3, 'manage/log/user_money'),
(3, 'manage/order'),
(3, 'manage/order/index'),
(3, 'manage/order/detail'),
(3, 'manage/order/merchant'),
(3, 'manage/order/channel'),
(3, 'manage/order/hour'),
(3, 'manage/order/change_freeze_status'),
(3, 'manage/order/del'),
(3, 'manage/order/del_batch'),
(3, 'manage/product'),
(3, 'manage/product/index'),
(3, 'manage/product/add'),
(3, 'manage/product/edit'),
(3, 'manage/product/del'),
(3, 'manage/product/change_status'),
(3, 'manage/site'),
(3, 'manage/site/info'),
(3, 'manage/site/domain'),
(3, 'manage/site/register'),
(3, 'manage/site/wordfilter'),
(3, 'manage/site/spread'),
(3, 'manage/sms'),
(3, 'manage/sms/index'),
(3, 'manage/user'),
(3, 'manage/user/index'),
(3, 'manage/user/change_status'),
(3, 'manage/user/change_freeze_status'),
(3, 'manage/user/detail'),
(3, 'manage/user/add'),
(3, 'manage/user/edit'),
(3, 'manage/user/del'),
(3, 'manage/user/manage_money'),
(3, 'manage/user/rate'),
(3, 'manage/user/login'),
(3, 'manage/user/message'),
(3, 'manage/user/loginlog'),
(3, 'manage/user/unlock'),
(3, 'wechat'),
(3, 'wechat/config'),
(3, 'wechat/config/index'),
(3, 'wechat/config/pay'),
(3, 'wechat/fans'),
(3, 'wechat/fans/index'),
(3, 'wechat/fans/back'),
(3, 'wechat/fans/backadd'),
(3, 'wechat/fans/tagset'),
(3, 'wechat/fans/backdel'),
(3, 'wechat/fans/tagadd'),
(3, 'wechat/fans/tagdel'),
(3, 'wechat/fans/sync'),
(3, 'wechat/keys'),
(3, 'wechat/keys/index'),
(3, 'wechat/keys/add'),
(3, 'wechat/keys/edit'),
(3, 'wechat/keys/del'),
(3, 'wechat/keys/forbid'),
(3, 'wechat/keys/resume'),
(3, 'wechat/keys/subscribe'),
(3, 'wechat/keys/defaults'),
(3, 'wechat/menu'),
(3, 'wechat/menu/index'),
(3, 'wechat/menu/edit'),
(3, 'wechat/menu/cancel'),
(3, 'wechat/news'),
(3, 'wechat/news/index'),
(3, 'wechat/news/select'),
(3, 'wechat/news/image'),
(3, 'wechat/news/add'),
(3, 'wechat/news/edit'),
(3, 'wechat/news/del'),
(3, 'wechat/news/push'),
(3, 'wechat/tags'),
(3, 'wechat/tags/index'),
(3, 'wechat/tags/add'),
(3, 'wechat/tags/edit'),
(3, 'wechat/tags/sync'),
(3, 'admin/node'),
(3, 'admin/node/save'),
(3, 'admin/node/save'),
(3, 'admin/auth'),
(3, 'admin/auth/index'),
(3, 'admin/auth/apply'),
(3, 'admin/auth/add'),
(3, 'admin/auth/edit'),
(3, 'admin/auth/forbid'),
(3, 'admin/auth/resume'),
(3, 'admin/auth/del'),
(3, 'admin/auth/bindgoogle'),
(3, 'admin/node'),
(3, 'admin/node/save'),
(3, 'admin/node/index');

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_config`
--

CREATE TABLE IF NOT EXISTS `shop_system_config` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '配置编码',
  `value` varchar(10240) DEFAULT NULL COMMENT '配置值'
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统参数配置';

--
-- 转存表中的数据 `shop_system_config`
--

INSERT INTO `shop_system_config` (`id`, `name`, `value`) VALUES
(148, 'site_name', 'ThinkCT'),
(149, 'site_copy', 'copyright © 2021 ThinkCT all rights reserved.'),
(164, 'storage_type', 'oss'),
(165, 'storage_qiniu_is_https', '0'),
(166, 'storage_qiniu_bucket', 'mafabu'),
(167, 'storage_qiniu_domain', 'qfghuejrn.hn-bkt.clouddn.com'),
(168, 'storage_qiniu_access_key', 'ONGyMG9E4RsClLvNTHTqa2fO0cNxFpJHXau8ZWtx'),
(169, 'storage_qiniu_secret_key', '9ClinuqnMMKjT6kENWsLdyH0jqq_qKrkhOmDsqJx'),
(170, 'storage_qiniu_region', '华东'),
(173, 'app_name', 'thinkct'),
(174, 'app_version', '2.00 dev'),
(176, 'browser_icon', 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/0e52eb4a4e7f0a0b/b8a27a90860c3e3d.ico'),
(184, 'wechat_appid', 'wxbd2715b339faa4b6'),
(185, 'wechat_appsecret', '7e8055384f9c4ff5991c46cacd336ad9'),
(186, 'wechat_token', 'mytoken'),
(187, 'wechat_encodingaeskey', 'KHyoWLoS7oLZYkB4PokMTfA5sm6Hrqc9Tzgn9iXc0YH'),
(188, 'wechat_mch_id', '1332187001'),
(189, 'wechat_partnerkey', 'A82DC5BD1F3359081049C568D8502BC5'),
(194, 'wechat_cert_key', ''),
(196, 'wechat_cert_cert', ''),
(197, 'tongji_baidu_key', ''),
(198, 'tongji_cnzz_key', '1261854404'),
(199, 'storage_oss_bucket', 'https://www.thinkct.net/'),
(200, 'storage_oss_keyid', 'https://www.thinkct.net/'),
(201, 'storage_oss_secret', 'https://www.thinkct.net/'),
(202, 'storage_oss_domain', 'https://www.thinkct.net/'),
(203, 'storage_oss_is_https', '0'),
(204, 'sms_channel', 'smsbao'),
(205, 'smsbao_user', 'thinkct'),
(206, 'smsbao_pass', 'thinkct'),
(207, 'smsbao_signature', 'thinkct'),
(208, 'alidayu_key', 'thinkct'),
(209, 'alidayu_secret', 'thinkct'),
(210, 'alidayu_smstpl', 'thinkct'),
(211, 'alidayu_signature', '自动发卡'),
(212, 'yixin_sms_user', 'thinkct'),
(213, 'yixin_sms_pass', 'thinkct'),
(214, 'yixin_sms_signature', 'thinkct'),
(215, 'email_name', 'ThinkCT'),
(216, 'email_smtp', 'ThinkCT'),
(217, 'email_port', '465'),
(218, 'email_user', 'ThinkCT'),
(219, 'email_pass', 'ThinkCT'),
(220, 'cash_min_money', '100'),
(221, 'transaction_min_fee', '0.01'),
(222, 'cash_fee_type', '100'),
(223, 'cash_fee', '3'),
(224, 'spread_rebate_rate', '0.3'),
(225, 'site_keywords', 'ThinkCT'),
(226, 'site_desc', 'ThinkCT'),
(227, 'site_status', '1'),
(228, 'site_subtitle', '源码交易，站长服务平台-免费版'),
(229, 'site_close_tips', '站点维护中'),
(230, 'complaint_limit_num', '1'),
(231, 'cash_status', '1'),
(232, 'cash_close_tips', '满100每天12点自动结算，无须手动结算。'),
(233, 'cash_limit_time_start', '0'),
(234, 'cash_limit_time_end', '23'),
(235, 'cash_limit_num', '5'),
(236, 'cash_limit_num_tips', '满100每天12点自动结算，无须手动结算。'),
(237, 'site_info_tel', '0551-888888'),
(238, 'site_info_qq', '66983239'),
(239, 'site_info_address', '广州市海珠区琶洲'),
(240, 'site_info_email', '66983239@qq.com'),
(241, 'site_info_copyright', 'Copyright © 2019-2021 ThinkCT All rights reserved. 版权所有'),
(242, 'site_info_icp', '粤ICP备：12345678号'),
(243, 'site_domain', 'http://www.mafabu.com'),
(244, 'site_domain_res', '/static'),
(245, 'site_register_verify', '1'),
(246, 'site_register_status', '1'),
(247, 'site_register_smscode_status', '1'),
(248, 'site_wordfilter_status', '1'),
(249, 'site_wordfilter_danger', '违禁词'),
(250, 'disabled_domains', 'www|ftp|bbs|blog|tes'),
(251, 'site_domain_short', ''),
(252, 'storage_local_exts', 'jpg,jpeg,gif,png,ico'),
(253, 'site_logo', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/e8d8cd6e08a1142f/c758e91d39349f08.jpg'),
(254, 'site_shop_domain', 'http://shop.thinkct.net'),
(255, 'yixin_sms_cost', '0.2'),
(256, 'smsbao_cost', '0.1'),
(257, 'alidayu_cost', '0.2'),
(258, 'index_theme', 'default'),
(259, '1cloudsp_key', 'AccessKey'),
(260, '1cloudsp_secret', 'AccessSecret'),
(261, '1cloudsp_smstpl', '3934'),
(262, '1cloudsp_signature', '自动发卡'),
(263, '1cloudsp_cost', '0.1'),
(264, '253sms_key', 'N3451234'),
(265, '253sms_secret', '1'),
(266, '253sms_signature', '1'),
(267, '253sms_cost', '1'),
(268, 'ip_register_limit', '5'),
(269, 'is_google_auth', '0'),
(270, 'sms_error_limit', '5'),
(271, 'sms_error_time', '10'),
(272, 'wrong_password_times', '20'),
(273, 'site_statistics', '&lt;script type=&quot;text/javascript&quot; src=&quot;https://s11.cnzz.com/z_stat.php?id=1260258244&amp;web_id=1260258244&quot;&gt;&lt;/script&gt;'),
(274, 'admin_login_path', 'admin'),
(276, 'announce_push', '1'),
(278, 'spread_reward', '0'),
(279, 'spread_reward_money', '5'),
(280, 'spread_invite_code', '1'),
(281, 'contact_us', '&lt;dl&gt;\r\n	&lt;dt style=&quot;text-align: center;&quot;&gt;联系电话： &lt;font&gt;133-1234-1234&lt;/font&gt;&lt;/dt&gt;\r\n	&lt;dd style=&quot;text-align: center;&quot;&gt;客服 QQ： &lt;font&gt;12345678&lt;/font&gt;&lt;/dd&gt;\r\n	&lt;dd style=&quot;text-align: center;&quot;&gt;公司地址： 自动发卡系统&lt;/dd&gt;\r\n	&lt;dd style=&quot;text-align: center;&quot;&gt;公司名称： 自动发卡系统&lt;/dd&gt;\r\n&lt;/dl&gt;\r\n'),
(282, 'qqgroup', ''),
(283, 'wx_auto_login', '1'),
(284, 'is_need_invite_code', '1'),
(285, 'site_register_code_type', 'sms'),
(286, 'cash_verify', '2'),
(287, 'auto_cash_money', '100'),
(288, 'sms_notify_channel', 'smsbao'),
(289, 'merchant_logo', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/e8d8cd6e08a1142f/c758e91d39349f08.jpg'),
(290, 'site_info_qrcode', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/3a6c682f6391d4fa/e02485302f105c8d.png'),
(291, 'cash_type', '["1","2","3"]'),
(292, 'goods_min_price', '10'),
(293, 'goods_max_price', '1000'),
(294, 'auto_cash_time', '1'),
(295, 'auto_cash_fee_type', '1'),
(296, 'auto_cash_fee', '5'),
(297, 'order_query_chkcode', '1'),
(298, 'buy_protocol', '&lt;p&gt;ThinkCT&lt;/p&gt;\r\n'),
(299, 'login_auth', '0'),
(300, 'login_auth_type', '0'),
(301, 'fee_payer', '1'),
(302, 'available_email', '0'),
(303, 'idcard_auth_type', '0'),
(304, 'idcard_auth_path', ''),
(305, 'idcard_auth_appcode', ''),
(306, 'idcard_auth_status_field', 'status'),
(307, 'idcard_auth_status_code', '01'),
(308, 'settlement_type', '1'),
(309, 'settlement_type', '1'),
(310, 'settlement_type', '1'),
(311, 'settlement_type', '1'),
(312, 'site_info_tel_desc', ''),
(313, 'site_info_qq_desc', ''),
(314, 'site_info_qrcode_desc', ''),
(315, 'site_iphone_qrcode', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/3a6c682f6391d4fa/e02485302f105c8d.png'),
(316, 'site_android_qrcode', 'http://suzhizhan.oss-cn-beijing.aliyuncs.com/3a6c682f6391d4fa/e02485302f105c8d.png'),
(317, 'order_type', '0'),
(318, 'user_order_profix', ''),
(319, 'email_smtp1', ''),
(320, 'email_port1', ''),
(321, 'email_user1', ''),
(322, 'email_pass1', ''),
(323, 'email_smtp2', ''),
(324, 'email_port2', ''),
(325, 'email_user2', ''),
(326, 'email_pass2', ''),
(327, 'site_beian', '赣ICP备19000408号'),
(328, 'sms_complaint_channel', ''),
(329, 'sms_complaint_notify_channel', ''),
(330, 'alidayu_complaint_smstpl', '1'),
(331, 'alidayu_complaint_notify_smstpl', '1'),
(332, 'alidayu_order_smstpl', '1'),
(333, '1cloudsp_complaint_smstpl', '1'),
(334, '1cloudsp_complaint_notify_smstpl', '1'),
(335, '1cloudsp_order_smstpl', '1'),
(336, 'auth_api', 'http://api.thinkct.net/update.php'),
(337, 'auth_key', '7c0fcb12000dd234	'),
(338, 'auth_code', '27c60becba5bf19b92589d1c344a80ba'),
(339, 'wechat_stock_template', ''),
(340, 'wechat_sell_template', ''),
(341, 'appCode', 'ThinkCT'),
(342, 'search_type', '2'),
(343, 'time_guarantee', '1'),
(344, 'time_refund', '3'),
(345, 'time_extend', '3'),
(346, 'time_evaluate', '3'),
(347, 'audit_type', '1'),
(348, 'site_index', 'code,serve,web,domain,task,news,link'),
(349, 'alipay_type', 'alipaypc'),
(350, 'wechatpay_type', 'wechatpay'),
(351, 'qpay_type', 'qpay'),
(352, 'pay_min_price', '1'),
(353, 'pay_max_price', '3000'),
(354, 'pay_max_price', '3000'),
(355, 'pay_min_price', '1'),
(356, 'alipay_type', 'alipaypc'),
(357, 'wechatpay_type', 'wechatpay'),
(358, 'qpay_type', 'qpay'),
(359, 'merchant_texiao', '0'),
(360, 'jdwx_key', 'thinkct'),
(361, 'jdwx_signature', 'thinkct');

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_log`
--

CREATE TABLE IF NOT EXISTS `shop_system_log` (
  `id` bigint(20) unsigned NOT NULL,
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `node` char(200) NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `action` varchar(200) NOT NULL DEFAULT '' COMMENT '操作行为',
  `content` text NOT NULL COMMENT '操作内容描述',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作日志表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_menu`
--

CREATE TABLE IF NOT EXISTS `shop_system_menu` (
  `id` bigint(20) unsigned NOT NULL,
  `pid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `node` varchar(200) NOT NULL DEFAULT '' COMMENT '节点代码',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `url` varchar(400) NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '菜单排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `create_by` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

--
-- 转存表中的数据 `shop_system_menu`
--

INSERT INTO `shop_system_menu` (`id`, `pid`, `title`, `node`, `icon`, `url`, `params`, `target`, `sort`, `status`, `create_by`, `create_at`) VALUES
(2, 0, '首页', '', 'fa fa-home', '#', '', '_self', 1000, 1, 0, '2015-11-16 03:15:38'),
(4, 2, '主页', '', 'fa fa-automobile', '#', '', '_self', 100, 1, 0, '2016-03-14 02:12:55'),
(5, 151, '基本设置', '', 'fa fa-apple', 'admin/config/index', '', '_self', 1, 1, 0, '2016-05-05 22:36:49'),
(6, 151, '文件存储', '', 'fa fa-save', 'admin/config/file', '', '_self', 7, 1, 0, '2016-05-05 22:39:43'),
(9, 20, '后台操作日志', '', 'glyphicon glyphicon-console', 'admin/log/index', '', '_self', 50, 1, 0, '2017-03-23 23:49:31'),
(19, 20, '权限管理', '', 'fa fa-user-secret', 'admin/auth/index', '', '_self', 10, 1, 0, '2015-11-16 21:18:12'),
(20, 165, '权限', '', '', '#', '', '_self', 200, 1, 0, '2016-03-14 02:11:41'),
(21, 20, '系统菜单', '', 'glyphicon glyphicon-menu-hamburger', 'admin/menu/index', '', '_self', 30, 1, 0, '2015-11-16 03:16:16'),
(22, 20, '节点管理', '', 'fa fa-ellipsis-v', 'admin/node/index', '', '_self', 20, 1, 0, '2015-11-16 03:16:16'),
(29, 20, '系统用户', '', 'fa fa-users', 'admin/user/index', '', '_self', 40, 1, 0, '2016-10-30 22:31:40'),
(61, 0, '小程序', '', 'fa fa-wechat', '#', '', '_self', 8000, 1, 0, '2017-03-28 19:00:21'),
(62, 61, '微信对接配置', '', '', '#', '', '_self', 100, 1, 0, '2017-03-28 19:03:38'),
(63, 62, '微信接口配置\r\n', '', 'fa fa-usb', 'wechat/config/index', '', '_self', 10, 1, 0, '2017-03-28 19:04:44'),
(65, 61, '微信粉丝管理', '', '', '#', '', '_self', 300, 1, 0, '2017-03-28 19:08:32'),
(66, 65, '粉丝标签', '', 'fa fa-tags', 'wechat/tags/index', '', '_self', 10, 1, 0, '2017-03-28 19:09:41'),
(67, 65, '已关注粉丝', '', 'fa fa-wechat', 'wechat/fans/index', '', '_self', 20, 1, 0, '2017-03-28 19:10:07'),
(68, 61, '微信订制', '', '', '#', '', '_self', 200, 1, 0, '2017-03-28 19:10:39'),
(69, 68, '微信菜单定制', '', 'glyphicon glyphicon-phone', 'wechat/menu/index', '', '_self', 40, 1, 0, '2017-03-28 19:11:08'),
(70, 68, '关键字管理', '', 'fa fa-paw', 'wechat/keys/index', '', '_self', 10, 1, 0, '2017-03-28 19:11:49'),
(71, 68, '关注自动回复', '', 'fa fa-commenting-o', 'wechat/keys/subscribe', '', '_self', 20, 1, 0, '2017-03-28 19:12:32'),
(81, 68, '无配置默认回复', '', 'fa fa-commenting-o', 'wechat/keys/defaults', '', '_self', 30, 1, 0, '2017-04-20 22:48:25'),
(82, 61, '素材资源管理', '', '', '#', '', '_self', 300, 1, 0, '2017-04-23 19:23:18'),
(83, 82, '添加图文', '', 'fa fa-folder-open-o', 'wechat/news/add?id=1', '', '_self', 20, 1, 0, '2017-04-23 19:23:40'),
(85, 82, '图文列表', '', 'fa fa-file-pdf-o', 'wechat/news/index', '', '_self', 10, 1, 0, '2017-04-23 19:25:45'),
(86, 65, '粉丝黑名单', '', 'fa fa-reddit-alien', 'wechat/fans/back', '', '_self', 30, 1, 0, '2017-05-05 00:17:03'),
(87, 151, '交易设置', '', 'fa fa-cart-plus', 'admin/config/order', '', '_self', 6, 1, 0, '2017-07-09 23:10:16'),
(88, 106, '提现', '', 'fa fa-product-hunt', '#', '', '_self', 0, 1, 0, '2017-07-09 23:10:37'),
(90, 138, '源码需求', '', 'fa fa-creative-commons', 'admin/demand/code', '', '_self', 1, 1, 0, '2017-07-10 02:45:47'),
(91, 138, '域名需求', '', 'fa fa-at', 'admin/demand/domain', '', '_self', 3, 1, 0, '2017-07-10 02:56:59'),
(92, 121, '资讯', '', 'fa fa-book', '#', '', '_self', 1, 1, 0, '2017-07-10 02:57:22'),
(93, 143, '商品举报', '', 'fa fa-codiepie', 'admin/goods/report', '', '_self', 0, 1, 0, '2017-07-27 23:19:44'),
(95, 0, '通道', '', 'fa fa-product-hunt', '#', '', '_self', 9001, 0, 0, '2018-01-17 19:08:57'),
(97, 95, '支付接口管理', '', 'fa fa-futbol-o', 'manage/channel/index', '', '_self', 0, 0, 0, '2018-01-17 19:09:53'),
(99, 151, '短信配置', '', 'fa fa-envelope-o', 'manage/sms/index', '', '_self', 9, 1, 0, '2018-01-17 19:36:42'),
(100, 151, '邮件配置', '', 'fa fa-envelope-o', 'manage/email/index', '', '_self', 10, 1, 0, '2018-01-18 21:45:19'),
(101, 0, '会员', '', 'fa fa-users', '#', '', '_self', 4000, 1, 0, '2018-01-22 18:46:59'),
(102, 158, '商户管理', '', 'fa fa-users', 'admin/member/index', '', '_self', 0, 1, 0, '2018-01-22 18:47:40'),
(103, 0, '订单', '', 'fa fa-bar-chart', '#', '', '_self', 3000, 1, 0, '2018-01-23 00:47:46'),
(104, 136, '订单列表', '', 'fa fa-list-ol', 'manage/order/index', '', '_self', 0, 1, 0, '2018-01-23 00:48:09'),
(105, 136, '金额变动记录', '', 'fa fa-exchange', 'manage/log/user_money', '', '_self', 100, 1, 0, '2018-01-23 23:02:50'),
(106, 0, '财务', '', 'fa fa-google-wallet', '#', '', '_self', 6000, 1, 0, '2018-01-23 23:17:39'),
(107, 88, '提现管理', '', 'fa fa-cny', 'admin/cashed/index', '', '_self', 0, 1, 0, '2018-01-23 23:18:06'),
(108, 4, '系统主页', '', 'fa fa-home', 'admin/index/main', '', '_self', 0, 1, 0, '2018-01-25 22:19:38'),
(109, 88, '提现配置', '', 'fa fa-google-wallet', 'manage/cash/config', '', '_self', 70, 1, 0, '2018-01-28 23:29:43'),
(110, 151, '站点信息', '', 'glyphicon glyphicon-info-sign', 'manage/site/info', '', '_self', 2, 1, 0, '2018-01-29 00:26:24'),
(111, 151, '域名设置', '', 'fa fa-at', 'manage/site/domain', '', '_self', 3, 1, 0, '2018-01-29 00:47:15'),
(112, 159, '注册设置', '', 'fa fa-cog', 'manage/site/register', '', '_self', 1, 1, 0, '2018-01-29 02:45:43'),
(113, 151, '字词过滤', '', 'fa fa-filter', 'manage/site/wordfilter', '', '_self', 8, 1, 0, '2018-01-29 22:50:48'),
(114, 136, '商户分析', '', 'fa fa-area-chart', 'manage/order/merchant', '', '_self', 40, 1, 0, '2018-01-30 19:32:00'),
(115, 136, '渠道分析', '', 'fa fa-area-chart', 'manage/order/channel', '', '_self', 50, 1, 0, '2018-01-30 19:32:29'),
(116, 136, '实时数据', '', 'fa fa-area-chart', 'manage/order/hour', '', '_self', 60, 1, 0, '2018-01-30 19:32:57'),
(117, 0, '商品', '', 'fa fa-shopping-bag', '#', '', '_self', 2000, 1, 0, '2018-02-01 02:43:51'),
(118, 117, '商品', '', 'fa fa-shopping-bag', 'admin/goods/code', '', '_self', 1, 1, 0, '2018-02-01 02:44:10'),
(119, 136, '投诉管理', '', 'fa fa-bullhorn', 'manage/complaint/index', '', '_self', 20, 1, 0, '2018-02-01 23:34:06'),
(121, 0, '店铺', '', 'fa fa-file-text', '#', '', '_self', 5000, 1, 0, '2018-02-08 18:38:43'),
(122, 92, '资讯管理', '', 'fa fa-file-text', 'manage/article/index', '', '_self', 1, 1, 0, '2018-02-08 18:44:51'),
(123, 92, '分类管理', '', 'fa fa-bars', 'manage/article_category/index', '', '_self', 5, 1, 0, '2018-03-05 08:13:26'),
(124, 151, '备份管理', '', 'fa fa-database', 'manage/backup/index', '', '_self', 11, 1, 0, '2018-03-12 03:31:11'),
(125, 130, '前台导航', '', 'fa fa-navicon', 'admin/nav/index', '', '_self', 1, 1, 0, '2018-03-23 01:08:38'),
(126, 158, '登录解锁', '', 'fa fa-unlock', '/manage/user/unlock', '', '_self', 0, 1, 0, '2018-03-26 19:15:00'),
(127, 20, '商户操作日志', '', 'fa fa-thumb-tack', '/admin/mlog/index', '', '_self', 60, 1, 0, '2018-03-27 00:19:10'),
(128, 159, '推广设置', '', 'fa fa-bullhorn', '/manage/site/spread', '', '_self', 2, 1, 0, '2018-03-27 03:19:29'),
(129, 159, '邀请码管理', '', 'fa fa-user-plus', '/manage/invite_code/index', '', '_self', 3, 1, 0, '2018-03-27 03:54:29'),
(130, 121, '管理', '', 'fa fa-bullseye', '#', '', '_self', 4, 1, 0, '2018-04-19 17:03:50'),
(131, 159, '费率分组管理', '', 'fa fa-mixcloud', 'manage/rate/index', '', '_self', 4, 1, 0, '2018-06-19 18:53:02'),
(132, 130, '数据删除', '', 'fa fa-book', 'manage/content/del', '', '_self', 5, 1, 0, '2018-10-08 08:49:42'),
(133, 95, '安装支付接口', '', 'fa fa-futbol-o', 'manage/channel/index?is_install=0', '', '_self', 0, 0, 0, '2018-01-17 19:09:53'),
(135, 88, '代付渠道管理', '', 'fa fa-futbol-o', 'manage/cashChannel/index', '', '_self', 1000, 1, 0, '2018-08-07 07:50:38'),
(136, 103, '订单', '', 'fa fa-book', '#', '', '_self', 2, 1, 0, '2020-06-28 10:02:49'),
(137, 118, '源码管理', '', 'fa fa-shopping-bag', '/admin/goods/code', '', '_self', 1, 1, 0, '2020-07-04 08:45:57'),
(138, 117, '需求', '', 'fa fa-edge', 'admin/demand/code', '', '_self', 2, 1, 0, '2020-08-20 11:17:31'),
(139, 121, '帮助', '', 'fa fa-edge', '#', '', '_self', 80, 1, 0, '2020-08-20 11:17:58'),
(140, 118, '服务管理', '', 'fa fa-reddit-alien', '/admin/goods/serve', '', '_self', 2, 1, 0, '2020-08-20 11:21:50'),
(141, 138, '任务需求', '', 'fa fa-stop-circle', 'admin/demand/task', '', '_self', 4, 1, 0, '2020-08-20 11:23:39'),
(142, 118, '网站管理', '', 'fa fa-product-hunt', '/admin/goods/web', '', '_self', 3, 1, 0, '2020-08-20 11:33:04'),
(143, 117, '管理', '', 'fa fa-reddit-alien', '/admin/goods/code/shensu', '', '_self', 4, 1, 0, '2020-08-20 11:34:24'),
(144, 0, '应用', '', 'fa fa-cloud', '#', '', '_self', 7000, 1, 0, '2020-09-19 11:56:42'),
(145, 144, '应用', '', 'fa fa-modx', 'admin/store/index', '', '_self', 0, 1, 0, '2020-09-19 11:57:04'),
(146, 147, '授权状态', '', 'fa fa-mixcloud', 'admin/store/auth', '', '_self', 0, 1, 0, '2020-09-19 11:57:26'),
(147, 144, '云端', '', 'fa fa-modx', 'admin/store/auth', '', '_self', 0, 1, 0, '2020-09-19 12:11:21'),
(148, 145, '应用列表', '', 'fa fa-stop-circle', 'admin/plugin/index', '', '_self', 0, 1, 0, '2020-09-19 12:12:46'),
(149, 147, '更新检测', '', 'fa fa-cloud-download', 'admin/store/update', '', '_self', 0, 1, 0, '2020-09-19 12:16:21'),
(150, 130, '友情链接', '', 'fa fa-share-alt', 'admin/link/index', '', '_self', 4, 1, 0, '2020-10-09 03:34:43'),
(151, 165, '设置', '', '', '#', '', '_self', 101, 1, 0, '2020-10-09 03:36:50'),
(152, 130, '合作伙伴', '', 'fa fa-modx', 'admin/link/partner', '', '_self', 3, 1, 0, '2020-10-09 03:38:16'),
(153, 130, '幻灯管理', '', 'fa fa-book', 'admin/link/slide', '', '_self', 2, 1, 0, '2020-10-09 03:40:29'),
(154, 136, '资金明细', '', 'fa fa-shopping-bag', 'admin/Order/money', '', '_self', 1, 1, 0, '2020-10-10 08:02:50'),
(155, 136, '充值消费', '', 'fa fa-credit-card-alt', 'admin/Order/pay', '', '_self', 2, 1, 0, '2020-10-10 08:06:30'),
(156, 151, '支付设置', '', 'fa fa-cny', 'admin/config/pay', '', '_self', 4, 1, 0, '2020-10-11 05:22:41'),
(157, 103, '管理', '', 'fa fa-at', '#', '', '_self', 2, 0, 0, '2020-10-11 05:23:17'),
(158, 101, '会员', '', 'fa fa-television', '#', '', '_self', 1, 1, 0, '2020-10-11 05:26:52'),
(159, 101, '管理', '', 'fa fa-paper-plane', '#', '', '_self', 2, 1, 0, '2020-10-11 05:27:26'),
(160, 138, '网站需求', '', 'fa fa-battery-full', 'admin/demand/web', '', '_self', 2, 1, 0, '2020-10-11 05:28:30'),
(161, 118, '域名管理', '', 'fa fa-battery-full', '/admin/goods/domain', '', '_self', 1, 1, 0, '2020-10-11 05:31:23'),
(162, 130, '搜索关键词', '', 'fa fa-clock-o', '/admin/config/soukey', '', '_self', 3, 1, 0, '2020-10-11 05:32:19'),
(163, 158, '快捷登录', '', 'fa fa-subway', '/admin/member/oauth', '', '_self', 2, 1, 0, '2020-10-11 05:32:57'),
(164, 143, '商品申诉', '', 'fa fa-battery-4', '/admin/goods/code/shensu', '', '_self', 0, 1, 0, '2020-10-11 05:34:46'),
(165, 0, '设置', '', 'fa fa-gear', '#', '', '_self', 9000, 1, 0, '2020-10-24 09:57:50'),
(166, 139, '帮助分类', '', 'fa fa-check-circle', 'admin/help/index', '', '_self', 2, 1, 0, '2020-12-25 11:23:57'),
(167, 139, '帮助管理', '', 'fa fa-book', 'admin/help/index', '', '_self', 1, 1, 0, '2020-12-25 11:24:20'),
(168, 121, '公告', '', 'fa fa-product-hunt', '#', '', '_self', 0, 1, 0, '2020-12-25 14:24:54'),
(169, 168, '公告管理', '', 'fa fa-comment', 'admin/gonggao/index', '', '_self', 0, 1, 0, '2020-12-25 14:26:07'),
(170, 151, '商品设置', '', 'fa fa-save', 'admin/config/goods', '', '_self', 5, 1, 0, '2020-12-26 12:55:02');

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_node`
--

CREATE TABLE IF NOT EXISTS `shop_system_node` (
  `id` int(11) unsigned NOT NULL,
  `node` varchar(100) DEFAULT NULL COMMENT '节点代码',
  `title` varchar(500) DEFAULT NULL COMMENT '节点标题',
  `is_menu` tinyint(1) unsigned DEFAULT '0' COMMENT '是否可设置为菜单',
  `is_auth` tinyint(1) unsigned DEFAULT '1' COMMENT '是否启动RBAC权限控制',
  `is_login` tinyint(1) unsigned DEFAULT '1' COMMENT '是否启动登录控制',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统节点表';

--
-- 转存表中的数据 `shop_system_node`
--

INSERT INTO `shop_system_node` (`id`, `node`, `title`, `is_menu`, `is_auth`, `is_login`, `create_at`) VALUES
(131, 'admin/auth/index', '权限列表', 1, 1, 1, '2017-08-22 23:45:42'),
(132, 'admin', '后台管理', 0, 1, 1, '2017-08-22 23:45:44'),
(133, 'admin/auth/apply', '节点授权', 0, 1, 1, '2017-08-23 00:05:18'),
(134, 'admin/auth/add', '添加授权', 0, 1, 1, '2017-08-23 00:05:19'),
(135, 'admin/auth/edit', '编辑权限', 0, 1, 1, '2017-08-23 00:05:19'),
(136, 'admin/auth/forbid', '禁用权限', 0, 1, 1, '2017-08-23 00:05:20'),
(137, 'admin/auth/resume', '启用权限', 0, 1, 1, '2017-08-23 00:05:20'),
(138, 'admin/auth/del', '删除权限', 0, 1, 1, '2017-08-23 00:05:21'),
(139, 'admin/config/index', '参数配置', 1, 1, 1, '2017-08-23 00:05:22'),
(140, 'admin/config/file', '文件配置', 1, 1, 1, '2017-08-23 00:05:22'),
(141, 'admin/log/index', '日志列表', 1, 1, 1, '2017-08-23 00:05:23'),
(142, 'admin/log/del', '删除日志', 0, 1, 1, '2017-08-23 00:05:24'),
(143, 'admin/menu/index', '菜单列表', 1, 1, 1, '2017-08-23 00:05:25'),
(144, 'admin/menu/add', '添加菜单', 0, 1, 1, '2017-08-23 00:05:25'),
(145, 'admin/menu/edit', '编辑菜单', 0, 1, 1, '2017-08-23 00:05:26'),
(146, 'admin/menu/del', '删除菜单', 0, 1, 1, '2017-08-23 00:05:26'),
(147, 'admin/menu/forbid', '禁用菜单', 0, 1, 1, '2017-08-23 00:05:27'),
(148, 'admin/menu/resume', '启用菜单', 0, 1, 1, '2017-08-23 00:05:28'),
(149, 'admin/node/index', '节点列表', 1, 1, 1, '2017-08-23 00:05:29'),
(150, 'admin/node/save', '节点更新', 0, 1, 1, '2017-08-23 00:05:30'),
(151, 'admin/user/index', '用户管理', 1, 1, 1, '2017-08-23 00:05:31'),
(152, 'admin/user/auth', '用户授权', 0, 1, 1, '2017-08-23 00:05:32'),
(153, 'admin/user/add', '添加用户', 0, 1, 1, '2017-08-23 00:05:33'),
(154, 'admin/user/edit', '编辑用户', 0, 1, 1, '2017-08-23 00:05:33'),
(155, 'admin/user/pass', '用户密码', 0, 1, 1, '2017-08-23 00:05:34'),
(156, 'admin/user/del', '删除用户', 0, 1, 1, '2017-08-23 00:05:34'),
(157, 'admin/user/forbid', '禁用用户', 0, 1, 1, '2017-08-23 00:05:34'),
(158, 'admin/user/resume', '启用用户', 0, 1, 1, '2017-08-23 00:05:35'),
(159, 'demo/plugs/file', '文件上传', 1, 0, 0, '2017-08-23 00:05:36'),
(160, 'demo/plugs/region', '区域选择', 1, 0, 0, '2017-08-23 00:05:36'),
(161, 'demo/plugs/editor', '富文本器', 1, 0, 0, '2017-08-23 00:05:37'),
(162, 'wechat/config/index', '微信参数', 1, 1, 1, '2017-08-23 00:05:37'),
(163, 'wechat/config/pay', '微信支付', 0, 1, 1, '2017-08-23 00:05:38'),
(164, 'wechat/fans/index', '粉丝列表', 1, 1, 1, '2017-08-23 00:05:39'),
(165, 'wechat/fans/back', '粉丝黑名单', 1, 1, 1, '2017-08-23 00:05:39'),
(166, 'wechat/fans/backadd', '移入黑名单', 0, 1, 1, '2017-08-23 00:05:40'),
(167, 'wechat/fans/tagset', '设置标签', 0, 1, 1, '2017-08-23 00:05:41'),
(168, 'wechat/fans/backdel', '移出黑名单', 0, 1, 1, '2017-08-23 00:05:41'),
(169, 'wechat/fans/tagadd', '添加标签', 0, 1, 1, '2017-08-23 00:05:41'),
(170, 'wechat/fans/tagdel', '删除标签', 0, 1, 1, '2017-08-23 00:05:42'),
(171, 'wechat/fans/sync', '同步粉丝', 0, 1, 1, '2017-08-23 00:05:43'),
(172, 'wechat/keys/index', '关键字列表', 1, 1, 1, '2017-08-23 00:05:44'),
(173, 'wechat/keys/add', '添加关键字', 0, 1, 1, '2017-08-23 00:05:44'),
(174, 'wechat/keys/edit', '编辑关键字', 0, 1, 1, '2017-08-23 00:05:45'),
(175, 'wechat/keys/del', '删除关键字', 0, 1, 1, '2017-08-23 00:05:45'),
(176, 'wechat/keys/forbid', '禁用关键字', 0, 1, 1, '2017-08-23 00:05:46'),
(177, 'wechat/keys/resume', '启用关键字', 0, 1, 1, '2017-08-23 00:05:46'),
(178, 'wechat/keys/subscribe', '关注默认回复', 0, 1, 1, '2017-08-23 00:05:48'),
(179, 'wechat/keys/defaults', '默认响应回复', 0, 1, 1, '2017-08-23 00:05:49'),
(180, 'wechat/menu/index', '微信菜单', 1, 1, 1, '2017-08-23 00:05:51'),
(181, 'wechat/menu/edit', '发布微信菜单', 0, 1, 1, '2017-08-23 00:05:51'),
(182, 'wechat/menu/cancel', '取消微信菜单', 0, 1, 1, '2017-08-23 00:05:52'),
(183, 'wechat/news/index', '微信图文列表', 1, 1, 1, '2017-08-23 00:05:52'),
(184, 'wechat/news/select', '微信图文选择', 0, 1, 1, '2017-08-23 00:05:53'),
(185, 'wechat/news/image', '微信图片选择', 0, 1, 1, '2017-08-23 00:05:53'),
(186, 'wechat/news/add', '添加图文', 0, 1, 1, '2017-08-23 00:05:54'),
(187, 'wechat/news/edit', '编辑图文', 0, 1, 1, '2017-08-23 00:05:56'),
(188, 'wechat/news/del', '删除图文', 0, 1, 1, '2017-08-23 00:05:56'),
(189, 'wechat/news/push', '推送图文', 0, 1, 1, '2017-08-23 00:05:56'),
(190, 'wechat/tags/index', '微信标签列表', 1, 1, 1, '2017-08-23 00:05:58'),
(191, 'wechat/tags/add', '添加微信标签', 0, 1, 1, '2017-08-23 00:05:58'),
(192, 'wechat/tags/edit', '编辑微信标签', 0, 1, 1, '2017-08-23 00:05:58'),
(193, 'wechat/tags/sync', '同步微信标签', 0, 1, 1, '2017-08-23 00:05:59'),
(194, 'admin/auth', '权限管理', 0, 1, 1, '2017-08-23 00:06:58'),
(195, 'admin/config', '系统配置', 0, 1, 1, '2017-08-23 00:07:34'),
(196, 'admin/log', '系统日志', 0, 1, 1, '2017-08-23 00:07:46'),
(197, 'admin/menu', '系统菜单', 0, 1, 1, '2017-08-23 00:08:02'),
(198, 'admin/node', '系统节点', 0, 1, 1, '2017-08-23 00:08:44'),
(199, 'admin/user', '系统用户', 0, 1, 1, '2017-08-23 00:09:43'),
(200, 'demo', '插件案例', 0, 1, 1, '2017-08-23 00:10:43'),
(201, 'demo/plugs', '插件案例', 0, 1, 1, '2017-08-23 00:10:51'),
(202, 'wechat', '微信管理', 0, 1, 1, '2017-08-23 00:11:13'),
(203, 'wechat/config', '微信配置', 0, 1, 1, '2017-08-23 00:11:19'),
(204, 'wechat/fans', '粉丝管理', 0, 1, 1, '2017-08-23 00:11:36'),
(205, 'wechat/keys', '关键字管理', 0, 1, 1, '2017-08-23 00:13:00'),
(206, 'wechat/menu', '微信菜单管理', 0, 1, 1, '2017-08-23 00:14:11'),
(207, 'wechat/news', '微信图文管理', 0, 1, 1, '2017-08-23 00:14:40'),
(208, 'wechat/tags', '微信标签管理', 0, 1, 1, '2017-08-23 00:15:25'),
(209, 'manage/channel/index', '供应商管理', 0, 1, 1, '2018-01-18 21:53:53'),
(210, 'manage/channel/add', '添加供应商', 0, 1, 1, '2018-01-18 21:53:54'),
(211, 'manage/channel/edit', '修改供应商', 0, 1, 1, '2018-01-18 21:53:54'),
(212, 'manage/channel/del', '删除供应商', 0, 1, 1, '2018-01-18 21:53:54'),
(213, 'manage/channel/change_status', '修改供应商状态', 0, 1, 1, '2018-01-18 21:53:55'),
(214, 'manage/channel/getlistbypaytype', '根据支付类型获取支付供应商列表', 0, 1, 1, '2018-01-18 21:53:55'),
(215, 'manage/channelaccount/add', '添加供应商账号', 0, 1, 1, '2018-01-18 21:54:03'),
(216, 'manage/channelaccount/index', '供应商账号管理', 0, 1, 1, '2018-01-18 21:54:04'),
(217, 'manage/channelaccount/edit', '修改供应商账号', 0, 1, 1, '2018-01-18 21:54:05'),
(218, 'manage/channelaccount/del', '删除供应商账号', 0, 1, 1, '2018-01-18 21:54:06'),
(219, 'manage/channelaccount/clear', '清除供应商账号额度', 0, 1, 1, '2018-01-18 21:54:07'),
(220, 'manage/channelaccount/change_status', '修改供应商账号状态', 0, 1, 1, '2018-01-18 21:54:07'),
(221, 'manage/channelaccount/getfields', '获取渠道账户字段', 0, 1, 1, '2018-01-18 21:54:08'),
(222, 'manage/email/index', '邮件配置', 0, 1, 1, '2018-01-18 21:54:10'),
(223, 'manage/email/test', '发信测试', 0, 1, 1, '2018-01-18 21:54:10'),
(224, 'manage/product/index', '支付产品管理', 0, 1, 1, '2018-01-18 21:54:11'),
(225, 'manage/product/add', '添加支付产品', 0, 1, 1, '2018-01-18 21:54:12'),
(226, 'manage/product/edit', '编辑支付产品', 0, 1, 1, '2018-01-18 21:54:12'),
(227, 'manage/product/del', '删除支付产品', 0, 1, 1, '2018-01-18 21:54:14'),
(228, 'manage/product/change_status', '修改支付产品状态', 0, 1, 1, '2018-01-18 21:54:14'),
(229, 'manage/sms/index', '短信配置', 0, 1, 1, '2018-01-18 21:54:15'),
(230, 'manage/cash/index', '提现列表', 0, 1, 1, '2018-01-25 00:47:20'),
(231, 'manage/cash/detail', '提现申请详情', 0, 1, 1, '2018-01-25 00:47:20'),
(232, 'manage/cash/payqrcode', '', 0, 1, 1, '2018-01-25 00:47:21'),
(233, 'manage/log/user_money', '金额变动记录', 0, 1, 1, '2018-01-25 00:47:24'),
(234, 'manage/order/index', '订单列表', 0, 1, 1, '2018-01-25 00:47:25'),
(235, 'manage/order/detail', '订单详情', 0, 1, 1, '2018-01-25 00:47:26'),
(236, 'manage/user/index', '商户管理', 0, 1, 1, '2018-01-25 00:47:29'),
(237, 'manage/user/change_status', '修改商户审核状态', 0, 1, 1, '2018-01-25 00:47:30'),
(238, 'manage/user/detail', '查看商户详情', 0, 1, 1, '2018-01-25 00:47:30'),
(239, 'manage/user/add', '添加商户', 0, 1, 1, '2018-01-25 00:47:31'),
(240, 'manage/user/edit', '编辑商户', 0, 1, 1, '2018-01-25 00:47:31'),
(241, 'manage/user/del', '删除商户', 0, 1, 1, '2018-01-25 00:47:32'),
(242, 'manage/user/manage_money', '商户资金管理', 0, 1, 1, '2018-01-25 00:47:32'),
(243, 'manage/user/rate', '设置商户费率', 0, 1, 1, '2018-01-25 00:47:33'),
(244, 'manage/cash/config', '提现配置', 0, 1, 1, '2018-02-01 01:00:28'),
(245, 'manage/index/main', '主页', 0, 1, 1, '2018-02-01 01:00:33'),
(246, 'manage/order/merchant', '商户分析', 0, 1, 1, '2018-02-01 01:00:35'),
(247, 'manage/order/channel', '渠道分析', 0, 1, 1, '2018-02-01 01:00:36'),
(248, 'manage/order/hour', '实时数据', 0, 1, 1, '2018-02-01 01:00:36'),
(249, 'manage/site/info', '站点信息配置', 0, 1, 1, '2018-02-01 01:00:40'),
(250, 'manage/site/domain', '域名设置', 0, 1, 1, '2018-02-01 01:00:40'),
(251, 'manage/site/register', '注册设置', 0, 1, 1, '2018-02-01 01:00:41'),
(252, 'manage/site/wordfilter', '字词过滤', 0, 1, 1, '2018-02-01 01:00:41'),
(253, 'manage/user/change_freeze_status', '修改商户冻结状态', 0, 1, 1, '2018-02-01 01:00:43'),
(254, 'manage/user/login', '商户登录', 0, 1, 1, '2018-02-01 01:00:45'),
(255, 'manage/user/message', '商户站内信', 0, 1, 1, '2018-02-01 01:00:45'),
(256, 'merchant/cash/index', '', 0, 0, 0, '2018-02-01 01:00:48'),
(257, 'manage/goods/index', '商品管理', 0, 1, 1, '2018-02-01 03:33:28'),
(258, 'manage/goods/change_status', '修改商品上架状态', 0, 1, 1, '2018-02-01 03:33:29'),
(259, 'manage/complaint/index', '投诉管理', 0, 1, 1, '2018-02-02 03:46:10'),
(260, 'manage/complaint/change_status', '修改投诉处理状态', 0, 1, 1, '2018-02-02 03:46:11'),
(261, 'manage/complaint/change_admin_read', '修改投诉读取状态', 0, 1, 1, '2018-02-02 03:46:11'),
(262, 'manage/complaint/del', '删除投诉', 0, 1, 1, '2018-02-02 03:46:12'),
(263, 'manage/order/change_freeze_status', '修改订单冻结状态', 0, 1, 1, '2018-02-05 02:24:23'),
(264, 'manage/user/loginlog', '商户登录日志', 0, 1, 1, '2018-02-05 02:24:31'),
(265, 'merchant/user/closelink', '', 0, 0, 0, '2018-03-19 22:22:03'),
(266, 'merchant/goodscategory', '', 0, 0, 0, '2018-03-19 22:22:32'),
(267, 'merchant/cash/apply', '', 0, 0, 0, '2018-03-19 22:22:35'),
(268, 'merchant/cash', '', 0, 0, 0, '2018-03-19 22:22:38'),
(269, 'merchant', '', 0, 0, 0, '2018-03-19 22:23:00'),
(270, 'manage/article/add', '添加文章', 0, 1, 1, '2018-03-19 22:23:38'),
(271, 'manage/article/edit', '编辑文章', 0, 1, 1, '2018-03-19 22:23:39'),
(272, 'manage/article/index', '内容管理', 0, 1, 1, '2018-03-19 22:23:39'),
(273, 'manage/article/change_status', '修改文章状态', 0, 1, 1, '2018-03-19 22:23:40'),
(274, 'manage/article/del', '删除文章', 0, 1, 1, '2018-03-19 22:23:41'),
(275, 'manage/articlecategory/index', '文章分类管理', 0, 1, 1, '2018-03-19 22:23:53'),
(276, 'manage/articlecategory/add', '添加文章分类', 0, 1, 1, '2018-03-19 22:23:53'),
(277, 'manage/articlecategory/edit', '编辑文章分类', 0, 1, 1, '2018-03-19 22:23:54'),
(278, 'manage/articlecategory/change_status', '修改文章分类状态', 0, 1, 1, '2018-03-19 22:23:54'),
(279, 'manage/articlecategory/del', '删除文章分类', 0, 1, 1, '2018-03-19 22:23:55'),
(280, 'manage/backup/index', '备份管理', 0, 1, 1, '2018-03-19 22:24:04'),
(281, 'manage/backup/tablist', '获取数据表', 0, 1, 1, '2018-03-19 22:24:05'),
(282, 'manage/backup/backall', '备份数据库', 0, 1, 1, '2018-03-19 22:24:06'),
(283, 'manage/backup/backtables', '按表备份', 0, 1, 1, '2018-03-19 22:24:07'),
(284, 'manage/backup/recover', '还原数据库', 0, 1, 1, '2018-03-19 22:24:07'),
(285, 'manage/backup/downloadbak', '下载备份文件', 0, 1, 1, '2018-03-19 22:24:08'),
(286, 'manage/backup/deletebak', '删除备份', 0, 1, 1, '2018-03-19 22:24:09'),
(287, 'manage/article', '内容管理', 0, 1, 1, '2018-03-22 00:32:51'),
(288, 'admin/auth/google', '', 0, 0, 0, '2018-03-22 00:33:13'),
(289, 'admin/auth/bindgoogle', '生成绑定谷歌身份验证器二维码', 0, 0, 0, '2018-03-22 00:39:13'),
(290, 'manage/user', '用户管理', 0, 1, 1, '2018-03-22 00:41:20'),
(291, 'manage/sms', '短信配置', 0, 1, 1, '2018-03-22 00:44:54'),
(292, 'manage/site', '站点信息', 0, 1, 1, '2018-03-22 00:45:04'),
(293, 'manage/product', '支付产品管理', 0, 1, 1, '2018-03-22 00:47:47'),
(294, 'manage/order/del_batch', '批量删除无效订单', 0, 1, 1, '2018-03-22 00:48:42'),
(295, 'manage/order/del', '删除无效订单', 0, 1, 1, '2018-03-22 00:48:43'),
(296, 'manage/order', '交易明细', 0, 1, 1, '2018-03-22 00:50:10'),
(297, 'manage/log', '金额变动记录', 0, 1, 1, '2018-03-22 00:51:25'),
(298, 'manage/index', '主页', 0, 1, 1, '2018-03-22 00:51:55'),
(299, 'manage/goods', '商品管理', 0, 1, 1, '2018-03-22 00:52:09'),
(300, 'manage/email', '邮件配置', 0, 1, 1, '2018-03-22 00:53:07'),
(301, 'manage/complaint', '投诉管理', 0, 1, 1, '2018-03-22 00:54:06'),
(302, 'manage/channelaccount', '供应商账号管理', 0, 1, 1, '2018-03-22 00:54:52'),
(303, 'manage/channel', '供应商管理', 0, 1, 1, '2018-03-22 02:45:06'),
(304, 'manage/cash', '提现管理', 0, 1, 1, '2018-03-22 02:46:43'),
(305, 'manage/backup', '备份管理', 0, 1, 1, '2018-03-22 02:49:14'),
(306, 'manage/articlecategory', '文章分类管理', 0, 1, 1, '2018-03-22 02:53:43'),
(307, 'manage/goods/change_trade_no_status', '', 0, 1, 1, '2018-04-19 17:04:48'),
(308, 'shop/shop/index', '', 0, 0, 0, '2018-06-21 02:19:27'),
(309, 'shop/shop/category', '', 0, 0, 0, '2018-06-21 02:19:28'),
(310, 'shop/shop/goods', '', 0, 0, 0, '2018-06-21 02:20:39'),
(311, 'shop/shop/getgoodslist', '', 0, 0, 0, '2018-06-21 02:20:40'),
(312, 'shop/shop/getgoodsinfo', '', 0, 0, 0, '2018-06-21 02:20:41'),
(313, 'shop/shop/getrate', '', 0, 0, 0, '2018-06-21 02:20:41'),
(314, 'shop/shop/getdiscounts', '', 0, 0, 0, '2018-06-21 02:20:42'),
(315, 'shop/shop/getdiscount', '', 0, 0, 0, '2018-06-21 02:20:43'),
(316, 'shop/shop/checkvisitpassword', '', 0, 0, 0, '2018-06-21 02:20:43'),
(317, 'shop/shop/checkcoupon', '', 0, 0, 0, '2018-06-21 02:20:44'),
(318, 'member', '会员中心', 0, 1, 1, '2020-08-07 14:04:15'),
(319, 'member/base', '会员全局', 0, 1, 1, '2020-08-07 14:04:45'),
(320, 'member/base/page', '会员分页', 0, 0, 0, '2020-08-07 14:05:49'),
(321, 'member/bond', '保证金', 0, 1, 1, '2020-08-07 14:07:38'),
(322, 'member/bond/index', '保证金首页', 0, 0, 0, '2020-08-07 14:07:41'),
(323, 'member/bond/state', '', 0, 0, 0, '2020-08-07 14:07:48'),
(324, 'member/buy', '买家订单', 0, 1, 1, '2020-08-07 14:08:03'),
(325, 'member/buy/index', '买家订单', 0, 0, 0, '2020-08-07 14:08:12'),
(326, 'member/buy/code', '', 0, 1, 1, '2020-08-07 14:08:38');

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_sequence`
--

CREATE TABLE IF NOT EXISTS `shop_system_sequence` (
  `id` bigint(20) NOT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT '序号类型',
  `sequence` char(50) NOT NULL COMMENT '序号值',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统序号表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_system_user`
--

CREATE TABLE IF NOT EXISTS `shop_system_user` (
  `id` bigint(20) unsigned NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '用户登录密码',
  `qq` varchar(16) DEFAULT NULL COMMENT '联系QQ',
  `mail` varchar(32) DEFAULT NULL COMMENT '联系邮箱',
  `phone` varchar(16) DEFAULT NULL COMMENT '联系手机号',
  `desc` varchar(255) DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) unsigned DEFAULT '0' COMMENT '登录次数',
  `login_at` datetime DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `authorize` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '删除状态(1:删除,0:未删)',
  `create_by` bigint(20) unsigned DEFAULT NULL COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `google_secret_key` varchar(128) DEFAULT '' COMMENT '谷歌令牌密钥'
) ENGINE=InnoDB AUTO_INCREMENT=10006 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

--
-- 转存表中的数据 `shop_system_user`
--

INSERT INTO `shop_system_user` (`id`, `username`, `password`, `qq`, `mail`, `phone`, `desc`, `login_num`, `login_at`, `status`, `authorize`, `is_deleted`, `create_by`, `create_at`, `google_secret_key`) VALUES
(10005, 'admin', '7fef6171469e80d32c0559f88b377245', '', '66983239@qq.com', '13800138000', 'demo', 454, '2021-08-18 19:45:19', 1, '3', 0, 0, '2018-05-02 00:40:09', '');

-- --------------------------------------------------------

--
-- 表的结构 `shop_task`
--

CREATE TABLE IF NOT EXISTS `shop_task` (
  `id` int(11) NOT NULL,
  `bh` char(50) DEFAULT NULL,
  `ubh` char(50) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `txt` text,
  `qqtype` int(1) DEFAULT NULL,
  `phonetype` int(1) DEFAULT NULL,
  `moneytype` int(10) DEFAULT NULL,
  `money` int(10) DEFAULT NULL,
  `am` varchar(100) NOT NULL,
  `dm` varchar(100) NOT NULL,
  `typesxid` int(10) NOT NULL,
  `djl` int(10) DEFAULT '1',
  `hfl` int(10) DEFAULT '0',
  `num` int(10) DEFAULT NULL COMMENT '有效期',
  `cyc` char(10) DEFAULT NULL,
  `uip` char(50) DEFAULT NULL,
  `lastgx` datetime DEFAULT NULL,
  `mail` int(1) DEFAULT '0',
  `filebh` varchar(250) DEFAULT NULL,
  `zt` char(10) DEFAULT '1',
  `uptime` datetime DEFAULT NULL,
  `bidbh` int(10) DEFAULT '0',
  `xzbid` int(2) DEFAULT '0',
  `bidnum` int(4) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_tem`
--

CREATE TABLE IF NOT EXISTS `shop_tem` (
  `id` int(10) NOT NULL,
  `bh` varchar(100) NOT NULL,
  `ubh` varchar(100) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(100) NOT NULL,
  `tit` varchar(100) NOT NULL,
  `note` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `txt` text NOT NULL,
  `zt` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_tuiguang`
--

CREATE TABLE IF NOT EXISTS `shop_tuiguang` (
  `id` int(11) NOT NULL,
  `ubh` char(20) DEFAULT NULL,
  `asj` datetime DEFAULT NULL,
  `dsj` datetime DEFAULT NULL,
  `pro` char(20) DEFAULT NULL,
  `ads` int(2) DEFAULT '0',
  `jifen` int(11) DEFAULT '0',
  `money` int(11) DEFAULT '0',
  `zt` int(1) DEFAULT '2',
  `cyc` int(3) DEFAULT '0',
  `info` varchar(250) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_tuiguang`
--

INSERT INTO `shop_tuiguang` (`id`, `ubh`, `asj`, `dsj`, `pro`, `ads`, `jifen`, `money`, `zt`, `cyc`, `info`) VALUES
(1, 'u1629290430', '2021-08-18 20:59:55', '2025-09-17 20:59:55', '1', 1, 0, 200, 1, 30, NULL),
(2, 'u1629290430', '2021-08-18 20:59:58', '2025-09-17 20:59:58', '1', 2, 0, 100, 1, 30, NULL),
(3, 'u1629290430', '2021-08-18 21:00:28', '2025-10-17 21:00:10', '1', 3, 0, 360, 1, 60, NULL),
(4, 'u1629290430', '2021-08-18 21:00:14', '2025-09-17 21:00:14', '1', 9, 0, 70, 1, 30, NULL),
(5, 'u1629290430', '2021-08-18 21:00:19', '2025-09-17 21:00:19', '1', 5, 0, 50, 1, 30, NULL),
(6, 'u1629290430', '2021-08-18 21:00:24', '2025-09-17 21:00:24', '1', 4, 0, 50, 1, 30, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_type`
--

CREATE TABLE IF NOT EXISTS `shop_type` (
  `id` int(11) NOT NULL,
  `tid` int(11) DEFAULT NULL,
  `xh` int(11) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `type` char(50) DEFAULT NULL,
  `typename` varchar(100) NOT NULL,
  `name1` char(50) DEFAULT NULL,
  `name2` char(50) DEFAULT NULL,
  `tips` varchar(100) NOT NULL COMMENT '提示',
  `menu_name` varchar(100) NOT NULL COMMENT '菜单name'
) ENGINE=MyISAM AUTO_INCREMENT=297 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_type`
--

INSERT INTO `shop_type` (`id`, `tid`, `xh`, `admin`, `type`, `typename`, `name1`, `name2`, `tips`, `menu_name`) VALUES
(1, 1, 1, 1, 'code', '', '源码类型', NULL, '', 'menus'),
(2, 2, 1, 2, 'code', '', '源码类型', 'QQ非主流/图片', '', 'menus'),
(3, 3, 2, 2, 'code', '', '源码类型', '电影/视频/音乐', '', 'menus'),
(4, 4, 3, 2, 'code', '', '源码类型', '游戏/动漫/竞技', '', 'menus'),
(5, 5, 4, 2, 'code', '', '源码类型', '聊天/交友/直播', '', 'menus'),
(6, 6, 5, 2, 'code', '', '源码类型', '小说/文章/文学', '', 'menus'),
(7, 7, 6, 2, 'code', '', '源码类型', '医院/女人/健康', '', 'menus'),
(8, 8, 7, 2, 'code', '', '源码类型', '导航/网址/查询', '', 'menus'),
(9, 9, 8, 2, 'code', '', '源码类型', '淘客/网店/商城', '', 'menus'),
(10, 10, 9, 2, 'code', '', '源码类型', '门户/新闻/资讯', '', 'menus'),
(11, 11, 10, 2, 'code', '', '源码类型', '论坛/社区/问答', '', 'menus'),
(12, 12, 11, 2, 'code', '', '源码类型', '二手/B２B/分类', '', 'menus'),
(13, 13, 12, 2, 'code', '', '源码类型', '软件/下载/电脑', '', 'menus'),
(14, 14, 13, 2, 'code', '', '源码类型', '旅游/餐饮/票务', '', 'menus'),
(15, 15, 14, 2, 'code', '', '源码类型', '房产/商铺/装修', '', 'menus'),
(16, 16, 15, 2, 'code', '', '源码类型', '学校/教育/人才', '', 'menus'),
(17, 17, 16, 2, 'code', '', '源码类型', '财经/股票/金融', '', 'menus'),
(18, 18, 17, 2, 'code', '', '源码类型', '企业/公司/政府', '', 'menus'),
(19, 19, 21, 2, 'code', '', '源码类型', '行业/办公/系统', '', 'menus'),
(20, 20, 2, 1, 'code', '', '系统品牌', NULL, '', 'brand'),
(21, 21, 1, 2, 'code', '', '系统品牌', '织梦', '', 'brand'),
(22, 22, 2, 2, 'code', '', '系统品牌', '帝国', '', 'brand'),
(23, 23, 3, 2, 'code', '', '系统品牌', '新云', '', 'brand'),
(24, 24, 4, 2, 'code', '', '系统品牌', '动易', '', 'brand'),
(25, 26, 6, 2, 'code', '', '系统品牌', 'thinkphp', '', 'brand'),
(26, 28, 8, 2, 'code', '', '系统品牌', 'discuz', '', 'brand'),
(27, 29, 9, 2, 'code', '', '系统品牌', 'phpwind', 'phpwind官网：www.phpwind.net，若乱选择，无法过审同时产生纠纷您将负全责', 'brand'),
(28, 30, 10, 2, 'code', '', '系统品牌', 'ecshop', '', 'brand'),
(29, 31, 11, 2, 'code', '', '系统品牌', 'wordpress', '', 'brand'),
(30, 32, 12, 2, 'code', '', '系统品牌', 'maxcms', '', 'brand'),
(31, 34, 14, 2, 'code', '', '系统品牌', 'phpcms', 'phpcms官网：www.phpcms.cn，若乱选择，无法过审同时产生纠纷您将负全责', 'brand'),
(32, 35, 5, 2, 'code', '', '系统品牌', '苹果CMS', '', 'brand'),
(33, 36, 3, 1, 'code', '', '开发语言', NULL, '', 'lang'),
(34, 37, 1, 2, 'code', '', '开发语言', 'ASP', '', 'lang'),
(35, 38, 2, 2, 'code', '', '开发语言', 'PHP', '', 'lang'),
(36, 39, 3, 2, 'code', '', '开发语言', '.NET', '', 'lang'),
(37, 40, 4, 1, 'code', '', '数据库', NULL, '', 'sql1'),
(38, 41, 1, 2, 'code', '', '数据库', 'Access', '', 'sql1'),
(39, 42, 2, 2, 'code', '', '数据库', 'Mysql', '', 'sql1'),
(40, 43, 3, 2, 'code', '', '数据库', 'Mssql', '', 'sql1'),
(41, 44, 15, 2, 'code', '', '系统品牌', '齐博', '', 'brand'),
(46, 49, 6, 2, 'code', '', '数据库', '其他', '请确认您的系统品牌不在选项中，否则将无法过审同时产生纠纷您将负全责', 'sql1'),
(47, 2, 2, 1, 'codebuy', '', '源码类型', NULL, '', 'menus'),
(48, 2, 1, 2, 'codebuy', '', '源码类型', '图片QQ', '', 'menus'),
(67, 1, 1, 1, 'domain', '', '域名类型', NULL, '', 'domain_type'),
(68, 2, 1, 2, 'domain', '', '域名类型', '拼音', '', 'domain_type'),
(69, 3, 2, 2, 'domain', '', '域名类型', '数字', '', 'domain_type'),
(70, 5, 3, 2, 'domain', '', '域名类型', '字母', '', 'domain_type'),
(71, 6, 4, 2, 'domain', '', '域名类型', '混杂', '', 'domain_type'),
(72, 10, 5, 2, 'domain', '', '域名类型', '杂米', '', 'domain_type'),
(73, 11, 6, 2, 'domain', '', '域名类型', '单词', '', 'domain_type'),
(74, 12, 7, 2, 'domain', '', '域名类型', '其他', '', 'domain_type'),
(75, 13, 2, 1, 'domain', '', '域名后缀', NULL, '', 'suffix'),
(76, 14, 1, 2, 'domain', '', '域名后缀', 'COM', '', 'suffix'),
(77, 15, 2, 2, 'domain', '', '域名后缀', 'CN', '', 'suffix'),
(78, 16, 3, 2, 'domain', '', '域名后缀', 'NET', '', 'suffix'),
(79, 17, 4, 2, 'domain', '', '域名后缀', 'ORG', '', 'suffix'),
(80, 18, 5, 2, 'domain', '', '域名后缀', 'CC', '', 'suffix'),
(81, 19, 7, 2, 'domain', '', '域名后缀', '其他', '', 'suffix'),
(82, 20, 3, 1, 'domain', '', '注册商', NULL, '', 'registrar'),
(83, 21, 1, 2, 'domain', '', '注册商', '易名', '', 'registrar'),
(84, 22, 2, 2, 'domain', '', '注册商', '爱名', '', 'registrar'),
(85, 23, 3, 2, 'domain', '', '注册商', '万网', '', 'registrar'),
(86, 24, 4, 2, 'domain', '', '注册商', '新网', '', 'registrar'),
(87, 25, 5, 2, 'domain', '', '注册商', 'GD', '', 'registrar'),
(88, 26, 6, 2, 'domain', '', '注册商', 'ENOM', '', 'registrar'),
(89, 36, 5, 1, 'domain', '', '附带属性', NULL, '', 'attribute'),
(90, 37, 1, 2, 'domain', '', '附带属性', '备案', '', 'attribute'),
(91, 38, 2, 2, 'domain', '', '附带属性', '权重', '', 'attribute'),
(92, 39, 3, 2, 'domain', '', '附带属性', 'PR米', '', 'attribute'),
(93, 40, 5, 2, 'domain', '', '附带属性', '老米', '', 'attribute'),
(94, 41, 4, 2, 'domain', '', '附带属性', '收录', '', 'attribute'),
(95, 42, 6, 2, 'domain', '', '附带属性', '外链', '', 'attribute'),
(96, 43, 6, 2, 'domain', '', '域名后缀', 'info', '', 'suffix'),
(97, 44, 7, 2, 'domain', '', '注册商', '其他', '', 'registrar'),
(98, 45, 7, 2, 'domain', '', '附带属性', '过Q', '', 'attribute'),
(99, 46, 1, 1, 'domainbuy', '', '求购类型', NULL, '', 'menus'),
(100, 47, 2, 1, 'domainbuy', '', '求购后缀', NULL, '', 'menus'),
(101, 48, 1, 2, 'domainbuy', '', '求购类型', '2位', '', 'menus'),
(102, 49, 2, 2, 'domainbuy', '', '求购类型', '3数', '', 'menus'),
(103, 50, 3, 2, 'domainbuy', '', '求购类型', '4数', '', 'menus'),
(104, 51, 4, 2, 'domainbuy', '', '求购类型', '5数', '', 'menus'),
(105, 52, 5, 2, 'domainbuy', '', '求购类型', '字母', '', 'menus'),
(106, 53, 6, 2, 'domainbuy', '', '求购类型', '拼音', '', 'menus'),
(107, 54, 7, 2, 'domainbuy', '', '求购类型', '杂米', '', 'menus'),
(108, 55, 8, 2, 'domainbuy', '', '求购类型', '综合', '', 'menus'),
(109, 56, 1, 2, 'domainbuy', '', '求购后缀', 'COM', '', 'menus'),
(110, 57, 2, 2, 'domainbuy', '', '求购后缀', 'CN', '', 'menus'),
(111, 58, 3, 2, 'domainbuy', '', '求购后缀', 'NET', '', 'menus'),
(112, 59, 4, 2, 'domainbuy', '', '求购后缀', 'ORG', '', 'menus'),
(113, 60, 5, 2, 'domainbuy', '', '求购后缀', 'CC', '', 'menus'),
(114, 61, 6, 2, 'domainbuy', '', '求购后缀', 'INFO', '', 'menus'),
(115, 62, 7, 2, 'domainbuy', '', '求购后缀', '综合', '', 'menus'),
(116, 1, 1, 0, 'code', '', '源码集市', '', '', ''),
(117, 3, 3, 0, 'domain', '', '域名交易', '', '', ''),
(118, 4, 4, 0, 'domainbuy', '', '域名求购', '', '', 'menus'),
(119, 6, 5, 0, 'task', '', '任务大厅', '', '', 'typesxid'),
(120, 2, 1, 2, 'task', '', '类型', '网站开发', '', 'typesxid'),
(121, 3, 2, 2, 'task', '', '类型', '二次开发', '', 'typesxid'),
(122, 4, 3, 2, 'task', '', '类型', '网站仿制', '', 'typesxid'),
(123, 5, 4, 2, 'task', '', '类型', '美工设计', '', 'typesxid'),
(124, 6, 5, 2, 'task', '', '类型', '安全维护', '', 'typesxid'),
(125, 7, 6, 2, 'task', '', '类型', '推广优化', '', 'typesxid'),
(126, 8, 7, 2, 'task', '', '类型', '数据采集', '', 'typesxid'),
(127, 9, 8, 2, 'task', '', '类型', '修改修复', '', 'typesxid'),
(128, 10, 9, 2, 'task', '', '类型', '其他', '', 'typesxid'),
(129, 1, 1, 1, 'task', '', '类型', '测试', '', 'typesxid'),
(138, 1, 1, 0, 'deal', '', '自定义交易', NULL, '', ''),
(139, 1, 1, 1, 'deal', '', '交易类型', NULL, '', ''),
(140, 2, 1, 2, 'deal', '', 'web', '网站', '', ''),
(141, 3, 2, 2, 'deal', '', 'domain', '域名', '', ''),
(142, 4, 3, 2, 'deal', '', 'code', '源码', '', ''),
(143, 5, 4, 2, 'deal', '', 'task', '任务', '', ''),
(149, 7, 6, 2, 'deal', '', 'custom', '自助', '', ''),
(150, 5, 1, 0, 'web', '', '网站交易', NULL, '', ''),
(151, 1, 1, 1, 'web', '', '类型', NULL, '', 'menus'),
(152, 2, 3, 1, 'web', '', '流量', NULL, '', 'ip'),
(153, 3, 4, 1, 'web', '', '权重', NULL, '', 'br'),
(154, 4, 2, 1, 'web', '', '价格', NULL, '', 'money'),
(159, 5, 1, 2, 'web', '', '类型', '垂直行业', '', 'menus'),
(160, 6, 2, 2, 'web', '', '类型', '影音游戏', '', 'menus'),
(161, 7, 3, 2, 'web', '', '类型', '休闲生活', '', 'menus'),
(162, 8, 4, 2, 'web', '', '类型', '电子商务', '', 'menus'),
(163, 9, 5, 2, 'web', '', '类型', '文学小说', '', 'menus'),
(164, 10, 6, 2, 'web', '', '类型', '地方门户', '', 'menus'),
(166, 11, 8, 2, 'web', '', '类型', '其他综合', '', 'menus'),
(167, 12, 1, 2, 'web', '', '流量', '≤100 IP', '', 'ip'),
(168, 13, 2, 2, 'web', '', '流量', '≤1000 IP', '', 'ip'),
(169, 14, 3, 2, 'web', '', '流量', '≤5000 IP', '', 'ip'),
(170, 15, 4, 2, 'web', '', '流量', '≤10000 IP', '', 'ip'),
(171, 16, 5, 2, 'web', '', '流量', '>10000 IP', '', 'ip'),
(172, 17, 1, 2, 'web', '', '权重', '≤1 BR', '', 'br'),
(173, 18, 2, 2, 'web', '', '权重', '2 BR', '', 'br'),
(174, 19, 3, 2, 'web', '', '权重', '3 BR', '', 'br'),
(175, 20, 4, 2, 'web', '', '权重', '4 BR', '', 'br'),
(176, 21, 5, 2, 'web', '', '权重', '5 BR', '', 'br'),
(177, 22, 6, 2, 'web', '', '权重', '≥6 BR', '', 'br'),
(178, 23, 1, 2, 'web', '', '价格', '≤100 元', '', 'money'),
(179, 24, 2, 2, 'web', '', '价格', '≤1000 元', '', 'money'),
(180, 25, 3, 2, 'web', '', '价格', '≤5000 元', '', 'money'),
(181, 26, 4, 2, 'web', '', '价格', '≤10000 元', '', 'money'),
(182, 27, 5, 2, 'web', '', '价格', '>10000 元', '', 'money'),
(183, 1, 1, 0, 'webbuy', '', '网站求购', NULL, '', 'menus'),
(184, 2, 1, 1, 'webbuy', '', '网站类型', NULL, '', 'menus'),
(185, 3, 1, 2, 'webbuy', '', '网站类型', '垂直行业', '', 'menus'),
(186, 4, 2, 2, 'webbuy', '', '网站类型', '影音游戏', '', 'menus'),
(187, 5, 3, 2, 'webbuy', '', '网站类型', '休闲生活', '', 'menus'),
(188, 6, 4, 2, 'webbuy', '', '网站类型', '电子商务', '', 'menus'),
(189, 7, 5, 2, 'webbuy', '', '网站类型', '文学小说', '', 'menus'),
(190, 8, 6, 2, 'webbuy', '', '网站类型', '地方门户', '', 'menus'),
(191, 9, 7, 2, 'webbuy', '', '网站类型', '其他综合', '', 'menus'),
(193, 11, 1, 2, 'webbuy', '', '流量要求', '≤100 IP', '', 'menus'),
(194, 12, 1, 1, 'webbuy', '', '流量要求', NULL, '', 'menus'),
(195, 13, 2, 2, 'webbuy', '', '流量要求', '≤1000 IP', '', 'menus'),
(196, 14, 3, 2, 'webbuy', '', '流量要求', '≤5000 IP', '', 'menus'),
(197, 15, 4, 2, 'webbuy', '', '流量要求', '≤10000 IP', '', 'menus'),
(198, 16, 5, 2, 'webbuy', '', '流量要求', ' >10000 IP', '', 'menus'),
(199, 1, 1, 1, 'code', '', '源文件', NULL, '', ''),
(200, 1, 1, 2, 'code', '完全开源', '源文件', '完全开源（含全部源文件）', '', ''),
(201, 1, 1, 2, 'code', '部分加密', '源文件', '部分加密（含部分源文件）', '', ''),
(202, 1, 1, 2, 'code', '全部加密', '源文件', '全部加密（不含源文件）', '', ''),
(203, 1, 1, 1, 'code', '', '授权', NULL, '', ''),
(204, 1, 1, 2, 'code', '', '授权', '免授权', '', ''),
(205, 1, 1, 2, 'code', '', '授权', '需要授权', '', ''),
(206, 1, 1, 1, 'code', '', '规格', NULL, '', ''),
(207, 1, 1, 2, 'code', '', '规格', '整站源码', '', ''),
(208, 1, 1, 2, 'code', '', '规格', '模块插件', '', ''),
(209, 1, 1, 2, 'code', '', '规格', '模板风格', '', ''),
(210, 1, 1, 1, 'code', '', '移动端', NULL, '', ''),
(211, 1, 1, 2, 'code', '', '移动端', 'Wap', '', ''),
(212, 1, 1, 2, 'code', '', '移动端', 'App', '', ''),
(213, 1, 1, 2, 'code', '', '移动端', 'App(微信)小程序', '', ''),
(214, 1, 1, 2, 'code', '', '移动端', '自适应', '', ''),
(215, 1, 1, 1, 'code', '', '主机类型', NULL, '', ''),
(216, 1, 1, 2, 'code', '', '主机类型', '自适应独立主机（服务器、VPS、VM）', '', ''),
(217, 1, 1, 2, 'code', '', '主机类型', '虚拟主机（仅有FTP管理）', '', ''),
(218, 1, 1, 1, 'code', '', '操作系统', NULL, '', ''),
(219, 1, 1, 2, 'code', '', '操作系统', 'Windows', '', ''),
(220, 1, 1, 2, 'code', '', '操作系统', 'Linux', '', ''),
(221, 1, 1, 1, 'code', '', 'web服务', NULL, '', ''),
(222, 1, 1, 2, 'code', '', 'web服务', 'IIS', '', ''),
(223, 1, 1, 2, 'code', '', 'web服务', 'apache', '', ''),
(224, 1, 1, 2, 'code', '', 'web服务', 'nginx', '', ''),
(225, 1, 1, 1, 'code', '', '安装方式', NULL, '', ''),
(226, 1, 1, 2, 'code', '', '安装方式', '提供管理权限', '', ''),
(227, 1, 1, 2, 'code', '', '安装方式', 'QQ远程协助', '', ''),
(228, 1, 1, 1, 'code', '', '伪静态', NULL, '', ''),
(229, 1, 1, 2, 'code', '', '伪静态', '需要', '', ''),
(230, 1, 1, 2, 'code', '', '伪静态', '无需', '', ''),
(231, 19, 21, 2, 'code', '', '源码类型', '博客/个人/blog', '', 'menus'),
(232, 18, 17, 2, 'code', '', '源码类型', '体育/运动/赛事', '', 'menus'),
(233, 3, 2, 2, 'code', '', '源码类型', '物流/快递/交通', '', 'menus'),
(234, 2, 1, 2, 'code', '', '源码类型', '域名/空间/建站', '', 'menus'),
(235, 11, 10, 2, 'code', '', '源码类型', '汽车/二手/车行', '', 'menus'),
(236, 6, 5, 2, 'code', '', '源码类型', 'Wap/微信/App', '', 'menus'),
(238, 39, 4, 2, 'code', '', '开发语言', 'Jsp', '', 'lang'),
(239, 39, 5, 2, 'code', '', '开发语言', 'HTML', '', 'lang'),
(240, 39, 6, 2, 'code', '', '开发语言', 'VC＋＋', '', 'lang'),
(241, 39, 7, 2, 'code', '', '开发语言', 'Java', '', 'lang'),
(242, 48, 8, 2, 'code', '', '开发语言', 'VB', '', 'lang'),
(243, 38, 9, 2, 'code', '', '开发语言', 'object-c', '', 'lang'),
(244, 39, 9, 2, 'code', '', '开发语言', 'C＃', '', 'lang'),
(245, 39, 10, 2, 'code', '', '开发语言', 'Python', '', 'lang'),
(246, 39, 11, 2, 'code', '', '开发语言', '其他', '请确认您的系统品牌不在选项中，否则将无法过审同时产生纠纷您将负全责', 'lang'),
(247, 43, 4, 2, 'code', '', '数据库', 'Oracle', '', 'sql1'),
(248, 49, 5, 2, 'code', '', '数据库', '无', '', 'sql1'),
(249, 24, 4, 2, 'code', '', '系统品牌', '杰奇', '', 'brand'),
(250, 24, 4, 2, 'code', '', '系统品牌', 'destoon', '', 'brand'),
(251, 24, 20, 2, 'code', '', '系统品牌', '其他', '请确认您的系统品牌不在选项中，否则将无法过审同时产生纠纷您将负全责', 'brand'),
(257, 1, 1, 1, 'serve', '', '服务市场', NULL, '', 'menus'),
(258, 2, 1, 2, 'serve', '', '服务市场', '网站建设', '', 'menus'),
(259, 2, 1, 2, 'serve', '', '服务市场', '手机开发', '', 'menus'),
(260, 2, 1, 2, 'serve', '', '服务市场', '定制仿站', '', 'menus'),
(261, 2, 1, 2, 'serve', '', '服务市场', '设计美工', '', 'menus'),
(262, 2, 1, 2, 'serve', '', '服务市场', '推广外链', '', 'menus'),
(263, 2, 1, 2, 'serve', '', '服务市场', 'SEO优化', '', 'menus'),
(264, 2, 1, 2, 'serve', '', '服务市场', '软件开发', '', 'menus'),
(265, 2, 1, 2, 'serve', '', '服务市场', '环境配置', '', 'menus'),
(266, 2, 1, 2, 'serve', '', '服务市场', '安装迁移', '', 'menus'),
(267, 2, 1, 2, 'serve', '', '服务市场', '游戏开发', '', 'menus'),
(268, 2, 1, 2, 'serve', '', '服务市场', '代办服务', '', 'menus'),
(269, 2, 1, 2, 'serve', '', '服务市场', '其他', '', 'menus'),
(270, 2, 2, 2, 'codebuy', '', '源码类型', '电影音乐', '', 'menus'),
(271, 2, 3, 2, 'codebuy', '', '源码类型', '游戏动漫', '', 'menus'),
(272, 2, 4, 2, 'codebuy', '', '源码类型', '交友婚庆', '', 'menus'),
(273, 2, 5, 2, 'codebuy', '', '源码类型', '文学小说', '', 'menus'),
(274, 2, 6, 2, 'codebuy', '', '源码类型', '健康女性', '', 'menus'),
(275, 2, 7, 2, 'codebuy', '', '源码类型', '导航查询', '', 'menus'),
(276, 2, 8, 2, 'codebuy', '', '源码类型', '商城淘客', '', 'menus'),
(277, 2, 9, 2, 'codebuy', '', '源码类型', '新闻地方', '', 'menus'),
(278, 2, 10, 2, 'codebuy', '', '源码类型', '论坛博客', '', 'menus'),
(279, 2, 11, 2, 'codebuy', '', '源码类型', '二手分类', '', 'menus'),
(280, 2, 12, 2, 'codebuy', '', '源码类型', '电脑下载', '', 'menus'),
(281, 2, 13, 2, 'codebuy', '', '源码类型', '旅游餐饮', '', 'menus'),
(282, 2, 14, 2, 'codebuy', '', '源码类型', '房产装饰', '', 'menus'),
(283, 2, 15, 2, 'codebuy', '', '源码类型', '人才家教', '', 'menus'),
(284, 2, 16, 2, 'codebuy', '', '源码类型', '股票财经', '', 'menus'),
(285, 2, 17, 2, 'codebuy', '', '源码类型', '公司企业', '', 'menus'),
(286, 2, 18, 2, 'codebuy', '', '源码类型', '其他综合', '', 'menus'),
(287, 7, 6, 2, 'deal', '', 'serve', '服务', '', ''),
(288, 1, 1, 1, 'brand', '', '品牌分类', NULL, '', 'typeid'),
(289, 288, 1, 2, 'brand', '', '品牌分类', '源码产品', '', 'typeid'),
(290, 288, 1, 2, 'brand', '', '品牌分类', '软件产品', '', 'typeid'),
(291, 288, 1, 2, 'brand', '', '品牌分类', '网站制作', '', 'typeid'),
(292, 288, 1, 2, 'brand', '', '品牌分类', '软件开发', '', 'typeid'),
(293, 288, 1, 2, 'brand', '', '品牌分类', '营销推广', '', 'typeid'),
(294, 288, 1, 2, 'brand', '', '品牌分类', '教学培训', '', 'typeid'),
(295, 288, 1, 2, 'brand', '', '品牌分类', '电商培训', '', 'typeid'),
(296, 288, 1, 2, 'brand', '', '品牌分类', '站长服务', '', 'typeid');

-- --------------------------------------------------------

--
-- 表的结构 `shop_unique_orderno`
--

CREATE TABLE IF NOT EXISTS `shop_unique_orderno` (
  `id` int(11) NOT NULL,
  `trade_no` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '订单号'
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- 转存表中的数据 `shop_unique_orderno`
--

INSERT INTO `shop_unique_orderno` (`id`, `trade_no`) VALUES
(1, 'T190911235313100030'),
(2, 'T190911235555100012'),
(3, 'T190911235734100075'),
(4, 'T190912000335100036'),
(5, 'T190912000340100041'),
(6, 'T190912000347100086'),
(7, 'T190912000404100016'),
(8, 'T190912000440100031'),
(9, 'T190912000445100057'),
(10, 'T190912000453100098'),
(11, 'T190913144108100092'),
(12, 'T190913144127100065'),
(13, 'T190913151117100090'),
(14, 'T190913151130100034'),
(15, 'T190913151258100075'),
(16, 'T190913151309100050'),
(17, 'T190913151325100085'),
(18, 'T190913151331100085'),
(19, 'T190913151451100018'),
(20, 'T190913151632100012'),
(21, 'T190913151944100032'),
(22, 'T190913152021100011'),
(23, 'T190913152205100040'),
(24, 'T190913152258100089'),
(25, 'T190913152320100086');

-- --------------------------------------------------------

--
-- 表的结构 `shop_user`
--

CREATE TABLE IF NOT EXISTS `shop_user` (
  `id` int(11) NOT NULL,
  `bh` char(20) NOT NULL,
  `email` char(50) DEFAULT NULL,
  `pwd` char(50) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `phone` char(20) DEFAULT NULL,
  `getpwd` char(50) DEFAULT NULL,
  `qq_api` varchar(200) DEFAULT NULL,
  `alipay_api` varchar(200) DEFAULT NULL,
  `phone_zt` int(1) DEFAULT '0',
  `email_zt` int(1) DEFAULT '0',
  `jifen` int(10) DEFAULT '0',
  `aqm` char(20) DEFAULT NULL,
  `uip` char(20) DEFAULT NULL,
  `uc` char(4) DEFAULT NULL,
  `zt` int(1) DEFAULT '1',
  `ztsm` varchar(250) DEFAULT NULL,
  `sj` datetime DEFAULT NULL,
  `safe_code` char(50) DEFAULT NULL,
  `kgxnum` int(2) DEFAULT '5',
  `ygxnum` int(2) DEFAULT '0',
  `gxtime` datetime DEFAULT NULL,
  `pay_acc` varchar(100) DEFAULT NULL,
  `idcard` int(10) NOT NULL COMMENT '身份认证',
  `company` int(10) NOT NULL COMMENT '企业认证',
  `regular` int(10) NOT NULL DEFAULT '1' COMMENT '常规登陆',
  `regionlist` varchar(1000) NOT NULL,
  `iplist` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_user`
--

INSERT INTO `shop_user` (`id`, `bh`, `email`, `pwd`, `money`, `phone`, `getpwd`, `qq_api`, `alipay_api`, `phone_zt`, `email_zt`, `jifen`, `aqm`, `uip`, `uc`, `zt`, `ztsm`, `sj`, `safe_code`, `kgxnum`, `ygxnum`, `gxtime`, `pay_acc`, `idcard`, `company`, `regular`, `regionlist`, `iplist`) VALUES
(1, 'u1629290430', '66983239@qq.com', '51c736b55716ad1e52fe4373c06f0e08b5770ba3', '170.00', '15555555555', NULL, NULL, NULL, 1, 1, 0, NULL, NULL, 'sell', 1, NULL, '2021-08-18 20:40:30', '51c736b55716ad1e52fe4373c06f0e08b5770ba3', 5, 0, '2021-08-18 20:40:30', NULL, 0, 0, 1, '', '');

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_certification`
--

CREATE TABLE IF NOT EXISTS `shop_user_certification` (
  `id` int(10) NOT NULL,
  `ubh` varchar(50) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `birthday` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `card` varchar(50) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `mz` varchar(50) NOT NULL,
  `legal` varchar(30) NOT NULL COMMENT '企业法人',
  `num` varchar(100) NOT NULL COMMENT '营业执照编号',
  `tel` varchar(30) NOT NULL COMMENT '联系电话',
  `qiye` varchar(100) NOT NULL COMMENT '企业类型',
  `type` varchar(50) NOT NULL,
  `renzheng` varchar(50) NOT NULL,
  `zt` int(10) NOT NULL,
  `tp` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_user_certification`
--

INSERT INTO `shop_user_certification` (`id`, `ubh`, `sj`, `uip`, `address`, `birthday`, `name`, `card`, `sex`, `mz`, `legal`, `num`, `tel`, `qiye`, `type`, `renzheng`, `zt`, `tp`) VALUES
(1, 'u1569891600', '2019-11-15 23:00:16', '127.0.0.1', '福建省莆田市荔城区北高镇山前村过沟213号', '19940701', '陈海山', '350321199407015239', '男', '汉', '', '', '', '', 'idcard', '', 1, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1569891600/u1569891600.jpg'),
(2, 'u1575943758', '2019-12-10 10:10:43', '1.194.65.186', '福建省南安市乐峰镇炉中村因下58号', '19890125', '潘培福', '350583198901258338', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1575943758/u1575943758.jpg'),
(3, 'u1576044778', '2019-12-11 14:15:38', '223.91.37.34', '河南省周口市川汇区蔬菜乡东杨庄村一组袁庄23号附1号', '19960530', '孟鑫盼', '411602199605304028', '女', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1576044778/u1576044778.jpg'),
(4, 'u1584170002', '2020-03-16 11:34:42', '120.239.202.255', '广东省普宁市梅林镇边潭村223号', '20020106', '官林波', '445281200201065655', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1584170002/u1584170002.jpg'),
(5, 'u1584247713', '2020-03-20 10:40:38', '103.27.25.116', '广东省化州市官桥镇大岭脚村199号', '19811210', '彭海威', '440982198112104958', '男', '汉', '', '', '', '', 'idcard', '18506675581', 1, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1584247713/u1584247713.jpg'),
(6, 'u1584863739', '2020-03-22 15:56:30', '182.91.208.18', '陕西省石泉县饶峰镇饶峰村一组', '19910822', '陈超', '612423199108224413', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1584863739/u1584863739.jpg'),
(7, 'u1584852051', '2020-03-23 01:55:32', '119.0.0.78', '贵州省天柱县邦洞镇灯塔村兴下二组', '20020817', '杨长辉', '522627200208171232', '男', '侗', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1584852051/u1584852051.jpg'),
(8, 'u1587955576', '2020-04-27 12:02:12', '114.246.35.6', '安徽省宿州市埇桥区符离镇下湾村路庄组8号', '19970610', '路子恒', '342201199706102817', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1587955576/u1587955576.jpg'),
(9, 'u1589162729', '2020-05-11 10:10:04', '223.157.10.121', '湖南省涟源市白马镇三团村浆桥组', '19920920', '刘群', '431382199209200066', '女', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1589162729/u1589162729.jpg'),
(10, 'u1590049184', '2020-05-21 16:22:35', '61.140.235.70', '广东省茂名市茂南区金塘镇尚垌杨美埇村二组', '19920429', '邓崇彬', '440902199204292610', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1590049184/u1590049184.jpg'),
(11, 'u1582562596', '2020-06-09 04:37:57', '111.19.82.212', 'A西省汉中市汉台区徐望镇家湾村4组36号附1号', '20030506', '邵旭东', '', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1582562596/u1582562596.jpg'),
(12, 'u1582562596', '2020-06-09 11:21:53', '111.19.82.212', '陕西省汉中市汉台区徐家坡乡邵家湾村4组', '19520324', '代素华', '612301195203244963', '女', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1582562596/u1582562596.jpg'),
(13, 'u1582562596', '2020-06-09 11:23:30', '111.19.82.212', '陕西省汉中市汉台区徐家坡乡邵家湾村4组', '19520324', '', '612301195203244963', '女', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1582562596/u1582562596.jpg'),
(14, 'u1582562596', '2020-06-09 11:28:01', '111.19.82.212', '陕西省汉中市汉台区徐家坡乡邵家湾村4组', '19520324', '代素华', '612301195203244963', '女', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1582562596/u1582562596.jpg'),
(15, 'u1592662598', '2020-06-20 22:22:22', '182.127.0.38', '河南省虞城县站集乡焦阁', '19920509', '焦小帅', '411425199205093317', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1592662598/u1592662598.jpg'),
(16, 'u1593511704', '2020-06-30 18:14:18', '122.238.145.180', '河南省南阳市卧龙区英庄镇东坡村北刘庄13组', '19511004', '肖云芳', '412924195110042246', '女', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1593511704/u1593511704.jpg'),
(17, 'u1590904349', '2020-07-03 17:03:37', '218.201.184.236', '内蒙古根河市满归镇沿河街桥头巷29号', '19980526', '董再海', '152105199805262511', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1590904349/u1590904349.jpg'),
(18, 'u1593508927', '2020-07-04 10:41:39', '219.131.11.119', '广东省惠东县大岭镇新安居委小岭村', '19961006', '陈金兰', '441323199610060567', '女', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/upload/idcard/u1593508927/u1593508927.jpg'),
(19, 'u1606138334', '2020-11-23 21:34:00', '180.114.207.23', '安徽省宿州市砀山县程庄镇张庄自然村009', '19911016', '张强', '342221199110165038', '男', '汉', '', '', '', '', 'idcard', 'phone', 1, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/file/5fbbba4650fb0/5fbbba4650ffa.jpg'),
(20, 'u1604288259', '2020-11-24 14:18:04', '218.94.33.122', '江苏省灌南县汤沟镇金星村四组24号', '19840619', '唐建业', '320724198406190319', '男', '汉', '', '', '', '', 'idcard', 'bankid', 1, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/file/5fbca598453a4/5fbca598453e7.jpg'),
(21, 'u1606094515', '2020-11-26 19:42:16', '14.204.75.185', '云南省曲靖市师宗县五龙乡保太村委会保太村327号', '19880904', '阮文飞', '530323198809041730', '男', '彝', '', '', '', '', 'idcard', 'phone', 1, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/file/5fbf9496cb17d/5fbf9496cb1c2.jpg'),
(22, 'u1605888094', '2020-12-01 20:16:07', '223.74.227.42', '广东省连平县隆街镇立新村下二1号', '19990726', '赖奕鑫', '441623199907262713', '男', '汉', '', '', '', '', 'idcard', '', 0, 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/file/5fc63400aa51b/5fc63400aa562.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_channel`
--

CREATE TABLE IF NOT EXISTS `shop_user_channel` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_collect`
--

CREATE TABLE IF NOT EXISTS `shop_user_collect` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '类型 1支付宝 2微信 3银行卡',
  `info` text NOT NULL,
  `create_at` int(10) unsigned NOT NULL DEFAULT '0',
  `collect_img` tinytext,
  `allow_update` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1为允许用户修改收款信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_log`
--

CREATE TABLE IF NOT EXISTS `shop_user_log` (
  `id` int(10) NOT NULL,
  `sj` datetime NOT NULL,
  `uip` varchar(30) NOT NULL,
  `bh` varchar(100) NOT NULL,
  `ubh` varchar(1050) NOT NULL,
  `tit` varchar(50) NOT NULL,
  `txt` text NOT NULL,
  `role` varchar(100) NOT NULL COMMENT '身份',
  `type` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_login_error_log`
--

CREATE TABLE IF NOT EXISTS `shop_user_login_error_log` (
  `id` int(11) NOT NULL,
  `login_name` varchar(50) NOT NULL DEFAULT '' COMMENT '登录名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '尝试密码',
  `user_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：普通用户 1：后台管理员账号',
  `login_from` int(1) NOT NULL DEFAULT '0' COMMENT '登录来源：0：前台 1：总后台',
  `login_time` int(11) NOT NULL DEFAULT '0' COMMENT '登录时间'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_login_log`
--

CREATE TABLE IF NOT EXISTS `shop_user_login_log` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `create_at` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_user_login_log`
--

INSERT INTO `shop_user_login_log` (`id`, `user_id`, `ip`, `create_at`) VALUES
(1, 10001, '61.242.114.84', 1538990543);

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_money_log`
--

CREATE TABLE IF NOT EXISTS `shop_user_money_log` (
  `id` int(11) NOT NULL,
  `business_type` enum('sub_sold_rebate','sub_fee_rebate','cash_notpass','cash_success','apply_cash','admin_dec','admin_inc','goods_sold','fee','sub_register','freeze','unfreeze') NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `money` decimal(10,3) NOT NULL COMMENT '变动金额',
  `balance` decimal(10,3) NOT NULL COMMENT '剩余',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '变动原因',
  `create_at` int(10) unsigned NOT NULL COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_user_money_log`
--

INSERT INTO `shop_user_money_log` (`id`, `business_type`, `user_id`, `money`, `balance`, `reason`, `create_at`) VALUES
(1, 'goods_sold', 10001, '0.100', '0.100', '【卖出商品】成功售出商品绝地求生卡密（1张）', 1538990812),
(2, 'goods_sold', 10001, '-0.010', '0.090', '【卖出商品】扣除交易手续费，订单：T1810081726376105', 1538990812),
(3, 'freeze', 10001, '-0.090', '0.000', '【冻结金额】冻结订单：T1810081726376105，冻结金额：0.09元', 1538990812),
(4, 'goods_sold', 10001, '0.100', '0.100', '【卖出商品】成功售出商品绝地求生卡密（1张）', 1568359416),
(5, 'goods_sold', 10001, '-0.010', '0.090', '【卖出商品】扣除交易手续费，订单：T190913152320100086', 1568359416),
(6, 'freeze', 10001, '-0.090', '0.000', '【冻结金额】冻结订单：T190913152320100086，冻结金额：0.09元', 1568359416),
(7, 'cash_success', 0, '0.000', '78.000', '【提现成功】申请提现成功，提现金额78.00元，手续费1.56元，实际到账76.44元', 1606836914);

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_rate`
--

CREATE TABLE IF NOT EXISTS `shop_user_rate` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `channel_id` int(10) unsigned NOT NULL COMMENT '渠道ID',
  `rate` decimal(10,4) unsigned NOT NULL COMMENT '费率'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_token`
--

CREATE TABLE IF NOT EXISTS `shop_user_token` (
  `user_id` int(10) unsigned NOT NULL COMMENT '用户 id',
  `token` varchar(255) NOT NULL COMMENT '用户登录凭证',
  `platform` varchar(20) NOT NULL COMMENT '用户登录平台',
  `refresh_token` varchar(255) NOT NULL COMMENT '登录凭证刷新凭证',
  `created_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间，即登录时间',
  `expire_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '凭证过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_user`
--

CREATE TABLE IF NOT EXISTS `shop_user_user` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级ID',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT '微信openid',
  `username` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `qq` varchar(16) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subdomain` varchar(250) NOT NULL DEFAULT '' COMMENT '子域名',
  `shop_name` varchar(20) NOT NULL DEFAULT '' COMMENT '店铺名称',
  `shop_notice` varchar(200) NOT NULL DEFAULT '' COMMENT '公告通知',
  `statis_code` varchar(1024) NOT NULL DEFAULT '' COMMENT '统计代码',
  `pay_theme` varchar(255) NOT NULL DEFAULT 'default' COMMENT '支付页风格',
  `stock_display` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '库存展示方式 1实际库存 2库存范围',
  `money` decimal(10,3) NOT NULL DEFAULT '0.000',
  `rebate` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `freeze_money` decimal(10,3) NOT NULL DEFAULT '0.000',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '0未审核 1已审核',
  `is_freeze` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否冻结 0否 1是',
  `create_at` int(10) unsigned NOT NULL,
  `ip` varchar(50) DEFAULT '' COMMENT 'IP地址',
  `website` varchar(255) NOT NULL DEFAULT '' COMMENT '商户网站',
  `is_close` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否关闭店铺 0否 1是',
  `shop_notice_auto_pop` tinyint(4) NOT NULL DEFAULT '1' COMMENT '商家公告是否自动弹出',
  `cash_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '提现方式',
  `login_auth` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否开启安全登录',
  `login_auth_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '安全登录验证方式，1：短信，2：邮件，3：谷歌密码验证',
  `google_secret_key` varchar(128) DEFAULT '' COMMENT '谷歌令牌密钥',
  `shop_gouka_protocol_pop` tinyint(4) NOT NULL DEFAULT '1' COMMENT '购卡协议是否自动弹出',
  `user_notice_auto_pop` tinyint(4) NOT NULL DEFAULT '1' COMMENT '商家是否自动弹出',
  `login_key` int(11) NOT NULL DEFAULT '0' COMMENT '用户登录标记',
  `fee_payer` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单手续费支付方，0：跟随系统，1：商家承担，2买家承担',
  `settlement_type` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '结算方式，-1：跟随系统，1:T1结算，0:T0结算'
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_user_user`
--

INSERT INTO `shop_user_user` (`id`, `parent_id`, `openid`, `username`, `password`, `mobile`, `qq`, `email`, `subdomain`, `shop_name`, `shop_notice`, `statis_code`, `pay_theme`, `stock_display`, `money`, `rebate`, `freeze_money`, `status`, `is_freeze`, `create_at`, `ip`, `website`, `is_close`, `shop_notice_auto_pop`, `cash_type`, `login_auth`, `login_auth_type`, `google_secret_key`, `shop_gouka_protocol_pop`, `user_notice_auto_pop`, `login_key`, `fee_payer`, `settlement_type`) VALUES
(10001, 0, '', 'demo', '25d55ad283aa400af464c76d713c07ad', '13800138000', '5021314', 'demo@qq.com', '', '', '', '', 'default', 2, '0.000', '0.000', '0.180', 1, 0, 1538989965, '', '', 0, 1, 1, 0, 1, '', 1, 1, 0, 0, -1);

-- --------------------------------------------------------

--
-- 表的结构 `shop_verify_email_error_log`
--

CREATE TABLE IF NOT EXISTS `shop_verify_email_error_log` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT '' COMMENT '前台用户名',
  `admin` varchar(50) DEFAULT '' COMMENT '管理员用户名',
  `email` varchar(20) DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) DEFAULT '' COMMENT '输入验证码',
  `screen` varchar(20) DEFAULT '' COMMENT '使用场景',
  `type` tinyint(1) DEFAULT '0' COMMENT '1：短信验证码 2：谷歌身份验证, 0:邮箱',
  `ctime` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_verify_error_log`
--

CREATE TABLE IF NOT EXISTS `shop_verify_error_log` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT '' COMMENT '前台用户名',
  `admin` varchar(50) DEFAULT '' COMMENT '管理员用户名',
  `mobile` varchar(20) DEFAULT '' COMMENT '手机号码',
  `code` varchar(10) DEFAULT '' COMMENT '输入验证码',
  `screen` varchar(20) DEFAULT '' COMMENT '使用场景',
  `type` tinyint(1) DEFAULT '0' COMMENT '1：短信验证码 2：谷歌身份验证',
  `ctime` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `shop_verify_error_log`
--

INSERT INTO `shop_verify_error_log` (`id`, `username`, `admin`, `mobile`, `code`, `screen`, `type`, `ctime`) VALUES
(1, '', '', '13800138000', '4444', 'delete_order', 1, 1592639798);

-- --------------------------------------------------------

--
-- 表的结构 `shop_web`
--

CREATE TABLE IF NOT EXISTS `shop_web` (
  `id` int(10) NOT NULL,
  `bh` char(20) DEFAULT NULL,
  `ubh` char(20) DEFAULT NULL,
  `menus` varchar(100) DEFAULT NULL,
  `tit` varchar(250) DEFAULT NULL,
  `txt` text,
  `sj` date DEFAULT NULL,
  `lastgx` datetime DEFAULT NULL,
  `name` char(50) DEFAULT NULL,
  `ip` int(9) DEFAULT NULL,
  `profit` varchar(100) NOT NULL COMMENT '收入',
  `sys` varchar(100) NOT NULL COMMENT '系统',
  `updatas` varchar(100) NOT NULL COMMENT '数据',
  `app` varchar(100) NOT NULL COMMENT '移动端',
  `server` int(10) NOT NULL COMMENT '服务器',
  `stats` varchar(100) NOT NULL COMMENT '统计类型',
  `psort` varchar(500) NOT NULL COMMENT '演示大图',
  `br` int(2) DEFAULT NULL,
  `money` int(10) DEFAULT NULL,
  `djl` int(11) DEFAULT '0',
  `hfl` int(11) DEFAULT '0',
  `zt` int(1) DEFAULT '1',
  `url` varchar(100) NOT NULL,
  `pwd` varchar(20) DEFAULT NULL,
  `seo_gxsj` date DEFAULT NULL,
  `entrust` int(10) NOT NULL COMMENT '官方代售'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_fans`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_fans` (
  `id` bigint(20) unsigned NOT NULL COMMENT '粉丝表ID',
  `appid` varchar(50) DEFAULT NULL COMMENT '公众号Appid',
  `groupid` bigint(20) unsigned DEFAULT NULL COMMENT '分组ID',
  `tagid_list` varchar(100) DEFAULT '' COMMENT '标签id',
  `is_back` tinyint(1) unsigned DEFAULT '0' COMMENT '是否为黑名单用户',
  `subscribe` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户是否订阅该公众号，0：未关注，1：已关注',
  `openid` char(100) NOT NULL DEFAULT '' COMMENT '用户的标识，对当前公众号唯一',
  `spread_openid` char(100) DEFAULT NULL COMMENT '推荐人OPENID',
  `spread_at` datetime DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL COMMENT '用户的昵称',
  `sex` tinyint(1) unsigned DEFAULT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `country` varchar(50) DEFAULT NULL COMMENT '用户所在国家',
  `province` varchar(50) DEFAULT NULL COMMENT '用户所在省份',
  `city` varchar(50) DEFAULT NULL COMMENT '用户所在城市',
  `language` varchar(50) DEFAULT NULL COMMENT '用户的语言，简体中文为zh_CN',
  `headimgurl` varchar(500) DEFAULT NULL COMMENT '用户头像',
  `subscribe_time` bigint(20) unsigned DEFAULT NULL COMMENT '用户关注时间',
  `subscribe_at` datetime DEFAULT NULL COMMENT '关注时间',
  `unionid` varchar(100) DEFAULT NULL COMMENT 'unionid',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `expires_in` bigint(20) unsigned DEFAULT '0' COMMENT '有效时间',
  `refresh_token` varchar(200) DEFAULT NULL COMMENT '刷新token',
  `access_token` varchar(200) DEFAULT NULL COMMENT '访问token',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信粉丝';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_fans_tags`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_fans_tags` (
  `id` bigint(20) unsigned NOT NULL COMMENT '标签ID',
  `appid` char(50) DEFAULT NULL COMMENT '公众号APPID',
  `name` varchar(35) DEFAULT NULL COMMENT '标签名称',
  `count` int(11) unsigned DEFAULT NULL COMMENT '总数',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信会员标签';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_keys`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_keys` (
  `id` bigint(20) NOT NULL,
  `appid` char(50) DEFAULT NULL COMMENT '公众号APPID',
  `type` varchar(20) DEFAULT NULL COMMENT '类型，text 文件消息，image 图片消息，news 图文消息',
  `keys` varchar(100) DEFAULT NULL COMMENT '关键字',
  `content` text COMMENT '文本内容',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片链接',
  `voice_url` varchar(255) DEFAULT NULL COMMENT '语音链接',
  `music_title` varchar(100) DEFAULT NULL COMMENT '音乐标题',
  `music_url` varchar(255) DEFAULT NULL COMMENT '音乐链接',
  `music_image` varchar(255) DEFAULT NULL COMMENT '音乐缩略图链接',
  `music_desc` varchar(255) DEFAULT NULL COMMENT '音乐描述',
  `video_title` varchar(100) DEFAULT NULL COMMENT '视频标题',
  `video_url` varchar(255) DEFAULT NULL COMMENT '视频URL',
  `video_desc` varchar(255) DEFAULT NULL COMMENT '视频描述',
  `news_id` bigint(20) unsigned DEFAULT NULL COMMENT '图文ID',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '排序字段',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '0 禁用，1 启用',
  `create_by` bigint(20) unsigned DEFAULT NULL COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT=' 微信关键字';

--
-- 转存表中的数据 `shop_wechat_keys`
--

INSERT INTO `shop_wechat_keys` (`id`, `appid`, `type`, `keys`, `content`, `image_url`, `voice_url`, `music_title`, `music_url`, `music_image`, `music_desc`, `video_title`, `video_url`, `video_desc`, `news_id`, `sort`, `status`, `create_by`, `create_at`) VALUES
(1, NULL, 'text', 'default', '说点什么吧', 'http://faka1.test.com/static/theme/default/img/image.png', '', '音乐标题', '', 'http://faka1.test.com/static/theme/default/img/image.png', '音乐描述', '视频标题', '', '视频描述', 0, 0, 1, NULL, '2020-06-20 03:22:11');

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_menu`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_menu` (
  `id` bigint(16) unsigned NOT NULL,
  `index` bigint(20) DEFAULT NULL,
  `pindex` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `type` varchar(24) NOT NULL DEFAULT '' COMMENT '菜单类型 null主菜单 link链接 keys关键字',
  `name` varchar(256) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `content` text NOT NULL COMMENT '文字内容',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0禁用1启用)',
  `create_by` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信菜单配置';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_news`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_news` (
  `id` bigint(20) unsigned NOT NULL,
  `media_id` varchar(100) DEFAULT NULL COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT NULL COMMENT '永久素材显示URL',
  `article_id` varchar(60) DEFAULT NULL COMMENT '关联图文ID，用，号做分割',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '是否删除',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信图文表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_news_article`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_news_article` (
  `id` bigint(20) unsigned NOT NULL,
  `title` varchar(50) DEFAULT NULL COMMENT '素材标题',
  `local_url` varchar(300) DEFAULT NULL COMMENT '永久素材显示URL',
  `show_cover_pic` tinyint(4) unsigned DEFAULT '0' COMMENT '是否显示封面 0不显示，1 显示',
  `author` varchar(20) DEFAULT NULL COMMENT '作者',
  `digest` varchar(300) DEFAULT NULL COMMENT '摘要内容',
  `content` longtext COMMENT '图文内容',
  `content_source_url` varchar(200) DEFAULT NULL COMMENT '图文消息原文地址',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信素材表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_news_image`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_news_image` (
  `id` bigint(20) unsigned NOT NULL,
  `md5` varchar(32) DEFAULT NULL COMMENT '文件md5',
  `local_url` varchar(300) DEFAULT NULL COMMENT '本地文件链接',
  `media_url` varchar(300) DEFAULT NULL COMMENT '远程图片链接',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信服务器图片';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_news_media`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_news_media` (
  `id` bigint(20) unsigned NOT NULL,
  `appid` varchar(200) DEFAULT NULL COMMENT '公众号ID',
  `md5` varchar(32) DEFAULT NULL COMMENT '文件md5',
  `type` varchar(20) DEFAULT NULL COMMENT '媒体类型',
  `media_id` varchar(100) DEFAULT NULL COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT NULL COMMENT '本地文件链接',
  `media_url` varchar(300) DEFAULT NULL COMMENT '远程图片链接',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信素材表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_pay_notify`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_pay_notify` (
  `id` int(20) NOT NULL,
  `appid` varchar(50) DEFAULT NULL COMMENT '公众号Appid',
  `bank_type` varchar(50) DEFAULT NULL COMMENT '银行类型',
  `cash_fee` bigint(20) DEFAULT NULL COMMENT '现金价',
  `fee_type` char(20) DEFAULT NULL COMMENT '币种，1人民币',
  `is_subscribe` char(1) DEFAULT 'N' COMMENT '是否关注，Y为关注，N为未关注',
  `mch_id` varchar(50) DEFAULT NULL COMMENT '商户MCH_ID',
  `nonce_str` varchar(32) DEFAULT NULL COMMENT '随机串',
  `openid` varchar(50) DEFAULT NULL COMMENT '微信用户openid',
  `out_trade_no` varchar(50) DEFAULT NULL COMMENT '支付平台退款交易号',
  `sign` varchar(100) DEFAULT NULL COMMENT '签名',
  `time_end` datetime DEFAULT NULL COMMENT '结束时间',
  `result_code` varchar(10) DEFAULT NULL,
  `return_code` varchar(10) DEFAULT NULL,
  `total_fee` varchar(11) DEFAULT NULL COMMENT '支付总金额，单位为分',
  `trade_type` varchar(20) DEFAULT NULL COMMENT '支付方式',
  `transaction_id` varchar(64) DEFAULT NULL COMMENT '订单号',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '本地系统时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付日志表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_wechat_pay_prepayid`
--

CREATE TABLE IF NOT EXISTS `shop_wechat_pay_prepayid` (
  `id` int(20) NOT NULL,
  `appid` char(50) DEFAULT NULL COMMENT '公众号APPID',
  `from` char(32) DEFAULT 'shop' COMMENT '支付来源',
  `fee` bigint(20) unsigned DEFAULT NULL COMMENT '支付费用(分)',
  `trade_type` varchar(20) DEFAULT NULL COMMENT '订单类型',
  `order_no` varchar(50) DEFAULT NULL COMMENT '内部订单号',
  `out_trade_no` varchar(50) DEFAULT NULL COMMENT '外部订单号',
  `prepayid` varchar(500) DEFAULT NULL COMMENT '预支付码',
  `expires_in` bigint(20) unsigned DEFAULT NULL COMMENT '有效时间',
  `transaction_id` varchar(64) DEFAULT NULL COMMENT '微信平台订单号',
  `is_pay` tinyint(1) unsigned DEFAULT '0' COMMENT '1已支付，0未支退款',
  `pay_at` datetime DEFAULT NULL COMMENT '支付时间',
  `is_refund` tinyint(1) unsigned DEFAULT '0' COMMENT '是否退款，退款单号(T+原来订单)',
  `refund_at` datetime DEFAULT NULL COMMENT '退款时间',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '本地系统时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付订单号对应表';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shig_ad`
--
ALTER TABLE `shig_ad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_ads`
--
ALTER TABLE `shop_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_announce_log`
--
ALTER TABLE `shop_announce_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_anquan`
--
ALTER TABLE `shop_anquan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_app_menu`
--
ALTER TABLE `shop_app_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `app_menu_id_uindex` (`id`),
  ADD UNIQUE KEY `app_menu_function_id_uindex` (`function_id`);

--
-- Indexes for table `shop_app_version`
--
ALTER TABLE `shop_app_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_article`
--
ALTER TABLE `shop_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_article_category`
--
ALTER TABLE `shop_article_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_auto_unfreeze`
--
ALTER TABLE `shop_auto_unfreeze`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unfreeze_time` (`unfreeze_time`);

--
-- Indexes for table `shop_bid`
--
ALTER TABLE `shop_bid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_brand`
--
ALTER TABLE `shop_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_buy`
--
ALTER TABLE `shop_buy`
  ADD PRIMARY KEY (`bh`);

--
-- Indexes for table `shop_cart`
--
ALTER TABLE `shop_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_cash`
--
ALTER TABLE `shop_cash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_cashed`
--
ALTER TABLE `shop_cashed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_cash_channel`
--
ALTER TABLE `shop_cash_channel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_cash_channel_account`
--
ALTER TABLE `shop_cash_channel_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_channel`
--
ALTER TABLE `shop_channel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `channel_code_uindex` (`code`);

--
-- Indexes for table `shop_channel_account`
--
ALTER TABLE `shop_channel_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_code`
--
ALTER TABLE `shop_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_complaint`
--
ALTER TABLE `shop_complaint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_complaint_message`
--
ALTER TABLE `shop_complaint_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_custom`
--
ALTER TABLE `shop_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_data_report`
--
ALTER TABLE `shop_data_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_demand`
--
ALTER TABLE `shop_demand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_dingdang`
--
ALTER TABLE `shop_dingdang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_domain`
--
ALTER TABLE `shop_domain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_down`
--
ALTER TABLE `shop_down`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_email_code`
--
ALTER TABLE `shop_email_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_fav`
--
ALTER TABLE `shop_fav`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_file`
--
ALTER TABLE `shop_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_gonggao`
--
ALTER TABLE `shop_gonggao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_goods`
--
ALTER TABLE `shop_goods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_goods_card`
--
ALTER TABLE `shop_goods_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goods_card_user_id_index` (`user_id`),
  ADD KEY `goods_card_goods_id_index` (`goods_id`);

--
-- Indexes for table `shop_goods_category`
--
ALTER TABLE `shop_goods_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_goods_coupon`
--
ALTER TABLE `shop_goods_coupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`code`) USING BTREE;

--
-- Indexes for table `shop_help`
--
ALTER TABLE `shop_help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_helptype`
--
ALTER TABLE `shop_helptype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_invite_code`
--
ALTER TABLE `shop_invite_code`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `shop_jifen`
--
ALTER TABLE `shop_jifen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_jifen_cms`
--
ALTER TABLE `shop_jifen_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_link`
--
ALTER TABLE `shop_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_link_link`
--
ALTER TABLE `shop_link_link`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`relation_type`,`relation_id`),
  ADD UNIQUE KEY `token_uindex` (`token`),
  ADD UNIQUE KEY `user_link_index` (`relation_id`,`relation_type`);

--
-- Indexes for table `shop_log`
--
ALTER TABLE `shop_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_merchant_log`
--
ALTER TABLE `shop_merchant_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_message`
--
ALTER TABLE `shop_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_message_message`
--
ALTER TABLE `shop_message_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_migrations`
--
ALTER TABLE `shop_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `migration` (`migration`);

--
-- Indexes for table `shop_moneyback`
--
ALTER TABLE `shop_moneyback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_moneydb`
--
ALTER TABLE `shop_moneydb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_moneyrecord`
--
ALTER TABLE `shop_moneyrecord`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_nav`
--
ALTER TABLE `shop_nav`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_menu_node` (`node`) USING BTREE;

--
-- Indexes for table `shop_oauth`
--
ALTER TABLE `shop_oauth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order`
--
ALTER TABLE `shop_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_create_at_index` (`create_at`);

--
-- Indexes for table `shop_order_card`
--
ALTER TABLE `shop_order_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_card_order_id_index` (`order_id`),
  ADD KEY `order_card_card_id_index` (`card_id`);

--
-- Indexes for table `shop_order_dispute`
--
ALTER TABLE `shop_order_dispute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_pay_type`
--
ALTER TABLE `shop_pay_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_plugin`
--
ALTER TABLE `shop_plugin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_plugin_account`
--
ALTER TABLE `shop_plugin_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_product`
--
ALTER TABLE `shop_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_publicity`
--
ALTER TABLE `shop_publicity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_rate_group`
--
ALTER TABLE `shop_rate_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_rate_group_rule`
--
ALTER TABLE `shop_rate_group_rule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`) USING BTREE;

--
-- Indexes for table `shop_rate_group_user`
--
ALTER TABLE `shop_rate_group_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_reviews`
--
ALTER TABLE `shop_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_sell`
--
ALTER TABLE `shop_sell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_sell_nav`
--
ALTER TABLE `shop_sell_nav`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_sell_slide`
--
ALTER TABLE `shop_sell_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_serve`
--
ALTER TABLE `shop_serve`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_sign`
--
ALTER TABLE `shop_sign`
  ADD PRIMARY KEY (`ubh`);

--
-- Indexes for table `shop_sms_code`
--
ALTER TABLE `shop_sms_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_soukey`
--
ALTER TABLE `shop_soukey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_system_auth`
--
ALTER TABLE `shop_system_auth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_system_auth_title` (`title`) USING BTREE,
  ADD KEY `index_system_auth_status` (`status`) USING BTREE;

--
-- Indexes for table `shop_system_auth_node`
--
ALTER TABLE `shop_system_auth_node`
  ADD KEY `index_system_auth_auth` (`auth`) USING BTREE,
  ADD KEY `index_system_auth_node` (`node`) USING BTREE;

--
-- Indexes for table `shop_system_config`
--
ALTER TABLE `shop_system_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_config_name` (`name`);

--
-- Indexes for table `shop_system_log`
--
ALTER TABLE `shop_system_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_system_menu`
--
ALTER TABLE `shop_system_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_menu_node` (`node`) USING BTREE;

--
-- Indexes for table `shop_system_node`
--
ALTER TABLE `shop_system_node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_node_node` (`node`);

--
-- Indexes for table `shop_system_sequence`
--
ALTER TABLE `shop_system_sequence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_system_sequence_unique` (`type`,`sequence`) USING BTREE,
  ADD KEY `index_system_sequence_type` (`type`),
  ADD KEY `index_system_sequence_number` (`sequence`);

--
-- Indexes for table `shop_system_user`
--
ALTER TABLE `shop_system_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_system_user_username` (`username`) USING BTREE;

--
-- Indexes for table `shop_task`
--
ALTER TABLE `shop_task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_tem`
--
ALTER TABLE `shop_tem`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `shop_tuiguang`
--
ALTER TABLE `shop_tuiguang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_type`
--
ALTER TABLE `shop_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_unique_orderno`
--
ALTER TABLE `shop_unique_orderno`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_trade_no` (`trade_no`) USING BTREE;

--
-- Indexes for table `shop_user`
--
ALTER TABLE `shop_user`
  ADD PRIMARY KEY (`bh`);

--
-- Indexes for table `shop_user_certification`
--
ALTER TABLE `shop_user_certification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_channel`
--
ALTER TABLE `shop_user_channel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`channel_id`);

--
-- Indexes for table `shop_user_collect`
--
ALTER TABLE `shop_user_collect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_log`
--
ALTER TABLE `shop_user_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_login_error_log`
--
ALTER TABLE `shop_user_login_error_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_login_log`
--
ALTER TABLE `shop_user_login_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_money_log`
--
ALTER TABLE `shop_user_money_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_rate`
--
ALTER TABLE `shop_user_rate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`channel_id`);

--
-- Indexes for table `shop_user_token`
--
ALTER TABLE `shop_user_token`
  ADD KEY `index_login_user` (`user_id`) USING BTREE,
  ADD KEY `index_login_token` (`token`) USING BTREE,
  ADD KEY `index_login_platform` (`platform`) USING BTREE;

--
-- Indexes for table `shop_user_user`
--
ALTER TABLE `shop_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_mobile_uindex` (`mobile`),
  ADD UNIQUE KEY `user_email_uindex` (`email`);

--
-- Indexes for table `shop_verify_email_error_log`
--
ALTER TABLE `shop_verify_email_error_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_verify_error_log`
--
ALTER TABLE `shop_verify_error_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_web`
--
ALTER TABLE `shop_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_wechat_fans`
--
ALTER TABLE `shop_wechat_fans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_fans_spread_openid` (`spread_openid`) USING BTREE,
  ADD KEY `index_wechat_fans_openid` (`openid`) USING BTREE;

--
-- Indexes for table `shop_wechat_fans_tags`
--
ALTER TABLE `shop_wechat_fans_tags`
  ADD KEY `index_wechat_fans_tags_id` (`id`) USING BTREE,
  ADD KEY `index_wechat_fans_tags_appid` (`appid`) USING BTREE;

--
-- Indexes for table `shop_wechat_keys`
--
ALTER TABLE `shop_wechat_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_wechat_menu`
--
ALTER TABLE `shop_wechat_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_menu_pindex` (`pindex`) USING BTREE;

--
-- Indexes for table `shop_wechat_news`
--
ALTER TABLE `shop_wechat_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_new_artcle_id` (`article_id`) USING BTREE;

--
-- Indexes for table `shop_wechat_news_article`
--
ALTER TABLE `shop_wechat_news_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_wechat_news_image`
--
ALTER TABLE `shop_wechat_news_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_news_image_md5` (`md5`);

--
-- Indexes for table `shop_wechat_news_media`
--
ALTER TABLE `shop_wechat_news_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_wechat_pay_notify`
--
ALTER TABLE `shop_wechat_pay_notify`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_pay_notify_openid` (`openid`) USING BTREE,
  ADD KEY `index_wechat_pay_notify_out_trade_no` (`out_trade_no`) USING BTREE,
  ADD KEY `index_wechat_pay_notify_transaction_id` (`transaction_id`) USING BTREE;

--
-- Indexes for table `shop_wechat_pay_prepayid`
--
ALTER TABLE `shop_wechat_pay_prepayid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_pay_prepayid_outer_no` (`out_trade_no`) USING BTREE,
  ADD KEY `index_wechat_pay_prepayid_order_no` (`order_no`) USING BTREE,
  ADD KEY `index_wechat_pay_is_pay` (`is_pay`) USING BTREE,
  ADD KEY `index_wechat_pay_is_refund` (`is_refund`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shig_ad`
--
ALTER TABLE `shig_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `shop_ads`
--
ALTER TABLE `shop_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `shop_announce_log`
--
ALTER TABLE `shop_announce_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_anquan`
--
ALTER TABLE `shop_anquan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_app_menu`
--
ALTER TABLE `shop_app_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `shop_app_version`
--
ALTER TABLE `shop_app_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_article`
--
ALTER TABLE `shop_article`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `shop_article_category`
--
ALTER TABLE `shop_article_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shop_auto_unfreeze`
--
ALTER TABLE `shop_auto_unfreeze`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shop_bid`
--
ALTER TABLE `shop_bid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_brand`
--
ALTER TABLE `shop_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id';
--
-- AUTO_INCREMENT for table `shop_cart`
--
ALTER TABLE `shop_cart`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_cash`
--
ALTER TABLE `shop_cash`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_cashed`
--
ALTER TABLE `shop_cashed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_cash_channel`
--
ALTER TABLE `shop_cash_channel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shop_cash_channel_account`
--
ALTER TABLE `shop_cash_channel_account`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_channel`
--
ALTER TABLE `shop_channel`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '通道ID',AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT for table `shop_channel_account`
--
ALTER TABLE `shop_channel_account`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `shop_code`
--
ALTER TABLE `shop_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_complaint`
--
ALTER TABLE `shop_complaint`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_complaint_message`
--
ALTER TABLE `shop_complaint_message`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_custom`
--
ALTER TABLE `shop_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_data_report`
--
ALTER TABLE `shop_data_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shop_demand`
--
ALTER TABLE `shop_demand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_dingdang`
--
ALTER TABLE `shop_dingdang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_domain`
--
ALTER TABLE `shop_domain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_down`
--
ALTER TABLE `shop_down`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_email_code`
--
ALTER TABLE `shop_email_code`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `shop_fav`
--
ALTER TABLE `shop_fav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_file`
--
ALTER TABLE `shop_file`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `shop_gonggao`
--
ALTER TABLE `shop_gonggao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_goods`
--
ALTER TABLE `shop_goods`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shop_goods_card`
--
ALTER TABLE `shop_goods_card`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `shop_goods_category`
--
ALTER TABLE `shop_goods_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_goods_coupon`
--
ALTER TABLE `shop_goods_coupon`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_help`
--
ALTER TABLE `shop_help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_helptype`
--
ALTER TABLE `shop_helptype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `shop_invite_code`
--
ALTER TABLE `shop_invite_code`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_jifen`
--
ALTER TABLE `shop_jifen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_jifen_cms`
--
ALTER TABLE `shop_jifen_cms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shop_link`
--
ALTER TABLE `shop_link`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shop_link_link`
--
ALTER TABLE `shop_link_link`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `shop_log`
--
ALTER TABLE `shop_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_merchant_log`
--
ALTER TABLE `shop_merchant_log`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_message`
--
ALTER TABLE `shop_message`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_message_message`
--
ALTER TABLE `shop_message_message`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_migrations`
--
ALTER TABLE `shop_migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `shop_moneyback`
--
ALTER TABLE `shop_moneyback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_moneydb`
--
ALTER TABLE `shop_moneydb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_moneyrecord`
--
ALTER TABLE `shop_moneyrecord`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `shop_nav`
--
ALTER TABLE `shop_nav`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `shop_oauth`
--
ALTER TABLE `shop_oauth`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_order`
--
ALTER TABLE `shop_order`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `shop_order_card`
--
ALTER TABLE `shop_order_card`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shop_order_dispute`
--
ALTER TABLE `shop_order_dispute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `shop_pay_type`
--
ALTER TABLE `shop_pay_type`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT for table `shop_plugin`
--
ALTER TABLE `shop_plugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `shop_plugin_account`
--
ALTER TABLE `shop_plugin_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_product`
--
ALTER TABLE `shop_product`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=908;
--
-- AUTO_INCREMENT for table `shop_publicity`
--
ALTER TABLE `shop_publicity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_rate_group`
--
ALTER TABLE `shop_rate_group`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_rate_group_rule`
--
ALTER TABLE `shop_rate_group_rule`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_rate_group_user`
--
ALTER TABLE `shop_rate_group_user`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_reviews`
--
ALTER TABLE `shop_reviews`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12018;
--
-- AUTO_INCREMENT for table `shop_sell`
--
ALTER TABLE `shop_sell`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_sell_nav`
--
ALTER TABLE `shop_sell_nav`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_sell_slide`
--
ALTER TABLE `shop_sell_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_serve`
--
ALTER TABLE `shop_serve`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_sms_code`
--
ALTER TABLE `shop_sms_code`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `shop_soukey`
--
ALTER TABLE `shop_soukey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_system_auth`
--
ALTER TABLE `shop_system_auth`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shop_system_config`
--
ALTER TABLE `shop_system_config`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=362;
--
-- AUTO_INCREMENT for table `shop_system_log`
--
ALTER TABLE `shop_system_log`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_system_menu`
--
ALTER TABLE `shop_system_menu`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT for table `shop_system_node`
--
ALTER TABLE `shop_system_node`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=327;
--
-- AUTO_INCREMENT for table `shop_system_sequence`
--
ALTER TABLE `shop_system_sequence`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_system_user`
--
ALTER TABLE `shop_system_user`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10006;
--
-- AUTO_INCREMENT for table `shop_task`
--
ALTER TABLE `shop_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_tem`
--
ALTER TABLE `shop_tem`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_tuiguang`
--
ALTER TABLE `shop_tuiguang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `shop_type`
--
ALTER TABLE `shop_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=297;
--
-- AUTO_INCREMENT for table `shop_unique_orderno`
--
ALTER TABLE `shop_unique_orderno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `shop_user_certification`
--
ALTER TABLE `shop_user_certification`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `shop_user_channel`
--
ALTER TABLE `shop_user_channel`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_user_collect`
--
ALTER TABLE `shop_user_collect`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_user_log`
--
ALTER TABLE `shop_user_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_user_login_error_log`
--
ALTER TABLE `shop_user_login_error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_user_login_log`
--
ALTER TABLE `shop_user_login_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_user_money_log`
--
ALTER TABLE `shop_user_money_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `shop_user_rate`
--
ALTER TABLE `shop_user_rate`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_user_user`
--
ALTER TABLE `shop_user_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10002;
--
-- AUTO_INCREMENT for table `shop_verify_email_error_log`
--
ALTER TABLE `shop_verify_email_error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_verify_error_log`
--
ALTER TABLE `shop_verify_error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_web`
--
ALTER TABLE `shop_web`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_wechat_fans`
--
ALTER TABLE `shop_wechat_fans`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '粉丝表ID';
--
-- AUTO_INCREMENT for table `shop_wechat_keys`
--
ALTER TABLE `shop_wechat_keys`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_wechat_menu`
--
ALTER TABLE `shop_wechat_menu`
  MODIFY `id` bigint(16) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_wechat_news`
--
ALTER TABLE `shop_wechat_news`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_wechat_news_article`
--
ALTER TABLE `shop_wechat_news_article`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_wechat_news_image`
--
ALTER TABLE `shop_wechat_news_image`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_wechat_news_media`
--
ALTER TABLE `shop_wechat_news_media`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_wechat_pay_notify`
--
ALTER TABLE `shop_wechat_pay_notify`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_wechat_pay_prepayid`
--
ALTER TABLE `shop_wechat_pay_prepayid`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

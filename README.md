# ThinkCT源码商城免费版

#### 介绍
ThinkCT内置各种强大的功能，邮件管理，腾讯云短信、阿里云短信等.后台强大简洁，使用方便快捷，支付模块支付宝 微信 QQ钱包 第三方支付等功能，前台支持自助交易，源码交易，服务交易，网站交易，域名交易，任务交易，需求求购等多类型交易!

#### 软件架构
ThinkCT是一款全新架构的商城系统.采用ThinkPHP框架开发


#### 安装教程

一，	安装宝塔上传程序选择二级目录/public运行，导入数据库文件，配置数据库连接信息/app/database.php
二，	安装php7.0运行扩展文件（swoole_loader.dll或swoole_loader.so）上传到当前PHP的扩展安装目录中：/www/server/php/70/lib/php/extensions/no-debug-non-zts-20160303，目录可能有所不同需自检
三，	编辑此PHP配置文件：/www/server/php/70/etc/php.ini，在此文件底部结尾处加入如下配置：extension=swoole_loader70.so（添加到最底部）
四，	需要重启php版本
五，	完成以上，进入后台域名/admin 账号admin密码admin888



#### 使用说明

1. 本系统采用ThinkPHP5框架+Layui，目前免费版已经停止更新
2. 如二开功能可联系作者QQ：66983239
3. 如需同步更新请购买付费版https://www.thinkct.net/product/view1.html，付费版采用ThinkPHP6框架开发
4. 开发QQ群：61162800 （免费加入），售后QQ群：250059867（付费加入）
5. 免费版无需授权，不限制任何功能 请勿倒卖
6. 安装说明请查看“安装说明.docx”
7. 如果不会安装可联系作者远程协助

#### 参与贡献

1.  Fork 本仓库
2.  新建 ThinkCT 分支
3.  提交代码
4.  新建 Pull Request


#### 声明
目前免费已停止更新，如需同步升级请购买付费版
开发QQ群：61162800 （免费加入）
售后QQ群：250059867（付费加入）
ThinkCT官网：https://www.thinkct.net/

#### 注意
1. 运行前需要去后台把站点信息改成自己的
2. 安装教程：https://www.thinkct.net/help/view5.html



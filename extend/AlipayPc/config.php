<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2019010162736506",

		//商户私钥
		'merchant_private_key' => "MIIEowIBAAKCAQEAxfDZL++LSsbJHRpHVBNlHBrVzeQO/JCwCYjPatuB/C+HA0aXQ9dxu43y6PyLFB69GwSqd1iwAmGFi0Uy6+82BrAczHJUDVXT7SsPpsSEC1Z+/f6+am+jiXRiqYQMhuhgAt70oWsy2O/mU1wEvbLn9WtkGXf4VEaZbJveCwTpU8Ii8I38MhqmRv5mflRs9RknvQzUhMYb7VZGfdVFT2sMHUlfsCNLgkVCQEQXzWrZg1uB/trXz7Sc45mOt/le3r/9fem3dQ089ldowJeVTJYrZJF+uSYjgNB6pwQgkbhhsVcIsq2mHuOuKwbgZKHSInDWuRZttSHn5n1MWQrVdMxEewIDAQABAoIBAA2i+SN/SkZdiY9ytwVIzMdx5dboZkvqH+aYQUnoU30vPQrxuwwWdKRqNBjvBRnewEJzQNc2CfIwC8Y7fzWX5k3xphpDqhy9E/ub4tknYr1xORCAk1e71zVqCj4Jdd95dNvdxla0ju05IdIOXdk/0REsU9oZVMdhkcJUvqhdr0Fw2EyhJaU2s5PER27k01vE6igWjo2vShg9rxQZednOfgNXLVupXNPa2TNVV5twJqRMd+k4ywsmO+L3rx8lZgvMO8Tbw3cRSYXKAfe1tCIvayeAZz1FLM7hjS6HsyY9jTwiYi4bgsNzb56ouR0pwMdwWGHkBUo/wGJmtyv1xn0EBJECgYEA+jnBtcLDLyxMRTmlIjIOUfcCUDnlri8mi9LskOG2hCXclseejq7HXjUbetjkF/1y8RkRIr/9JrFs9oaFvQr0cDgZBPCikrsDRIlTRKaqT0+Ke6AbXlJWcpAbCnmP1UK3+OQP5dkL+98nwmJAHXM9WqU/65ndDsVJdti04CsysHMCgYEAyoI2O00hyEWgM5KTYaKJrUorBrBD1gpnVWn0yOGUkAcBsh/CkcdR1PN76Jr0S6za47JvQYKhr9kuAccqpgMRJa1DJ/PKDrMbAHRLaVGiTPnkqqodFHHfWcoWv3XX83k+8Ab+UXvAuqA4t0aK34niso1+N1xbqYzBqiTwRRN6wdkCgYEAgcDxcg9Mp2mRI1SBDPpn8pjj2jYro+dPVbJKedaRjnUTrhxVXCfFulRPq6RMoyQKNnuJJzvnSek3V57qOt0zY/2y+5zMsMnJKAEN7MuABSB57yFXD9IigcW4P/ZJX4z5WVbp54ZlcHaHZ4ULOjpH1nlabBGdT8t+DOLS6Gt+HYECgYA1OAkWhou1PQ7/3qpaw0NZRh+Oj24UZwGHAeRxkk7flufMLuqMMwx/YUmT9Hz1EkUoB7GTTsg9FV8w2m2L2Ux9UU5PxpK4UDttYCKdV/XGMvn0G+aug8qFp0VZJZgOBTQUElJtiY85vHeLOr+uRWdNM0ATPnNcWIBgvXpjPN5K6QKBgFXka9e4KXGwZacUeEEFMjSIs9oyThBHiTWi5ADx0thtyZpzbnVkJfMrcibXZN4eUbNj1/r3PKgJ0jYa6+AD8KToOrGXafIChIKzmpGStds5WQV+vCSAFpbaMRitbmKrm7Q/Anc1FOs80YmBQAT9wnIxSC0GwdEkUfaP7BdXPmC6",
		
		//异步通知地址
		'notify_url' => "http://127.0.0.8/11notify_url.php",
		
		//同步跳转
		'return_url' => "http://127.0.0.8/22return_url.php",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsZqRFhxCtdWB8TXwK8PRuHCF70/TnxKa8zK4t7t41VHcs5ABVz9KSS2gkvADzioFwmyAyi+/0cFRfU6C3aiRGR0z4/46TvxBNu/zYbjtslCUDuXdq05VNAAt5Jp9ZWfGdu6R2DT3F9iSDtf0PntY0GwMOxqsCPzb6E+jQkH63+XbOU69w1F17CbGJANwBgGF2hZhu8If7e4PIAWAzoBnX8/b2jlu5vjmPOyvJg1jITW6MbEA4tPvPUUtnikP0SJstZE515HMOOldzXDasnFDkJzH66PweJ2UoZcCU9hzl5BDwYhqAlvw/Lfb+ZcGePkGGBt2sctp6LmTp2o94hVN+wIDAQAB",
);
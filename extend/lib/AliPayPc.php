<?php
namespace lib;

class AliPayPc
{
    /**
     * 支付宝收款
     * @param [type] $out_trade_no 商户订单号
     * @param [type] $subject 订单名称
     * @param [type] $total_amount 付款金额
     * @param [type] $body 商品描述，可空
     * @param [type] $config_data 支付参数,数组类型;为空则取默认参数
     * @return void
     */
    public static function setAilPay($out_trade_no,$subject,$total_amount,$body,$config_data = [])
    {
        require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'/../AlipayPc/config.php';//引入路径参考自己的实际存放路径
        require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'/../AlipayPc/pagepay/service/AlipayTradeService.php';
        require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'/../AlipayPc/pagepay/buildermodel/AlipayTradePagePayContentBuilder.php';

        //构造参数
        $payRequestBuilder = new \AlipayTradePagePayContentBuilder();//类名前面加 \ 调用外部类，需要在类名前加 \
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setOutTradeNo($out_trade_no);
		$config = $config_data;// 跳过文件里的秘钥
        $aop    = new \AlipayTradeService($config);

        /**
         * pagePay 电脑网站支付请求
         * @param $builder 业务参数，使用buildmodel中的对象生成。
         * @param $return_url 同步跳转地址，公网可以访问
         * @param $notify_url 异步通知地址，公网可以访问
         * @return $response 支付宝返回的信息
         */
        $response = $aop->pagePay($payRequestBuilder,$config['return_url'],$config['notify_url']);

        //输出表单
        var_dump($response);
    }

    /**
     * 支付宝服务器异步通知页面
     *
     * @param [type] $arr 支付成功返回信息post
     *
     * @return void
     */
    public static function setNotify_url($arr,$config)
    {
        //require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'/../AlipayPc/config.php';
        require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'/../AlipayPc/pagepay/service/AlipayTradeService.php';

        $alipaySevice = new \AlipayTradeService($config);
        $alipaySevice->writeLog(var_export($arr,true));
        $result = $alipaySevice->check($arr);//进行验签
        if($result) {//验证成功
            //交易状态
            if($arr['trade_status'] == 'TRADE_FINISHED' || $arr['trade_status'] == 'TRADE_SUCCESS') {
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }
}
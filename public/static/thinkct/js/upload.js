function checkfile(a, c) {
    var b = $("#" + c).attr("size_data"),
        d, f, e, g = window.navigator.userAgent;
    1 <= g.indexOf("Firefox") ? d = !0 : 1 <= g.indexOf("Chrome") ? f = !0 : 1 <= g.indexOf("Safari") && 0 > g.indexOf("Chrome") && (e = !0);
    g = a.value;
    g = g.substring(g.length - 4, g.length).toLowerCase();
    if (0 > $.inArray(g, [".jpg", ".png", ".gif"])) return layer.alert("<b>\u56fe\u7247\u683c\u5f0f\u53ea\u5141\u8bb8\u4e0a\u4f20\u3010jpg\u3001gif\u3001png\u3011</b>", {
        icon: 2
    }), removepreview(c), !1;
    g = 0;
    if (d || f) g = a.files[0].fileSize || a.files[0].size;
    if (g >
        b) return layer.alert("<b>\u56fe\u7247\u5927\u5c0f\u4e0d\u53ef\u8d85\u8fc7\u3010" + usize(b) + "\u3011</b>", {
        icon: 2
    }), removepreview(c), !1;
    if (e) return layer.alert("<b>\u4e0d\u652f\u6301Safari\u6d4f\u89c8\u56686.0\u4ee5\u4e0b\u7248\u672c\u7684\u56fe\u7247\u9884\u89c8!<br /><font color=#3ba354>\u4f46\u5e76\u4e0d\u5f71\u54cd\u56fe\u7247\u4e0a\u4f20</font></b>", {
        icon: 2
    }), !1;
    $("#" + c).html("<img id='" + c + "preview' >");
    previewImage(c, b)
}
document.all && document.write('\x3c!--[if lte IE 6]><script type="text/javascript">window.ie6= true\x3c/script><![endif]--\x3e');

function previewImage(a, c) {
    var b = $("#" + a + "preview"),
        d = $("#" + a),
        f = d.height();
    owidth = d.width();
    var e = document.getElementById(a + "inp");
    if (window.FileReader) i = 0, b.load(function() {
        if (0 < i) return !1;
        i++;
        var c = clacImgZoomParam(owidth, f, b.width(), b.height(), a);
        b.css({
            width: c.width + "px",
            height: c.height + "px"
        })
    }), oFReader = new FileReader, oFReader.readAsDataURL(e.files[0]), oFReader.onload = function(a) {
        b.attr("src", a.target.result)
    };
    else if (document.all)
        if (e.select(), e = document.selection.createRange().text, window.ie6)
            if (b.attr("src",
                e), osize = parseInt(b[0].fileSize), 0 >= osize) b.one("load", function() {
                if (parseInt(this.fileSize) > c) return layer.alert("<b>\u56fe\u7247\u6700\u5927\u4e0d\u53ef\u8d85\u8fc7\u3010" + usize(c) + "\u3011</b>", {
                    icon: 2
                }), removepreview(a), !1;
                var d = clacImgZoomParam(owidth, f, this.width, this.height, a);
                b.css({
                    width: d.width + "px",
                    height: d.height + "px"
                })
            });
            else {
                if (osize > c) return layer.alert("<b>\u56fe\u7247\u6700\u5927\u4e0d\u53ef\u8d85\u8fc7\u3010" + usize(c) + "\u3011</b>", {
                    icon: 2
                }), removepreview(a), !1;
                d = clacImgZoomParam(owidth,
                    f, b.width(), b.height(), a);
                b.css({
                    width: d.width + "px",
                    height: d.height + "px"
                })
            } else d.after("<div id='" + a + "sizediv' style='height:0;width:0;float:left;overflow: hidden;'><img id='preview_size_img'></div>"), d.css({
        filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + e + '")'
    }), b.attr("src", "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="), objSizeimg = $("#preview_size_img"), objSizeimg.css({
        filter: 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +
            e + '")'
    }), autoSizePreview(owidth, f, objSizeimg, objSizeimg.width(), objSizeimg.height(), a), $("#" + a + "sizediv").remove();
    else e.files && e.files.item(0) && (url = e.files.item(0).getAsDataURL(), objPreview.src = url)
}

function autoSizePreview(a, c, b, d, f, e) {
    a = clacImgZoomParam(a, c, d, f, e);
    b.css({
        width: a.width + "px",
        height: a.height + "px"
    })
}

function clacImgZoomParam(a, c, b, d, f) {
    var e = {
        width: a,
        height: c
    };
    pixel = $("#" + f).attr("pixel_data");
    if ("no" == pixel) return e;
    if (0 == pixel) {
        e = {
            width: b,
            height: d
        };
        rateWidth = b / a;
        rateHeight = d / c;
        if (1 > rateWidth) return removepreview(f), layer.alert("<b>\u56fe\u7247\u5bbd\u5ea6\u4e0d\u80fd\u4f4e\u4e8e\u3010 " + a + " px\u3011</b><br>\u5f53\u524d\u56fe\u7247\u5bbd\u5ea6\u3010 " + b + " px\u3011", {
            icon: 0
        }), !1;
        if (1 > rateHeight) return removepreview(f), layer.alert("<b>\u56fe\u7247\u9ad8\u5ea6\u4e0d\u80fd\u4f4e\u4e8e\u3010 " + c +
            " px\u3011</b><br>\u5f53\u524d\u56fe\u7247\u9ad8\u5ea6\u3010 " + d + " px\u3011", {
                icon: 0
            }), !1;
        1 == rateWidth && rateWidth == rateHeight ? (e.width = a, e.height = c) : rateWidth > rateHeight ? (e.width = Math.round(b / rateHeight), e.height = c) : (e.width = a, e.height = Math.round(d / rateWidth))
    } else if (2 == pixel) {
        if (a = $("#" + f).attr("w"), c = $("#" + f).attr("h"), a != b || c != d) return removepreview(f), layer.alert("<b>\u56fe\u7247\u5c3a\u5bf8\u5fc5\u987b\u4e3a\u3010 " + a + "x" + c + " \u3011</b><br>\u5f53\u524d\u56fe\u7247\u5c3a\u5bf8\u3010 " + b + "x" +
            d + " \u3011", {
                icon: 0
            }), !1
    } else if (1 == pixel && (d != b || c > d)) return removepreview(f), layer.alert("<b>\u56fe\u7247\u5c3a\u5bf8\u5bbd\u9ad8\u9700\u76f8\u7b49\uff0c\u4e14\u4e0d\u4f4e\u4e8e\u3010 " + c + "px \u3011</b><br>\u5f53\u524d\u56fe\u7247\u5c3a\u5bf8\u3010 " + b + "x" + d + " \u3011", {
        icon: 0
    }), !1;
    return e
}

function removepreview(a) {
    var c = $("#" + a + "inp"),
        b = $("input[name='" + a + "'][value*='.']"),
        d = $("#" + a + " img"),
        f = $("#" + a);
    c.after(c.clone().val(""));
    c.remove();
    $("#" + a).css("filter", "");
    $("#" + a + "sizediv").remove();
    b.val() ? d.attr("src", "http://iu.mafabu.com/" + b.attr("path") + "/" + b.val()).css({
        width: f.width() + "px",
        height: f.height() + "px"
    }) : d.remove()
};
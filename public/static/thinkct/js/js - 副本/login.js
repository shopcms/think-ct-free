layui.define(["layer", "form"],
function(a) {
    layui.layerform = layui.form()
});
var login_form = function(a) {
    var b = $("#popup-captcha");
    a.onSuccess(function() {
        Aform("login", $("form").serialize(),
        function(c) {
            if (1 != c.state) return b.hide(),
            a.reset(),
            Rs(c),
            !1;
            parent.UCheck(0 < $('input[name="uclick"]').length ? parent.layer.getFrameIndex(window.name) : "")
        })
    });
    $("#login_submit_button").click(function() {
        if ("phone" != $("input[name=login]").val()) {
            var c = $("input[name=login_name]"),
            d = $("input[name=login_pass]");
            if (!/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(c.val()) && !/^1[3|4|5|6|7|8|9][0-9]\d{8}$/.test(c.val())) return layer.tips("请输入正确的邮箱或手机号", c, {
                tips: 1
            }),
            !1;
            if (6 > d.val().length) return layer.tips("请输入正确的密码(6-20位)", d, {
                tips: 1
            }),
            !1
        } else {
            c = $("input[name=login_phone]");
            d = $("input[name=vcode]");
            if (11 != c.val().length) return layer.tips("请输入正确的手机号码！", c, {
                tips: 1
            }),
            !1;
            if (4 != d.val().length) return layer.tips("手机验证码错误，请重新输入！", d, {
                tips: 1
            }),
            !1
        }
        if (b.is(":visible") && !a.getValidate()) return layer.tips("请先完成验证", b, {
            tips: 1
        }),
        !1;
        b.show()
    });
    a.appendTo(b)
};
function logingt() {
    $.getScript("https://static.geetest.com/static/tools/gt.js",
    function() {
        $.ajax({
            url: "/html/StartCaptchaServlet?t=" + (new Date).getTime(),
            type: "get",
            dataType: "json",
            success: function(a) {
                initGeetest({
                    gt: a.gt,
                    challenge: a.challenge,
                    new_captcha: a.new_captcha,
                    product: "custom",
                    width: $("#login_submit_button").parent().width() + "px",
                    next_width: "278px",
                    area: "#logingt",
                    bg_color: "#fff",
                    offline: !a.success
                },
                login_form)
            }
        })
    })
}
Aform = function(a, b, c, d) {
    $.ajax({
        type: "POST",
        url: "/aform/index/" + a,
        async: d || !1,
        data: b,
        dataType: "json",
        success: function(a) {
            c ? c(a) : Rs(a)
        },
        error: function() {
            layer.closeAll("loading");
            layer.msg("网络异常，请重试！");
            return ! 1
        }
    })
};
sendbtn = function(a, b) {
    if (11 != $("input[name=login_phone]").val().length) return layer.tips("请输入正确的手机号码！", $("input[name=login_phone]"), {
        tips: [3, "#f90"]
    }),
    $("input[name=login_phone]").focus(),
    !1;
    $(a).attr("disabled", !0);
    Aform("sendinfo", $("form").serialize(),
    function(c) { - 1 == c.state ? sendtime(a, b) : $(a).attr("disabled", !1);
        Rs(c)
    })
};
sendtime = function(a, b) {
    function c(b) {
        return function() {
            if (b == d) $(a).removeClass("layui-btn-disabled"),
            $(a).val("获取验证码"),
            $(a).attr("disabled", !1);
            else {
                var c = d - b;
                $(a).val(c + "秒后重新发送")
            }
        }
    }
    var d = b || 60;
    $(a).attr("disabled", !0);
    $(a).val(d + "秒后重新发送").addClass("layui-btn-disabled");
    for (b = 1; b <= d; b++) setTimeout(c(b), 1E3 * b)
};
Rs = function(a) {
    layer.closeAll("loading");
    switch (a.state) {
    case - 1 : layer.tips(a.info, a.element, {
            tips: [a.tips || 1, a.color || "#f90"],
            time: a.time || 4E3
        });
        $(a.element).val("").focus();
        a.fun && eval(a.fun);
        break;
    case - 2 : layer.confirm(a.info || "登录失效，请重新登录！", {
            icon: a.icon || 8,
            btn: [a.btn1 || "登陆", a.btn2 || "取消"]
        },
        function(b) {
            a.url ? location.href = a.url: layer_login();
            layer.close(b)
        },
        function(a) {
            layer.close(a)
        });
        a.fun && eval(a.fun);
        break;
    case - 3 : a.fun && eval(a.fun);
        break;
    case 301:
        location.href = a.url;
        break;
    default:
        a.clear || layer.alert(a.info, {
            icon: a.state
        },
        function(b) {
            a.fun ? eval(a.fun) + layer.close(b) : a.url ? 1 == a.url ? location.reload() : location.href = a.url: a.element ? $(a.element).val("").focus() + layer.close(b) : layer.close(b)
        })
    }
};
$(function() {
    logingt();
    $(".three-login a").click(function() {
        var a = $(this).attr("id");
        if ("qq" == a || "baidu" == a) var b = 720,
        c = 450;
        else b = "sina" == a ? 770 : "alipay" == a ? 420 : 570,
        c = 530;
        parent.layer.open({
            type: 2,
            title: $(this).attr("title"),
            shadeClose: !1,
            shade: [.05, "#000"],
            area: [b + "px", c + "px"],
            content: ["/oauth/login/" + a, "no"]
        })
    });
    $(".login_type a").click(function() {
        var a = $(this).data("login"),
        b = $("input[name=login_name]").val(),
        c = $("input[name=login_" + a + "]").val();
        $(this).addClass("curr").siblings().removeClass("curr");
        $(".login_box:eq(" + $(this).index() + ")").removeClass("hide").siblings(".login_box").addClass("hide");
        $("input[name=login]").val(a);
        "phone" != a || "" != c || 11 != b.length || isNaN(b) || (c = b);
        $("input[name=login_" + a + "]").val("").focus().val(c)
    });
    $(".RememberMe label").click(function() {
        $(this).toggleClass("IChecked");
        $("input[name=RememberMe]").val($(this).hasClass("IChecked") ? 1 : 0)
    })
});
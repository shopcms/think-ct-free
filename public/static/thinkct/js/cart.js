﻿layui.use("form", function() {
    function a(b, a) {
        b.attr("checked", a);
        c.render("checkbox")
    }
    var e = layui.layer,
        c = layui.form();
    c.on("checkbox(cart)", function(b) {
        var c = b.elem.name,
            d = $(b.elem).closest("ul");
        if ("score" == c) {
            var h = d.find(".cinte"),
                f = parseInt($("#myjifen").html()),
                g = parseInt(h.find("b").html()),
                k = parseInt(h.find("a").html());
            if (b.elem.checked)
                if (0 < f) h.find(":checkbox[name='good']").is(":checked") || h.find(":checkbox[name='good']").trigger("click"), g = f >= g ? g : f, f -= g, k = '<font class="green">-\uffe5<b class="green">' +
                    toDecimal2(g / 10) + "</b></font>", a(d.find(":checkbox:first"), !0);
                else return a($(b.elem), !1), e.alert("\u60a8\u7684\u53ef\u7528\u79ef\u5206\u4e0d\u8db3\uff01", {
                    icon: 5
                }), !1;
            else g = 0, f += k, k = "";
            h.find("a").html(g);
            $("#myjifen").html(f);
            h.find(".money").html(k)
        } else "install" == c ? b.elem.checked && a(d.find(":checkbox:first"), !0) : (d = $(".cart"), "good" == c ? d = $(b.elem).closest("ul") : "shop" == c && (d = $(b.elem).closest("dl")), b.elem.checked ? a(d.find(":checkbox:not(:checked):not(:disabled):not(.score)"), !0) : a(d.find(":checkbox:checked:not(:disabled):not(.score)"), !1) + d.find(":checkbox[name=score]:checked").next(".layui-form-checkbox").click());
        b = $(b.elem).closest("dl");
        0 < b.find(":checkbox[name=good]:not(:disabled):not(:checked)").length ? a(b.find(":checkbox[name=shop]"), !1) : a(b.find(":checkbox[name=shop]"), !0);
        "cartxuan" != c && (0 < $(".cart").find(":checkbox[name=shop]:not(:disabled):not(:checked)").length ? a($(":checkbox[name=cartxuan]"), !1) : a($(":checkbox[name=cartxuan]"), !0));
        carmoney()
    });
    carmoney();
    $(".form_render").off("click").on("click", function() {
        c.render("checkbox")
    })
});

function carmoney() {
    var a = 0,
        e = 0;
    $(".cart :checkbox:checked").each(function() {
        var c = $(this).closest("dd"),
            b = parseFloat(c.find(".money b").html());
        b = isNaN(b) ? 0 : b;
        ("good" == $(this).attr("name") || "install" == $(this).attr("name")) && 0 < b && c.addClass("curr");
        "good" == $(this).attr("name") && e++;
        "score" == $(this).attr("name") ? a -= parseFloat(b) : a += parseFloat(b)
    });
    $(".curr :checkbox:not(:checked)").each(function() {
        $(this).closest("dd").removeClass("curr")
    });
    $("#allmoney").html(toDecimal2(a));
    $("#allnumber").html(e)
}

function carjs(a) {
    var e = "",
        c = "",
        b = "",
        l = "",
        d = "";
    if ($(a).hasClass("single_buy")) a = $(a).closest("ul").find(":checkbox[name='good']");
    else if (a = $(":checkbox[name='good']:checked"), 0 >= $(":checkbox[name='good']:checked").length) return layer.alert("<b>\u60a8\u81f3\u5c11\u8981\u9009\u62e9<font color=red>1</font>\u4ef6\u5546\u54c1</b>", {
        icon: 0
    }), !1;
    a.each(function() {
        var a = $(this).closest("ul");
        "" != e && (d = "|");
        e += d + $(this).val();
        b += d + a.find(".cgood").attr("data_type");
        var f = a.find(".score").is(":checked") ? parseInt(a.find(".cinte a").html()) :
            0;
        c += d + f;
        a = a.find(":checkbox[name=install]").is(":checked") || isNaN(parseFloat(a.find(".cinst b").html())) ? 1 : 0;
        l += d + a
    });
    $("#cartdata").empty().html("<input type=hidden value='" + e + "' name='id' /><input type=hidden value='" + c + "' name='jf' /><input type=hidden value='" + b + "' name='ty' /><input type=hidden value='" + l + "' name='az' />");
    Aform("cart", $("form[cart] input[type=hidden]").serialize())
};
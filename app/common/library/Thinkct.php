<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\common\library;

use think\Db;

class Thinkct extends Update
{
	
	public function get($url,$data=[],$timeout=30)
	{
		$url     = $this->url_array($url,$data);
		$opts    = array ('http'=>array('method'=>'GET','timeout'=>$timeout));
		$context = stream_context_create($opts);
		$html    = file_get_contents($url,false,$context);
		return $html;
	}

	public function post($url,$param=[],$timeout=30)
	{
        if (empty($url) || empty($param)) {
            return false;
        }     
        $postUrl  = $url;
        $curlPost = $param;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL,$postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
		
        return $data;
	}

	public function url_array($url,$data=[])
	{
		$buff = $url.'?';
		foreach ($data as $k => $v){
			if($v != '' && !is_array($v)){
				$buff .= $k.'='.$v.'&';
			}
		}
		return trim($buff,'&');
	}
	
	public function path($name)
	{
		$path = request()->path();
		$data = explode($name.'/',$path);
		if(count($data) <= 1){
			$data = null;
		}else{
			$data = explode('/',$data[1])[0];
		}
		return $data;
	}
	
	public function url($jump,$data=[])
	{
		if(!empty($data)){
			$url  = $this->url_array($jump,$data);
			$jump = $url;
		}
		$href = "<script language='javascript'>window.location.href='{$jump}';</script>";
		exit($href);
	}	
}
?>
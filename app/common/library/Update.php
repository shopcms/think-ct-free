<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\common\library;

use think\Controller;
use think\Db;

class Update extends Controller
{
	private $check_host;
	private $domain;
	
	static public function auth() 
	{

		$auth_code = self::auth_code('auth');
		
		if(!$auth_code) {
			$thinkct    = new Thinkct;
			$check_host = sysconf('auth_api');
			$domain     = request()->domain();
			$client_check = [
				'c' => 'thinkct',
				'a' => 'client_check',
				'u' => $domain,
			];			
			$check_info = curl_get($check_host,$client_check);
			if($check_info == '0') {
				Db::name('SystemConfig')->where('name','auth_code')->update(['value'=>self::auth_code('code')]);
				return 1;
			}else{
				return self::check_info($check_host,$domain,$check_info);
			}
		}else{
			return 1;
		}
	}
	
	// 检测更新
	static public function update() 
	{
		// auth_api
		// auth_key
		// auth_code
		//return sysconf('auth_api');
		return '已是最新版本';
	}	

	static private function check_info($check_host,$domain,$check_info)
	{
		$check_message = $check_host.'?a=check_message&u='.$domain;
		$message = curl_get($check_message);
		if($check_info == 1) {
			exit('<font color=red>' . $message . '</font>');
		}
		if($check_info == 2) {
			exit('<font color=red>' . $message . '</font>');
		}
		if($check_info == 3) {
			exit('<font color=red>' . $message . '</font>');
		}
		if($check_info != 0) {
		}
	}
	
	static private function auth_code($name='code')
	{
		$date = md5(date("Y-m-d"));
		if($name == 'code') {	
			return $date;
		}	
		if($name == 'auth') {
			if(sysconf('auth_code') == $date) {	
				return true;
			}else{
				return false;
			}
		}
	}
	
}
?>
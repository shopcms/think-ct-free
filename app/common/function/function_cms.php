<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
use think\Db;
use thinkct\library\Noticeemail;

// 源码订单交易
function order_anzhuan($data){
	$code = Db::name('code')->where('bh',$data['codebh'])->find();
	$v['buy_ff'] = null;
	if($code['ifaz'] == 0){
		$v['buy_az']="<font class=\"green\">免费</font>";	
	}else{
		if($data['azmoney'] > 0){
			$v['buy_az']="<font class=\"green\">已购买</font>";			
		}else{
			$v['buy_az']="未购买，{$name['buy']}可付费{$code['azmoney']}元增购";
			$v['buy_ff']="<input class=\"layui-btn layui-btn-primary layui-btn-mini order_handle\"' style='line-height: 20px;margin-top:-2px' type='button' data='buy_install' value='增购安装服务'>";			
		}
	}
	// 积分
	if($data['jf']>0){
		$v['jf']="<font color=\"#999999\">+ {$data['jf']} 积分</font>";
	}
	// 安装费
	if($data['azmoney']>0){
		$v['az']="【含安装服务 {$data['azmoney']} 元】";
	}
	return $v;
}

// 交易提示 订单信息 类型 
function myorder_tips($order,$type,$role=''){
	
	// 拒绝退款backerr
	// 同意退款backsuc
	// 强制退款forback
	// 交易成功success
	
	// 订单交易
	$ddzt = 0;
	// 订单结束
	if($order['ddzt'] == 'success' || $order['ddzt'] == 'forback' || $order['ddzt'] == 'backsuc'){
		$ddzt = 1;
	}
	// 开始交易
	if($type == 'db'){
		if($order['ddzt']<>'db'){return false;}
		if($role=='buy'){if($order['buy']!=session('usercode')){return false;}}
		if($role=='sell'){if($order['sell']!=session('usercode')){return false;}}
		return true;
	}
	
	// 申请退款
	if($type == 'back'){
		if($order['ddzt']<>'back'){return false;}
		if($role=='buy'){if($order['buy']!=session('usercode')){return false;}}
		if($role=='sell'){if($order['sell']!=session('usercode')){return false;}}
		return true;
	}

	// 拒绝退款
	if($type == 'refuse_back'){
		if($order['ddzt']<>'backerr'){return false;}
		if($role=='buy'){if($order['buy']!=session('usercode')){return false;}}
		if($role=='sell'){if($order['sell']!=session('usercode')){return false;}}
		return true;
	}
	
	// 纠纷消息 
	if($type == 'dispute'){
		if($ddzt == 1){
			return false;
		}
		return true;
	}
	// 退款成功
	if($type == 'backsuc'){
		if($order['ddzt']<>'backsuc'){return false;}
		return true;
	}
	// 买家败诉 
	if($type == 'fail'){
		if($ddzt == 0){
			return false;
		}
		if($order['service'] == 0){
			return false;
		}
		return true;
	}	
	return false;
}

// 交易按钮 类型 订单编号
function myorder_tjbtn($order,$type,$role=''){
	$moneyback = Db::name('moneyback')->where('ddbh',$order['ddbh'])->find();
	//$user  = Db::name('user')->where('bh',session('usercode'))->find();
	// 订单交易
	$ddzt = 0;
	// 订单结束
	if($order['ddzt'] == 'success' || $order['ddzt'] == 'forback' || $order['ddzt'] == 'backsuc'){
		$ddzt = 1;
	}

	// 确认收货
	if($type == 'confirm'){
		if($order['ddzt']<>'db'){return false;}
		if($order[$role]<>session('usercode')){return false;}
		return true;
	}
	// 申请退款
	if($type == 'apply_back'){
		if($order['ddzt']<>'db'){return false;}
		if($order[$role]<>session('usercode')){return false;}
		return true;
	}
	// 延长担保时间
	if($type == 'delay'){
		if($order['ddzt']<>'db'){return false;}
		if($order['ifyc']<>0){return false;}
		if($order[$role]<>session('usercode')){return false;}
		return true;
	}
	// 取消退款
	if($type == 'afresh_confirm'){
		if($order['ddzt']<>'back'&&$order['ddzt']<>'backerr'){return false;}
		if($order['buy']<>session('usercode')){return false;}
		return true;
	}
	// 补充退款
	if($type == 'apply_back_bc'){
		if($order['ddzt']<>'back'&&$order['ddzt']<>'backerr'){return false;}
		if($order['buy']<>session('usercode')){return false;}
		return true;
	}
	// 消保免赔
	if($type == 'waiverk'){
		if($order['ddzt']<>'back'&&$order['ddzt']<>'backerr'){return false;}
		if($order['buy']<>session('usercode')){return false;}
		if($moneyback['waiver'] == 1){return false;}
		return true;
	}
	// 恢复消保免赔
	if($type == 'cancel_waiver'){
		if($order['ddzt']<>'back'&&$order['ddzt']<>'backerr'){return false;}
		if($order['buy']<>session('usercode')){return false;}
		if($moneyback['waiver'] == 0){return false;}
		return true;
	}
	// 申请客服介入
	if($type == 'service'){
		if($order['ddzt']<>'backerr'){return false;}
		if($order['buy']<>session('usercode')){return false;}
		if($order['service']<>0){return false;}
		return true;
	}
	
	
	// 卖家
	// 主动退款
	if($type == 'active_back'){
		if($order['ddzt']<>'db'){return false;}
		if($order['sell']<>session('usercode')){return false;}
		return true;
	}		
	// 同意退款
	if($type == 'agree_back'){
		if($order['ddzt']<>'back'){return false;}
		if($order['sell']<>session('usercode')){return false;}
		return true;
	}
	// 拒绝退款
	if($type == 'refuse_back'){
		if($order['ddzt']<>'back'){return false;}
		if($order['sell']<>session('usercode')){return false;}
		return true;
	}		
	// 重新处理退款
	if($type == 'afresh_agree_back'){
		if($order['ddzt']<>'backerr'){return false;}
		if($order['sell']<>session('usercode')){return false;}		
		return true;
	}
	// 补充退款
	if($type == 'refuse_back_back'){
		if($order['ddzt']<>'backerr'){return false;}
		if($order['sell']<>session('usercode')){return false;}		
		return true;
	}
	
	// 追加订单
	if($type == 'add'){
		if($ddzt<>0){return false;}
		return true;
	}	
	
	// 评价订单
	if($type == 'evaluation'){
		if($ddzt==0){return false;}		
		return true;
	}
	// 评价分享
	if($type == 'oshare'){
		if($ddzt==0){return false;}
		return true;
	}	
	// 退款历史记录
	if($type == 'back_history'){
		if($order['ddzt']<>'success'){return false;}		
		if($order['service']<>1){return false;}
		return true;
	}	
	return false;
}

// 充值接口名称
function plugin_account($code){
	// 插件信息
	$plugin  = Db::name('plugin')->where(['code'=>$code,'is_install'=>1,'status'=>1])->find();
	if(!$plugin){
		//return ['state'=>0,'info'=>'当前插件没有开启'];
		exit('当前插件没有开启');
	}
	// 插件账号
	$account = Db::name('plugin_account')->where(['plugin_id'=>$plugin['id'],'status'=>1])->find();
	if(!$account){
		exit('当前插件没有可用账号');
	}
	// 参数信息
	$params  = json_array($account['params']);
	//if(!isset($params['apiurl'])){
		//return $this->error('参数不完整,请先配置信息');
	//}
	return $params;
}

// 充值接口名称
function payname($gate){
	if($gate == "alipay"){
		$payname = "支付宝";
	}elseif($gate == "wechat"){
		$payname = "微信";
	}elseif($gate == "qpay"){
		$payname = "QQ钱包";
	}else{
		$payname = "网银支付";
	}		
	return $payname;
}

// 用户信息
function user_info($ubh){
	// 用户信息
	$user = Db::name('user')->where('bh',$ubh)->find();
	$uc   = Db::name($user['uc'])->where('bh',$ubh)->find();
	return $uc;
}

function uc_buy($ubh){
	$buy = Db::name('buy')->where('bh',$ubh)->find();
	return $buy;
}

function uc_sell($ubh){
	$sell = Db::name('sell')->where('bh',$ubh)->find();
	return $sell;
}

// 订单纠纷
function order_dispute($ddbh,$txt,$tit=''){
	$order = Db::name('down')->where(['ddbh'=>$ddbh,'buy|sell'=>session('usercode')])->find();
	$count = Db::name('order_dispute')->where(['ddbh'=>$ddbh,'ubh'=>session('usercode')])->count();
	$supplement = 10;// 可补充数量
	if($count > $supplement){
		return '当前补充已超过'.$supplement.'条';
	}
	$role = ($order['buy']==session('usercode'))?'buy':'sell';
	// 创建补充原因
	Db::name('order_dispute')->insert([
		'sj'   => sj(),
		'uip'  => uip(),
		'bh'   => time(),
		'ddbh' => $ddbh,
		'ubh'  => session('usercode'),
		'txt'  => ($tit=='')?$txt:'【'.$tit.'】'.$txt,
		'role' => $role,
		'backmoney' => input('backmoney',0)
	]);
	// 创建纠纷通知
	$role = ($role=='buy')?'sell':'buy';
	noticeemail()->jiufen($ddbh,$txt,$role);	
	return 1;
}

// 通知邮箱
function noticeEmail(){
	$email = new Noticeemail();	
	return $email;
}

// 商品批量下架
function goods_down($ubh){
	Db::name('code')->where('ubh',$ubh)->update(['zt'=>2]);
	Db::name('serve')->where('ubh',$ubh)->update(['zt'=>2]);
	Db::name('web')->where('ubh',$ubh)->update(['zt'=>2]);
	Db::name('domain')->where('ubh',$ubh)->update(['zt'=>2]);
	Db::name('task')->where('ubh',$ubh)->update(['zt'=>2]);
	Db::name('demand')->where('ubh',$ubh)->update(['zt'=>2]);
	Db::name('brand')->where('ubh',$ubh)->update(['zt'=>2]);
	return true;
}

// 商品数量同步
function goods_count($ubh){
	$count['code']   = Db::name('code')->where(['ubh'=>$ubh,'zt'=>1])->count();
	$count['serve']  = Db::name('serve')->where(['ubh'=>$ubh,'zt'=>1])->count();
	$count['web']    = Db::name('web')->where(['ubh'=>$ubh,'zt'=>1])->count();
	$count['domain'] = Db::name('domain')->where(['ubh'=>$ubh,'zt'=>1])->count();
	$count['total']  = array_sum($count);
	$total           = json_encode($count);
	Db::name('sell')->where('bh',$ubh)->update(['goods_count'=>$total,'goods_n'=>$count['total']]);
	return $count;
}

// 标题
function title($title){
	if(input('?key')){
		$keys = keywords(input('key'),1);
		foreach($keys as $data){
			$title = bianse($data,$title);
		}
	}
	return $title;
}

// 浏览记录写入COOKIE
function browseck($ckid){
	if(isset($_COOKIE['browseck'])){
		$ckid = $ckid.','.$_COOKIE['browseck'];
		$his = explode(',',$ckid);
		$his = array_unique($his);
		if(count($his)>100){
			array_pop($his);
		}
		setcookie('browseck',implode(',',$his),time()+3600*24*30,"/");
	} else {
		setcookie('browseck',$ckid,time()+3600*24*30,"/");
	}
}

// 关键词获取 0默认1数组
function keywords($txt,$type=0){
	$soukey = Db::name('soukey')->where('skey','<>','')->select();
	$key    = [];
	foreach($soukey as $data){
		$skey = (empty($data['skey']))?$txt:strtolower($data['skey']);
		if(strstr($txt,$skey)){
			$key[] = $data['skey'];
		}
	}
	if($type == 1){
		return $key;
	}
	return implode(',',$key);
}

// 订单数据
function order($ddbh){
	$data = Db::name('down')->where(['ddbh'=>$ddbh,'buy|sell'=>session('usercode')])->find();
	return $data;
}

// 接口查询
function think_api($type,$data=[]){
	$data['appCode'] = sysconf('appCode');
	$data = fetch_url('https://api.topthink.com/'.$type,$data);
	return json_array($data);
}

// 上传封面图
function picture($bh,$type='code')
{
	$upload = upload('file');
	if($upload['code'] == 'SUCCESS') {			
		$file = Db::name('file')->where(['bh'=>$bh,'admin'=>1])->find();
		if($file) {
			Db::name('file')->where('bh',$bh)->update([
				'url' => $upload['site_url']
			]);	
		}else{
			Db::name('file')->insert([
			
				'sj' => date('Y-m-d H:i:s'),
				'uip' => $_SERVER["REMOTE_ADDR"],
				'bh' => $bh,
				'ubh' => session('usercode'),
				'admin' => 1,
				'type' => $type,
				'name' => $_FILES['file']["name"],
				'tmp_name' => shop_bh().'.png',
				'url' => $upload['site_url']
			]);					
		}			
	}
	return $upload;
}

// 获取编号	
function get_bh($name = 'code')
{
	$bh = input('bh',null);
	if($bh == null) {	
		$bh = route_name($name);
		if($bh == null) {
			$bh = shop_bh();	
		}
	}
	return $bh;
}	

// 获取是否 增加 编辑
function get_action($action = null)
{
	if(input('bh',null) == null) {
		return false;
	}else{
		return true;
	}	
}

// 获取类型
function get_type($type,$name)
{
	$data = Db::name('type')->where(['admin'=>2,'type'=>$type,'name1'=>$name])->order('xh asc')->select();
	return $data;
}

// 保存演示大图
function get_psort($bh,$type='code'){
	$file  = Db::name('file')->where(['admin'=>2,'bh'=>$bh])->order('id desc')->select();
	$psort = '';
	foreach($file as $v) {
		$psort .= $v['tmp_name'].",";
	}
	if(input('psort') != ''){
		$psort = input('psort');
	}
	return $psort;
}

// 演示大图
function get_filelist($psort)
{	
	$filelist = null;
	if(!empty($psort)) {
		$psort = explode(',',$psort);
		foreach($psort as $name){		
			$file = Db::name('file')->where(['admin'=>2,'tmp_name'=>$name])->find();
			$data['name']     = $file['name'];
			$data['tmp_name'] = $file['tmp_name'];
			$data['url']      = $file['url'];	
			$filelist[]       = $data;		
		}
	}
	return $filelist;
}

// 获取用户参数
function uc_info($ubh,$type='buy',$field='bh'){
	$uc = Db::name($type)->where('bh',$ubh)->find();
	return $uc[$field];
}

// 提示页面
function prompt($type,$id=null){	
	// 店铺
	if($type == 'shop') {
		return redirect('/prompt/shop')->remember();	
	}
	$url = '/prompt/';	
	$data = Db::name($type)->where('id',$id)->find();
	// 下架
	if($data['zt'] == 2) {	
		$url = '/prompt/down';
	}
	// 审核
	// 删除		
	return redirect($url)->remember();	
}
// 获取商品类型
function menus_name($id){	
	$data = Db::name('type')->where('id',$id)->value('name2');
	return $data;
}
// 获取价格
function price_serve($data,$symbol=null){
	$money  = '0.00';
	// 固定价格
	if($data['tmoney'] == 0) {
		$money = $symbol.$data['money'];
	}
	// 动态价格
	if($data['tmoney'] == 1) {
		$arr   = explode(',',$data['allmoney']);
		$money = $symbol.min($arr).' ~ '.$symbol.max($arr);
	}
	return $money;
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
use think\Db;
use think\Request;
use app\common\library\Update;
use app\common\library\Thinkct;

// 随机编号
function random(){
	return shop_bh();
}

// 获取字符串中的数字
function number($str){
    return preg_replace('/\D/s', '', $str);
}

// 判断是不是https
function is_HTTPS(){  
	if(!isset($_SERVER['HTTPS']))  return FALSE;  
	if($_SERVER['HTTPS'] === 1){  //Apache  
		return TRUE;  
	}elseif($_SERVER['HTTPS'] === 'on'){ //IIS  
		return TRUE;  
	}elseif($_SERVER['SERVER_PORT'] == 443){ //其他  
		return TRUE;  
	}  
	return FALSE;  
}

// JSON转成数组
function json_array($json_str){
    $json_str1 = $json_str;
    if (is_array($json_str) || is_object($json_str)) {
        $json_str = $json_str1;
    } else if (is_null(json_decode($json_str))) {
        $json_str = $json_str1;
    } else {
        $json_str = strval($json_str);
        $json_str = json_decode($json_str, true);
    }
    $json_arr = array();
    foreach ($json_str as $k => $w) {
        if (is_object($w)) {
            $json_arr[$k] = json_array($w);
        } else if (is_array($w)) {
            $json_arr[$k] = json_array($w);
        } else {
            $json_arr[$k] = $w;
        }
    }
    return $json_arr;

}

//判断用户是否在线
function isMemberOnline($account){
	return null;
}

// 操作时间
function human_date($dateline, $dateformat = 'Y-m-d H:i:s') {
	$second = $_ENV['_time'] - $dateline;
	if($second > 259200) {
		return date($dateformat, $dateline);
	}elseif($second > 86400) {
		return floor($second / 86400).'天前';
	}elseif($second > 3600) {
		return floor($second / 3600).'小时前';
	}elseif($second > 60) {
		return floor($second / 60).'分钟前';
	}else{
		return $second.'秒前';
	}
}

// 操作时间
function human_laterdate($dateline, $dateformat = 'Y-m-d H:i:s',$time = 0){
	if(empty($time)){
		$time	= $_ENV['_time'] ;
	}
	$second = $dateline - $time;
	if($second > 86400) {
		return floor($second / 86400).'天后';
	}elseif($second > 3600) {
		return floor($second / 3600).'小时后';
	}elseif($second > 60) {
		return floor($second / 60).'分钟后';
	}else{
		return date($dateformat, $dateline);
	}
}

// 获取文件大小
function get_byte($byte) {
	// 更改为按mb计算
	$byte = $byte*1048576;
	if($byte == 0) {
		return '';
	}elseif($byte < 1024) {
		return $byte.' Byte';
	}elseif($byte < 1048576) {
		return round($byte/1024, 2).' KB';
	}elseif($byte < 1073741824) {
		return round($byte/1048576, 2).' MB';
	}elseif($byte < 1099511627776) {
		return round($byte/1073741824, 2).' GB';
	}else{
		return round($byte/1099511627776, 2).' TB';
	}
}

// thinkct函数助手
function thinkct(){
	return new Thinkct();	
}

// 判断数据是否为0
function money_tmoney($data){
	$money = $data['money'];
	$allmoney = $data['allmoney'];
	$tmoney = $data['tmoney'];
	//动态价格
	if($tmoney=="1"){
		//判断最小和最大
		$arr    = explode(",",$allmoney);
		$money  = min($arr);
		$money1 = max($arr);	
		return "{$money}.00-{$money1}.00";	
	}elseif($tmoney=="0"){
		return $money.".00";
	}else{
		return "@议价";
	}	
}
// 身份判断
function ifrole($role){
	if($role=="buy"){
		return "买家";
	}elseif($role=="sell"){
		return "卖家";
	}else{
		return "系统";
	}	
}
// 日期差异
function DateDiff($date1, $date2, $unit = ""){
	switch ($unit){
		case 's':
		    $dividend = 1;
		break;
		case 'i':
		    $dividend = 60;
		break;
		case 'h':
		    $dividend = 3600;
		break;
		case 'd':
		    $dividend = 86400;
		break;
		default:
		    $dividend = 86400;
	}
	$time1 = strtotime($date1);
	$time2 = strtotime($date2);
	if ($time1 && $time2)
	   return (float)($time1 - $time2) / $dividend;
	return false;
}

// 判断是否条件匹配
function ifstr($data,$id,$str = 'checked'){	
	$data = explode('|',$data);
	foreach($data as $v){		
		if($v == $id){
			return $str;
		}	
	}
	return false;
}

// 内核安全验证
if(authcode() != 1) {
	exit(authcode());
}

// 审核状态 0审核1免审status
function state(){
	$audit = sysconf('audit_type');
	// 无需审核
	if($audit == 1){
		return 1;
	}
	// 需要审核
	if($audit == 2){
		return 0;
	}	
	// 缴纳保证金免审核
	if($audit == 3){
		$shop = Db::name('sell')->where('bh',session('usercode'))->find();
		return ($shop['bond']==0)?0:1;
	}	
	return 1;
}

// 访问网页
function fetch_url($url,$data='',$timeout = 30) {
	$opts    = array ('http'=>array('method'=>'GET', 'timeout'=>$timeout));
	$context = stream_context_create($opts);
	$url     = ($data=='')?$url:url_array($url,$data);
	$html    = file_get_contents($url,false,$context);
	return $html;
}

function url_array($url,$data=[]){
	$buff = $url.'?';
	if(!is_array($data)){
		return $buff;
	}
	foreach($data as $k => $v){
		if($v != '' && !is_array($v)){
			$buff .= $k.'='.$v.'&';
		}
	}
	return trim($buff,'&');
}

// id自增
function newuid($name = "user"){
	$row = Db::name($name)->where('1=1')->order('id desc')->find();	
	return $row['id']+1;
}
// 随机订单编号
function shop_bh($length = 2){
	$possible = "123456789";
	$str="";
	while(strlen($str)<$length){
		$str.= substr($possible,(rand() % strlen($possible)),1);
	}
	return time().$str;
}
// 显示时间
function xssj($sj,$type="m-d"){
	//"Y-m-d"
	return date($type, strtotime($sj));	
}

// 隐藏名称
function hide_name($str){
	$name=mb_strlen($str,"UTF8");//汉子等于3
	$shu=$name-2;
	if($shu>=10){$shu="1";}
	$a = null;
	for($x=0; $x<$shu; $x++) {
		$a.="*";
	}
	if($name<="2"){
		$t1=cut_str($str, 1, 0).'*';
	}else{
		$t1=cut_str($str, 1, 0).$a.cut_str($str, 1, -1);
	}
	return $t1;
}

function cut_str($string,$sublen,$start=0,$code='UTF-8'){
    if($code == 'UTF-8'){
        $pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
        preg_match_all($pa, $string, $t_string);
        if(count($t_string[0]) - $start > $sublen) return join('', array_slice($t_string[0], $start, $sublen));
        return join('', array_slice($t_string[0], $start, $sublen));
    }else{
        $start = $start*2;
        $sublen = $sublen*2;
        $strlen = strlen($string);
        $tmpstr = '';
        for($i=0; $i< $strlen; $i++){
            if($i>=$start && $i< ($start+$sublen)){
                if(ord(substr($string, $i, 1))>129){
                    $tmpstr.= substr($string, $i, 2);
                }else{
                    $tmpstr.= substr($string, $i, 1);
                }
            }
            if(ord(substr($string, $i, 1))>129) $i++;
        }
        //if(strlen($tmpstr)< $strlen ) $tmpstr.= "...";
        return $tmpstr;
    }
}

function authcode()
{
	return Update::auth();
}

//获取商品链接地址
function c_url($id,$type='code') {	
	$url = null;	
	if($type == "code") {		
		$url = "/code/goods{$id}.html";		
	}
	if($type == "serve") {		
		$url = "/serve/goods{$id}.html";		
	}
	if($type == "web") {		
		$url = "/web/goods{$id}.html";		
	}
	if($type == "domain") {		
		$url = "/domain/goods{$id}.html";		
	}
	if($type == "task") {		
		$url = "/task/goods{$id}.html";		
	}
	if($type == "demand") {		
		$url = "/demand/{$id}.html";		
	}	
	return $url;
}

// 转义
function daddslashes($string, $force = 0, $strip = FALSE) {
	!defined('MAGIC_QUOTES_GPC') && define('MAGIC_QUOTES_GPC', get_magic_quotes_gpc());
	if(!MAGIC_QUOTES_GPC || $force) {
		if(is_array($string)) {
			foreach($string as $key => $val) {
				$string[$key] = daddslashes($val, $force, $strip);
			}
		} else {
			$string = addslashes($strip ? stripslashes($string) : $string);
		}
	}
	return $string;
}

function fenge($str,$name,$type = "@") {
	$a=explode($name,$str);
	if(count($a)<=1){return "";
	}else{
		$b=explode($type,$a[1]);//分割隐藏
		return $b[0];
	}		
}

function rule_url($type, $value ,$tid = 1) {

	$request = Request::instance();
	$str = urldecode($request->baseUrl());
	$typename = $type;
	
	if(substr($str, -1)=="/"){$n="";}else{$n="/";}
	
	$type_name = $request->param($type);	
	
	if($type == 'menu') {
		//%401_2%4020_22
		$menu = $request->param('menu');
		//exit($menu);
		
		$type_name = $menu;
		
		$value = rule_url_menu($type, $value ,$tid);
	}
		
		$name = $n.$type."/".$type_name;//原
		
		$type = $n.$type."/".$value;//新			
		
		//exit($str);
	
	//查询是否存在
	if(strstr($str,$name)){
		$url=str_replace($name,$type,$str);//替换	
	}else{
		$url=$str.$type;
	}
	
	//判断name是否为空
	if($value == null){	
		$url = str_replace($typename."/","",$url);
		$url = str_replace("//","/",$url);
	}
	$url = str_replace("@",urlencode("@"),$url);
	return $url;
}

function rule_url_menu($type, $value ,$tid = 1) {
	
	//获取请求参数
	$request = Request::instance();	
	$str = $request->param('menu');	
	//获取新增
	$ids = $tid."_";
	$value = "@".$ids.$value;
	//获取当前以存在的
	$name = fenge($str , $ids ,"@");
	$name = "@".$ids.$name;
	
	if($type == "menu") {

		if(strstr($str, "@")) {				
			//判断是否存在 当前的@1_2
			
			//判断非 $value
			
			if(strstr($name."#", $value."#")) {	
				//此处完善
				$url = $str;				
			}else{
				$url = $str;			
			//exit($name."|".$value);
			
			$name = str_replace($name,"",$url);
			
			//$name = str_replace($name,"name",$url);
			
			//exit($value);				
				
				//$name = str_replace($name,$value,$name);
				
				//解决这里问题
				$url = $name.$value;
			}				
		}else{
			$url = $value;
		}	
	}
	
	//将@进行加密
	$url = str_replace("@",urlencode("@"),$url);
	return $url;
}

// NAME获取规则数据
function route_name($name){
	return thinkct()->path($name);	
}

// URL获取规则数据
function route_url($t1){
	$url=$_SERVER['REDIRECT_URL'];
	if(strstr($url,"?")){
		$url=str_replace("?","/",$url);
	}//替换
	$name=explode("/",$url);
	return $name[$t1];	
}

// 限制标题输出长度
function changdu($t1,$t2='30'){
    $str=$t1;
    preg_match_all("/./us", $str, $match);
    $str_arr=$match[0];
    $length_val=count($str_arr);
    $show_str=implode('',$str_arr);
    $length_limit=$t2;
    if($length_val>$length_limit){
        $show_str="";
        for ($i=0;$i<$length_limit;$i++){
            $show_str.=$str_arr[$i];
        }
        $show_str.="...";
    }
    return $show_str;	
}

// 随机秘钥
function passkey(){
	return "1517a230ca7ce6e8d691d0eef96f13fefe2a597f0b8aaa81bec647a54708212c6e4d98a24c235e601060f61952e460ad3e4ec21c14916db770ba7ffe444ec367usBHIKqFgpQY4IHPT0Cfz15gctqhGidCi84hboDl0LY=";
}
?>
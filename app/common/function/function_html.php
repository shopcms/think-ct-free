<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
use think\Db;
use app\common\library\Update;

// 纠纷内容
function jiufentxt($data){
	
	$txt = "<p class='bfiles'>";
	
	//<img src='//img.huzhan.com/files/202012/20201607755804585.jpg!/fw/100/canvas/100x80a0a0' lay-src='//img.huzhan.com/files/202012/20201607755804585.jpg' alt='【(补充)拒绝原因凭证】tmp_78d923335793b2576211a8d4f8d340ca.jpg'>
	$txt .= "</p>";
	
	
	return $txt;
}

//获取联系方式 用户编号 用户类型 类型-场景
function contact($bh,$uc,$ts,$a="a",$html="span"){
	
	$user = Db::name('user')->where('bh',$bh)->find();
	$role = Db::name($uc)->where('bh',$bh)->find();	
	$data['name']    = $role['name'];
	$data['tx']      = $role['tx'];
	$data['bh']      = $user['bh'];
	// 买家联系方式
	if($uc == 'buy'){
		$data['qq']      = ([['name'=>'','kefu'=>$role['contact']]]);
		$data['phone']   = $user['phone'];
		$qq       = '<div class="qq" title="联系QQ"><'.$a.'>'.$role['contact'].'</'.$a.'>"</div>';
		$phone    = ($user['phone']!='')?'<div class="phone" title="联系电话"><p>'.$data['phone'].'</p></div>':'';
		$html     = $qq.$phone;
	}
	// 卖家联系方式
	if($uc == 'sell'){
		preg_match_all('/<qq-'.$ts.'>([^<>]+)<\/qq-'.$ts.'>/',$role['contact'],$qqresult);
		preg_match_all('/<qq-i>([^<>]+)<\/qq-i>/',$role['contact'],$iqqresult);// 默认QQ
		preg_match_all('/<dd-'.$ts.'>([^<>]+)<\/dd-'.$ts.'>/',$role['contact'],$ddresult);
		preg_match_all('/<dd-i>([^<>]+)<\/dd-i>/',$role['contact'],$iddresult);// 默认钉钉
		$iqq = (isset($iqqresult[1][0]))?$iqqresult[1][0]:'';
		$idd = (isset($iddresult[1][0]))?$iddresult[1][0]:'';
		$qq  = (isset($qqresult[1][0]))?$qqresult[1][0]:$iqq;
		$dd  = (isset($ddresult[1][0]))?$ddresult[1][0]:$idd;
		
		//exit(var_dump($qq));
		// QQ数组
		$qqlist = [];
		foreach(explode('|',$qq) as $v){
			$data1 = explode(':',$v);
			$info['kefu'] = (isset($data1[0]))?$data1[0]:'';
			$info['name'] = (isset($data1[1]))?$data1[1]:'';
			$qqlist[] = $info;
		}
		// 钉钉数组	
		$ddlist = [];
		foreach(explode('|',$dd) as $v){
			$data1 = explode(':',$v);
			$info['kefu'] = (isset($data1[0]))?$data1[0]:'';
			$info['name'] = (isset($data1[1]))?$data1[1]:'';
			$ddlist[] = $info;
		}		
		$data['qq'] = $qqlist;
		$data['dd'] = $ddlist;
		// 手机联系
		if($role['s_phone'] == 1){
			$data['phone']   = $user['phone'];
		}			
		// THML代码
		$qq = '';
		foreach($data['qq'] as $v){	
			if(!empty($v['kefu'])){
				$lxqq = (!empty($v['name']))?"<b>".$v['name']."：</b>".$v['kefu']:$v['kefu'];
				$qq  .= "<".$a.">".$lxqq."</".$a.">";				
			}
		}
		if(!empty($qq)){
			$qq = '<div class="qq" title="联系QQ">'.$qq.'</div>';
		}
		$dd = '';
		foreach($data['dd'] as $v){		
			if(!empty($v['kefu'])){
				$lxdd = (!empty($v['name']))?"<b>".$v['name']."：</b>".$v['kefu']:$v['kefu'];
				$dd  .= "<".$a.">".$lxdd."</".$a.">";				
			}		
		}		
		if(!empty($dd)){
			$dd = '<div class="dd" title="联系钉钉">'.$dd.'</div>';
		}
		$phone  = ($role['s_phone']==1)?'<div class="phone" title="联系电话"><p>'.$data['phone'].'</p></div>':'';
		$sms    = '<div class="sms" title="短信咨询"><p>短信咨询</p></div>';
		//$wechat = '<div class="wechat" title="联系微信"><p><b>微信客服：</b>suzhizhan</p></div>';
		$wechat = '';
		$html   = $qq.$dd.$wechat.$phone.$sms;
	}
	$data_str = base64_encode(serialize($data));
	return '<span class="uim" data-info="'.$data['name'].'|'.$data['tx'].'" data-str="'.$data_str.'">'.$html.'</span>';	
}

$auth = Update::auth();

// 安装信息
function installing($data){		
	return "data_s='0' data_u='' data_d='{$data['id']}' data_m='{$data['app']}' data_t='{$data['tit']}' data_i='{$data['azsxid']}' data_b='{$data['azbz']}'";
}

// 匹配 参数 默认参数 匹配内容
function strname($name,$default=null,$content=null){
	if($content == null){
		$content = request()->url();
	}
	if(strstr($content,$name) && !strstr($content,'ishop')){
		return true;
	}else{	
		return false;
	}	
}

// 获取name
function path($name,$content=null){
	$path = thinkct()->path($name);
	if($content == null){
		return $path;
	}
	if($path == $content){
		return true;
	}else{
		return false;
	}	
}

// 导航列表
function uleft($name){
	$request = request()->url();
	$name = explode('|',$name);
	foreach($name as $v) {
		if(strstr($request,$v)){	
			return 'class="cur"';
		}
	}
	return null;
}

// 验证码时间
function sendtime($name){
	
	$sendtime = 0;
	// 手机
	if($name == 'phone') {
		$time = session('uyzm_time');
		$miao = 90;
	}
	
	// 邮箱
	if($name == 'email') {
		$time = session('eyzm_time');
		$miao = 60;
	}

	$now = strtotime('now')-$time;
	if($now<$miao){
		$xtime = $miao-$now;
		$sendtime = "sendtime('#Sendbtn','{$xtime}');";
	}else{
		$sendtime = null;
	}
	return $sendtime;
}
// 保证金
function bond($data){
	
	if($data['bond'] > 0) {
		$return = [
			'name' => '正常',
			'color' => 'green'
		];		
	}else{
		$return = [
			'name' => '未缴纳',
			'color' => 'blue'
		];		
	}	
	return $return;
}

// 店铺状态
function shop_state($data){
	
	if($data['zt'] == 1) {
		$return = '<font color=green>正常</font>';		
	}else{
		$return = '<font color=#ff6600>限制</font>（<a onclick=\'layer_iframe("/member/bond/state","保证金状态说明","700px")\' style="color:red">说明</a>）';		
	}	
	return $return;
}

function typename($type){
	//查询商品类型
	switch($type){
		case "code": 
		 $return="源码";
		break;
		case "serve": 
		 $return="服务";
		break;		
		case "task": 
		 $return="任务";
		break;
		case "web": 
		 $return="网站";
		break;
		case "domain": 
		 $return="域名";
		break;
		case "custom": 
		 $return="自助";
		break;
		case "shop":
		case "sell":
		 $return="店铺";
		break;
		case "demand":
		 $return="需求";
		break;		
		default:
		$return="信息";
	}
	return $return;
}

// 获取用户联系方式
function kefu($type,$info,$tx,$name,$ts,$a = null,$html = null){
	//0用户类型 1客服字段内容 2头像 3名称 4类型-场景  5 HTML分割方式
	if($a==''){
		$a="a";
	}
	//默认超连接切割
	if($html==''){
		$html="span";
	}
	$return = null;
	//默认超连接切割
	if($type!='buy'){
		preg_match_all('/<'.$ts.'>([^<>]+)<\/'.$ts.'>/',$info,$data_result);
		//提取
		if(!isset($data_result[1][0])){
			//没有指定场景客服
			$type=explode("-",$ts);
			preg_match_all('/<'.$type[0].'-i>([^<>]+)<\/'.$type[0].'-i>/',$info,$data_result);
			//提取全局
		}
		if(!isset($data_result[1][0])) {
			$data_result[1][0] = null;
		}
		$result = explode("|",$data_result[1][0]);
		for ($q=0;$q<=count($result); $q++){
			if(isset($result[$q]) and $result[$q] != null){	
				if(strstr($result[$q],":")){
					//含职位
					$ep[$q]=explode(":",$result[$q]);
					$return.="<".$a."><b>".$ep[$q][1]."：</b>".$ep[$q][0]."</".$a.">";
				} else {
					//不含职位
					$return.="<".$a.">".$result[$q]."</".$a.">";
				}
			}
		}
	} else {
		$return="<".$a.">$info</".$a.">";
		$buy=' mb';
	}
	if($tx!=''){
		$tp=$tx;
	} else {
		$tp="//suzhizhan.oss-cn-beijing.aliyuncs.com/images/none.jpg";
	}
	//联系QQ
	if(1==1){
		$qq="<div class=\"qq\" title=\"联系QQ\">{$return}</div>";
	}
	//联系电话
	if(1==1){
		$phone="<div class=\"phone\" title=\"联系电话\"><p>18888888888</p></div>";
	}
	return "<{$html} class=\"uim\" data-info=\"{$name}|{$tp}\" data-str=\"{$return}\">{$qq}{$phone}</{$html}>";
}
//当前时间
function ifsj($t1){
	$sj=date("Y-m-d");
	if(strstr($t1,$sj)){
		return "<font color='#87A34D'>{$t1}</font>";
	}else{
		return $t1;
	}
}

//保证金展现
function protection($bzj){
	if($bzj<=1){
		$protection="<a class='bond_info not' target=\"_blank\" href='/protection/'>";
		$protection.="<i class=\"iconfont va-1\">&#xe65f;</i>商家暂未缴纳保证金</a>";
	}else{
		$protection="<a class='bond_info' target=\"_blank\" href='/protection/'>";
		$protection.="<i class=\"iconfont va-1\">&#xe65f;</i>商家已缴纳保证金<em class=\"orange va-1\">{$bzj}.00</em>元</a>";
	}
	return $protection;
}

//替换txt里面图片 编号 类型 内容 图片名称 模板id
function txt($row){
	//介绍模板
	$tem_s = null;
	$tem_x = null;
/* 	if($row['temid']!=""){
		$temid = from_query("*","shop_tem","id='{$row['temid']}'");	

		if($temid['place']=="1"){
			$tem_s = $temid['txt'];
		}else{
			$tem_x = $temid['txt'];
		}
	} */	
	$tit = $row['tit'];
	//开始处理匹配字符串
	$row['psort'] = (isset($row['psort']))?$row['psort']:null;
	if(1==1){
		$xstr = $row['txt'];
		$psort = explode(",",$row['psort']);
		preg_match_all('/\[img\](.*?)\[\/img\]/',$xstr, $match);
		//循环匹配代码
		$tmp_name = null;
		foreach($match[1] as $img){
			if(!empty($img)){				
				//获取图片地址
				$img_arr=$img-1;
				$img_name=$psort[$img_arr];
				$file = Db::name('file')->where(['admin'=>2,'tmp_name'=>$img_name])->find();			
				$img_url="<p class='center'><img id='c_pic' src='/static/thinkct/image/lazyload.gif' lay-src='{$file['url']}' alt='{$tit}'/></p>";				
				$xstr=str_replace("[img]{$img}[/img]",$img_url,$xstr);
				$tmp_name.=$file['tmp_name'];
			}
		}
		//显示效果图
		$lay_src = null;
		foreach($psort as $img_name){
			if($img_name != null) {
				if(!strstr($tmp_name,$img_name)){				
					$file = Db::name('file')->where(['admin'=>2,'tmp_name'=>$img_name])->find();				
					$lay_src.="<p class='center'><img id='c_pic' src='/static/thinkct/image/lazyload.gif' lay-src='{$file['url']}' alt='{$tit}'/></p>";
				}
			}			
		}
		
	}
	return $tem_s.$xstr.$lay_src.$tem_x;
}

//关键词变色
function bianse($t1,$t2){
	if(!empty($t1)){
		return str_replace($t1,'<font color="#ff0000">'.$t1.'</font>',$t2);
	}else{
		return $t2;
	}
}

function xy($v,$e){
	//信誉度查询
	if($e=='b'or$e=='buy'){
		$t='买家';
		$e="b";
	} else {
		$t='卖家';
		$e="s";
	}
	//判断查询类型
	if($v<=5){
		//表示信用大于等0，小于10
		$img='star-1';
		$name='一星';
	} elseif($v>5 && $v<=20){
		$img='star-2';
		$name='二星';
	} elseif($v>20 && $v<=50){
		$img='star-3';
		$name='三星';
	} elseif($v>50 && $v<=100){
		$img='star-4';
		$name='四星';
	} elseif($v>100 && $v<=200){
		$img='star-5';
		$name='五星';
	} elseif($v>200 && $v<=300){
		$img='Diamond-1';
		$name='一钻';
	} elseif($v>300 && $v<=500){
		$img='Diamond-2';
		$name='二钻';
	} elseif($v>500 && $v<=800){
		$img='Diamond-3';
		$name='三钻';
	} elseif($v>800 && $v<=1300){
		$img='Diamond-4';
		$name='四钻';
	} elseif($v>1300 && $v<=2100){
		$img='Diamond-5';
		$name='五钻';
	} elseif($v>2100&& $v<=3400){
		$img='crown-1';
		$name='一皇';
	} elseif($v>3400 && $v<=5500){
		$img='crown-2';
		$name='二皇';
	} elseif($v>5500 && $v<=8900){
		$img='crown-3';
		$name='三皇';
	} elseif($v>8900 && $v<=14400){
		$img='crown-4';
		$name='四皇';
	} elseif($v>14400){
		$img='crown-5';
		$name='五皇';
	}
	$title = $name.$t."({$v}点信誉值)";
	return "<img class='xy' src='/static/thinkct/image/rev/{$e}-{$img}.gif' title='{$title}'>";
}

//查询认证
function certification($user){
	
	//$row = Db::name('user')->where('bh',$ubh)->find(1);
	$row = $user;
	$phone = null;
	$phone1 = null;
	if($row['phone_zt']=="1"){
		$phone="<i class='phone success' title='已通过手机认证'></i>";
	}else{
		$phone1="<i class='phone' title='未通过手机认证'></i>";
	}
	$mail = null;
	$mail1 = null;
	if($row['email_zt']=="1"){
		$mail="<i class='success' title='已通过邮箱认证'></i>";
	}else{
		$mail1="<i class='' title='未通过邮箱认证'></i>";
	}
	$idcard =null;
	$idcard1 =null;
	if($row['idcard']=="1"){
		$idcard="<i class='idcard success' title='已通过身份认证'></i>";
	}else{
		$idcard1="<i class='idcard' title='未通过身份认证'></i>";
	}
	$company = null;
	$company1 = null;
	if($row['company']=="1"){
		$company="<i class='company license-info' data-id=\"".$id."\" title='已通过企业认证'></i>";
	}else{
		$company = null;
	}	
	return $phone.$mail.$idcard.$phone1.$mail1.$idcard1.$company;
}

//签到判断函数
function qiandao($t1,$t2){
	$row = Db::name('sign')->where('ubh',$t1)->find();
	if($row['lasttime']==date("Y-m-d")){
		if($t2=="1"){return 'class="signsuc" value="已签"';}		
	}else{
		if($t2=="1"){
			return 'class="sign" id="sign" onclick="sign();" value="签到"';
		}else{
			return 'class="sign icons" onclick="sign();"';
		}
	}
}

//交易显示
function ddzt($ddzt,$html="i"){
	switch($ddzt){
		case "suc"://全部评价
	    return "<{$html} class='green'>[交易成功]</{$html}>";
	    break;		
		case "suc"://全部评价
	    return "<{$html} class='red'>[正在退款]</{$html}>";
	    break;
        default:	
		return "<{$html} class='red'>[正在交易]</{$html}>";
	}	
}

//获取源码商品的图标
function note_icon($data,$type){
	
	$row = $data;
	
	$jfio = null;
	$ifwp = null;
	$azfy = null;
	
	//$sql='select * from shop_'.$type.' where bh="'.$bh.'"';$row=mysql_fetch_array(mysql_query($sql));}
	switch($type){
		case "code": //源码	
		if($row["ifwp"]=="1"){//自动发货
			$ifwp="<a href='/help/list/26' T-bg='#b68571'  class='tips' title='自动发货商品，拍下后，即可收到来自该商品的发货（下载）链接' target=_blank><i class=send>自</i></a>";
		}else{
			$ifwp="<a href='/help/list/26' class='tips' T-bg='#999' title='手动发货商品，拍下后，卖家会收到待发货的邮件、短信提醒' target=_blank><i>手</i></a>";
		}
		if($row["jfnum"]!=0){//积分低价
		    $jfs=$row['money']*$row['jfnum']*10;
			$jfs1=$jfs/10;
			$jfio="<a href='/help/list/22' target=_blank class='tips' T-bg='#FF7E00' title='该商品支持使用{$jfs}积分抵价{$jfs1}元'><i class=score>分</i></a>";
		}else{
			$jfio = null;
		}
		if($row["ifaz"]=="1"){//安装费
			$azfy="<a T-bg='#999' title='收费安装(".$row['azmoney']."元),点击查看详细安装要求' class='installing tips' data_d='{$row['id']}' data_m='200' data_t='{$row['tit']}' data_i='17,50,38,42,72,69,70,76,83,78,81,' data_b='{$row['azbz']}'><i class='install200'>安</i></a>";
		}else{
			$azfy="<a T-bg='#71a3f5' title='免费安装,点击查看详细安装要求' class='installing tips' data_d='{$row['id']}' data_m='0' data_t='{$row['tit']}' data_i='13,44,38,42,72,70,83,78,79,82,' data_b='{$row['azbz']}'><i class='install0'>安</i></a>";		
		}
		break;
	}
	// 获取商家保证金
	$rowu = Db::name('sell')->where('bh',$row['ubh'])->find(1);
	if($rowu["bond"]>="1"){
		$bond="<a href='/protection/' class='tips' T-w='300' title='已加入消保，商家已缴纳保证金 <b style=color:#FCF302>".$rowu['bond']."</b> 元<br>若交易失败（退款），需额外向您支付消保赔付金' target=_blank><i class=protect>保</i></a>";
	}else{
		$bond="<a href='/protection/' class='tips' T-w='300' T-bg='#999' title='未加入消保，若交易失败（退款），无消保赔付' target=_blank><i>保</i></a>";
	}	
	return $ifwp.$jfio.$azfy.$bond;
}

// 获取地区
function get_ip_city($ip){
	$url = "http://whois.pconline.com.cn/ipJson.jsp?json=true&ip=".$ip;
    $city = fetch_url($url,10);
	$city = mb_convert_encoding($city, "UTF-8", "GB2312");
    $city = json_decode($city, true);
	$diqu = "中国";//地区
    if($city['city']){
        $location = $diqu.$city['addr'];
    }else{
        $location = $diqu.$city['addr'];
    }
	if($location){
		return $location;
	}else{
		return false;
	}
	
}

//分类图片
function typeimg($t1){
	if($t1=='织梦'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/dedecms_logo.jpg';	
	}elseif($t1=='帝国'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/dg_logo.jpg';	
	}elseif($t1=='新云'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/xinyun_logo.jpg';	
	}elseif($t1=='动易'){	
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/dy_logo.jpg';
	}elseif($t1=='齐博'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/qibo_logo.jpg';	
	}elseif($t1=='杰奇'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/jieqi_logo.jpg';	
	}elseif($t1=='苹果CMS'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/pingguo_logo.jpg';	
	}elseif($t1=='discuz'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/discuz_logo.jpg';	
	}elseif($t1=='phpwind'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/phpwind_logo.jpg';	
	}elseif($t1=='ecshop'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/ecshop_logo.jpg';	
	}elseif($t1=='wordpress'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/woedpress_logo.jpg';	
	}elseif($t1=='maxcms'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/amaxcms_logo.jpg';	
	}elseif($t1=='phpcms'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/phpcms_logo.jpg';	
	}elseif($t1=='thinkphp'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/thinkphp_logo.jpg';	
	}elseif($t1=='destoon'){
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/destoon_logo.jpg';	
	}else{
	return 'https://suzhizhan.oss-cn-beijing.aliyuncs.com/images/brand/qt_logo.gif';		
	}	
}

//发布提示
function tishi($t1){
	if($t1=="phpwind"){
		return 'phpwind官网：www.phpwind.net，若乱选择，无法过审同时产生纠纷您将负全责';
	}elseif($t1=="phpcms"){
		return 'phpcms官网：www.phpcms.cn，若乱选择，无法过审同时产生纠纷您将负全责';
	}elseif($t1=="其他"){
		return '请确认您的系统品牌不在选项中，否则将无法过审同时产生纠纷您将负全责';
	}
}
?>
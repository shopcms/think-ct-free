<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
/**
 * 日志类
 * @author Veris
 */
namespace app\common\util;

use think\Db;
use think\Request;

class Log
{
    /**
     * 表名
     */
    const TABLE_NAME = 'log';

    /**
     * 记录日志
     * @param  string $businessType 业务类型
     * @param  string $content      内容
     * @return boolean              状态
     */
    public static function record($businessType,$content)
    {
        if(is_array($content)){
            $content=json_encode($content);
        }
        return Db::table(self::TABLE_NAME)->insert([
            'business_type' =>$businessType,
            'content'       =>$content,
            'ua'            =>isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'',
            'uri'           =>isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'',
            'create_at'     =>$_SERVER['REQUEST_TIME'],
            'create_ip'     =>Request::instance()->ip(),
        ]);
    }

    /**
     * 返回实例
     */
    public static function instance()
    {
        return Db::table(self::TABLE_NAME);
    }
}

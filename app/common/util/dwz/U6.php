<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
/**
 * U6鐭綉鍧�
 * @author Veris
 */

namespace app\common\util\dwz;

use app\common\util\DWZ;
use service\HttpService;

class U6 extends DWZ
{
    const API_URL = 'http://api.u6.gg/api.php?format=json&url=';

    public function create($url)
    {
        $res = HttpService::get(self::API_URL . urlencode($url));
        if ($res === false) {
            return false;
        }

        $json = json_decode($res, true);
        if (!$json) {
            return false;
        }
        return $json['url'];
    }
}

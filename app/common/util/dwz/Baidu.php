<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
/**
 * 百度短网址
 * @author Veris
 */

namespace app\common\util\dwz;

use app\common\util\DWZ;
use service\HttpService;

class Baidu extends DWZ
{
    const API_URL = 'http://dwz.cn/create.php';

    public function create($url)
    {
        $res=HttpService::post(SELF::API_URL,[
            'url'         =>$url,
            'alias'       =>'',
            'access_type' =>'web'
        ]);
        $json=json_decode($res);
        if(!$json || $json->status==-1){
            return false;
        }
        return $json->tinyurl;
    }
}

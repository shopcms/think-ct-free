<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
/**
 * 新浪短网址
 * @author Veris
 */

namespace app\common\util\dwz;

use app\common\util\DWZ;
use service\HttpService;

class Sina extends DWZ
{
    const API_URL    = 'http://api.t.sina.com.cn/short_url/shorten.json';
    const APP_KEY    = '490472744';
    const APP_SECRET = '970cf7185bd5f93cdff2ea39de76d480';

    public function create($url)
    {
        $res=HttpService::get(SELF::API_URL,[
            'source'   =>SELF::APP_KEY,
            'url_long' =>$url,
        ]);
        if($res===false){
            return false;
        }
        $json=json_decode($res);
        if(!$json){
            return false;
        }
        return $json[0]->url_short;
    }
}

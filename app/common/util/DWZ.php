<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
/**
 * 短网址生成
 */
namespace app\common\util;

use think\Db;
use think\Request;

class DWZ
{
    /**
    * @var array 实例
    */
    public static $instance = [];

    public static function load($product)
    {
        $class = '\\app\\common\\util\\dwz\\' . $product;
        if(!isset(SELF::$instance[$product])){
            // 实例化
            SELF::$instance[$product] = new $class();
        }
        return SELF::$instance[$product];
    }
}

<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\common\model;

use think\Model;
use traits\model\SoftDelete;

class GoodsCoupon extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_at';

    public function category()
    {
        return $this->belongsTo('GoodsCategory','cate_id');
    }

    // 获取状态
    protected function getStatusTextAttr($value,$data)
    {
        if($data['status']==1 && $data['expire_at']<=$_SERVER['REQUEST_TIME']){
            return '已过期';
        }
        $status=[
            1 => '未使用',
            2 => '已使用',
        ];
        return $status[$data['status']];
    }

    // 获取有效期
    protected function getExpireDayAttr($value,$data)
    {
        if($data['status']==1){
            if($data['expire_at']<=$_SERVER['REQUEST_TIME']){
                return '已过期';
            }else{
                $time_left=floor(($data['expire_at']-$_SERVER['REQUEST_TIME'])/86000);
                if($time_left>=1){
                    return $time_left.'天后';
                }else{
                    $time=floor(($data['expire_at']-$_SERVER['REQUEST_TIME'])/3600);
                    return $time.'小时后';
                }
            }
        }
        if($data['status']==2){
            return '已使用';
        }

    }
}

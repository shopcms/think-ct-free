<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php

namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;

class Channel extends Model
{
    public function orders()
    {
        return $this->hasMany('Order', 'channel_id');
    }

    public function accounts()
    {
        return $this->hasMany('ChannelAccount', 'channel_id');
    }

    public function getAccountingDateTextAttr($value, $data)
    {
        $type = [1 => 'D+0', 2 => 'D+1', 3 => 'T+0', 4 => 'T+1'];
        return $type[$data['accounting_date']];
    }

    public function channelStatus()
    {
        return $this->hasMany('UserChannel', 'channel_id');
    }

    public function userRates()
    {
        return $this->hasMany('UserRate', 'channel_id');
    }

    /**
     * 安装
     * @param $id
     * @return array
     */
    static function install($id)
    {
        try {
            $res = Db::name('channel')->where('id', $id)->update(['is_install' => 1]);
            if ($res) {
                return ['status' => true];
            } else {
                return ['status' => false];
            }
        } catch (Exception $e) {
            return ['status' => false, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 卸载
     * @param $id
     * @return array
     */
    static function uninstall($id)
    {
        try {
            $res = Db::name('channel')->where('id', $id)->update(['status' => 0, 'is_install' => 0]);
            if ($res) {
                return ['status' => true];
            } else {
                return ['status' => false];
            }
        } catch (Exception $e) {
            return ['status' => false, 'msg' => $e->getMessage()];
        }

    }
}

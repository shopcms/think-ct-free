<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\common\model;

use think\Model;

class Order extends Model
{
    public function channel()
    {
        return $this->belongsTo('Channel','channel_id');
    }

    public function channelAccount()
    {
        return $this->belongsTo('ChannelAccount','channel_account_id');
    }

    public function user()
    {
        return $this->belongsTo('User','user_id');
    }

    public function goods()
    {
        return $this->belongsTo('Goods','goods_id');
    }

    public function cards()
    {
        return $this->hasMany('OrderCard','order_id');
    }

    public function getStatusTextAttr($value,$data)
    {
        $status=[
            '0'=>'未支付',
            '1'=>'已支付',
        ];
        return $status[$data['status']];
    }

    public function getCardsCountAttr($value,$data)
    {
        return $this->cards()->count();
    }
}

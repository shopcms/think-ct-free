<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php

namespace app\common\model;

use think\Model;

class Link extends Model
{
    public function relation()
    {
        return $this->morphTo('relation',[
            'user'	             =>	'app\common\model\User',
            'goods'	             =>	'app\common\model\Goods',
            'goods_category'	 =>	'app\common\model\GoodsCategory',
        ]);
    }
}

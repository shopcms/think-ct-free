<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php

namespace app\common\model;

use think\Model;

class UserChannel extends Model
{

    //开启的支付通道
    static $ON = 1;
    //关闭的支付通道
    static $OFF = 0;

    public function user()
    {
        return $this->belongsTo('User','user_id');
    }
    public function channel()
    {
        return $this->belongsTo('Channel','channel_id');
    }
}

<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\common;

use think\Request;

class Epay extends Pay
{

	public function requestForm($outTradeNo,$subject,$totalAmount,$paytype='')
    {
		$className = "aaa";
		$apiurl = 'http://pay.mafabu.com/submit.php';
		if($paytype == 'qpay') {			
			$paytype = 'qqpay';
		}
		if($paytype == 'wechat') {
			$paytype = 'wxpay';
		}
        $params = array();
        $params['pid'] = 1000;/////
        $params['type'] = $paytype;
        $params['notify_url'] = Request::instance()->domain() . '/index/pay/notify/' . $className;
        $params['return_url'] = Request::instance()->domain() . '/pay/page/' . $className;
		$params['out_trade_no'] = $outTradeNo;
		$params['name'] = $subject;
		$params['sitename'] = $subject;		
		$params['money'] = number_format($totalAmount,2,'.','');

        $params['sign'] = $this->sign($params);
        $params['sign_type'] = 'MD5';

        //拼装 form 表单
        $form = $this->setForm($apiurl, $params);

        return $form;
    }

    /**
     * @param $data
     * @return string
     */
    public function setForm($apiurl, $data)
    {
        $html = "<form id='pay_form' class=\"form-inline\" method=\"post\" action=\"{$apiurl}\">";
        foreach ($data as $k => $v) {
            $html .= "<input type=\"hidden\" name=\"$k\" value=\"$v\">";
        }
        $html .= "</form>";
        $html .= "<script>document.forms['pay_form'].submit();</script>";
        return $html;
    }

    /**
     * 页面回调
     */
    public function page_callback($params, $order)
    {
        header("Location:" . url('/orderquery', ['orderid' => $order->trade_no]));
		exit;
    }

    /**
     * 服务器回调
     */
    public function notify_callback($params, $order)
    {
        $sign = $this->sign($params);

        if ($sign && $sign == $params['sign']) {

            if ($params["trade_status"] == "TRADE_SUCCESS") {
                // 金额异常检测
                if ($order->total_price != $params['money']) {
                    record_file_log('NZF_notify_error', '金额异常！' . "\r\n" . $order->trade_no . "\r\n订单金额：{$order->total_price}，已支付：{$params['amount']}");
                    die('金额异常！');
                }

                $this->completeOrder($order);
                echo 'success';
                return true;
            } else {
                exit('fail');
            }

        }
    }


    /**
     * @param $params
     * @return string
     */
    protected function sign($params)
    {
        ksort($params);

        $keyStr = '';
        foreach ($params as $key => $val) {
			if($key == "sign" || $key == "sign_type" || $val == "")continue;
            $keyStr .= "$key=$val&";
        }
		$keyStr = trim($keyStr,'&');

        $sign = md5($keyStr . "9BlBblKW17I89LK191Xg1p9b7bX8li19");//////////

        return $sign;
    }
}
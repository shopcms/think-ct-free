<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
/**
 * 微信扫码支付
 * @author mapeijian
 */
namespace app\common\pay;

use think\Db;
use app\common\Pay;
use thinkct\library\Purchase;

class Wechatpay extends Pay
{	
	// 发起支付请求 订单信息数据
	public function order($data)
	{
		header("Content-type:text/html;charset=utf-8");
		require EXTEND_PATH.'/weixin/WxPay.Api.php'; //引入微信支付
		require EXTEND_PATH.'/phpqrcode/phpqrcode.php'; //引入二维码
		$input        = new \WxPayUnifiedOrder();//统一下单
		$config       = new \WxPayConfig();// 配置参数
		$paymoney     = $data['money1'];
		$out_trade_no = $data['bh'].'|'.$data['ubh'];// 商户订单号(自定义)
		$goods_name   = '扫码支付'.$paymoney.'元'; // 商品名称(自定义)
		$input->SetBody($goods_name);
		$input->SetAttach($goods_name);
		$input->SetOut_trade_no($out_trade_no);
		$input->SetTotal_fee($paymoney*100);// 金额乘以100
		$input->SetTime_start(date("YmdHis"));
		$input->SetTime_expire(date("YmdHis", time() + 600));
		$input->SetGoods_tag("test");
		$input->SetNotify_url(request()->domain().'/aform/notify_url/wechatpay');// 回调地址
		$input->SetTrade_type("NATIVE");
		$input->SetProduct_id($data['bh']);// 商品id
		$result = \WxPayApi::unifiedOrder($config, $input);
		if($result['result_code'] == 'SUCCESS' && $result['return_code'] == 'SUCCESS') {
		$url = $result["code_url"];
		$this->assign('url',$url);
		}else{
		$this->error('参数错误');
		}
		if (empty($result['code_url'])){
		$qrCode_url = '';
		}else{
		$qrCode_url = $result["code_url"];
		}
		\QRcode::png($qrCode_url,false,'L',7);exit;		
	}	

	// 支付异步通知处理
	public function notify_callback()
	{
        // 获取微信回调的数据
        $notifiedData = file_get_contents('php://input');
        // XML格式转换
        $xmlObj = simplexml_load_string($notifiedData, 'SimpleXMLElement', LIBXML_NOCDATA);
        $xmlObj = json_decode(json_encode($xmlObj), true);		
        //支付成功
        if ($xmlObj['return_code'] == "SUCCESS" && $xmlObj['result_code'] == "SUCCESS") {
            foreach ($xmlObj as $k => $v) {
                if ($k == 'sign') {
                    $xmlSign = $xmlObj[$k];
                    unset($xmlObj[$k]);
                };
            }
            $sign = $this->WxSign($xmlObj);
            //if($sign === $xmlSign){
			if($sign === $sign){// 目前跳过验证	
				// 省略订单处理逻辑
                $out_trade_no   = $xmlObj['out_trade_no'];// 商户自定义订单号
                $transaction_id = $xmlObj['transaction_id'];// 微信交易单号	
				$money          = $xmlObj['total_fee']/100;// 金额		
				$ubh            = explode('|',$out_trade_no)[1];// 用户编号
				$trade_status   = $xmlObj['return_code'];// 订单交易状态
				// 获取订单信息
				$dingdan = Db::name('dingdang')->where(['ddbh'=>$out_trade_no,'ubh'=>$ubh])->find();
				// 验证订单是否存在
				if(!$dingdan){
					return json(['state'=>5,'info'=>'订单不存在或者信息错误']);
				}
				// 验证金额是否一致			
				if($money != $dingdan['money1']){
					return json(['state'=>5,'info'=>'非法操作,支付金额不一致']);
				}
				// 验证状态是否符合
				if($dingdan['ddzt'] == '等待买家付款'){
					// 在线充值
					if($dingdan['type'] == 'online') {							
						PointUpdate($dingdan['ubh'],$money,"微信扫码充值".$money."元");					
					}
					// 在线购买
					if($dingdan['type'] == 'cashier') {
						// 1执行入款
						PointUpdate($dingdan['ubh'],$money,"微信扫码充值".$money."元");
						// 2开始购买商品
						$buy = Purchase::index($out_trade_no);
						if($buy == false) {
							$this->success('订单号不存在');
						}
					}
					// 更改状态
					Db::name('dingdang')->where(['ubh'=>$ubh,'ddbh'=>$out_trade_no])->update([
						'sj'       => sj(),
						'alipayzt' => $trade_status,
						'ddzt'     => '支付成功'
					]);						
					return json(['state'=>1,'info'=>'交易支付成功']);
				}
				// 订单已支付
				if($dingdan['ddzt'] == '支付成功'){
					return json(['state'=>1,'info'=>'订单已经支付成功']);
				}
                // 返回成功标识给微信
                return sprintf("<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>");
            }

        }		
	}
	
	//微信签名算法
    private function WxSign($param)
    {
        $signkey = 'xxx';//秘钥
        $sign = '';
        foreach ($param as $key => $val) {
            $sign .= $key . '=' . $val . '&';
        }
        $sign .= 'key=' . $signkey;
        $sign = strtoupper(MD5($sign));
        return $sign;
    }	
}
?>
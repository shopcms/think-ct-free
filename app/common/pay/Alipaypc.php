<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
/**
 * 支付宝电脑支付
 * @author mapeijian
 */
namespace app\common\pay;

use think\Db;
use app\common\Pay;
use thinkct\library\Purchase;
use lib\AliPayPc as AliPay;

class Alipaypc extends Pay
{
	// 配置信息
	public function getConfig()
	{
		$account = plugin_account('alipaypc');// 获取插件账号
		return $config = [	
			//应用ID,您的APPID。
			'app_id'               => $account['app_id'],
			//商户私钥
			'merchant_private_key' => $account['merchant_private_key'],			
			//异步通知地址
			'notify_url'           => request()->domain().'/aform/notify_url/alipaypc',			
			//同步跳转
			'return_url'           => request()->domain().'/member/success/online',
			//编码格式
			'charset' 			   => "UTF-8",
			//签名方式
			'sign_type'			   => "RSA2",
			//支付宝网关
			'gatewayUrl'           => "https://openapi.alipay.com/gateway.do",
			//支付宝公钥
			'alipay_public_key'    => $account['alipay_public_key'],
		];
	}
	
	// 发起支付请求 订单信息数据
	public function order($data)
	{
		$config = $this->getConfig();
		// 标题内容
		$subject      = ($data['type']=='online')?'在线充值':'在线收银';
		$subject      = sysconf('site_name').$subject;
		// 订单号
        $out_trade_no = $data['bh'].'|'.$data['ubh'];
        // 商品金额 元
		$total_amount = $data['money1'];
        // 商品描述
		$body         = $subject.$out_trade_no;
        // 因为声明为静态类，所以直接调用
        return AliPay::setAilPay($out_trade_no,$subject,$total_amount,$body,$config);
	}
	
	// 支付异步通知处理
	public function notify_callback()
	{
		$config = $this->getConfig();
        $result = AliPay::setNotify_url($_POST,$config);		
		// 交易订单号
		$out_trade_no = input('out_trade_no');
		// 交易状态
		$trade_status = input('trade_status');
		// 验证订单成功
		if($result){
			$money  = input('total_amount');// 金额			
			$ubh    = explode('|',$out_trade_no)[1];// 用户编号
			// 获取订单信息
			$dingdan = Db::name('dingdang')->where(['ddbh'=>$out_trade_no,'ubh'=>$ubh])->find();
			// 验证订单是否存在
			if(!$dingdan){
				return json(['state'=>5,'info'=>'订单不存在或者信息错误']);
			}
			// 验证金额是否一致			
			if($money != $dingdan['money1']){
				return json(['state'=>5,'info'=>'非法操作,支付金额不一致']);
			}
			// 验证状态是否符合
			if($dingdan['ddzt'] == '等待买家付款'){
				// 在线充值
				if($dingdan['type'] == 'online') {							
					PointUpdate($dingdan['ubh'],$money,"支付宝充值".$money."元");					
				}
				// 在线购买
				if($dingdan['type'] == 'cashier') {
					// 1执行入款
					PointUpdate($dingdan['ubh'],$money,"支付宝充值".$money."元");
					// 2开始购买商品
					$buy = Purchase::index($out_trade_no);
					if($buy == false) {
						$this->success('订单号不存在');
					}
				}
				// 更改状态
				Db::name('dingdang')->where(['ubh'=>$ubh,'ddbh'=>$out_trade_no])->update([
					'sj'       => sj(),
					'alipayzt' => $trade_status,
					'ddzt'     => '支付成功'
				]);						
				return json(['state'=>1,'info'=>'交易支付成功']);
			}
			// 订单已支付
 			if($dingdan['ddzt'] == '支付成功'){
				return json(['state'=>1,'info'=>'订单已经支付成功']);
			}
		}
		return json(['state'=>5,'info'=>'交易创建，等待买家付款']); 
	}
}
?>
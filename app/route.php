<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
$admin_login_path = sysconf('admin_login_path');
if ($admin_login_path == '') {
    $admin_login_path = 'admin';
}
$route_config = [
	// 首页
	'code' => ['index/code/index',['deny_ext'=>'html']],	
	'code/goods<id>' => ['index/code/goods',['ext'=>'html']],
	'serve' => ['index/serve/index',['deny_ext'=>'html']],	
	'serve/goods<id>' => ['index/serve/goods',['ext'=>'html']],	
	'web' => ['index/web/index',['deny_ext'=>'html']],	
	'web/goods<id>' => ['index/web/goods',['ext'=>'html']],	
	'domain' => ['index/domain/index',['deny_ext'=>'html']],	
	'domain/goods<id>' => ['index/domain/goods',['ext'=>'html']],
	'task' => ['index/task/index',['deny_ext'=>'html']],	
	'task/goods<id>' => ['index/task/goods',['ext'=>'html']],
	'demand/:type' => ['index/demand/index',['deny_ext'=>'html']],	
	'demand/:id' => ['index/demand/goods',['ext'=>'html']],
	'shop' => ['index/shop/index',['deny_ext'=>'html']],
	'ishop<id>' => ['index/shop/shop',['deny_ext'=>'html']],
	'brand' => ['index/brand/index',['deny_ext'=>'html']],
	'b/:name' => ['index/brand/view',['deny_ext'=>'html']],
	'jifen' => ['index/jifen/index',['deny_ext'=>'html']],
	'jifen/:id' => ['index/jifen/view',['ext'=>'html']],

	// 会员中心
	'member/myorder/:ddbh' => ['member/myorder/index',['deny_ext'=>'html']],
	'member/evaluation/:ddbh' => ['member/evaluation/index',['deny_ext'=>'html']],	
	//控制器
    '__alias__'  =>  [	
        'api'        =>  'aform/api',	
        'apage'      =>  'aform/apage',
		'deal'       =>  'aform/deal',
		'dform'      =>  'aform/dform',
		'execute'    =>  'aform/execute',
		'help'       =>  'index/help',
		'html'       =>  'index/html',
		'oauth'      =>  'index/oauth',
		'top'        =>  'index/html/top',
		'protection' =>  'index/html/protection',
		'publicity'  =>  'index/html/publicity',
		'prompt'     =>  'index/html/prompt',
		'prompt'     =>  'index/html/prompt',
		'recruit'    =>  'index/html/recruit',
		'entrusts'   =>  'index/html/entrusts',
		'oshare'     =>  'index/html/oshare',
		'upload'     =>  'index/upload'		
    ],
	
	//'/:id' => ['index/code/goods',['ext'=>'html']],

    // 子域名绑定
    '__domain__' => [
        '*' => 'shop/shop?subdomain=*',
		//'my.shop.com' => 'member',	
    ],
];

if ($admin_login_path != 'admin') {
    $admin_route_config = [
        //后台
        'admin/$' => 'admin/index/index',
        'admin/login$' => 'admin/index/index',
        'admin/login/index' => 'admin/index/index',
        $admin_login_path . '/$' => 'admin/login/index',
    ];
    $route_config = array_merge($route_config, $admin_route_config);
}
return $route_config;

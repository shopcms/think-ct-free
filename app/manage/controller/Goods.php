<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\manage\controller;

use controller\BasicAdmin;
use think\Db;
use think\Request;

use service\LogService;

class Goods extends BasicAdmin 
{
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('self_url', '#' . Request::instance()->url());
        $this->assign('self_no_url', Request::instance()->url());
    }
	
	public function code()
	{
		$this->assign('title', '源码列表');
        ////////////////// 查询条件 //////////////////
        $query = [
            'user_id' => input('user_id/s', ''),
            'username' => input('username/s', ''),
            'name' => input('name/s', ''),
            'status' => input('status/s', ''),
            'date_range' => input('date_range/s', ''),
        ];
        //$where = $this->genereate_where($query);
		$code = route_name('code');
		$where['zt'] = 1;
		if($code == 'down'){
			$where['zt'] = 2;
		}
		if($code == 'audit'){
			$where['zt'] = ['IN','0,3'];
		}		
		
        $goodsList = Db::name('code')->alias('a')
            ->join('shop_sell c', 'a.ubh = c.bh')
            ->field('a.*,c.name')
            ->where($where)
            ->order('a.id desc')
            ->paginate(30, false, [
                'query' => $query
            ]);
		
        // 分页
        $page = str_replace('href="', 'href="#', $goodsList->render());
        $this->assign('page', $page);
        $this->assign('goodsList', $goodsList);

        $sum_money = Db::name('code')->alias('a')->where($where)->sum('money');
        $this->assign('sum_money', $sum_money);
        $sum_order = Db::name('code')->alias('a')->where($where)->count();       
		$this->assign('sum_order', $sum_order);
        return $this->fetch();		
		
	}
	public function del()
	{
		$bh   = input('bh');
		$type = input('type');
		Db::name($type)->where('bh',$bh)->update(['zt' =>4]);		
		return J(200, '删除成功！');
	}
	

	
	
	
}
?>
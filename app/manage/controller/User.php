<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\manage\controller;

use controller\BasicAdmin;
use think\Db;
use app\common\model\User as UserModel;

use service\LogService;

class User extends BasicAdmin 
{
	

	
		
	// 详情
    public function detail() {
        $bh  = input('bh');
        $user = UserModel::get(['bh'=>$bh]);
        $this->assign('user', $user);
        return $this->fetch();
    }	
	
	// 站内信
	public function message()
	{
       if (!$this->request->isPost()) {
            return $this->fetch();
        }
        $user_id = input('user_id/d', 0);
        $title   = input('title/s', '');
        $content = input('content/s', '');
        $user    = UserModel::get([
            'id' => $user_id,
        ]);
        if (!$user) {
            $this->error('不存在该用户！');
        }
        if (!$title) {
            $this->error('请输入标题！');
        }
        if (!$content) {
            $this->error('请输入内容！');
        }
        $res = sendMessage(0, $user_id, $title, $content);
        if ($res !== false) {
            LogService::write('用户管理', '发送站内信成功，商户ID:' . $user_id);
            $this->success('发送成功！', '');
        } else {
            $this->error('发送失败，请重试！');
        }			
	}
	
	// 设置费率
	public function rate()
	{
		return $this->fetch();
	}	
	
	// 删除商户
	public function del()
	{
		// 账号user
		// 角色buy|sell
		// 商品code|web|
		$this->success('删除成功！', '');
	}
	
	// 登录商户后台
	public function login()
	{
        $bh = input('bh',0);
        $user    = UserModel::get([
            'bh' => $bh,
        ]);
        if (!$user) {
            $this->error('不存在该用户！');
        }
        //后台管理员进行登录不需要二次验证
        session('usercode', $user->bh);
        LogService::write('用户管理', '登录商户平台成功，商户编号:'.$bh);
        return thinkct()->url('/member/');
    }	
}
?>
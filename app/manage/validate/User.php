<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php

namespace app\manage\validate;

use think\Validate;

class User extends Validate
{
    protected $rule =   [
        'username'        =>'require|unique:user,username',
        'password'        =>'require|min:8|max:16',
        'email'           =>'require|email',
        'mobile'          =>'require',
        'qq'              =>'require',
    ];

    protected $message  =   [
        'username.require'        =>'用户名未填写',
        'username.unique'         =>'已存在该用户名',
        'password.require'        =>'密码未填写',
        'password.min'            =>'密码位数必须在8~16位之间',
        'password.max'            =>'密码位数必须在8~16位之间',
        'email.require'           =>'邮箱未填写',
        'email.email'             =>'邮箱格式不正确',
        'mobile.require'          =>'手机号未填写',
        'qq.require'              =>'QQ号未填写',
    ];

    protected $scene = [
        'register' => ['username','password','email','mobile','qq'],
        'edit'     => ['username'=>'unique:user,username','password'=>'min:8|max:16','email','mobile','qq'],
    ];

}

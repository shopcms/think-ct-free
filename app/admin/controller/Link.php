<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php

namespace app\admin\controller;

use controller\BasicAdmin;
use think\Db;
use service\LogService;
use service\DataService;

class Link extends BasicAdmin
{
	public function index()
	{	
        $this->title = '友情链接管理';
        $db = Db::name('link')->where('type','link')->order('id asc');
        return parent::_list($db, false);		
	}
	
	public function partner()
	{
        $this->title = '合作伙伴管理';
        $db = Db::name('link')->where('type','partner')->order('id asc');
        return parent::_list($db, false);	
	}
	
	public function add()
	{
		return $this->_form('link', 'form');
	}
	
	public function forbid()
	{
		Db::name('link')->where('id',input('id'))->update(['zt'=>0]);
		$this->success("链接禁用成功!", '');
	}
	
	public function resume()
	{
		Db::name('link')->where('id',input('id'))->update(['zt'=>1]);
		$this->success("链接启用成功!", '');
	}	
	
	public function del()
	{
		Db::name('link')->delete(input('id'));
		$this->success("链接删除成功!", '');			
	}
	
	public function edit()
	{
		 return $this->_form('link','form');
	}
	
	public function slide()
	{
		echo '幻灯';
	}
}
?>
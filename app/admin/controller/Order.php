<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\admin\controller;

use controller\BasicAdmin;
use think\Db;

class Order extends BasicAdmin
{
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('self_url', '#' . request()->url());
        $this->assign('self_no_url', request()->url());
    }
	// 订单列表
	public function index()
	{
		return '目前位置/manage/order/index';
	}	
	
	// 订单详情
	public function detail()
	{
		
		return $this->fetch();	
	}
	
	// 订单回复
	public function reply()
	{		
		return $this->fetch();
	}
	
	// 订单消息发送
	public function huifu()
	{
		if(input('action') == 'add'){
			$this->success('操作成功！', '');
		}		
		return $this->fetch();
	}
	
	// 资金明细
	public function money()
	{	
		$where = [];
		$query = [];
        $data = Db::name('moneyrecord')->where($where)->order('id desc')
		->paginate(30,false,['query'=>$query]);

        $sum_money = Db::name('moneyrecord')->where($where)->sum('moneynum');
		$sum_order = Db::name('moneyrecord')->where($where)->count(); 	
        // 分页
        $page = str_replace('href="', 'href="#', $data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        $this->assign('sum_money',$sum_money);  
		$this->assign('sum_order',$sum_order);
        return $this->fetch();			
	}
	
	// 充值消费
	public function pay()
	{	
		$where = [];
		$query = [];
        $data = Db::name('dingdang')->where($where)->order('id desc')
		->paginate(30,false,['query'=>$query]);

        $sum_money = Db::name('dingdang')->where($where)->sum('money1');
		$sum_order = Db::name('dingdang')->where($where)->count(); 	
        // 分页
        $page = str_replace('href="', 'href="#', $data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        $this->assign('sum_money',$sum_money);  
		$this->assign('sum_order',$sum_order);
        return $this->fetch();	
	}	
}
?>
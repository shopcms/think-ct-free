<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\admin\controller;

use controller\BasicAdmin;
use think\Db;

class Goods extends BasicAdmin
{
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('self_url', '#' . request()->url());
        $this->assign('self_no_url', request()->url());
    }
	
	// 源码管理
	public function code()
	{
		// 查询条件
		$query = $this->query('code');
		// 数据信息
        $data = Db::name('code')->where($query)->order('id desc')->paginate(30);
        $sum_money = Db::name('code')->where($query)->sum('money');
		$sum_order = Db::name('code')->where($query)->count(); 	
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        $this->assign('sum_money',$sum_money);  
		$this->assign('sum_order',$sum_order);
        return $this->fetch();			
	}
	
	// 服务管理
	public function serve()
	{
		// 查询条件
		$query = $this->query('serve');
		// 数据信息
        $data = Db::name('serve')->where($query)->order('id desc')->paginate(30);
        $sum_money = Db::name('serve')->where($query)->sum('money');
		$sum_order = Db::name('serve')->where($query)->count(); 	
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        $this->assign('sum_money',$sum_money);  
		$this->assign('sum_order',$sum_order);
        return $this->fetch();			
	}

	// 网站管理
	public function web()
	{
		// 查询条件
		$query = $this->query('web');
		// 数据信息
        $data = Db::name('web')->where($query)->order('id desc')->paginate(30);
        $sum_money = Db::name('web')->where($query)->sum('money');
		$sum_order = Db::name('web')->where($query)->count(); 	
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        $this->assign('sum_money',$sum_money);  
		$this->assign('sum_order',$sum_order);
        return $this->fetch();			
	}

	// 域名管理
	public function domain()
	{
		// 查询条件
		$query = $this->query('domain');
		// 数据信息
        $data = Db::name('domain')->where($query)->order('id desc')->paginate(30);
        $sum_money = Db::name('domain')->where($query)->sum('money');
		$sum_order = Db::name('domain')->where($query)->count(); 	
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        $this->assign('sum_money',$sum_money);  
		$this->assign('sum_order',$sum_order);
        return $this->fetch();			
	}	
	
	// 查询条件
	private function query($type)
	{
		$type = route_name($type);
		$query['zt'] = 1;
		if($type == 'down'){
			$query['zt'] = 2;
		}
		if($type == 'audit'){
			$query['zt'] = ['IN','0,3'];
		}
		return $query;
	}
	
	// 批量上架
	public function go_down()
	{
		$ids   = input('ids');
		$type  = input('type');
		$data  = explode(',',$ids);		
		foreach($data as $id){
			$data   = Db::name($type)->where('id',$id)->find();
			$status = ($data['zt']==1)?2:1;
			Db::name($type)->where('id',$id)->update(['zt'=>$status]);
		}
        $this->success('批量上架成功！','');
    }

	// 批量通过
	public function go_true()
	{	
		$ids   = input('ids');
		$type  = input('type');
		$data  = explode(',',$ids);		
		foreach($data as $id){
			Db::name($type)->where('id',$id)->update(['zt'=>1]);
		}	
        $this->success('批量通过成功！','');
    }
    
	// 批量删除
    public function go_del()
	{
		$ids   = input('ids');
		$type  = input('type');
		$data  = explode(',',$ids);		
		foreach($data as $id){
			Db::name($type)->where('id',$id)->update(['zt'=>4]);
		}		
        $this->success('批量删除成功！','');
    }
	
	// 审核状态
	public function change_freeze()
	{
		$value = input('value');
		$id    = input('id');
		$field = input('field');
		$type  = ($value==1)?'审核通过':'审核不通过';
		Db::name($field)->where('id',$id)->update(['zt'=>$value]);
		return $this->success($type.'操作成功','');
	}
	
	// 删除商品
	public function del()
	{
		$bh   = input('bh');
		$type = input('type');
		Db::name($type)->where('bh',$bh)->update(['zt' =>4]);		
		return J(200,'删除成功！');
	}

	// 商品举报
	public function report()
	{
		$this->assign('title','商品举报');
        $report = Db::name('data_report')->where('1=1')->order('id desc')->paginate(30);
		$list = [];
		foreach($report as $data){
			$buy   = Db::name('buy')->where('bh',$data['ubh'])->find();
			$goods = Db::name($data['good'])->where('bh',$data['number'])->find();
			$data['name'] = ($buy['name']=='')?'匿名用户':$buy['name'];
			$data['tit']  = $goods['tit'];
			$data['gid']  = $goods['id'];
			
			$list[] = $data;
		}
        // 分页
        $page = str_replace('href="','href="#',$report->render());
        $this->assign('page',$page);
        $this->assign('list',$list);
        return $this->fetch();
	}
}
?>
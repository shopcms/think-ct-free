<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\admin\controller;

use controller\BasicAdmin;
use app\common\library\Update;
use think\Db;

class Plugin extends BasicAdmin 
{
	// 应用列表
	public function index() 
	{
		$list = Db::name('plugin')->where('1=1')->select();
		$this->assign('title','应用列表');
		$this->assign('list',$list);
		return $this->fetch();
	}	
	
	// 编辑插件
	public function edit()
	{
		// 修改账号
		if(input('?plugin_id')){
			Db::name('plugin')->where('id',input('plugin_id'))->update([
				'name'           => input('name'),
				'show_name'      => input('show_name'),
				'applyurl'       => input('applyurl'),
				'account_fields' => input('account_fields'),
				'is_available'   => input('is_available'),
				'status'         => input('status')
			]);
			return $this->success('操作成功！', '');
		}
		$data = Db::name('plugin')->where('id',input('id'))->find();
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	// 操作插件
	public function operation_plugin()
	{
		// 插件状态
		if(input('action') == 'status'){
			Db::name('plugin')->where('id',input('plugin_id'))->update(['status'=>input('status')]);
			$this->success('操作成功！','');
		}
		// 插件可用
		if(input('action') == 'available'){
			Db::name('plugin')->where('id',input('plugin_id'))->update(['is_available'=>input('type')]);
			$this->success('操作成功！','');
		}	
		// 安装插件
		if(input('action') == 'install'){
			Db::name('plugin')->where('id',input('id'))->update(['is_install'=>1]);
			$this->success('操作成功！','');
		}
		
		// 卸载插件
		if(input('action') == 'uninstall'){
			Db::name('plugin')->where('id',input('id'))->update(['is_install'=>0]);
			$this->success('操作成功！','');	
		}
		// 删除插件
		if(input('action') == 'del'){
			$this->error('不能删除当前插件！');
		}
	}
	
	// 应用账号管理
	public function account()
	{
		$plugin  = Db::name('plugin')->where('id',input('plugin_id'))->find();
		$account = Db::name('plugin_account')->where('id',input('account_id'))->find();
		// 拼接接口所需字段信息
		$fields = [];
		if(!empty($plugin['account_fields'])){
			$plugin['account_fields'] = explode('|',$plugin['account_fields']);  		
			foreach($plugin['account_fields'] as $value) {
				$value = explode(':',$value);			
				if(isset($value[1])) {
					$fields[$value[1]] =  array('name'=>$value[0]);
				}
			}
		}
		// 添加账号		
		if(input('action') == 'add'){
			Db::name('plugin_account')->insert([
				'name'      => input('name'),
				'code'      => input('code'),
				'rate_type' => input('rate_type'),
				'lowrate'   => input('lowrate',0),
				'params'    => json_encode($_POST['params']),
				'plugin_id' => input('plugin_id'),
				'status'    => input('status'),
			]);
			return $this->success('添加成功！', '');
		}
		// 编辑账号
		$params = (input('type')=='up')?json_array($account['params']):[];	
		if(input('action') == 'up'){
			Db::name('plugin_account')->where('id',input('account_id'))->update([
				'name'      => input('name'),
				'code'      => input('code'),
				'rate_type' => input('rate_type'),
				'lowrate'   => input('lowrate',0),
				'params'    => json_encode($_POST['params']),
				'plugin_id' => input('plugin_id'),
				'status'    => input('status'),
			]);
			return $this->success('修改成功！', '');
		}
		$this->assign('plugin',$plugin);
		$this->assign('account',$account);
		$this->assign('params',$params);
		$this->assign('fields',$fields);
		return $this->fetch();
	}
	
	// 应用账号列表
	public function account_list()
	{
		$account = Db::name('plugin_account')->where('plugin_id',input('plugin_id'))->select();
		$list = [];
		foreach($account as $data){
			$data['plugin'] = Db::name('plugin')->where('id',$data['plugin_id'])->find();
			$list[] = $data;
		}
		$this->assign('title','账号列表');
		$this->assign('list',$list);
		return $this->fetch();
	}
	
	// 操作账号
	public function operation_account()
	{
		// 账号状态
		if(input('action') == 'status'){
			Db::name('plugin_account')->where('id',input('account_id'))->update(['status'=>input('status')]);
			$this->success('操作成功！','');
		}
		// 账号删除
		if(input('action') == 'del'){
			Db::name('plugin_account')->where('id',input('account_id'))->delete();
			$this->success('操作成功！','');
		}		
	}
}
?>
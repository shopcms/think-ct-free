<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\admin\controller;

use controller\BasicAdmin;
use think\Db;

class Cashed extends BasicAdmin
{
	// 提现列表
	public function index()
	{
		//统计数据
		$cashed = Db::name('cashed')->order('id desc')->paginate(20);
		$stat['todaysum']    = number_format(Db::name('cashed')->whereTime('complete_sj','today')->where('zt',1)->sum('money'),2,'.','');
		$stat['totalsum']    = number_format(Db::name('cashed')->where('zt',1)->sum('money'),2,'.','');
		$stat['totalfee']    = number_format(Db::name('cashed')->where('zt',1)->sum('fee'),2,'.','');
		$stat['totalactual'] = number_format(Db::name('cashed')->where('zt',1)->sum('actual_money'),2,'.','');
		$stat['totalmoney']  = number_format(Db::name('cashed')->where('zt',0)->sum('actual_money'),2,'.','');			
		// 分页
        $page = str_replace('href="', 'href="#',$cashed->render());
        $this->assign('title','提现记录列表');
		$this->assign('self_url','#'.request()->url());
		$this->assign('page',$page);	
        $this->assign('cashed',$cashed);
		$this->assign('stat',$stat);
        return $this->fetch();
	}
	
	// 处理提现
	public function detail()
	{
		$action = input('action');
		$cashed = Db::name('cashed')->where('bh',input('bh'))->find();
		// 打款
		if($action == 'pass'){
			if($cashed['zt'] != 0){
				$this->error('当前状态不允许打款','');
			}
			Db::name('cashed')->where('bh',$cashed['bh'])->update(['zt'=>1,'sm'=>'处理了您的提现申请，请注意查收！','complete_sj'=>sj()]);	
		}
		// 驳回
		if($action == 'notpass'){
			if($cashed['zt'] != 0){
				$this->error('当前状态不允许驳回','');
			}
			PointUpdate(session('usercode'),$cashed['money'],"提现失败");
			Db::name('cashed')->where('bh',$cashed['bh'])->update(['zt'=>2,'sm'=>'拒绝了您的提现申请！','complete_sj'=>sj()]);	
		}
		// 删除
		if($action == 'del'){
			Db::name('cashed')->where('bh',$cashed['bh'])->delete();
		}
		$this->success('操作成功！', '');
	}
	
	
	
}
?>
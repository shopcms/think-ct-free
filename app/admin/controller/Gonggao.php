<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\admin\controller;

use controller\BasicAdmin;
use think\Db;
use service\LogService;

class Gonggao extends BasicAdmin
{
	// 公告列表
	public function index()
	{
		$list  = Db::name('gonggao')->where('1=1')->order('id desc')->paginate(20);
		$count = Db::name('gonggao')->where('1=1')->count();
        $page  = str_replace('href="','href="#',$list->render());
        $this->assign('title','公告列表');
		$this->assign('list',$list);
		$this->assign('count',$count);
		$this->assign('page',$page);		
		return $this->fetch();
	}
	
	// 新增公告
	public function add()
	{
		if(input('?fl')){
			Db::name('gonggao')->insert(['tit'=>input('tit'),'desc'=>input('desc'),'txt'=>input('txt'),'fl'=>input('fl'),'sj'=>sj()]);
			LogService::write('公告管理', '添加公告成功，标题:'.input('tit'));
			$this->success('添加成功','');
		}
		return $this->fetch('form');
	}	
	
	// 编辑公告
	public function edit()
	{
		$data = Db::name('gonggao')->where('id',input('id'))->find();
		if(input('?fl')){
			Db::name('gonggao')->where('id',$data['id'])->update(['tit'=>input('tit'),'desc'=>input('desc'),'txt'=>input('txt'),'fl'=>input('fl')]);
			LogService::write('公告管理', '编辑公告成功，ID:'.input('id'));
			$this->success('编辑成功','');
		}
		$this->assign('data',$data);	
		return $this->fetch('form');
	}
	
	// 删除公告
	public function del()
	{
		Db::name('gonggao')->where('id',input('id'))->delete();
		$this->success('删除成功','');
	}	
	
	
	
}
?>
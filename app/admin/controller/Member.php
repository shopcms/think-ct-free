<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\admin\controller;

use think\Db;
use controller\BasicAdmin;
use service\LogService;
use app\common\model\User as UserModel;
//200成功500失败
//return J(200,'删除成功！');
class Member extends BasicAdmin
{
	// 商户列表
	public function index() 
	{
		$this->assign('title', '商户管理');
		
		$where = null;
		$query = null;
		
        $users = UserModel::where($where)->order('id desc')->paginate(30);
        foreach ($users as $k => $v) {
            $users[$k]['idcard_number'] = '';
            $uc = Db::name($v['uc'])->where('bh',$v['bh'])->find();;			
			$contact = '66983239';
			$users[$k]['name'] = $uc['name'];
			$users[$k]['contact'] = ($v['uc'] == 'buy')? $uc['contact']:$contact;
			$users[$k]['tx'] = $uc['tx'];
			$users[$k]['name'] = $uc['name'];			
        }
        // 分页
        $page = str_replace('href="', 'href="#', $users->render());
        $this->assign('page', $page);
        $user_count = UserModel::where($where)->count();
        $this->assign('user_count',$user_count);
        $this->assign('users',$users);		
        return $this->fetch();		
	}
	
	public function message()
	{
		if(input('?type')){
			Db::name('message')->insert([
				'ubh'  => input('bh'),
				'sj'   => sj(),
				'tit'  => input('title'),
				'txt'  => input('content'),
				'type' => input('type'),
			]);
			$this->success('发送成功！', '');
		}
        return $this->fetch();
	}
	
	// 商户信息
	public function info()
	{
		$bh   = input('bh');
		$data = Db::name('user')->where('bh',$bh)->find();
		$buy  = Db::name('buy')->where('bh',$data['bh'])->find();
		$sell = Db::name('sell')->where('bh',$data['bh'])->find();
		$data['buy']  = $buy;
		$data['sell'] = $sell;
		$data['tx']   = ($data['uc']=='buy')?$buy['tx']:$sell['tx'];
		$data['b_xy'] = xy($buy['rev_m'],"b");
		$data['s_xy'] = xy($sell['rev_m'],"s");
		// 充值列表
		$wherec       = ['ubh'=>$data['bh'],'type'=>'online','ddzt'=>'支付成功'];
		$data['a1']   = Db::name('dingdang')->where($wherec)->limit(10)->order('id desc')->select();
		$data['a2']   = Db::name('dingdang')->where($wherec)->count();
		// 提现列表
		$data['b1']   = Db::name('cash')->where('ubh',$data['bh'])->limit(10)->order('id desc')->select();
		$data['b2']   = Db::name('cash')->where('ubh',$data['bh'])->count();		
		// 交易列表
		$data['c1']   = [];
		$data['down'] = Db::name('down')->where('buy|sell',$data['bh'])->limit(20)->order('id desc')->select();
		foreach($data['down'] as $datas){
			$goods = Db::name($datas['type'])->where('bh',$datas['codebh'])->find();
			$datas['url'] = c_url($goods['id'],$datas['type']);
			
			$data['c1'][] = $datas;
		}
		$data['c2']   = Db::name('down')->where('buy|sell',$data['bh'])->count();
		// 买入交易
		$data['d1']   = Db::name('down')->where(['buy'=>$data['bh']])->count();
		// 卖出交易
		$data['d2']   = Db::name('down')->where(['sell'=>$data['bh']])->count();
		// 卖出退款
		$data['d3']   = Db::name('down')->where(['sell'=>$data['bh'],'ddzt'=>'backsuc'])->count();
		
		$this->assign('data',$data);
        return $this->fetch();
	}
	
	// 商户状态
	public function state()
	{
		Db::name('user')->where('bh',input('bh'))->update(['zt'=>input('type'),'ztsm'=>input('auditTxt')]);
		$this->success('操作成功！', '');
	}
	
	// 资金管理
	public function money()
	{
		$bh     = input('bh');
		$action = input('action');
		$money  = input('money');
		$mark   = input('mark');
		$user   = Db::name('user')->where('bh',$bh)->find();		
		// 加款金额
		if($action == 'inc') {
			PointUpdate($bh,$money,$mark);
			$this->success('加款成功！', '');
		}
		// 扣除金额
		if($action == 'dec') {
			PointUpdate($bh,$money*(-1),$mark); 
			$this->success('扣除成功！', '');
		}		
		// 解冻金额
		if($action == 'unfreeze') {
			$this->error('暂不支持解冻金额');
		}		
		// 冻结金额
		if($action == 'freeze') {
			$this->error('暂不支持冻结金额');
		}
		$this->assign('user',$user);
		return $this->fetch();
	}	
	// 积分管理
	public function jifen()
	{
		$bh     = input('bh');
		$action = input('action');
		$jifen  = input('jifen');
		$mark   = input('mark');
		$user   = Db::name('user')->where('bh',$bh)->find();		
		// 增加积分
		if($action == 'inc') {
			PointUpdatejf($bh,$mark,$jifen);
			$this->success('增加积分成功！', '');
		}
		// 扣除积分
		if($action == 'dec') {
			PointUpdatejf($bh,$mark,$jifen*(-1)); 
			$this->success('扣除积分成功！', '');
		}
		$this->assign('user',$user);
		return $this->fetch();		
	}
	
	// 重置密码
	public function password()
	{	
		$bh     = input('bh');
		$user   = Db::name('user')->where('bh',$bh)->find();
		if(input('?new')){
			if(input('new') != input('renew')){
				$this->error('两次密码不一致！');
			}
			Db::name('user')->where('bh',$user['bh'])->update(['pwd'=>sha1(input('new'))]);
			$this->success('操作成功！', '');
		}	
		$this->assign('user',$user);
		return $this->fetch();
	}

	// 验证邮箱
	public function email()
	{	
		$bh     = input('bh');
		$user   = Db::name('user')->where('bh',$bh)->find();
		if(input('?email')){
			Db::name('user')->where('bh',$user['bh'])->update(['email'=>input('email'),'email_zt'=>input('type')]);
			$this->success('操作成功！', '');
		}		
		$this->assign('user',$user);
		return $this->fetch();
	}

	// 验证手机
	public function phone()
	{		
		$bh     = input('bh');
		$user   = Db::name('user')->where('bh',$bh)->find();
		if(input('?phone')){
			Db::name('user')->where('bh',$user['bh'])->update(['phone'=>input('phone'),'phone_zt'=>input('type')]);
			$this->success('操作成功！', '');
		}		
		$this->assign('user',$user);
		return $this->fetch();
	}
	
	// 清除安全码
	public function safe()
	{
		if(input('?id')){
			Db::name('user')->where('id',input('id'))->update(['safe_code'=>sha1(input('value'))]);
			$this->success('安全码已清除，请通知会员及时重设安全码！',''); 			
		}
		if(input('?safe')){
			Db::name('user')->where('bh',input('bh'))->update(['safe_code'=>sha1(input('safe'))]);
			$this->success('操作成功！', '');
		} 	     
		return $this->fetch();
	}
	
	// 登录商户后台
	public function login()
	{
        $bh   = input('bh',0);
        $user = UserModel::get(['bh'=>$bh]);
        if(!$user) {
            $this->error('不存在该用户！');
        }
        //后台管理员进行登录不需要二次验证
        session('usercode',$user->bh);
        LogService::write('用户管理', '登录商户平台成功，商户编号:'.$bh);
        return thinkct()->url('/member/');
    }
	
	// 第三方登录
	public function oauth()
	{
		$this->assign('title', '快捷登录管理');
		$query = '';
		$data  = Db::name('oauth')->where('status',1)->order('id desc')->paginate(30);
        // 分页
        $page = str_replace('href="', 'href="#', $data->render());
        $this->assign('page', $page);
        $this->assign('data',$data);		
        return $this->fetch();	
	}
}
?>
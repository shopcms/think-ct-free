<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php

namespace app\admin\controller;

use controller\BasicAdmin;
use service\ExtendService;
use service\LogService;
use think\Db;

/**
 * 后台参数配置控制器
 * Class Config
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 18:05
 */
class Config extends BasicAdmin
{

    /**
     * 当前默认数据模型
     * @var string
     */
    public $table = 'SystemConfig';

    /**
     * 当前页面标题
     * @var string
     */
    public $title = '网站参数配置';

    /**
     * 显示系统常规配置
     */
    public function index()
    {
        if (!$this->request->isPost()) {
            return view('', ['title' => $this->title]);
        }
        $system_module = [];
        foreach (scandir(APP_PATH) as $dir) {
            if($dir == '.' || $dir == '..') {
                continue;
            }
            if(is_dir(APP_PATH.$dir)) {
                array_push($system_module, $dir);
            }
        }
        foreach ($this->request->post() as $key => $vo) {
            if($key == 'admin_login_path' && $vo != 'admin' && in_array($vo, $system_module)) {
                $this->error('后台地址不能与现有系统模块名同名');
            }
            sysconf($key, $vo);
        }
        LogService::write('系统管理', '系统参数配置成功');
        $this->success('系统参数配置成功！', '');
    }

    /**
     * 文件存储配置
     */
    public function file()
    {
        $alert = [
            'type'    => 'danger',
            'title'   => '操作安全警告（默认使用本地服务存储）',
            'content' => '请根据实际情况配置存储引擎，合理做好站点下载分流。建议尽量使用云存储服务，同时保证文件访问协议与网站访问协议一致！'
        ];
        $this->title = '文件存储配置';
        $this->assign('alert', $alert);
        return $this->index();
    }
	
	// 搜索词管理
	public function soukey()
	{
		$query     = '1=1';
		$data      = Db::name('soukey')->where($query)->order('id desc')->paginate(30);
		$sum_money = Db::name('soukey')->where($query)->sum('num');
		$sum_order = Db::name('soukey')->where($query)->count(); 	
		// 分页
		$page = str_replace('href="','href="#',$data->render());
		$this->assign('page',$page);
		$this->assign('data',$data);
		$this->assign('sum_money',$sum_money);  
		$this->assign('sum_order',$sum_order);
		return $this->fetch();	
	}
	
	// 支付设置
	public function pay()
	{
        if(!$this->request->isPost()) {
            $this->assign('title','支付设置');
            return view();
        }
        foreach($this->request->post() as $key => $vo) {
            sysconf($key, $vo);
        }
        LogService::write('支付设置','修改支付设置成功');
        $this->success('数据修改成功！', '');
	}
	
	// 交易设置
	public function order()
	{
        if(!$this->request->isPost()) {
            $this->assign('title','交易设置');
            return view();
        }
        foreach($this->request->post() as $key => $vo) {
            sysconf($key, $vo);
        }
        LogService::write('交易设置','修改交易设置成功');
        $this->success('数据修改成功！', '');
	}
	
	// 商品设置
	public function goods()
	{
        if(!$this->request->isPost()) {
            $this->assign('title','商品设置');
            return view();
        }
        foreach($this->request->post() as $key => $vo) {
            sysconf($key, $vo);
        }
        LogService::write('交易设置','修改商品设置成功');
        $this->success('数据修改成功！', '');
	}
}
?>
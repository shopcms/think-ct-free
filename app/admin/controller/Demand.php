<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\admin\controller;

use controller\BasicAdmin;
use think\Db;

class Demand extends BasicAdmin
{
	// 源码需求
	public function code()
	{
		// 查询条件
		$query = $this->query('code');
		$query['type'] = 'code';
		// 数据信息
        $data = Db::name('demand')->where($query)->order('id desc')->paginate(30);
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        return $this->fetch();			
	}

	// 域名需求
	public function domain()
	{
		// 查询条件
		$query = $this->query('domain');
		$query['type'] = 'domain';
		// 数据信息
        $data = Db::name('demand')->where($query)->order('id desc')->paginate(30);
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        return $this->fetch();			
	}	
	
	// 网站需求
	public function web()
	{
		// 查询条件
		$query = $this->query('web');
		$query['type'] = 'web';
		// 数据信息
        $data = Db::name('demand')->where($query)->order('id desc')->paginate(30);
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        return $this->fetch();			
	}
	
	// 任务需求
	public function task()
	{
		// 查询条件
		$query = $this->query('task');
		// 数据信息
        $data = Db::name('task')->where($query)->order('id desc')->paginate(30);
        // 分页
        $page = str_replace('href="','href="#',$data->render());
        $this->assign('page',$page);
        $this->assign('data',$data);
        return $this->fetch();			
	}	

	// 查询条件
	private function query($type)
	{
		$type = route_name($type);
		$query['zt'] = 1;
		if($type == 'down'){
			$query['zt'] = 2;
		}
		if($type == 'audit'){
			$query['zt'] = ['IN','0,3'];
		}
		return $query;
	}
	
	// 批量上架
	public function go_down()
	{
		$ids   = input('ids');
		$type  = input('type');
		$name  = input('name','demand');
		$data  = explode(',',$ids);		
		foreach($data as $id){
			$data   = Db::name($name)->where('id',$id)->find();
			$status = ($data['zt']==1)?2:1;
			Db::name($name)->where('id',$id)->update(['zt'=>$status]);
		}
        $this->success('批量上架成功！','');
    }

	// 批量通过
	public function go_true()
	{	
		$ids   = input('ids');
		$type  = input('type');
		$name  = input('name','demand');
		$data  = explode(',',$ids);		
		foreach($data as $id){
			Db::name($name)->where('id',$id)->update(['zt'=>1]);
		}	
        $this->success('批量通过成功！','');
    }
    
	// 批量删除
    public function go_del()
	{
		$ids   = input('ids');
		$type  = input('type');
		$name  = input('name','demand');
		$data  = explode(',',$ids);		
		foreach($data as $id){
			Db::name($name)->where('id',$id)->update(['zt'=>4]);
		}		
        $this->success('批量删除成功！','');
    }	
	
	// 审核状态
	public function status()
	{
		if(input('action') == 'status'){
			$name  = input('name','demand');
			Db::name($name)->where('bh',input('bh'))->update(['zt'=>input('status')]);
			$this->success('操作成功！','');
		}
	}
}
?>
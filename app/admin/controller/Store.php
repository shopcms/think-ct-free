<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\admin\controller;

use controller\BasicAdmin;
use app\common\library\Update;
use think\Db;

class Store extends BasicAdmin 
{	
	// 检测授权
	public function auth() 
	{
		
		return $this->fetch();
		//$auth = Update::auth();	
		//if($auth == 1) {
		//	return '<font color=green>正常授权</font>';
		//}
	}
	
	// 检测更新
	public function update() 
	{
		return $this->fetch();
		//return Update::update();
	}	
	
}
?>
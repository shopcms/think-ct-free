<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\member\model;

use think\Model;
use think\Db;

class Brand extends Model
{
	// 添加品牌
    public function add($bh)
	{
		// 处理数据
		$picture  = picture($bh,'brand');
		// 新增数据
		$this->bh       = $bh;
		$this->ubh      = session('usercode');
		$this->sj       = sj();
		$this->lastgx   = sj();
		$this->uip      = uip();		
		$this->typeid   = $_POST['menus'][0];
		$this->company  = input('company');
		$this->tit      = input('tit');
		$this->website  = input('website');
		$this->url      = input('url');
		$this->desc     = input('desc');
		$this->txt      = input('txt');		
		$this->zt       = state('brand');
		$this->psort    = trim(get_psort($bh),',');	
		$this->save();
		$audit = ($this->zt==0)? '/audit':null;
		return thinkct()->url("/member/brand/{$bh}{$audit}");
	}
	
	// 更新品牌
    public function up($bh)
	{
		// 处理数据
		$picture  = picture($bh,'brand');
		$moneydes = (isset($_POST['moneydes']))?implode(',',$_POST['moneydes']):null;
		$allmoney = (isset($_POST['allmoney']))?implode(',',$_POST['allmoney']):null;
		// 更新数据
		$this->save([
			'menus'    => $_POST['menus'][0],
			'tit'      => input('tit'),
			'desc'     => input('desc',input('tit')),
			'txt'      => input('txt'),
			'money'    => input('money'),
			'tmoney'   => input('tmoney'),
			'piece'    => input('piece'),
			'beizhu'   => input('beizhu'),
			'psort'    => trim(get_psort($bh),',')
		],['bh'=>$bh]);
		$audit = ($this->zt==0)? '/audit':null;
		return thinkct()->url("/member/brand/{$bh}{$audit}");		
	}	
}
?>

<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\member\model;

use think\Model;
use think\Db;

class Code extends Model
{
	// 更新商品
    public function up($bh,$data = null)
	{
		$post = input('post.',null);
		$menus = $post['menus'];
		// 缩略图
		$picture = picture($bh,'code');
		$app     = (isset($post['app']))?implode('|',$post['app']):0;
		
		$post['ckey']   = keywords($post['tit'].$post['cdes'].$post['txt']);
		$post['menus']  = $menus[0];
		$post['brand']  = $menus[1];
		$post['lang']   = $menus[2];
		$post['sql1']   = $menus[3];
		$post['app']    = $app;
		$post['azsxid'] = implode("|",$post['azsxid']);
		$post['psort']  = trim(get_psort($bh),',');		
		$audit = (state('code') == 0)? '/audit':null;
		$this->allowField(true)->save($post,['bh'=>$bh]);
		return redirect("/member/prompt/form/code/{$bh}{$audit}")->remember();	
	}
	
	// 新增商品
	public function add($bh,$data = null)
	{
		$post = input('post.',null);
		$menus = $post['menus'];
		// 缩略图
		$picture = picture($bh,'code');
		//$psort   = $this->psort($bh);		
		$app     = (isset($post['app']))?implode('|',$post['app']):null;
		
		$post['bh']     = $bh;
		$post['sj']     = sj();
		$post['lastgx'] = sj();
		$post['uip']    = uip();			
		$post['ubh']    = session('usercode');
		$post['zt']     = state('code');
		
		$post['ckey']   = keywords($post['tit'].$post['cdes'].$post['txt']);
		$post['menus']  = $menus[0];
		$post['brand']  = $menus[1];
		$post['lang']   = $menus[2];
		$post['sql1']   = $menus[3];
		$post['app']    = $app;
		$post['azsxid'] = implode("|",$post['azsxid']);
		$post['psort']  = trim(get_psort($bh),',');
		
		$audit = (state('code') == 0)? '/audit':null;
		$this->allowField(true)->save($post);		
		return redirect("/member/prompt/form/code/{$bh}{$audit}")->remember();
	}		
}
?>

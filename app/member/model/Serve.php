<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\member\model;

use think\Model;
use think\Db;

class Serve extends Model
{
	// 添加服务
    public function add($bh)
	{
		// 处理数据
		$picture  = picture($bh,'serve');
		$moneydes = (isset($_POST['moneydes']))?implode(',',$_POST['moneydes']):null;
		$allmoney = (isset($_POST['allmoney']))?implode(',',$_POST['allmoney']):null;
		// 新增数据
		$this->bh       = $bh;
		$this->ubh      = session('usercode');
		$this->sj       = sj();
		$this->lastgx   = sj();
		$this->uip      = uip();
		$this->menus    = $_POST['menus'][0];
		$this->tit      = input('tit');
		$this->cdes     = input('cdes',input('tit'));
		$this->txt      = input('txt');
		$this->money  	= input('money');
		$this->tmoney   = input('tmoney');
		$this->piece  	= input('piece');
		$this->pf1      = 5;
		$this->pf2    	= 5;
		$this->pf3    	= 5;
		$this->beizhu 	= input('beizhu');
		$this->zt       = state('serve');
		$this->moneydes = $moneydes;
		$this->allmoney = $allmoney;	
		$this->psort    = trim(get_psort($bh),',');	
		$this->save();
		$audit = (state('serve') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/serve/{$bh}{$audit}");
	}
	
	// 更新服务
    public function up($bh)
	{
		// 处理数据
		$picture  = picture($bh,'serve');
		$moneydes = (isset($_POST['moneydes']))?implode(',',$_POST['moneydes']):null;
		$allmoney = (isset($_POST['allmoney']))?implode(',',$_POST['allmoney']):null;
		// 更新数据
		$this->save([
			'menus'    => $_POST['menus'][0],
			'tit'      => input('tit'),
			'cdes'     => input('cdes',input('tit')),
			'txt'      => input('txt'),
			'money'    => input('money'),
			'tmoney'   => input('tmoney'),
			'piece'    => input('piece'),
			'beizhu'   => input('beizhu'),
			'moneydes' => $moneydes,
			'allmoney' => $allmoney,
			'psort'    => trim(get_psort($bh),',')
		],['bh'=>$bh]);
		$audit = (state('serve') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/serve/{$bh}{$audit}");		
	}	
}
?>

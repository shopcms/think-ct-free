<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\member\model;

use think\Model;
use think\Db;

class Task extends Model
{
	// 添加任务
    public function add($bh)
	{
		$post             = input('post.');
		$post['typesxid'] = $_POST['menus'][0];
		$post['num']      = input('day');
		$post['bh']       = $bh;
		$post['sj']       = sj();
		$post['lastgx']   = sj();
		$post['uip']      = uip();			
		$post['ubh']      = session('usercode');
		$post['zt']       = state('task');		
		$audit = (state('task') == 0)? '/audit':null;
		$this->allowField(true)->save($post);	
		return thinkct()->url("/member/prompt/form/task/{$bh}{$audit}");
	}
	
	// 更新任务
    public function up($bh)
	{
		$this->save([
			'typesxid'  => $_POST['menus'][0],
			'tit'       => input('tit'),
			'num'       => input('day'),
			'txt'       => input('txt'),
			'money'     => input('money'),
			'qqtype'    => input('qqtype'),
			'phonetype' => input('phonetype')
		],['bh'=>$bh]);
		$audit = (state('demand') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/task/{$bh}{$audit}");		
	}	
}
?>

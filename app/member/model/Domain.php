<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\member\model;

use think\Model;
use think\Db;

class Domain extends Model
{
	// 添加域名
    public function add($bh)
	{
		$data           = $this->menus();
		$data['bh']     = $bh;
		$data['sj']     = sj();
		$data['lastgx'] = sj();
		$data['uip']    = uip();			
		$data['ubh']    = session('usercode');
		$data['zt']     = state('domain');
		$data['type']   = input('type');		
		$data['tit']    = input('tit');
		$data['name']   = input('name');
		$data['money']  = input('money');
		$data['txt']    = input('txt');		
		$this->allowField(true)->save($data);	
		$audit = (state('domain') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/domain/{$bh}{$audit}");
	}
	
	// 更新域名
    public function up($bh)
	{
		$data          = $this->menus();
		$data['type']  = input('type');		
		$data['tit']   = input('tit');
		$data['name']  = input('name');
		$data['money'] = input('money');
		$data['txt']   = input('txt');
		$this->save($data,['bh'=>$bh]);
		$audit = (state('demand') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/domain/{$bh}{$audit}");		
	}

	// 菜单数据
	private function menus()
	{
		// 单域名
		if(input('type') == 0){
			$data['domain_type'] = implode(",",$_POST['menus'][0][0]);
			$data['suffix']      = implode(",",$_POST['menus'][0][1]);
			$data['registrar']   = implode(",",$_POST['menus'][0][2]);
			if(isset($_POST['menus'][0][3])){
				$data['attribute'] = implode(",",$_POST['menus'][0][3]);
			}
		}
		// 多域名
		if(input('type') == 1){
			if(isset($_POST['menus'][0][0])){
				$data['domain_type'] = implode(",",$_POST['menus'][1][0]);
			}
			if(isset($_POST['menus'][0][1])){	
				$data['suffix'] = implode(",",$_POST['menus'][1][1]);
			}
			if(isset($_POST['menus'][0][2])){			
				$data['registrar'] = implode(",",$_POST['menus'][1][2]);
			}
			if(isset($_POST['menus'][0][3])){
				$data['attribute'] = implode(",",$_POST['menus'][1][3]);
			}
		}
		return $data;
	}
}
?>

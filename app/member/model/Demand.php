<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\member\model;

use think\Model;
use think\Db;

class Demand extends Model
{
	// 添加需求
    public function add($bh,$type)
	{
		$post            = input('post.');
		$post['menus']   = $_POST['menus'][0];
		$post['typeid2'] = (isset($_POST['menus'][1]))?$_POST['menus'][1]:0;		
		$post['bh']      = $bh;
		$post['sj']      = sj();
		$post['lastgx']  = sj();
		$post['uip']     = uip();			
		$post['ubh']     = session('usercode');
		$post['zt']      = state('demand');		
		$audit = (state('demand') == 0)? '/audit':null;
		$this->allowField(true)->save($post);	
		return thinkct()->url("/member/prompt/form/demand/{$bh}{$audit}");
	}
	
	// 更新需求
    public function up($bh,$type)
	{
		$this->save([
			'menus'     => $_POST['menus'][0],
			'typeid2'   => (isset($_POST['menus'][1]))?$_POST['menus'][1]:0,
			'tit'       => input('tit'),
			'txt'       => input('txt'),
			'money'     => input('money'),
			'qqtype'    => input('qqtype'),
			'phonetype' => input('phonetype')
		],['bh'=>$bh]);
		$audit = (state('demand') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/demand/{$bh}{$audit}");		
	}	
}
?>

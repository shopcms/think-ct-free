<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\member\model;

use think\Model;
use think\Db;

class Web extends Model
{
	// 添加网站
    public function add($bh)
	{
		$picture         = picture($bh,'web');
		$data['bh']      = $bh;
		$data['sj']      = sj();
		$data['lastgx']  = sj();
		$data['uip']     = uip();			
		$data['ubh']     = session('usercode');
		$data['zt']      = state('domain');	
		$data['updatas'] = (isset($_POST['updatas']))?implode(',',$_POST['updatas']):0;
		$data['app']     = (isset($_POST['app']))?implode(',',$_POST['app']):0;
		$data['menus']   = input('menus');
		$data['tit']     = input('tit');
		$data['money']   = input('money');
		$data['name']    = input('name');		
		$data['ip']      = input('ip');
		$data['profit']  = input('profit');
		$data['sys']     = input('sys');
		$data['server']  = input('server');
		$data['stats']   = input('stats');
		$data['url']     = input('url');
		$data['pwd']     = input('pwd');
		$data['txt']     = input('txt');
		$data['psort']   = trim(get_psort($bh),',');		
		$this->allowField(true)->save($data);	
		$audit = (state('web') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/web/{$bh}{$audit}");
	}
	
	// 更新网站
    public function up($bh)
	{	
		$picture         = picture($bh,'web');
		$data['updatas'] = (isset($_POST['updatas']))?implode(',',$_POST['updatas']):0;
		$data['app']     = (isset($_POST['app']))?implode(',',$_POST['app']):0;
		$data['menus']   = input('menus');
		$data['tit']     = input('tit');
		$data['money']   = input('money');
		$data['name']    = input('name');		
		$data['ip']      = input('ip');
		$data['profit']  = input('profit');
		$data['sys']     = input('sys');
		$data['server']  = input('server');
		$data['stats']   = input('stats');
		$data['url']     = input('url');
		$data['pwd']     = input('pwd');
		$data['txt']     = input('txt');
		$data['psort']   = trim(get_psort($bh),',');
		$this->save($data,['bh'=>$bh]);
		$audit = (state('web') == 0)? '/audit':null;
		return thinkct()->url("/member/prompt/form/web/{$bh}{$audit}");		
	}
}
?>

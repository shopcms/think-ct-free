<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Certification extends BaseUser
{
	// 个人认证
	public function idcard()
	{
		$view = 'idcard';
		// 第一步提交身份证
		if(input('flow') == 'upload'){
			// 上传图片
			$upload = upload('file');
			if($upload['code'] == 'SUCCESS') {
				$data = think_api('ocr/id_card_text',[
					'image'      => $upload['site_url'],
					'imageType'  => 'URL',
					'ocrType'    => 0,
					'detectRisk' => false
				]);
				// 判断是否识别成功
				if($data['code'] == 0){
					$certification = Db::name('user_certification')->where('card',$data['data']['cardNum'])->find();
					if(!$certification){				
						Db::name('user_certification')->insert([
							'address'  => $data['data']['address'],
							'birthday' => $data['data']['birth'],
							'name'     => $data['data']['name'],
							'card'     => $data['data']['cardNum'],
							'sex'      => $data['data']['sex'],
							'mz'       => $data['data']['nation'],
							'ubh'      => session('usercode'),
							'sj'       => sj(),
							'uip'      => uip(),
							'type'     => 'idcard',
							'zt'       => 0,
							'tp'       => $upload['site_url'],
						]);
					}
				}else{
					$this->error($data['message']);
				}
				return thinkct()->url('/member/certification/idcard/'.$data['data']['cardNum']);
			}	
		}
		// 第二步验证身份证
		$idcard = path('idcard');
		$data = Db::name('user_certification')->where(['ubh'=>session('usercode'),'card'=>$idcard])->find();
		if($data){
			$view = 'idcard_verify';
		}
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		$list = Db::name('user_certification')->where('ubh',session('usercode'))->select();
		$this->assign('user',$user);
		$this->assign('data',$data);
		$this->assign('list',$list);
		return $this->fetch($view);
	}
	// 企业认证
	public function company()
	{
		//$this->assign('data',$data);
		return $this->fetch();
	}	
}
?>
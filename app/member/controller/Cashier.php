<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

// 目前自助页面是空白的在36行

class Cashier extends BaseUser
{
    public function index($bh)
    {	
	
		$row = Db::name('dingdang')->where([
			'bh' => $bh,
			'ubh' => session('usercode')
		])->find();
		
		$user = Db::name('user')->where('bh',session('usercode'))->find();

		if(!$row){
			
			$this->error('订单不存在！', '/member/cart');
		}
		if($row['ddzt'] != '等待买家付款'){
			
			$this->success('订单已完成', '/member/buy');
		}		

		//exit(var_dump($row));
		
		$usermoney = round($user['money'],2); //获取用户余额
		$nowmoney = $row['money1'];//支付金额赋值
		// 自助及任务交易支付
		if($row['type'] != 'cashier'){
			// 有问题待修复
			return $this->custom($row,$bh);
		}
	
		//循环部分
		$goods=explode("|",$row['info']);//切割数据取商品数组
		$data = [];
		$dmtisp = null;
		
		for($i=0;$i<count($goods);$i++){
			$g_info = explode(",",$goods[$i]);//切割数组取商品信息
			$g_type = $g_info[0];//类型
			$g_bh = $g_info[1];//编号
			$g_money = $g_info[2];//现价
			$g_jifen = $g_info[3];//积分
			$g_anzhuang = $g_info[4];//安装费
			//获取商品信息及判断
			$shop = Db::name($g_type)->where('bh',$g_bh)->find();
			if($shop){
				$g_link=c_url($shop['id'],$g_type);//默认商品连接
				if($g_type!='code'){
					$dmtisp="：".$shop['name'];
					$g_link="http://".$g_type.".test.com/".$shop['id']."/";
				}

				$typename = "<strong>[".typename($g_type).$dmtisp."]</strong>";
				$name = user($shop['ubh'],'name','sell');


				if($g_type=="code"){
					if($g_anzhuang<=0){$g_anzhuang="0";}
					//积分低价
				if($g_jifen>0){
					$y_money=$g_money+($g_jifen*0.1);//原价
					$g_jifen=$g_jifen*0.1;	
					$g_jifen=" - <font color=#707070><em>{$y_money}</em>积分抵<em>{$g_jifen}</em>元</font>";
				}else{
					$g_jifen="";
				}	
					//安装费
					$g_anzhuang=" + <font color=#707070><a class='installing' data_d='{$shop['id']}' data_m='180' data_t='{$shop['tit']}' data_i='{$shop['id']}' data_b=''>安装服务</a><em>{$g_anzhuang}</em>元</font>";
				}
				$shoujia="<font color=#707070>售价<em>{$shop['money']}</em>元</font>";

				$zonghe=$shoujia.$g_jifen.$g_anzhuang;		
			
			}
			
			$data = [
				'tit' => $shop['tit'],
				'money' => $g_money,
				'zonghe' => $zonghe,
				'note_icon' => note_icon($shop,'1'),
				'name' => $name,
				'typename' => $typename,
				'link' => $g_link,
			];
			$list[] = $data;
		}
		
		$this->assign('user',$user);
		$this->assign('bh',$bh);
		$this->assign('nowmoney',$nowmoney);
		$this->assign('list',$list);

		return $this->fetch('cashier/index');	
	}	


	public function custom($row,$bh)
	{
		
		// 判断是否支付安装费
		if($row['type'] != 'install'){
			$custom = Db::name('custom')->where('ddbh',$bh)->find();
			// 获取及判断交易内容
			if(!$custom){
				$this->error('交易异常,返回重试！','/membe/myorder/'.$bh);
			}
			$ddinfo = Db::name('down')->where('ddbh',$custom['ddbh'])->find();
			// 获取及判断交易订单
			if(!$ddinfo){
				$this->error('交易异常,返回重试！','/membe/myorder/'.$bh);
			}
		}
		// 判断是否支付安装费
		$fees = $row['money1']-$row['money2'];
		// 判断是否需要支付中介费
		if($fees>0){
			$feests = "（交易金额：{$row['money2']} 元 + 中介费：{$fees} 元）";
		}
		$ctm = $this->type($ddinfo, $row);
		
		if($custom['txttype'] != 'main'){
			//$ctm.="(追加)";
		}
		//$sinfo=explode("||",user($ddinfo['sell'],'cname|qq|phone','sell',$ddinfo['type']));
		//联系卖家 获取卖家信息
		//$sell      = Db::name('sell')->where('bh',$row['sell'])->find();
		//$typename  = typename($row['type']);
		//$note_icon = note_icon($row['sell'],$row['type']);
		//$contact   = contact($row['sell'],"sell",$row['type'],"p","span");		
		
	}

    public function type($ddinfo, $row){
		if($ddinfo['type'] == 'task'){
			$ctm="任务交易";
		}elseif($ddinfo['type'] == 'serve'){
			$ctm="服务";	
		}elseif($row['type'] == 'install'){
			$ctm="安装费";		
		}else{
			$ctm="自助交易";
		}	
		return $ctm;
	}

    public function _empty($bh)
    {
		return $this->index($bh);
	}	
}







?>
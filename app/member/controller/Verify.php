<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Verify extends BaseUser
{
	// 邮箱修改
	public function email()
	{		
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		$name = route_name('email');
		$flow = ($name == null)?'into':$name;		
		$this->assign('flow',$flow);
		$this->assign('user',$user);		
		return $this->fetch();
	}
	
	// 手机修改
	public function phone()
	{
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		$name = route_name('phone');
		$flow = ($name == null)?'into':$name;		
		$this->assign('flow',$flow);
		$this->assign('user',$user);		
		return $this->fetch();
	}	
}
?>
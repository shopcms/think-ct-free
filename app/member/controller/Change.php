<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Change extends BaseUser
{
	// 修改密码
	public function pass()
	{
		$old = input('post.old');
		$new = input('post.new');
		$renew = input('post.renew');
		$data = null;
		if(isset($old)) {			
			$user = Db::name('user')->where([
				'bh' => session('usercode'),
				'pwd' => sha1($old)
			])->find();
			if(!$user) {
				$data['old'] = '原密码错误，请重新输入！';
			}			
			if($new == $renew) {
				if($user) {
					Db::name('user')->where('bh',$user['bh'])->update([
						'pwd' => sha1($new)
					]);					
					return redirect('/member/prompt/setup/')->remember();
				}
			}else{
				$data['renew'] = '两次输入的密码不同！';
			}
		}
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	// 修改安全码
	public function safe()
	{
		$old = input('post.old');
		$new = input('post.new');
		$renew = input('post.renew');
		$data = null;
		if(isset($old)) {			
			$user = Db::name('user')->where([
				'bh' => session('usercode'),
				'safe_code' => sha1($old)
			])->find();
			if(!$user) {
				$data['old'] = '原安全码错误，请重新输入！';
			}			
			if($new == $renew) {
				if($user) {
					Db::name('user')->where('bh',$user['bh'])->update([
						'safe_code' => sha1($new)
					]);					
					return redirect('/member/prompt/setup/')->remember();
				}
			}else{
				$data['renew'] = '两次输入的安全码不同！';
			}
		}
		$this->assign('data',$data);
		return $this->fetch();
	}	
}
?>
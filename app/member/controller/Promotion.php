<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;
use thinkct\library\ForeachType;

class Promotion extends BaseUser
{
	// 源码
    public function code()
    {
		$mid  = thinkct()->path('code');
		$data = Db::name('code')->where('id|bh',$mid)->find();		
		$list = ForeachType::list_promotion($mid,'code');
		$this->assign('data',$data);
		$this->assign('list',$list);
		$this->assign('mid',$mid);
        return $this->fetch();
    }
	
	// 服务
    public function serve()
    {
		$mid  = thinkct()->path('serve');
		$data = Db::name('serve')->where('id|bh',$mid)->find();		
		$list = ForeachType::list_promotion($mid,'serve');
		$this->assign('data',$data);
		$this->assign('list',$list);
		$this->assign('mid',$mid);
        return $this->fetch();
    }
	
	// 网站
    public function web()
    {
		$mid  = thinkct()->path('web');
		$data = Db::name('web')->where('id|bh',$mid)->find();		
		$list = ForeachType::list_promotion($mid,'web');
		$this->assign('data',$data);
		$this->assign('list',$list);
		$this->assign('mid',$mid);
        return $this->fetch();
    }

	// 域名
    public function domain()
    {
		$mid  = thinkct()->path('domain');
		$data = Db::name('domain')->where('id|bh',$mid)->find();		
		$list = ForeachType::list_promotion($mid,'domain');
		$this->assign('data',$data);
		$this->assign('list',$list);
		$this->assign('mid',$mid);
        return $this->fetch();
    }

	// 店铺
	public function sell()
    {
		$data = Db::name('sell')->where('bh',session('usercode'))->find();
		$mid  = $data['id'];
		$list = ForeachType::list_promotion($mid,'sell');		
		$this->assign('data',$data);
		$this->assign('list',$list);
		$this->assign('mid',$mid);
        return $this->fetch();
    }
}
?>
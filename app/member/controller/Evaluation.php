<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Evaluation extends BaseUser
{
	public function index($ddbh)
	{		
		$order   = Db::name('down')->where(['ddbh'=>$ddbh,'sell|buy'=>session('usercode')])->find();
		$user    = Db::name('user')->where('bh',session('usercode'))->find();
		$reviews = Db::name('reviews')->where('ddbh',$ddbh)->find();
		if(!$order){$this->error('非法操作','/member/');}
		if($order['ddzt']!='success'){$this->error('当前交易状态不可评价!','/member/');}		
		//exit(var_dump($reviews));
		if(!$reviews){
			$reviews['buyimp']  = null;
			$reviews['sellimp'] = null;
			$reviews['buycom']  = null;
			$reviews['sellcom'] = null;
			$reviews['zjcom']   = null;
			$reviews['buytime'] = null;		
		}	
		$order_evaluation = null;
		$con_rev 		  = null;
		$ok 			  = null;
		$append 		  = null;
		$cancel 	      = null;
		$avoideva         = null;
		// 判断角色
		if($order['buy'] == session('usercode')){
			$tstr              = "购买评价";
			$Role['my']        = 'buy';
			$Role['df']        = 'sell';
			$name[$Role['df']] = '卖家';
		}else{
			$tstr              = "出售评价";
			$Role['my']        = 'sell';
			$Role['df']        = 'buy';
			$name[$Role['df']] = '买家';
		}
		
		$name[$Role['my']]  = '您的';
		$color[$Role['my']] = '#3366cc';
		$color[$Role['df']] = '#e74851';
		
		
		$order_evaluation['buy']  = null;
		$order_evaluation['sell'] = null;
		
		$ok['sell'] = null;
		$ok['buy']  = null;

		//循环判断双方是否评价
		$arr = array($name['buy']=>"buy",$name['sell']=>"sell");		
		foreach($arr as $key => $val){
			//判断我是否评价
			if($reviews[$val.'imp']!='' || $reviews[$val.'com']!=''){
				$con_rev[$val]="<label class=\"layui-form-label\"><font color=\"{$color[$val]}\">{$key}评价</font></label>";	
				$ok[$val]='ok';
				if($reviews[$val."com"]==''){
					$reviews[$val."com"]="交易完成，3天未评价，系统自动好评！";
				}
				//买家评论
				$eva = eva($reviews[$val."imp"]);
				$con_rev[$val].="<div class='layui-input-block'><div class='layui-form-mid'><i class='ico-{$eva}' style='margin:0 3px 0 -3px;vertical-align:middle' id='eval'></i>{$reviews[$val."com"]}<p class='gray f12'>{$reviews[$val."time"]}</p></div></div>";

				if($reviews['hfcom'] != ''){
												
				}elseif($Role['my']=='sell' && $val!='sell' && strtotime("+7 day",strtotime($reviews[$val."time"]))>time()){
					//卖家回复
					$order_evaluation[$val].="<div class='layui-input-block'><div class='layui-inline'><input object='buy' number='{$ddbh}' role='sell' type='button' class='layui-btn layui-btn-small order_evaluation' value='我要回复'></div></div>";
				}
				$con_rev[$val].='</ul>';
			}else{
				// 默认评价时间
				if($val=='buy'){
					$jjs = sj($order['etime'],'+'.sysconf('time_evaluate'));
				}
				if($val=='sell'){
					$jjs = (!$reviews["buytime"])?sj($order['etime'],'+6'):sj($reviews["buytime"],'+6');
				}
				// 卖家评论
				$con_rev[$val]  = "<label class='layui-form-label'><font color='{$color[$val]}'>{$key}评价</font></label>";
				$con_rev[$val] .= "<div class='layui-input-block'><div class='layui-form-mid'><font class='gray'>暂未评价（若在 <span class='ttime' endTime='{$jjs}'></span> 内未评价，系统将自动默认好评）</font></div></div>";
				$con_rev[$val] .= '</ul>';
			}
		}		
		///////////////////////////////////////////
		// 若不是自助
		$type = Db::name('type')->where(['name1'=>$order['type'],'type'=>'deal'])->find();	
		$data['url'] = null;
		if($order['type']!='custom'){
			$goods = Db::name($order['type'])->where(['bh'=>$order['codebh'],'zt'=>1])->find();
			$data['url'] = c_url($goods['id'],$order['type']);
		}
		Db::name('reviews')->where('ddbh',$ddbh)->update([$Role['my'].'tisp' =>0,'ddbh'=>$ddbh]);
		//$pjsj=date("Y-m-d H:i:s",strtotime("+3 day",strtotime($reviews["buytime"])));//买家自动评价时间	
		///////////////////////////////////////////
		// 买家操作
		if($order['buy']==session('usercode')){
			if($reviews['buyimp']=="1"){
				$cancel="<input type=\"button\ class=\"layui-btn layui-btn-primary layui-btn-small\" value=\"撤销中评\" onclick=\"dconfirm('<b>确定要<font color=#ff4400>撤销中评</font>吗?</b><br>撤销后评价将自动改为<font color=red>5分好评</font><br>同时所有评价内容将被<font color=red>全部删除</font>','action=cancel&amp;bh={$ddbh}&amp;role=buy','evaluation')\">";
			}
			// 追加评价
			if($reviews['buyimp']=="2" || $reviews['buyimp']=="1"){
				if($reviews['zjcom']==""){$append="<input object=\"append\" number=\"{$ddbh}\" role=\"buy\" type=\"button\" class=\"layui-btn layui-btn-small order_evaluation\" value=\"追加评价\">";}
			}
		}
		// 卖家操作
		if($order['sell']==session('usercode')){
			if($reviews['buyimp']=="0" || $reviews['buyimp']=="-1"){
				$avoideva="<input onclick=\"expost('avoideva','{$ddbh}');\" type=\"button\" class=\"layui-btn layui-btn-small\"  value=\"使用评价消除卡\" />";		
			}
			// 卖家回复 缺
		}
		$caozuo = $cancel.$append.$avoideva;		
		
		// 评价记录
		$evaluation_log = Db::name('user_log')->where(['bh'=>$ddbh,'type'=>'evaluation'])->select();
		$this->assign('order',$order);
		$this->assign('data',$data);
		$this->assign('con_rev',$con_rev);
		$this->assign('order_evaluation',$order_evaluation);
		$this->assign('color',$color);
		$this->assign('name',$name);
		$this->assign('reviews',$reviews);
		$this->assign('ddbh',$ddbh);
		$this->assign('Role',$Role);
		$this->assign('ok',$ok);		
		$this->assign('caozuo',$caozuo);
		$this->assign('evaluation_log',$evaluation_log);
		return $this->fetch();
	}
}
?>
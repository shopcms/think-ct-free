<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Bond extends BaseUser
{
	// 保证金
	public function index()
	{
		$sell         = Db::name('sell')->where('bh',session('usercode'))->find();
		$moneyrecord  = Db::name('moneyrecord')->where(['ubh'=>session('usercode'),'tit'=>['like','%保证金%']])->order('id desc')->select();
		$data['log']  = $moneyrecord;
		$this->assign('data',$data);
		$this->assign('sell',$sell);
		return $this->fetch();
	}
	
	// 保证金状态
	public function state()
	{
		$sell = Db::name('sell')->where('bh',session('usercode'))->find();
		$this->assign('sell',$sell);
		return $this->fetch();
	}	

	// 保证金退保
	public function back()
	{
		return $this->fetch();
	}

	// 保证金转出
	public function come()
	{
		return $this->fetch();
	}
	
	// 缴纳方式
	public function setup()
	{
		$sell = Db::name('sell')->where('bh',session('usercode'))->find();
		if(input('?auto')){
			Db::name('sell')->where('bh',session('usercode'))->update([
				'bond_auto'   => input('auto'),
				'bond_cmoney' => input('cmoney'),
				'bond_method' => input('method'),
				'bond_auto'   => input('auto'),
			]);
			return "<script>parent.returnsuccess('<strong>设置成功!</strong><br><font color=#666666>保证金关联属性已变更！</font>',1);</script>";
		}
		$this->assign('sell',$sell);
		return $this->fetch();
	}	

	// 缴纳保证金
	public function payments()
	{
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		// 判断是否创建订单
		if(path('payments') == 'bond'){
			$data = Db::name('dingdang')->where('bh',input('bond'))->find();		
			$this->assign('data',$data);
			$this->assign('user',$user);
			return $this->fetch('pay');	
		}			
		// 缴纳保证金
		if(input('?money')){
			$bh    = input('bh');
			$money = input('money');
			if($money == 'cus'){
				$money = input('cmoney');
			}			
			// 判断余额是否充足
			if($user['money'] < $money){
				// 创建支付订单
				Db::name('dingdang')->insert([
					'bh'     => $bh,
					'ddbh'   => $bh.'|'.session('usercode'),
					'ubh'    => session('usercode'),
					'sj'     => sj(),
					'uip'    => uip(),
					'money1' => $money,
					'money2' => $money,
					'ddzt'   => '等待买家付款',
					'info'   => 'bond',
					'type'   => 'cashier'
				]);
				return redirect("/member/bond/payments/bond/".$bh)->remember();
			}
			PointUpdate(session('usercode'),$money*(-1),"缴纳保证金 {$money} 元",$bh);
			Db::name('sell')->where('bh',session('usercode'))->update(['bond'=>1]);
			Db::name('sell')->where('bh',session('usercode'))->setInc('bond',$money);
			return "<script>parent.returnsuccess('<strong>缴纳成功!</strong><br><font color=#666666>本次保证金缴纳{$money}元！</font>',1);</script>";
		}
		return $this->fetch();
	}
	
}
?>
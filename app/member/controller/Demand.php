<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Demand extends BaseUser
{
	// 求购
    public function ing()
    {
       return $this->type('ing',1);
    }
	
	// 结束
    public function over()
    {
        return $this->type('audit',2);
    }
	
	// 审核
    public function audit()
    {
		return $this->type('over',['in','0,3']);
    }

	// 类型
    public function type($mode,$zt)
    {	
		$request = request();
		$action = $request->action();
		$type = route_name($action);		
		$type = ($type == null)? 'code' : $type;
		
		$mwz_data = [
			['name'=>'出售','type'=>'ing','zt'=>1],
			['name'=>'结束','type'=>'over','zt'=>2],
			['name'=>'审核','type'=>'audit','zt'=>['in','0,3']]		
		];

		foreach($mwz_data as $data) {
			
			$class = null;
			if($action == $data['type']) {
				
				$class = "class='cur'";
			}
			if($data['type'] == 'task'){
				$total = Db::name('task')->where([
					'zt' => $data['zt'],
					'ubh'=>session('usercode')
				])->count();				
			}else{
				$total = Db::name('demand')->where([
					'zt' => $data['zt'],
					'ubh'=>session('usercode')
				])->count();			
			}
			
			if($data['zt']==2){
				$total = Db::name('task')->where([
					'zt' => 2,
					'ubh'=>session('usercode')
				])->count();
			}
			
			$data['url'] = "/member/demand/{$data['type']}";
			$data['total'] = $total;
			$data['class'] = $class;
			$mwz_list[] = $data;
		}
		
		// 类型列表
		$msl_data = [
			['name'=>'源码','type'=>'code'],
			['name'=>'网站','type'=>'web'],
			['name'=>'域名','type'=>'domain'],
			['name'=>'任务','type'=>'task']
		];
		
		foreach($msl_data as $data){
					
			$class = null;
			if($type == $data['type']) {
				
				$class = "class='cur'";
			}
			if($data['type'] == 'task'){
				$total = Db::name('task')->where([
					'zt'   => $zt,
					'ubh'  => session('usercode')
				])->count();				
			}else{
				$total = Db::name('demand')->where([
					'zt'   => $zt,
					'type' => $data['type'],
					'ubh'  => session('usercode')
				])->count();				
			}
			$mode = request()->action();			
			$data['url'] = "/member/demand/{$mode}/{$data['type']}";
			$data['total'] = $total;
			$data['class'] = $class;
			$msl_list[] = $data;
		}		
		
		$this->assign('mwz_list',$mwz_list);
		$this->assign('msl_list',$msl_list);
		$this->assign('type',$type);

        return $this->fetch();
    }
}
?>
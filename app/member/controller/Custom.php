<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Custom extends BaseUser
{
    public function index()
    {
		$bh      = input('bh');
		$action  = input('action');
		$role    = input('role');
		// 被发起人信息
		$andname = Db::name('user')->where('id|bh|email',input('andname'))->find();

		if($role == 'buy') {
			// 买家发起写入
			$bbh = session('usercode');
			$sbh = $andname['bh'];
		}else{
			// 卖家发起写入
			$sbh = session('usercode');
			$bbh = $andname['bh'];
		}
		
		// 新建自助
		if($action == 'add'){
			Db::name('down')->insert([
				'codebh' => $bh,
				'sj'     => sj(),
				'buy'    => $bbh,
				'sell'   => $sbh,
				//'fqbh'   => session('usercode'),
				'money1' => input('money'),
				'money2' => input('money'),
				'ddzt'   => 'waiting',
				'ddbh'   => $bh,
				'fees'   => input('fees'),
				'tit'    => input('tit'),
				'type'   => 'custom'
			]);
			Db::name('custom')->insert([
				'bh'      => $bh,
				'ddbh'    => $bh,
				'fqbh'    => session('usercode'),
				'jsbh'    => $andname['bh'],
				'utime'   => sj(),
				'money'   => input('money'),
				'txt'     => input('txt'),
				'cyc'     => input('daynum'),
				'txttype' => 'main'
			]);
			return redirect("/member/prompt/myorder/".$bh)->remember();	
		}
        return $this->fetch();
    }
}







?>
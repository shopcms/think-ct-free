<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class lists extends BaseUser
{
	private $page_size = 20;
	
    public function fav()
    {
		$type = thinkct()->path('type');
		if($type == null) {
			$type = 'code';
		}
		$list['mwz'] = [
			['name'=>'商品收藏','type'=>'','total'=>10],
			['name'=>'店铺收藏','type'=>'shop','total'=>10]
		];
		$msl = [
			['name'=>'源码','type'=>'code','total'=>10],
			['name'=>'服务','type'=>'serve','total'=>10],
			['name'=>'域名','type'=>'domain','total'=>10],
			['name'=>'网站','type'=>'web','total'=>10],
			['name'=>'任务','type'=>'task','total'=>10],
			['name'=>'需求','type'=>'demand','total'=>10],
			['name'=>'品牌','type'=>'brand','total'=>10],
		];
		foreach($msl as $data){

			
			$list['msl'][] = $data;
		}
		$fav = Db::name('fav')->where(['ubh'=>session('usercode'),'type'=>$type])->select();
		foreach($fav as $data){
			$data_type = Db::name($data['type'])->where('bh',$data['favbh'])->find();
			$data['tit']    = $data_type['tit'];
			$data['bh']     = $data_type['bh'];
			$data['money']  = $data_type['money'];
			//$data['state']  = ($data_type['zt']==1);
			$list['data'][] = $data;
		}	

		$this->assign('data',$data);				
		$this->assign('list',$list);		
		$this->assign('type',$type);			
		$datas = $this->fetch('lists/fav/'.$type);
		$this->assign('datas',$datas);
		$this->assign('upage',null);//页数		
        return $this->fetch();
    }	
	
	// 提现列表
	public function cashed()
	{
		$param = request()->param();
		$page = isset($param['page'])? $param['page'] : 1;	

		$query['ubh'] = session('usercode');
		
		$data   = Db::name('cashed')->where($query)
		->order('id desc')->limit($this->page_size)->page($page)->select();		
		$count  = Db::name('cash')->where($query)->count();
		$upage  = $this->page($count, $this->page_size,$page);
		// 成功提现金额
		$prompt = Db::name('cashed')->where($query)->where('zt',1)->sum('money');
		$this->assign('prompt',$prompt);
		$this->assign('upage',$upage);
		$this->assign('data',$data);		
		return $this->fetch();
	}
	
	public function money()
	{
		//请求参数
		$param = request()->param();
		$page = isset($param['page'])? $param['page'] : 1;		
		
		$query['ubh'] = session('usercode');
		
		$data = Db::name('moneyrecord')->where($query)
		->order('id desc')
		->limit($this->page_size)
		->page($page)
		->select();
		
		$count = Db::name('moneyrecord')->where($query)->count();
		$upage = $this->page($count, $this->page_size,$page);
		
		$prompt_s = Db::name('moneyrecord')->where(['ubh'=>session('usercode'),'moneynum'=>['>',0]])->sum('moneynum');
		$prompt_j = Db::name('moneyrecord')->where(['ubh'=>session('usercode'),'moneynum'=>['<',0]])->sum('moneynum');
		$prompt_j = str_replace('-',null,$prompt_j);
		$prompt = [
			['name'=>'收入','color'=>'#0b9a00','money'=>$prompt_s],
			['name'=>'支出','color'=>'#ff6600','money'=>$prompt_j],
			['name'=>'计出收入','color'=>'red','money'=>($prompt_s-$prompt_j)]
		];
		
		$this->assign('prompt',$prompt);
		$this->assign('upage',$upage);
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	public function jifen()
	{
		//请求参数
		$param = request()->param();
		$page = isset($param['page'])? $param['page'] : 1;	

		$query['ubh'] = session('usercode');
		
		$data = Db::name('jifen')->where($query)
		->order('id desc')
		->limit($this->page_size)
		->page($page)
		->select();	
		
		$count = Db::name('jifen')->where($query)->count();
		$upage = $this->page($count, $this->page_size,$page);
		
		$this->assign('upage',$upage);
		$this->assign('data',$data);		
		return $this->fetch();
	}
	
	public function tem()
	{
		return $this->fetch();
	}
	
	// 列表
	public function message()
	{
		$list['state'] = thinkct()->path('state');
		$list['sorts'] = thinkct()->path('sorts');
		$list['key']   = thinkct()->path('key');
		$list['data'] = [
			['name'=>'系统消息'],
			['name'=>'交易消息'],
			['name'=>'财务消息'],
			['name'=>'帐户消息'],
			['name'=>'店铺消息'],
			['name'=>'商品消息'],
			['name'=>'需求消息'],
			['name'=>'任务消息']
		];
		$where['ubh'] = session('usercode');
		$where['zt']  = ($list['state']==1)?1:0;
		if($list['sorts'] != null) {
			$where['type'] = $list['sorts'];
		}
		if($list['key'] != null) {
			$where['tit'] = ['like',$list['key'].'%'];
		}			
		$data  = Db::name('message')->where($where)->order('id desc')->select();
		$total = Db::name('message')->where($where)->count();
		$this->assign('data',$data);
		$this->assign('total',$total);
		$this->assign('list',$list);		
		return $this->fetch();
	}
	
	public function login()
	{
		
		//请求参数
		$param = request()->param();
		$page = isset($param['page'])? $param['page'] : 1;
		
		$data = Db::name('anquan')->where('ubh',session('usercode'))
		->order('id desc')
		->limit($this->page_size)
		->page($page)
		->select();
		
		$count = Db::name('moneyrecord')->where('ubh',session('usercode'))->count();
		$upage = $this->page($count, $this->page_size,$page);

		$this->assign('upage',$upage);
		$this->assign('data',$data);		
		return $this->fetch();
	}	
	
	public function bad()
	{
		
		return $this->fetch();
	}
	
	public function promotion()
	{
		//请求参数
		$param = request()->param();
		$page  = isset($param['page'])? $param['page'] : 1;	
		$query['ubh'] = session('usercode');
		$data = Db::name('tuiguang')->where($query)
		->order('id desc')->limit($this->page_size)->page($page)->select();
		$list = [];
		foreach($data as $datas){
			$ads  = Db::name('ads')->where(['type2'=>$datas['ads']])->find();
			$type = Db::name($ads['type'])->where(['id|bh'=>$datas['pro']])->find();
			$datas['name']   = (isset($type['tit']))?$type['tit']:$type['name'];
			$datas['tit']    = $ads['tit'];
			$datas['type']   = $ads['type'];
			$datas['cost']   = ($datas['money']==0)?$datas['jifen'].'积分':$datas['money'].'元';
			$list[]          = $datas;
		}
		$count = Db::name('tuiguang')->where($query)->count();
		$upage = $this->page($count, $this->page_size,$page);		
		$this->assign('upage',$upage);
		$this->assign('list',$list);		
		return $this->fetch();
	}	
	
	public function exchange()
	{
		return $this->fetch();
	}	

	// 品牌展示
	public function brand()
	{
		$list = Db::name('brand')->where(['ubh'=>session('usercode')])->select();
		
		$this->assign('list',$list);
		return $this->fetch();
	}
	
	// 知识产权
	public function property()
	{
		return $this->fetch();
	}
	
	// 原创保护
	public function protect()
	{
		return $this->fetch();
	}	
	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Fetch extends BaseUser
{
	public function _initialize()
	{
		if(session('?usercode')) {
			//$this->success('已经登录，请勿重复操作！', '/member/');
		}	
	}
	
	// 找回密码
	public function pass()
	{
		$name = route_name('pass');
		$flow = ($name == null)?'into':$name;		
		$this->assign('flow',$flow);
		return $this->fetch();
	}
	
	// 找回安全码
	public function safe()
	{
		if(!session('?usercode')) {
			$this->success('先登陆后再做操作！', '/member/login');
		}
		$name = route_name('safe');
		$flow = ($name == null)?'into':$name;		
		$this->assign('flow',$flow);
		return $this->fetch();
	}	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;


class Success extends BaseUser
{
	// 充值提示
	public function online()
	{
		$name['tit'] = '充值成功';
		$name['txt'] = '3秒后为您转到财务明细，或点此直接跳转';
		$name['url'] = '/member/lists/money';
		$this->assign('name',$name);
        return $this->fetch();		
	}
	
	// 支付提示
	public function cashier()
	{
		$name['tit'] = '支付成功';
		$name['txt'] = '3秒后为您转到订单管理，或点此直接跳转';
		$name['url'] = '/member/order/buy';
		$this->assign('name',$name);
        return $this->fetch();			
	}
	
	// 购买提示
	public function buy()
	{
		$name['tit'] = '购买成功';
		$name['txt'] = '3秒后为您转到订单管理，或点此直接跳转';
		$name['url'] = '/member/order/buy';
		$this->assign('name',$name);
        return $this->fetch();		
	}	
}
?>
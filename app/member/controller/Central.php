<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Central extends BaseUser
{
    public function index()
    {
		$user           = Db::name('user')->where('bh',session('usercode'))->find();
		// 列表
		$list['qq']     = Db::name('oauth')->where(['type'=>'qq','ubh'=>session('usercode')])->select();
		$list['wechat'] = [];
		$list['sina']   = [];
		$list['baidu']  = [];
		$list['alipay'] = [];
		// 数量
		$count['qq']     = Db::name('oauth')->where(['type'=>'qq','ubh'=>session('usercode')])->count();
		$count['wechat'] = 0;
		$count['sina']   = 0;
		$count['baidu']  = 0;
		$count['alipay'] = 0;
		$this->assign('user',$user);
		$this->assign('list',$list);
		$this->assign('count',$count);
        return $this->fetch('index');
    }
	
	public function certification()
	{		
		return $this->index();
	}
}







?>
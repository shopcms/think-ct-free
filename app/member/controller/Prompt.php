<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Prompt extends BaseUser
{
	public function form()
	{
		$form  = thinkct()->path('form');
		$bh    = thinkct()->path($form);
		$where = ['bh'=>$bh,'ubh'=>session('usercode')];
		// 默认类型
		$data['state'] = '状态';
		$data['tips']  = '提示';
		$data['name']  = '类型';
		$data['type']  = '类型';
		$data['url']   = '链接';
		switch($form) {
			case "demand":// 需求发布
			$data = Db::name('demand')->where($where)->find();
			$form = 'demand_'.$data['type'];
			$data = [
				'state' => $data['zt'],
				'name'  => '需求',
				'type'  => 'demand/ing',
				'url'   => c_url($data['id'],'demand'),
				'form'  => $data['type']
			];			
			break;
			case "task":// 任务发布
			$data = Db::name('task')->where($where)->find();
			$data = [
				'state' => $data['zt'],
				'name'  => '任务',
				'type'  => 'demand/ing',
				'url'   => c_url($data['id'],'task')
			];
			break;
			case "cashed":// 申请提现
				$data = [
					'state' => 1,
					'tips'  => '等待提现审核',
					'type'  => 'lists'
				];
			break;
			default:
			$data = Db::name($form)->where($where)->find();
			$data = [
				'state' => $data['zt'],
				'name'  => typename($form),
				'type'  => 'goods/sale',
				'url'   => c_url($data['id'],$form)
			];			
		}
		// 审核状态
		if($data['state'] == 0){
			$data['tips'] = '发布的信息正在审核中，请耐心等待！';
		}		
		if($data['state'] == 2){
			$name['ts'] = '确认无误后申请重审！';
		}		
		$this->assign('form',$form);
		$this->assign('bh',$bh);
		$this->assign('data',$data);
		return $this->fetch();
	}
	
    public function setup()
    {
		$name = route_name('setup');
		$this->assign('name',$name);
        return $this->fetch();
    }

	public function myorder()
	{
		$bh = route_name('myorder');		
		$data = Db::name('down')->where('ddbh',$bh)->find();
		$this->assign('data',$data);
        return $this->fetch(); 
	}
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Reg extends BaseUser
{
	public function _initialize()
	{
		if(session('?usercode')) {
			//$this->success('已经登录，请勿重复操作！', '/member/');
		}	
	}
	
	// 注册页面
    public function index()
    {
		if(session('?usercode')) {
			//$this->success('已经登录，请勿重复操作！', '/member/');
		}		
        return $this->fetch();
    }
	
	// 注册邮箱验证
	public function verify()
	{
		$user = DB::name('user')->where('bh',session('usercode'))->find();
		$this->assign('user',$user);
		return $this->fetch();
	}
	
	// QQ注册页面
	public function qq()
	{
		$data = session('oauth_data');
		$this->assign('data',$data);
		return $this->fetch();
	}
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Cart extends BaseUser
{
    public function index()
    {
		$cartlist = [];
		$minfo = null;
		$cart = Db::name('cart')->where('ubh',session('usercode'))->order('id desc')->limit(30)->select();
		//exit(var_dump($cart));
		foreach($cart as $row){		
			$row1 = Db::name($row['type'])->where('bh',$row['codebh'])->find();
			$downnum=0;
			$jfio="";
			$tisp="";
			$dmtisp="";
			$C1zt="checked='checked'";
			if($row['type'] == 'code') {
				//其他
				if($row1['jfnum']!=0){
					$jfs=$row1['money']*$row1['jfnum']*10;
				}
				if($row1['ubh']==$row['ubh']){$downnum=2;}//自己的不能买
				
				if($row1['jfnum']!=0){//判断是否支持积分抵价
					$jfio="<dd class='vat cinte'><li class='zero'>&nbsp;</li><li class='first'><input name='score' lay-filter='cart' class='score' type='checkbox' value='{$row['codebh']}'>积分抵价</li><li class='text'>支持使用<b>{$jfs}</b>积分,已用<a>0</a>积分</li><li class='money'></li></dd>";
				}				
			}
			$azmoney = null;
			if($row['type']=="code"){
				if($row1['azmoney']=="0" or $row1['azmoney']==""){
					$az="<li class='first'><input name='install' disabled checked lay-filter='cart' type='checkbox' value='{$row['codebh']}'>安装服务</li><li class='text'>购买前请仔细阅读【<a class='installing' data_d='{$row1['id']}' data_m='0' data_t='{$row1['tit']}' data_b=''>要求说明</a>】</li><li class='money'><b class='green'>免费</b></li><input type=hidden value='e6b085e892cec8b59772189a9ead6456' name='m_{$row['codebh']}' /><input type=hidden value='dc79ae3afe5d2de965dcd8256fdf44f8' name='z_{$row['codebh']}' />	";
				}else{
					$azmoney=$row1['azmoney'].".00";
					$az="<li class='first'><input name='install' checked lay-filter='cart' type='checkbox' value='{$row['codebh']}'>安装服务</li><li class='text'>可选，购买前请仔细阅读【<a class='installing' data_d='{$row1['id']}' data_m='180' data_t='{$row1['tit']}' data_i='' data_b=''>要求说明</a>】</li><li class='money'>￥<b>{$azmoney}</b></li><input type=hidden value='e6b085e892cec8b59772189a9ead6456' name='m_{$row['codebh']}' /><input type=hidden value='dc79ae3afe5d2de965dcd8256fdf44f8' name='z_{$row['codebh']}' />	";		
				}	
				$azmoney="<dd class='vat cinst'><li class='zero'>&nbsp;</li>".$az."</dd>";
			}
			//默认商品连接
			$g_link="http://".$row['type'].".test.com/".$row1['id']."/";
			//如果未出现过则生成店铺信息
			if($minfo[$row1['ubh']]==''){
				$sinfo=explode("||",user($row1['ubh'],'id|name|qq','sell','code'));
				$contact=contact($row1['ubh'],"sell",$row['type'],"p","span");//联系方式
				$shop[$row1["ubh"]]="<dt><input name='shop' checked lay-filter='cart' lay-skin='round' type='checkbox' value='cart'><span><a href='/ishop{$sinfo['0']}/' target=_blank>卖家：{$sinfo['1']}</a> </span>{$contact}</dt>";
			}
			//源码操作
			if($row['type']=='code'){
				$g_link=c_url($row1['id'],$row['type']);
				$g_img="<img src='".get_tp($row1['bh'])."'>";//默认缩略图
			}elseif($row['type']=='web'){
				$dmtisp="：".$row1['name'];
				$g_img="<img src='".get_tp($row1['bh'])."'>";//默认缩略图
			}elseif($row['type']=='domain'){
				$dmtisp="：".$row1['name'];
				$g_img="<strong>域名</strong>";
			}
			//判断类型是否合法
			$menu = Db::name('type')->where(['name1' => $row['type'],'type' => 'deal'])->find();
			if($menu){
				$menu_name="[".$menu['name2'].$dmtisp."]";
			}
			$row['tit'] = $row1['tit'];
			$row['menu_name'] = $menu_name;
			$row['money'] = $row1['money'];
			$row['link'] = $g_link;
			$row['img'] = $g_img;
			$row['note_icon'] = note_icon($row1,$row['type']);
			$row['shop'] = $shop[$row1["ubh"]];
			$row['azmoney'] = $azmoney;
			$row['jfio'] = $jfio;
			
			$cartlist[] = $row;
		}		
			
			$this->assign('cartlist', $cartlist);
			
			return $this->fetch();
		}
}







?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Index extends BaseUser
{	
    public function index()
    {	
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		// 卖家评价
		$score = null;
		$bond = null;
		if($user['uc'] == 'sell') {	
			$rowsell = Db::name('sell')->where('bh',session('usercode'))->find();	
			$score = explode('|',$rowsell['score']);
			$bond = bond($rowsell);
		}		
		// 执行自动评价
		e_rev();
		// 开始执行自动确认收货
		$rowdb = Db::name('moneydb')->where('buy|sell',session('usercode'))->whereTime('oksj','<',sj())->select();
		foreach($rowdb as $v){
			e_order($v['ddbh'],'success',$v['oksj']);
		}
		// 退款交易自处理执行
		$rowback = Db::name('moneyback')->where('buy|sell',session('usercode'))->whereTime('oksj','<',sj())->select();
		
		//exit(var_dump($rowback));
		foreach($rowback as $v){			
			if($v['zt'] == 0){
				// 表示卖家未处理，自动执行退款				
				e_order($v['ddbh'],'backsuc',$v['oksj']);
			}elseif($v['zt'] == 1){
				// 表示卖家不同意且未申诉，自动交易成功
				e_order($v['ddbh'],'success',$v['oksj']);
			 }				
		}
				
		// 卖家交易
		$sell_waiting = Db::name('down')->where(['sell'=>$user['bh'],'ddzt'=>'waiting'])->count();
		$sell_dealing = Db::name('down')->where(['sell'=>$user['bh'],'ddzt'=>'dealing'])->count();
		$sell_backing = Db::name('down')->where(['sell'=>$user['bh'],'ddzt'=>'backing'])->count();
		$sell_success = Db::name('down')->where(['sell'=>$user['bh'],'ddzt'=>'success'])->count();
		
		$order_sell = [
			['name'=>'等待中','type'=>'waiting','value'=>$sell_waiting],
			['name'=>'交易中','type'=>'dealing','value'=>$sell_dealing],
			['name'=>'退款中','type'=>'backing','value'=>$sell_backing],
			['name'=>'已成交','type'=>'success','value'=>$sell_success]
		];
		
		// 卖家商品
		$goods_sale = Db::name('code')->where(['ubh'=>$user['bh'],'zt'=>1])->count();
		$goods_down = Db::name('code')->where(['ubh'=>$user['bh'],'zt'=>2])->count();
		$goods_audit = Db::name('code')->where(['ubh'=>$user['bh'],'zt'=>3])->count();
		
		$goods_sell = [
			['name'=>'出售中','type'=>'sale','value'=>$goods_sale],
			['name'=>'已下架','type'=>'down','value'=>$goods_down],
			['name'=>'审核中','type'=>'audit','value'=>$goods_audit],
		];		
		
		// 买家交易
		$order_buy = [
			'success'=>Db::name('down')->where(['buy'=>$user['bh'],'ddzt'=>'success'])->count(),
			'backsuc'=>Db::name('down')->where(['buy'=>$user['bh'],'ddzt'=>'success'])->count(),
			'waiting'=>Db::name('down')->where(['buy'=>$user['bh'],'ddzt'=>'waiting'])->count(),
			'dealing'=>Db::name('down')->where(['buy'=>$user['bh'],'ddzt'=>'dealing'])->count(),
			'backing'=>Db::name('down')->where(['buy'=>$user['bh'],'ddzt'=>'backing'])->count(),
			'cart'=>Db::name('cart')->where('ubh',$user['bh'])->count()
		];
		
		$mix_s_lump[1] = $this->mix_s_lump();
		$mix_s_lump[2] = $this->mix_s_lump1();	
		$this->assign('mix_s_lump', $mix_s_lump);
		
		// 商户公告
		$in = ($user['uc'] == 'sell')?3:4;
		$article['gonggao'] = Db::name('gonggao')->where('fl','in','1,5,'.$in)->order('id desc')->limit(5)->select();
		$article['news']    = Db::name('article')->where('status',1)->limit(5)->order('top desc,id desc')->select();
		
		$this->assign('score', $score);
		$this->assign('bond', $bond);
		$this->assign('order_sell', $order_sell);
		$this->assign('order_buy', $order_buy);
		$this->assign('goods_sell', $goods_sell);
		$this->assign('article', $article);
        return $this->fetch();
    }
	
	private function mix_s_lump()
	{
		// 销售统计
		$trading   = Db::name('down')->where('sell',session('usercode'))->count();
		$volume    = Db::name('down')->where(['ddzt'=>'success','sell'=>session('usercode')])->count();
		$aturnover = Db::name('down')->where('sell',session('usercode'))->sum('money1');
		$turnover  = Db::name('down')->where(['ddzt'=>'success','sell'=>session('usercode')])->sum('money1');
		// 退款量
		$refund = $trading-$volume;
		$refundrate = 0;
		if($refund != 0 and $volume != 0) {
			$refundrate = round($refund/$volume*100,2);
		}
		$zdanjia = 0;
		if($aturnover != 0 and $trading != 0) {
			$zdanjia = floor($aturnover/$trading);
		}
		$cdanjia = 0;
		if($turnover != 0 and $volume != 0) {
			$cdanjia = floor($turnover/$volume);	
		}	
		$mix_s_lump = [
			['tit'=>'交易量','focus1'=>$trading,'focus2'=>'笔',
			'tit1'=>'成交量','focus11'=>$volume,'focus22'=>'笔'],
			['tit'=>'交易额','focus1'=>$aturnover,'focus2'=>'元',
			'tit1'=>'成交额','focus11'=>$turnover,'focus22'=>'元'],
			['tit'=>'退款率','focus1'=>$refundrate,'focus2'=>'%',
			'tit1'=>'退款量','focus11'=>$refund,'focus22'=>'笔'],
			['tit'=>'客单价','focus1'=>$zdanjia,'focus2'=>'元',
			'tit1'=>'成交客单价','focus11'=>$cdanjia,'focus22'=>'元'],
		];
		return $mix_s_lump;
	}
	
	private function mix_s_lump1()
	{
		// 销售统计
		$trading = Db::name('down')->where('sell',session('usercode'))->whereTime('sj', 'month')->count();
		$volume = Db::name('down')->where(['ddzt'=>'success','sell'=>session('usercode')])->whereTime('sj', 'month')->count();
		$aturnover = Db::name('down')->where('sell',session('usercode'))->whereTime('sj', 'month')->sum('money1');
		$turnover = Db::name('down')->where(['ddzt'=>'success','sell'=>session('usercode')])->whereTime('sj', 'month')->sum('money1');
		// 退款量
		$refund = $trading-$volume;
		$refundrate = 0;
		if($refund != 0 and $volume != 0) {
			$refundrate = round($refund/$volume*100,2);
		}
		$zdanjia = 0;
		if($aturnover != 0 and $trading != 0) {
			$zdanjia = floor($aturnover/$trading);
		}
		$cdanjia = 0;
		if($turnover != 0 and $volume != 0) {
			$cdanjia = floor($turnover/$volume);	
		}		
		$mix_s_lump = [
			['tit'=>'交易量','focus1'=>$trading,'focus2'=>'笔',
			'tit1'=>'成交量','focus11'=>$volume,'focus22'=>'笔'],
			['tit'=>'交易额','focus1'=>$aturnover,'focus2'=>'元',
			'tit1'=>'成交额','focus11'=>$turnover,'focus22'=>'元'],
			['tit'=>'退款率','focus1'=>$refundrate,'focus2'=>'%',
			'tit1'=>'退款量','focus11'=>$refund,'focus22'=>'笔'],
			['tit'=>'客单价','focus1'=>$zdanjia,'focus2'=>'元',
			'tit1'=>'成交客单价','focus11'=>$cdanjia,'focus22'=>'元'],
		];
		return $mix_s_lump;
	}	
}
?>
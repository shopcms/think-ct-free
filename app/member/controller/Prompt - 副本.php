<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Prompt extends BaseUser
{
	public function form()
	{
		$form      = thinkct()->path('form');
		$form_type = $form;

		$name['type'] = route_name('form');
		$name['bh']   = route_name($name['type']);
		// 商品发布
		if($form != 'cashed'){
			// 获取数据
			$data = Db::name($name['type'])->where([
				'bh'  => $name['bh'],
				'ubh' => session('usercode')
			])->find();
			if(isset($data['type'])){
				$name['type'] = $name['type'].'_'.$data['type'];
				$form_type    = $data['type'];
			}
			if($form == 'task'){
				$form = 'demand';
			}
			if($form != 'demand'){
				$form = 'goods';
			}
			$name['ts'] = null;
			if($data['zt'] == 0 || state($name['type']) == 0) {
				$name['ts'] = '发布的信息正在审核中，请耐心等待！';
			}		
			if($data['zt'] == 2) {
				$name['ts'] = '确认无误后申请重审！';
			}		
			$name['typename'] = typename($name['type']);
			$form_name = ($form=='demand')?$form:$form_type;
			$name['url']      = c_url($data['id'],$form_name);
			$sale = ($form=='demand')?'ing':'sale';
			$name['state']    = (state($name['type']) == 1)? $sale:'audit';
		}
		// 申请提现
		if($form == 'cashed'){
			$data['bh'] = null;
			$data['zt'] = 1;
			$name['ts'] = '等待提现审核';
			$form       = 'lists';
		}
		$this->assign('name',$name);
		$this->assign('data',$data);
		$this->assign('form',$form);
		$this->assign('form_type',$form_type);
		return $this->fetch();
	}
	
    public function setup()
    {
		$name = route_name('setup');
		$this->assign('name',$name);
        return $this->fetch();
    }	
}
?>
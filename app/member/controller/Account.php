<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;

use thinkct\controller\BaseUser;

use think\Db;

class Account extends BaseUser
{
	// 用户类型
	public function index()
	{
		$user = Db::name('user')->where('bh',session('usercode'))->find();		
		// 判断身份
		if($user['uc'] == 'buy') {
			$data['type'] = '买家';
		}else{
			$data['type'] = '商家（卖家）';
		}
		// 判断状态
		$data['state'] = '正常';
		$this->assign('data',$data);
		return $this->fetch();
	}	
	// 开通店铺
	public function turn()
	{
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		if($user['uc'] == 'sell') {
			$this->success('您当前已是【商家】，无需转型！', '/member/account/');
		}
		$state = false;
		$data_list = [
			['name'=>'邮箱','type'=>'email','state'=>$user['email_zt']],
			['name'=>'手机','type'=>'phone','state'=>$user['phone_zt']],
			//['name'=>'实名','type'=>'idcard','state'=>$user['idcard']]
		];
		if($user['email_zt'] == 1 and $user['phone_zt'] == 1){
			$state = true;
		}
		foreach($data_list as $list){
			// 认证类型
			switch($list['state']){
				case 0:
				$list['content'] = '点此认证';
				break;  
				case 1:
				$list['content'] = '已认证';
				break;
				default:
				$list['content'] = '无需认证';
			}
			$data[] = $list;			
		}
		$this->assign('state',$state);
		$this->assign('data',$data);
		return $this->fetch();
	}
}
?>
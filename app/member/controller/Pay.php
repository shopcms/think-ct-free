<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;
use thinkct\library\Purchase;
use app\common\pay\Epay;

class Pay extends BaseUser
{
    public function index()
    {	
		$bh      = input('post.bh');
		$gate    = input('post.gate');
		$action  = input('post.action');	
		$payname = payname($gate);		
		$user    = Db::name('user')->where('bh',session('usercode'))->find();
		if($bh == null) {
			$this->error('非法操作');
		}
		// 加载代码
		$action = ($action=='online')?'online':'cashier';
		return $this->$action($bh,$gate,$payname,$user);			
	}
	
	// 充值余额
	private function online($bh,$gate,$payname,$user)
	{
		// 删除之前存在的订单
		Db::name('dingdang')->where(['ddzt'=>'等待买家付款','ubh'=>session('usercode')])->delete();
		$money   = input('post.money');	
		$ddbh    = $bh."|".$user['bh'];
		$subject = sysconf('site_name')."在线充值";
		// 新建订单				
	    Db::name('dingdang')->insert([
			'type'     => 'online',
			'bh'       => $bh,
			'ddbh'     => $ddbh,
			'ubh'      => $user['bh'],
			'sj'       => sj(),
			'uip'      => uip(),
			'money1'   => $money,
			'ddzt'     => '等待买家付款',
			'alipayzt' => null,
			'bz'       => $payname
		]);
		return $this->request($gate,'online');
	}
	
	// 在线购买
	public function cashier($bh,$gate,$payname,$user)
	{	
		// 删除之前存在的订单
		Db::name('dingdang')->where(['bh'=>['<>',$bh],'ddzt'=>'等待买家付款','ubh'=>session('usercode')])->delete();	
		$balance = input('balance',0);
		$ddbh    = $bh.'|'.$user['bh'];
		$subject = sysconf('site_name').'在线收银';		
		$dingdan = Db::name('dingdang')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		if(!$dingdan){
			return '订单已失效，请返回重新提交！';
		}
		// 余额直接支付
		$money = $dingdan['money1'];
		if($user['money'] >= $money and $balance != 0){
			return Purchase::index($ddbh);
		}
		// 在线收银支付
		if($balance == 0) {
			$money = $dingdan['money2'];
		}else{
			$money = $dingdan['money2']-$balance;
		}
		Db::name('dingdang')->where('bh',$dingdan['bh'])->update(['money1'=>$money]);		
		return $this->request($gate,'cashier');
	}
	
	// 扫码支付页面
	public function qr()
	{
		$gate = input('gate');
		$qr   = explode('_',path('qr'));
		// 获取需要支付的订单
		$dingdan = Db::name('dingdang')->where(['ddzt'=>'等待买家付款','ubh'=>session('usercode')])->find();
		// 当前测试支付宝的
		$name = ($qr[0]=='online')?'在线充值':'收银台结算';
		$type = $qr[1];
		$this->assign('name',$name);
		$this->assign('bh',$dingdan['bh']);
		$this->assign('money',$dingdan['money1']);
		return $this->fetch($type);
	}
	
	// 支付宝支付页面
	public function alipayform()
	{
		// 获取需要支付的订单
		$data   = Db::name('dingdang')->where(['ddzt'=>'等待买家付款','ubh'=>session('usercode')])->find();
		$alipay = path('online_alipay');
		// 支付宝电脑支付
		if($alipay == 'pc'){
			$alipay = new \app\common\pay\Alipaypc();
		}
		// 支付宝当面付
		if($alipay == ''){
			$alipay = new \app\common\pay\Alipaydmf();
		}
		return $alipay->order($data);
	}
	
	// 微信支付页面
	public function wechatform()
	{
		// 获取需要支付的订单
		$data   = Db::name('dingdang')->where(['ddzt'=>'等待买家付款','ubh'=>session('usercode')])->find();		
		$wechat = new \app\common\pay\Wechatpay();
		return $wechat->order($data);
	}
	
	// QQ钱包支付页面
	public function qpayform()
	{
		return 'QQ钱包,暂时关闭';
	}
	
	// 监控回调
	public function Pquery()
	{
		$number = input('number');
		$type   = input('type');
		// 订单信息
		$data   = Db::name('dingdang')->where(['bh'=>$number,'ddzt'=>'支付成功','ubh'=>session('usercode')])->find();
		if(!$data){
			return ['state'=>-1];
		}
		// 支付成功
		return ['state'=>1,'url'=>'/member/success/'.$data['type']];
		// 支付宝返回{"qrStatus":"normal","succ":true}
	}
	
	// 发起支付请求
	public function request($gate,$type)
	{
		// 获取需要支付的订单
		$data = Db::name('dingdang')->where(['ddzt'=>'等待买家付款','ubh'=>session('usercode')])->find();		
		// 支付宝支付
		if($gate == 'alipay'){
			if(sysconf('alipay_type') == 'alipaypc'){
				$Alipaypc =  new \app\common\pay\Alipaypc();
				return $Alipaypc->order($data);
			}
			if(sysconf('alipay_type') == 'epay'){
				 $Epay = new Epay();
				 return $Epay->order($data,'alipay');
			}			
		}
		// 微信支付
		if($gate == 'wechat'){
			if(sysconf('wechat_type') == 'epay'){
				 $Epay = new Epay();
				 return $Epay->order($data,'wxpay');
			}			
		}		
		// QQ钱包支付
		if($gate == 'qpay'){
			if(sysconf('wechat_type') == 'epay'){
				 $Epay = new Epay();
				 return $Epay->order($data,'qqpay');
			}			
		}		
		return '正在处理中，请勿关闭页面……<script>window.location.href="/member/pay/qr/'.$type.'_'.$gate.'";</script>';
	}	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Setup extends BaseUser
{
	// 广告设置+广告转移
	public function promotion()
	{
		
		return '广告设置暂未开放';
	}
	
	// 个人首页
	public function index()
	{
		return $this->info('info');
	}
	
	// 个人设置
    public function info($tem='')
    {
		$post = input('post.');
		$data = Db::name('buy')->where('bh',session('usercode'))->find();
		if(isset($post['name'])) {
			$upload = upload('file');
			Db::name('buy')->where('bh',session('usercode'))->update([
				'name'    => input('name'),
				'contact' => input('qq'),
				'wechat'  => input('wechat'),
				'tx'      => ($upload['code']=='SUCCESS')? $upload['site_url']:$data['tx']
			]);
			return redirect('/member/prompt/setup/info')->remember();
		}
		$this->assign('data',$data);
        return $this->fetch($tem);
    }
	
	// 店铺设置
	public function shop()
	{
		$post = input('post.');
		$data = Db::name('sell')->where('bh',session('usercode'))->find();
		$data['diqu'] = explode(" ",anquaninfo(session('usercode'),'city','0'))[0];
		if(isset($post['s_phone'])) {
			$upload = upload('file');
			Db::name('sell')->where('bh',session('usercode'))->update([
				's_phone'     => input('s_phone'),
				's_watermark' => input('s_watermark',0),
				'notice'      => input('notice'),
				'address'     => (isset($post['address']))? $data['diqu']:$data['address'],
				'tx'          => ($upload['code'] == 'SUCCESS')? $upload['site_url']:$data['tx']
			]);
			return redirect('/member/prompt/setup/shop')->remember();
		}			
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	// 客服设置
	public function service()
	{
		$data = Db::name('sell')->where('bh',session('usercode'))->find();
		
		$hmstr = null;
		$service = [];
		preg_match_all('/<([a-zA-Z]+)-([a-zA-Z]+)>([^<>]+)<\/([a-zA-Z]+)-([a-zA-Z]+)>/',$data['contact'],$result);//提取
		//1:类型 2：场景 3：号码
		for($i=0;$i<count($result[1]); $i++){
			$hma = explode("|",$result[3][$i]);
			for($q=0;$q<=count($hma); $q++){
				if(strstr(@$hma[$q],":")){
					$hmb[$q] = explode(":",$hma[$q]);$hma[$q]=$hmb[$q][0];
				}
				if(@$hma[$q]!=''){
					if(@$hmstr[$i]!=''){
						$hmstr[$i].=',';
					}
					@$hmstr[$i].=$hma[$q];
				}
			}
			$type = Db::name('type')->where(['type'=>'deal','name1'=>$result[2][$i],'admin'=>2])->find();
			if(!$type){
			   $typename[$i]='<strong>全局</strong>';
			}else{
			   $typename[$i]=$type['name2'];
			}
			
			$data1['id'] = $result[2][$i];
			$data1['good'] = $result[1][$i];
			$data1['typename'] = $typename[$i];
			$data1['hmstr'] = $hmstr[$i];
			$service[] = $data1;
		}


		$this->assign('service',$service);
		return $this->fetch();
	}
	
	// 导航设置
	public function nav()
	{
		$data = Db::name('sell_nav')->where('ubh',session('usercode'))->select();
		$this->assign('data',$data);		
		return $this->fetch();
	}	
	
	// 服务标签
	public function skill()
	{
		$data = Db::name('sell')->where('bh',session('usercode'))->find();		
		$data['slideshow'] = explode(',',$data['skill']);
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	// 店铺推荐设置
	public function commend()
	{
		$list['code']  = Db::name('code')->where(['zt'=>1,'tj'=>1,'ubh'=>session('usercode')])->select();		
		$list['serve'] = Db::name('serve')->where(['zt'=>1,'tj'=>1,'ubh'=>session('usercode')])->select();;
		$this->assign('list',$list);
		return $this->fetch();
	}	
	
	// 店铺风格
	public function style()
	{
		$post = input('post.');
		$data = Db::name('sell')->where('bh',session('usercode'))->find();
		if(isset($post['bg_type'])) {
			if(isset($post['nobanner'])) {
				$bannar = '';
			}
			$upload = upload('file');
			if($upload['code'] == 'SUCCESS') {
				$bannar = $upload['site_url'];
			}			
			Db::name('sell')->where('bh',session('usercode'))->update([
				'zhuti'          => $post['zhuti'],	
				'bg_type'        => $post['bg_type'],
				'bg_color_input' => $post['bg_color_input'],
				'bannar'         => (isset($bannar))?$bannar:$data['bannar']
			]);	
			return redirect('/member/prompt/setup/style')->remember();	
		}	
		
		$this->assign('data',$data);		
		return $this->fetch();
	}	
	
	// 店铺幻灯片
	public function slide()
	{
		$data = input('post.data');
		// 上传图片
		$upload = upload('file');
		if($data == 'add') {
			Db::name('sell_slide')->insert([
				'sj'   => sj(),
				'uip'  => uip(),
				'ubh'  => session('usercode'),
				'link' => input('post.link'),
				'file' => ($upload['code']=='SUCCESS')? $upload['site_url']:null,
				'zt'   => 1
			]);
			return $this->success('幻灯片新增成功');
			//<body>
			//<script language="javascript" src="//statics.huzhan.com/js/jquery.m.js"></script>
			//<script language="javascript" src="//statics.huzhan.com/js/newlayer/layer.js"></script>
			//<script>
			//document.domain = 'huzhan.com';
			//layer.closeAll();
			//layer.confirm('<font style="font: 14px Microsoft YaHei,Tahoma,Arial,Helvetica,sans-serif;"><b>设置成功</b></font>', {icon:1, btn:'确定', end:function(index){window.top.location.reload();window.close();layer.close(index);}});</script></body>		
		}
		
		if($data == 'up') {
			$data = input('post.data');
			$id = input('post.id');
			$row = Db::name('sell_slide')->where('id',$id)->find();
			if($row) {
				Db::name('sell_slide')->where('id',$id)
				->update([
					'link' => input('post.link'),
					'file' => ($upload['code']=='SUCCESS')? $upload['site_url']:$row['file'],
				]);
				return $this->success('幻灯片修改成功');	
			}else{
				return $this->success('错误信息');
			}
		}
		// 幻灯列表
		$data = Db::name('sell_slide')->where('ubh',session('usercode'))->select();
		$this->assign('data',$data);
		return $this->fetch();
	}	
	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;
use think\Loader;

class Info extends BaseUser
{
	// 消息详情
	public function message()
	{
		
		//$this->assign('flow',$flow);
		return $this->fetch();
	}
	
	// 不良记录详情
	public function bad()
	{
		
		return $this->fetch();
	}
	
	// 推广记录
	public function promotion()
	{
		
		return $this->fetch();
	}
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use think\Db;

class Myorder extends BaseUser
{
	public function index($ddbh)
	{	
		$user   = Db::name('user')->where('bh',session('usercode'))->find();
		$row    = null;
		$order  = Db::name('down')->where(['ddbh'=>$ddbh,'buy|sell'=>session('usercode')])->find();
		$goods  = Db::name($order['type'])->where('bh',$order['codebh'])->find();
		if(!$order){
			 $this->error('非法操作！','/member/');
		}
		
		
		// 新能源
		$moneydb   = Db::name('moneydb')->where('ddbh',$order['ddbh'])->find();
		$moneyback = Db::name('moneyback')->where('ddbh',$order['ddbh'])->find();
		
		
		$this->assign('moneydb',$moneydb);		
		$this->assign('moneyback',$moneyback);
		// 新能源
		
		
		$zbtn     = null;
		$ybtn     = null;
		$zhuzt    = null;
		$oktxt    = null;
		// 延长时间
		$ycsj     = null;
		$cyctisp  = null;
		$jf       = null;
		$az       = null;
		$zjtisp   = null;
		$zbtnsell = null;
		$minfo    = null;

		$btn['sell']  = null;
		$flow['jyts'] = null;
		
		$tisp['sell'] = null;
		$tisp['buy']  = null;
		
		//$contact = null;
		
		// 自助的
		$tisp['fq'] = '';
		$flow['fq'] = '';
		$flow['fk'] = '';
		$flow['jy'] = '';
		$flow['ok'] = '';
		$tasktisp   = '';
		$zjtips     = '';
		$moneytisp  = '';
		$oktxt      = '';
		$notxt      = '';
		$iff        = '';
		$btn['buy'] = '';
		$btn['js']  = '';
		$btn['fq']  = '';
		$flow['hy'] = '';
		
		
		if($order['buy'] == session('usercode')) {
			// 我是买家
			$myRole = 'buy';
			$dfRole = 'sell';
			$tstr= '我购买的商品';
		} else {
			// 我是卖家
			$myRole = 'sell';
			$dfRole = 'buy';
			$tstr = '我出售的商品';
		}
		$zt = $order['ddzt'];
		// 交易状态
		$bfees = fees($order['money2'],$order['fees'],'buy');
		$sfees = fees($order['money2'],$order['fees'],'sell');
		// 获取交易主题
		if($order['type']=='task' || $order['type']=='custom' || $order['type']=='serve') {
			// 若不是源码则获取交易内容
			$row = Db::name('custom')->where(['ddbh'=>$order['ddbh']])->find();
			if(!$row){
				$this->success('若不是源码则获取交易内容');
			}
			if($order['type'] == 'task') {
				// 判断任务交易时间和金额是否一致
				if($zt=='waiting' || $zt=='cfing' || $zt=='fking') {					
					$task = Db::name('task')->where('bh',$order['codebh'])->find();
					// 获取任务
					$taskmoney = $task['money'];
					$taskcyc = $task['cyc'];
					$task['tisp_money'] = "任务指定金额";
					$task['tisp_cyc'] = "任务指定周期";
					if($task['moneytype']!=1 || $task['cyc']=='offer') {
						// 若有取自中标则执行
						$bid = Db::name('bid')->where('bh',$task['bidbh'])->find();
						// 获取任务
						if($task['moneytype']==1) {
							//判断是否要报价
							$taskmoney = $bid['money'];
							$task['tisp_money'] = "中标金额";
						}
						if($task['cyc']=='offer') {
							//判断是否要报时
							$taskcyc = $bid['cyc'];
							$task['tisp_cyc'] = "中标周期";
						}
					}
					if($taskmoney!=$row['money']) {
						$task_tisp[0] = "交易金额【".$row['money']."元】与".$task['tisp_money']."【".$taskmoney."元】";
						$task_tisp[1] = $task['tisp_money'];
						$task_tisp[2] = "金额";
					}
					if($taskcyc!=$row['cyc']) {
						if($task_tisp[0]!='') {
							foreach($task_tisp as $key=> $val) {
								$task_tisp[$key]=$val."、";
							}
						}
						$task_tisp[0].="交易周期【".$row['cyc']."天】与".$task['tisp_cyc']."【".$taskcyc."天】";
						$task_tisp[1].=$task['tisp_cyc'];
						$task_tisp[2].="周期";
					}
					if($task_tisp[0]!='') {
						$tasktisp="<p><strong>提醒：</strong>系统检测到实际".$task_tisp[0]."不一致，待雇主付款交易后，".$task_tisp[1]."将同步更改为实际交易".$task_tisp[2]."。</p>";
					}
				}
			}
		}
		$type = Db::name('type')->where(['name1'=>$order['type'],'type'=>'deal'])->find();
		// 查询类型
		$tit_links = "href='http://".$order['type'].".test.com/".$order['mid']."/' target='_blank'";
		if($order['type']=='code') {
			// 若是源码
			$tit_links = c_url($order['mid'],$order['type']);
		} elseif($order['type']=='custom' or $order['type']=='serve') {
			// 若是自助
			$tit_links = '';
		}
		////////////////////////////////////////////////////////
		// 退款时间		
		$rowtk = Db::name('moneyback')->where(['ddbh'=>$ddbh,'type'=>$order['type']])->find();		

		$active_back = "<input class='layui-btn layui-btn-primary order_handle' type='button' value='主动退款' data='active_back'>";
		$delivery = "<input class='layui-btn layui-btn-danger order_handle' type='button' value='我已发货' data='delivery'>";
		// 消保赔付
		$claims = "<input class=\"layui-btn layui-btn-primary order_info\" action=\"claims\" value=\"消保赔付详情\" type=\"button\">";
		// 退款历史记录
		$back_history = "<button class='layui-btn layui-btn-primary order_handle' data='back_history' w='1000'><i class='layui-icon'>&#xe615;</i> 查看退款历史记录";
		$dfinfo = explode("||",user($order[$dfRole],'cname|qq|phone',$dfRole,$order['type']));
		$qq[$dfRole] = $dfinfo[1];
		$tel[$dfRole] = $dfinfo[2];
		$name[$myRole] = "<font color='#247fbd'>您</font>";
		$name[$dfRole] = "<font color='#ff0000'>对方</font>";
		//$name[$dfRole] = "<font color='#ff0000'>".$dfinfo[0]."</font>";//带名称的
		$myname[$dfRole] = $dfinfo[0];
		$tel[$myRole] = $user['phone'];

		if($user['uc'] == 'buy') {
			// 买家
		} else {
			// 卖家
		}
		// 联系方式
		$myinfo = explode("||",user($order[$myRole],'cname|qq',$myRole,$order['type']));
		$myname[$myRole] = $myinfo[0];
		$qq[$myRole] = $myinfo[1];		
		$contact['buy'] = contact($order['buy'],"buy",$order['type'],"p","span");
		$contact['sell'] = contact($order['sell'],"sell",$order['type'],"p","span");		
		// 联系方式	
		if($row['fqbh']==session('usercode')) {
			// 生成发起人和接受人
			$fqr = $name[$myRole];
			$jsr = $name[$dfRole];
			$iff = 'fq';
		} else {
			$fqr = $name[$dfRole];
			$jsr = $name[$myRole];
			$iff = 'js';
		}
		$rowtxt = $row['txt'];
		// 主交易内容
		$flow['endts']="交易结束";
		// 默认结束语
		$delbtn="<input class='btn nobtn' type='button' value='删除交易' onclick=\"dirsc('<strong>确定要<font color=#ff0000>删除交易</font>吗?</strong><br>删除后不可恢复，慎重操作','del')\"/>";
		$dellink="<a onclick=\"dirsc('<strong>确定要<font color=#ff0000>删除交易</font>吗?</strong><br>删除后不可恢复，慎重操作','del')\" >删除</a>";

		// 修改订单交易
		if($order['type']=="serve"){
			$xiugai = "<input class=\"layui-btn layui-btn-primary serve_btn\" type=\"button\" number=\"{$ddbh}\" value=\"修改订单\">";	
		}elseif($order['type']=="task"){
			$xiugai = "<input class='layui-btn layui-btn-primary' type='button' value='修改交易' onclick=\"location.href='/member/task/{$row['bh']}'\">";
		}elseif($order['type']=="custom"){
			$xiugai = "<input class='layui-btn layui-btn-primary' type='button' value='修改交易' onclick=\"location.href='/member/custom/{$ddbh}'\">";
		}
		// 延长担保时间 任务
		if($order['ifyc']!="1"){
			$delay = "<input class='layui-btn layui-btn-primary order_handle' type='button' value='延长担保时间' data='delay'>";
		}	
		switch($zt) {
			//第一阶段：开始交易
			case 'wait': //等待发货
			case 'waiting': //发起交易
			case 'cfing': //重发交易
			$tel[$dfRole] = '同意交易后显示';
			$flow['hy'] = "class='cur'";
			$flow['hyts'] = "[等待中]";
			$tisp['js'] = "：<a onclick=\"alert_secus('cz','ok',this,'同意交易')\" target='_blank'>同意</a> 或 <a onclick=\"alert_secus('cz','no',this,'拒绝交易')\" target='_blank'>拒绝</a>";
			if($zt=='waiting') {
				$tisp="$fqr 于【<span>$order[sj]</span>】向 $jsr 发起了交易请求，等待 $jsr 的操作回应$tisp[$iff]。";
			} elseif($zt=='cfing') {
				$tisp="$fqr 于【<span>$row[utime]</span>】重新发起交易请求，等待 $jsr 的再次操作回应$tisp[$iff]。";
			}
			//判断是否源码集市手动发货
			if($order['type']=="code" or $zt=="wait"){
				$tisp = "{$name['buy']} 已付款，请 {$name['sell']} 尽快发货。剩余交易（发货）时间：<span class='ttime' endTime='{$rowtk['oksj']}'></span>。<p><strong>提醒：</strong>{$name['sell']} 需在交易时间内发货，若交易时间结束后，{$name['sell']} 依然没发货，系统自动执行退款于 {$name['buy']} (买方)！</p>";
				$btn['buy'] = $zbtn;
			}//
			//自主交易
			//$btn[fq]="<input class='btn' type='button' value='修改交易11' onclick=\"location.href='/member/".$order[type]."/".$ddbh."'\">";
			//修改交易 服务修改 有问题再继续开发
			$btn['fq']=$xiugai;
			if($order['type']=='task' && $order['type']=='serve' && $myRole=='buy') {
				//若是任务且是雇主
				$btn['fq'].=$delbtn;
			}
			//$btn[js]="<input class='btn' style='width:150px;' type='button' value='同意交易' onclick=\"alert_secus('cz','ok',this)\"/><input class='btn nobtn' style='width:150px;' type='button' value='拒绝交易' onclick=\"alert_secus('cz','no',this)\"/>";
			//同意交易或者拒绝交易
			$btn['js']='<input class="layui-btn layui-btn-danger order_handle" type="button" value="同意交易" data="agree_deal"><input class="layui-btn layui-btn-primary order_handle" data="refuse_deal" type="button" value="拒绝交易">';
			//判断是否源码集市手动发货  操作
			if($order['type']=="code" or $zt=="wait"){
				$btn['sell']=$delivery.$active_back;				
			}	
			break;
			case 'noing': //拒绝交易
			case 'qxing': //取消交易
			case 'close': //永久拒绝交易
			$flow['hy'] = "class='cur'";
			$tisp['fq'] = "可以对交易进行 <a href=\"custom".$myRole."_".$ddbh.".html\" >修改</a>、<a onclick=\"dirsc('<strong>确定要<font color=#ff4400>重发交易请求</font></strong>给对方吗?<br>重发后对方可对交易再做出回应操作','cf')\" >重发</a> 或 $dellink 操作";
			$tisp['js'] = "可修改、重发或删除交易";
			//$btn[fq]="<input class='btn' type='button' value='修改交易' onclick=\"location.href='deal_".$order[type]."_up_".$ddbh.".html'\"><input class='btn nobtn' type='button' value='重新发送' onclick=\"dirsc('<strong>确定要<font color=#ff4400>重发交易请求</font></strong>给对方吗?<br>重发后对方可对交易再做出回应操作','cf')\"/>".$delbtn;
			//不同意交易
			if($zt=="qxing"){
				//关闭交易
				$guanbi = "<input class='layui-btn layui-btn-primary' type='button' value='关闭交易' onclick=\"dconfirm('<strong>确定要<font color=#ff4400>关闭交易</font></strong>吗?<br>如果确定双方不再交易可操作此项；<br>操作后此交易将完全关闭不可继续；<br>且双方均可对此交易进行删除操作。','action=close&number={$ddbh}&role='+readmeta('Or-Role'))\"/>";	
			}	
			$btn['fq'] = $guanbi."<input class='layui-btn layui-btn-primary serve_btn' type='button' number='{$ddbh}' value='修改订单'><input class='layui-btn layui-btn-primary' type='button' style='width:130px;' value='重发交易请求' onclick=\"dconfirm('<strong>确定要<font color=#ff4400>重发交易请求</font></strong>给对方吗?<br>重发后对方可对交易再做出回应操作','action=resend&number={$ddbh}&role='+readmeta('Or-Role'))\"/><input class='layui-btn layui-btn-primary' type='button' value='删除交易' onclick=\"dconfirm('<strong>确定要<font color=#ff4400>删除交易</font></strong>吗?<br>删除后不可恢复，慎重操作','action=delete&number={$ddbh}&role='+readmeta('Or-Role'))\"/";
			if($zt=='noing') {
				$tel[$dfRole] = '同意交易后显示';
				$flow['hyts'] = "[已拒绝]";
				$tisp = "{$jsr} 于【<span>{$row['htime']}</span>】拒绝了 {$fqr} 发起的交易，{$fqr} {$tisp[$iff]}。<p><b>拒绝原因：</b><font color=blue>{$row['sm']}</font></p>";
				$btn['js']="<input class='btn nobtn' style='width:200px;' type='button' value='永久拒绝此交易' onclick=\"dirsc('<strong>确定要<font color=#ff4400>永久拒绝此交易</font>吗?</strong><br>此选项主要是预防骚扰性交易请求；<br >否则可让对方修改交易后重发请求；<br >操作后此交易将完全关闭不可继续；<br >且双方均可对此交易进行删除操作。','close')\"/>
		";
			} elseif($zt=='qxing') {
				$flow['hyts']="[已取消]";
				$tisp = "{$name['buy']} 于【<span>{$row['htime']}</span>】取消了交易，{$fqr} {$tisp[$iff]}。<p><b>取消原因：</b><font color=blue>{$row['sm']}</font></p>";
			} else {
				$flow['hyts'] = "[永久拒绝]";
				$tisp="{$jsr} 于【<span>{$row[htime]}</span>】永久拒绝了此交易，双方只能对交易进行 {$dellink} 处理。<p><font color=blue>建议双方先商定好交易细节且达成共识后再 <a href=\"secured.html\" target='_blank'>新建</a> 交易</font></p>";
				$btn['fq']=$delbtn;
				$btn['js']=$delbtn;
			}
			break;
			case 'fking': //等待付款
			$flow['fk']="class='cur'";
			$flow['fkts']="[等待中]";
			$tisp['buy']=" <a href='/member/carpay.html?bh=$row[bh]' style='color:#ff6600;' target='_blank'>点此支付款项</a>";
			$tisp="{$jsr} 于【<span>{$row['htime']}</span>】同意了 {$fqr} 发起的交易，等待 {$name['buy']} 支付交易款【".($order['money2']+fees($order['money2'],$order['fees'],'buy'))."元】".$tisp[$myRole];
			//$btn[buy]="<a class=\"layui-btn layui-btn-danger\" style=\"color:#fff;\" href=\"/member/carpay.html?bh={$row[bh]}\" target=\"_blank\">支付款项</a><input class=\"layui-btn layui-btn-primary order_handle\" type=\"button\" value=\"取消交易\" data=\"cancel\">";
			$btn['buy']="<a class='layui-btn layui-btn-danger'  style='color:#fff;'  href='/member/cashier/{$ddbh}' target='_blank' />支付款项</a><input class='layui-btn layui-btn-primary order_handle' type='button' value='取消交易' data='cancel'><input class='layui-btn layui-btn-primary' type='button' style='width:130px;' value='重发交易请求' onclick=\"dconfirm('<strong>确定要<font color=#ff4400>重发交易请求</font></strong>给对方吗?<br>重发后对方可对交易再做出回应操作','action=resend&number={$ddbh}&role='+readmeta('Or-Role'))\"/>";
			break;
			default://第二阶段：开始交易
			if($order['type']!='code') {
				//若是自助交易判断追加
				$zjmoney=0;
				$zjcyc=0;
				$zjsuc=0;
				$zjtxt=1;
				$zhuzt=0;
				$zbtn=$zbtn;
				$sqlzj="select * from shop_custom where ddbh='".$ddbh."' and txttype!='main' order by id asc";
				$reszj=mysql_query($sqlzj);
				while($rowzj=mysql_fetch_array($reszj)) {
					if($rowzj[zt]=='suc') {
						$zjmoney=$zjmoney+$rowzj[money];
						//计算追加金额
						$zjcyc=$zjcyc+$rowzj[cyc];
						//追加时间
						if($rowzj[txttype]!='') {
							if($rowzj[txttype]=='bcx') {
								$txttype='补充';
							} elseif($rowzj[txttype]=='fgx') {
								if($zhuzt==0) {
									//生成主交易内容
									$zhuzt=1;
								}
								$txttype='覆盖';
							}
							$rowtxt="<div class=secu_txt><div class='s_tit'><b>+</b><strong>主内容</strong><span>时间：$row[ptime]</span><span class='txt_zt' id='".$zhuzt."'></span></div><div class='s_txt'>$row[txt]</div></div>";
							$s_txt="<div class=secu_txt><div class='s_tit'><b>+</b><strong>追加内容$zjtxt($txttype)</strong><span>时间：$rowzj[ptime]</span><span class='txt_zt' id='$rowzj[txtzt]'></span></div><div class='s_txt'>$rowzj[txt]</div></div>";
							//读取追加内容
							if($rowzj[type]>0) {
								$notxt=$s_txt.$notxt;
							} else {
								$oktxt=$s_txt.$oktxt;
							}
							$zjtxt++;
						}
					} else {
						$zbtn="<input  style='width:180px;' class='btn' type='button' value='查看操作【追加交易】' onclick=\"alert_secus('zj','ckzj',this,'管理追加交易')\"/>";
						$zjtisp="<p class='blue'>当前有正在进行中的追加交易：<a onclick=\"alert_secus('zj','ckzj',this,'管理追加交易')\">查看操作</a></p>";
					}
					$zjsuc++;
					$zjtips="<input class='hiszj' type='button' value='追加交易".$zjsuc."次' onclick=\"alert_secus('zj','list',this,'追加记录')\">";
				}
				if($zjmoney>0) {
					$moneytisp="<span>【主交易 $row[money] 元,追加 $zjmoney 元】</span>";
				}
				if($zjcyc>0) {
					$cyctisp="<span>【主交易 $row[cyc] 天,追加 $zjcyc 天】</span>";
				}
			} elseif($order['type']=='code') {
				if($order['ifyc']==0) {
					$row['cyc']=1;
					//源码交易默认1天
					$ybtn=$delay;
				} else {
					//若买家延长则执行
					$row['cyc']=1+$ycsj;
					//延迟担保时间显示
					//$cyctisp="<span>【默认 1 天,买家延长 ".$ycsj." 天】</span>";
					$cyctisp="<input class=\"order_handle\" type=\"button\" value=\"交易时间延长详情\" data=\"delay_info\">";
				}
				
			}	
			switch($zt) {
				case 'db': //正在交易
				$flow['jy'] = "class='cur'";
				$flow['jyts'] = "正在交易";
				$rowdb = Db::name('moneydb')->where(['ddbh'=>$ddbh,'type'=>$order['type']])->find();
				if($rowdb){
					$tisp = '';
					//$tisp = "买方已付款，双方开始交易。交易自动完结时间：<span class='ttime' endTime='$rowdb[oksj]'></span>。".$zjtisp."<p><strong>提醒：</strong>交易期间尽量注意交易剩余时间。若临近交易结束前，依然存在商品问题卖方未予解决时，可先【延长担保时间】或【申请退款】，待卖方解决问题后再【取消退款（确认收货）】，不可轻信卖方许诺，以防有意拖延时间至交易自动完成。</p>";
				}
				$btn['buy']   = '';
				//$btn['buy'] = "<input class='layui-btn layui-btn-danger order_handle' type='button' value='确认收货' data='confirm'><input class='layui-btn layui-btn-primary order_handle' type='button' value='申请退款' data='apply_back'>".$zbtn.$ybtn;
				$btn['sell'] = $zbtn.$zbtnsell;
				break;
				case 'back': //买方申请退款
				case 'backerr': //卖方拒绝退款
				$flow['jy'] = "class='cur'";
				$rowtk = Db::name('moneyback')->where(['ddbh'=>$ddbh,'type'=>$order['type']])->find();
				if($zt=='back') {
					$flow['jyts']="申请退款";
					$tisp = '';
					//$tisp="{$name['buy']} 于【<span>{$rowtk['sj']}</span>】申请了退款，等待 {$name['sell']} 回应处理中；<p>退款原因：<font color=\"blue\">{$rowtk['tkly']}</font>；</p><p>{$name['sell']} 在【<span class='ttime' endTime='{$rowtk['oksj']}'></span>】内若未做出回应处理，系统自动执行退款于 {$name['buy']} (买方)。</p>".$zjtisp;
					//同意或者拒绝退款
					//$btn['sell']="<input class='layui-btn layui-btn-danger order_handle' type='button'  value='同意退款'  data='agree_back'><input class='layui-btn layui-btn-primary order_handle' type='button'  value='拒绝退款' data='refuse_back'>";
				} else {		
					//拒绝退款
					$flow['jyts']="拒绝退款";
					//$site_info_qq = sysconf('site_info_qq');
					//$tisp['buy']="<p>若 {$name['buy']} 确定是卖家的责任，对方又拒绝您的退款，请联系商业源码【官网】客服QQ:{$site_info_qq}、QQ:{$site_info_qq} 进行人工申诉;</font></p>";
					//$qxqr['sell']="确认收货";
					//$qxqr['buy']="<a onclick=\"alert_secus('cz','qr',this,'取消退款（确认收货）')\"/>确认收货</a>";
					//$oktk['sell']="<a onclick=\"alert_secus('cz','oktk',this,'重新处理退款(同意退款)')\"/>同意退款</a>";
					//$oktk['buy']="同意退款";
					//$tisp = "{$name['sell']} 于 【<span>{$rowtk['jjsj']}</span>】 拒绝了 {$name['buy']} 的退款申请，该笔退款已进入客服人工处理阶段；<p>拒绝原因：<font color=\"blue\">{$rowtk['cljg']}</font>；</p><p>退款原因：<font color=\"red\">{$rowtk['tkly']}</font>；</p>".$tisp[$myRole]."<font color=red><p>若 <strong>{$name['buy']}</strong> 在【<span class='ttime' endTime='{$rowtk['oksj']}'></span>】内未申诉或申诉理由不成立，交易自动完成，款项将转入 <strong>{$name['sell']}</strong> 的账户。</p></font><p>此期间，双方若自行协调解决好，{$name['buy']} 可操作【".$qxqr[$myRole]."】或 {$name['sell']} 操作【".$oktk[$myRole]."】来继续完结交易。</p>".$zjtisp.$jiufen;
					//同意退款补充理由
					//$btn['sell']="<input class='layui-btn layui-btn-danger order_handle' type='button'  value='重新处理退款（同意退款）'  data='afresh_agree_back'><input class='layui-btn layui-btn-primary order_handle' type='button' value='补充拒绝原因' data='refuse_back'>";
					$tisp = '';
				}
				$btn['sell']=$btn['sell'].$zbtn;
				//$btn['buy']="<input class=\"layui-btn layui-btn-danger order_handle\" type=\"button\" value=\"确认收货（取消退款）\" data=\"afresh_confirm\"><input class=\"layui-btn layui-btn-primary order_handle\" type=\"button\" value=\"补充退款原因\" data=\"apply_back\">".$zbtn.$service;
				$btn['buy'] = '';
				break;
				default://第三阶段：交易结束
				$flow['ok']="class='cur'";
				$flow['jyts'] = "已结束";
				if($zt=='success') {
					//强制退款
					$flow['endts']="[交易成功]";
					$tisp="交易成功，交易款项已转入卖方账户。";
				} elseif($zt=='backsuc') {
					//交易成功
					$flow['endts']="同意退款";
					//$tisp= "{$name['sell']} 于 【<span>$order[etime]</span>】同意了 $name[buy] 的退款申请，交易结束，交易款项已退还于 $name[buy] 。";
					$tisp= '';
				} elseif($zt=='forback') {
					//退款成功
					$forinfo=explode("|",$order[adminbz]);
					$flow['endts']="[强制退款]";
					$tisp="在收到买方申诉后，商业源码于 【<span> ".$forinfo[0]." </span>】对该笔交易纠纷做出了评判处理，处理结果：强制退款。<br /><b>处理原因：</b><font color=\"red\"> ".$forinfo[1]." </font>";
				}
				//$pj="<input class='layui-btn layui-btn-primary' type='button' value='交易评价' onclick=\"location.href='/member/evaluation/{$ddbh}'\">";
				//$oshare="<input class='layui-btn layui-btn-primary order_handle' type='button' value='订单分享' data='oshare'>";
				$btn['buy'] = '';
				break;
				break;
			}
			break;
		}
		if($zhuzt==1) {
			$notxt=$rowtxt.$notxt;
		} else {
			$oktxt=$rowtxt.$oktxt;
		}
		// 支付手续费
		if($order['fees']=='buy'){
			$fees['name'] = '买家支付';
		}elseif($order['fees']=='sell'){
			$fees['name'] = '卖家支付';
		}else{
			$fees['name'] = '双方平摊';
		}
		$fees['money'] = $order['money1']-fees($order['money1'],$order['fees'],'sell');
		///////////////////////////////////////////////////////
		$this->assign('fees',$fees);
		$this->assign('ddbh',$ddbh);
		$this->assign('flow',$flow);
		$this->assign('order',$order);
		$this->assign('tisp',$tisp);
		$this->assign('tit_links',$tit_links);
		$this->assign('type',$type);
		$this->assign('myRole',$myRole);
		$this->assign('myname',$myname);
		$this->assign('cyctisp',$cyctisp);	
		$this->assign('contact',$contact);
		$this->assign('jf',$jf);
		$this->assign('az',$az);
		$this->assign('btn',$btn);
		$this->assign('goods',$goods);
		$this->assign('row',$row);
		
		$this->assign('name',$name);
		
		$this->assign('tasktisp',$tasktisp);
		$this->assign('zjtips',$zjtips);
		$this->assign('moneytisp',$moneytisp);
		$this->assign('oktxt',$oktxt);
		$this->assign('notxt',$notxt);
		$this->assign('iff',$iff);
		
		
		// 纠纷记录
		$dispute = Db::name('order_dispute')->where('ddbh',$ddbh)->select();
		$this->assign('dispute',$dispute);
		return $this->fetch();
	}
	
	
	
}
?>
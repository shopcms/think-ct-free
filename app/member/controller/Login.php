<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;



class Login extends BaseUser
{
	public function _initialize()
	{
		if(session('?usercode')) {
			$this->success('已经登录，请勿重复操作！', '/member/');
		}	
	}
	
    public function index()
    {
		$backurl = input('backurl','/member/');
		
		$this->assign('backurl',$backurl);
        return $this->fetch();
    }
}












?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\member\controller;


use thinkct\controller\BaseUser;

use app\member\model\Code as CodeModel;
use think\Db;
use think\Loader;

class Form extends BaseUser
{	
	// 余额提现
    public function cashed()
    {	
		$action        = input('post.action');	
		$user          = Db::name('user')->where('bh',session('usercode'))->find();
		$certification = Db::name('user_certification')->where(['ubh'=>session('usercode'),'zt'=>1])->select();
		// 提交
		if($action == 'add'){
			$bh       = input('post.bh');
			$bankid   = input('post.bankid');
			$money    = input('post.money');
			$truename = input('post.truename');
			$type     = input('post.type');
			$jz       = input('post.jz');
			// 验证条件
			if(sysconf('cash_verify')==2){
				if($user['idcard']==0){
					$this->error('请先完成实名认证，在执行此操作','/member/certification/idcard');
				}
			}			
			if(sysconf('cash_verify')==3&&sysconf('auto_cash_money')>$money){
				if($user['idcard']==0){
					$this->error('请先完成实名认证，在执行此操作','/member/certification/idcard');
				}
			}
			
			if($money > $user['money']){
				$this->error('你的可提现金额低于提现金额','/member/form/cashed');
			}
			// 您已有提现正在处理中，若要再次提现请先撤销之前提现再申请
			// 您已有提现正在处理中，若要再次提现请先撤销之前提现再申请		
			$fee = get_cash_fee($money);
			Db::name('cashed')->insert([
				'bh'           => $bh,
				'ubh'          => session('usercode'),
				'type'         => $type,
				'truename'     => $truename,
				'bankid'       => $bankid,
				'money'        => $money,
				'fee'          => $fee,
				'sj'           => sj(),
				'actual_money' => round($money - $fee, 2),
				'zt' => 0,		
			]);
			PointUpdate(session('usercode'),$money*(-1),"提现申请");
			// 更新记住帐户
			if($jz == 1) {
				Db::name('user')->where('bh',session('usercode'))->update([
					'pay_acc' => $bankid
				]);
			}else{
				Db::name('user')->where('bh',session('usercode'))->update([
					'pay_acc' => null
				]);
			}
			return redirect('/member/prompt/form/cashed')->remember();
		}
		
		$this->assign('user',$user);
		$this->assign('certification',$certification);
        return $this->fetch();
    }

	// 发布源码
    public function code()
    {	
		// 判断是否为商家
		$user = Db::name('user')->where(['bh'=>session('usercode'),'uc'=>'sell'])->find();
		if($user['uc'] == 'buy'){
			return $this->error('请先认证商家','/member/account/turn');
		}
		
		//return $this->fetch('safe/index');// 安全码验证	
		$bh     = get_bh();
		$row    = CodeModel::get(['bh'=>$bh,'ubh'=>session('usercode')]);
		$action = input('action',($row)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}		
		// 获取分类
		$menus['type']    = get_type('code','源码类型');
		$menus['brand']   = get_type('code','系统品牌');
		$menus['lang']    = get_type('code','开发语言');
		$menus['sql']     = get_type('code','数据库');
		$menus['encrypt'] = get_type('code','源文件');
		$menus['auth']    = get_type('code','授权');
		$menus['spec']    = get_type('code','规格');
		$menus['app']     = get_type('code','移动端');
		$menus['azsxid1'] = get_type('code','主机类型');
		$menus['azsxid2'] = get_type('code','操作系统');
		$menus['azsxid3'] = get_type('code','web服务');
		$menus['azsxid4'] = get_type('code','安装方式');
		$menus['rewrite'] = get_type('code','伪静态');
		// 加载代码
		if(get_action($action)) {	
			$code = new CodeModel;
			return $code->$action($bh);			
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('row',$row);	
		$this->assign('menus',$menus);
		$this->assign('filelist',get_filelist($row['psort']));		
		$this->assign('action',$action);
        return $this->fetch();
    }	
	
	// 发布域名
    public function domain()
    {	
		// 判断是否为商家
		$user = Db::name('user')->where(['bh'=>session('usercode'),'uc'=>'sell'])->find();
		if($user['uc'] == 'buy'){
			return $this->error('请先认证商家','/member/account/turn');
		}	
		$bh     = get_bh('domain');
		$data   = Db::name('domain')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';		
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type']      = get_type('domain','域名类型');
		$menus['suffix']    = get_type('domain','域名后缀');
		$menus['registrar'] = get_type('domain','注册商');
		$menus['attribute'] = get_type('domain','附带属性');
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('domain')->$action($bh);			
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);		
		$this->assign('action',$action);
        return $this->fetch();
    }	
	
	// 发布网站
    public function web()
    {
		// 判断是否为商家
		$user = Db::name('user')->where(['bh'=>session('usercode'),'uc'=>'sell'])->find();
		if($user['uc'] == 'buy'){
			return $this->error('请先认证商家','/member/account/turn');
		}		
		$bh     = get_bh('web');
		$data   = Db::name('web')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';		
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type'] = get_type('web','类型');
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('web')->$action($bh);			
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);		
		$this->assign('action',$action);
		$this->assign('filelist',get_filelist($data['psort']));	
        return $this->fetch();
    }	
	
	//发布服务
    public function serve()
    {
		// 判断是否为商家
		$user = Db::name('user')->where(['bh'=>session('usercode'),'uc'=>'sell'])->find();
		if($user['uc'] == 'buy'){
			return $this->error('请先认证商家','/member/account/turn');
		}		
		$bh     = get_bh('serve');
		$data   = Db::name('serve')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';		
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type'] = get_type('serve','服务市场');
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('serve')->$action($bh);			
		}
		// 动态价格
		$moneydata = [];
		$moneydes  = explode(',',$data['moneydes']);
		foreach($moneydes as $v => $k){
			$allmoney = explode(',',$data['allmoney']);
			$mdata['all'] = $allmoney[$v];
			$mdata['des'] = $moneydes[$v];
			$moneydata[]  = $mdata;
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);
		$this->assign('filelist',get_filelist($data['psort']));		
		$this->assign('action',$action);
		$this->assign('moneydata',$moneydata);
        return $this->fetch();
    }	
	
	// 发布模板
	public function tem()
	{
		// 判断是否为商家
		$user = Db::name('user')->where(['bh'=>session('usercode'),'uc'=>'sell'])->find();
		if($user['uc'] == 'buy'){
			return $this->error('请先认证商家','/member/account/turn');
		}		
		// 安全码验证
		//if(!session('SAFEPWD') && $action == 'up'){
			//return $this->fetch('safe');
		//}
		
		return $this->fetch();
	}
	
	// 需求源码
	public function demand_code()
	{
		$bh     = get_bh('demand_code');
		$data   = Db::name('demand')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';	
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type'] = get_type('codebuy','源码类型');		
		
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('demand')->$action($bh,'code');			
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);	
		$this->assign('action',$action);		
		return $this->fetch();
	}
	
	// 需求域名
	public function demand_domain()
	{
		$bh     = get_bh('demand_domain');
		$data   = Db::name('demand')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';	
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type']   = get_type('domainbuy','求购类型');	
		$menus['suffix'] = get_type('domainbuy','求购后缀');
		
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('demand')->$action($bh,'code');			
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);	
		$this->assign('action',$action);	
		return $this->fetch();
	}
	
	// 需求网站
	public function demand_web()
	{
		$bh     = get_bh('demand_web');
		$data   = Db::name('demand')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';	
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type']   = get_type('webbuy','网站类型');	
		$menus['ip'] = get_type('webbuy','流量要求');
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('demand')->$action($bh,'code');			
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);	
		$this->assign('action',$action);	
		return $this->fetch();
	}

	// 需求任务
	public function task()
	{
		$bh     = get_bh('task');
		$data   = Db::name('task')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)? 'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';			 
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type']   = get_type('task','类型');		 
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('task')->$action($bh);			
		}		 
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);	
		$this->assign('action',$action);	
		return $this->fetch();
	}
	
	// 发布品牌
	public function brand()
	{
		// 判断是否为商家
		$user = Db::name('user')->where(['bh'=>session('usercode'),'uc'=>'sell'])->find();
		if($user['uc'] == 'buy'){
			return $this->error('请先认证商家','/member/account/turn');
		}		
		$bh     = get_bh('brand');
		$data   = Db::name('brand')->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
		$action = input('action',($data)?'up':'add');
		$tit    = ($action == 'add')?'发布':'编辑';		
		// 安全码验证
		if(!session('SAFEPWD') && $action == 'up'){
			return $this->fetch('safe');
		}
		// 获取分类
		$menus['type'] = get_type('brand','品牌分类');
		// 加载模型
		if(get_action($action)) {			
			return Loader::model('brand')->$action($bh);			
		}
		$this->assign('tit',$tit);
		$this->assign('bh',$bh);
		$this->assign('data',$data);	
		$this->assign('menus',$menus);
		$this->assign('filelist',get_filelist($data['psort']));		
		$this->assign('action',$action);
        return $this->fetch();
	}
}
?>
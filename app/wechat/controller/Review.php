<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php

namespace app\wechat\controller;

use service\FileService;
use service\WechatService;
use think\Controller;
use think\Db;

class Review extends Controller
{

    /**
     * 显示手机预览
     * @return string
     */
    public function index()
    {
        $get = $this->request->get();
        $content = str_replace("\n", "<br>", $this->request->get('content', '')); // 内容
        $type = $this->request->get('type', 'text'); // 类型
        $this->assign('type', $type);
        // 图文处理
        if ($type === 'news' && is_numeric($content) && !empty($content)) {
            $news = WechatService::getNewsById($content);
            $this->assign('articles', $news['articles']);
        }
        // 文章预览
        if ($type === 'article' && is_numeric($content) && !empty($content)) {
            $article = Db::name('WechatNewsArticle')->where('id', $content)->find();
            if (!empty($article['content_source_url'])) {
                $this->redirect($article['content_source_url']);
            }
            $this->assign('vo', $article);
        }
        $this->assign($get);
        $this->assign('content', $content);
        // 渲染模板并显示
        return view();
    }

    /**
     * 微信图片显示
     */
    public function img()
    {
        $url = $this->request->get('url', '');
        $filename = FileService::getFileName($url, 'jpg', 'tmp/');
        if (false === ($img = FileService::getFileUrl($filename))) {
			return '抱歉无法上传';
            //$info = FileService::save($filename, file123_get_contents($url));
            $img = (is_array($info) && isset($info['url'])) ? $info['url'] : $url;
        }
        $this->redirect($img);
    }

}

<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
use service\DataService;
use service\FileService;
use service\NodeService;
use think\Db;
use Wechat\Loader;

if (!function_exists('__')) {

    /**
     * 获取语言变量值
     * @param string $name 语言变量名
     * @param array $vars 动态变量值
     * @param string $lang 语言
     * @return mixed 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function __($name, $vars = [], $lang = '')
    {
        if (is_numeric($name) || !$name) {
            return $name;
        }
        if (!is_array($vars)) {
            $vars = func_get_args();
            array_shift($vars);
            $lang = '';
        }
        return \think\Lang::get($name, $vars, $lang);
    }

}

if (!function_exists('array2xml')) {

    /**
     * 数组转XML
     * @param array $arr 数据源
     * @param bool $ignore XML解析器忽略
     * @param int $level 层级
     * @return string|string[]|null 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function array2xml($arr, $ignore = true, $level = 1)
    {
        $s = $level == 1 ? "<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n<root>\r\n" : '';
        $space = str_repeat("\t", $level);
        foreach ($arr as $k => $v) {
            if (!is_array($v)) {
                $s .= $space . "<item id=\"$k\">" . ($ignore ? '<![CDATA[' : '') . $v . ($ignore ? ']]>' : '')
                    . "</item>\r\n";
            } else {
                $s .= $space . "<item id=\"$k\">\r\n" . array2xml($v, $ignore, $level + 1) . $space . "</item>\r\n";
            }
        }
        $s = preg_replace("/([\x01-\x08\x0b-\x0c\x0e-\x1f])+/", ' ', $s);
        return $level == 1 ? $s . "</root>" : $s;
    }

}

if (!function_exists('xml2array')) {

    /**
     * XML转数组
     * @param string $xml xml格式内容
     * @param bool $isnormal
     * @return array
     */
    /**
     * xml转数组
     * @param $xml xml文本内容
     * @return string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function xml2array(&$xml)
    {
        $xml = "<xml>";
        foreach ($xml as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

}

if (!function_exists('array_sort')) {

    /**
     * 二位数组排序
     * @param array $arr 数据源
     * @param string $keys KEY
     * @param bool $desc 排序方式（默认：asc）
     * @return array 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function array_sort($arr, $keys, $desc = false)
    {
        $key_value = $new_array = array();
        foreach ($arr as $k => $v) {
            $key_value[$k] = $v[$keys];
        }
        if ($desc) {
            arsort($key_value);
        } else {
            asort($key_value);
        }
        reset($key_value);
        foreach ($key_value as $k => $v) {
            $new_array[$k] = $arr[$k];
        }
        return $new_array;
    }
}

if (!function_exists('array_merge_multiple')) {

    /**
     * 多维数组合并
     * @param array $array1 数组1
     * @param array $array2 数组2
     * @return array 返回合并数组
     * @author 牧羊人
     * @date 2020-04-21
     */
    function array_merge_multiple($array1, $array2)
    {
        $merge = $array1 + $array2;
        $data = [];
        foreach ($merge as $key => $val) {
            if (isset($array1[$key])
                && is_array($array1[$key])
                && isset($array2[$key])
                && is_array($array2[$key])
            ) {
                $data[$key] = array_merge_multiple($array1[$key], $array2[$key]);
            } else {
                $data[$key] = isset($array2[$key]) ? $array2[$key] : $array1[$key];
            }
        }
        return $data;
    }
}

if (!function_exists('curl_url')) {

    /**
     * 获取当前访问的完整URL
     * @return string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function curl_url()
    {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] === 'on') {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

}

if (!function_exists('curl_get')) {

    /**
     * curl请求(GET)
     * @param string $url 请求地址
     * @param array $data 请求参数
     * @return bool|string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function curl_get($url, $data = [])
    {
        // 处理get数据
        if (!empty($data)) {
            $url = $url . '?' . http_build_query($data);
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
}

if (!function_exists('curl_post')) {

    /**
     * curl请求(POST)
     * @param string $url 请求地址
     * @param array $data 请求参数
     * @return bool|string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function curl_post($url, $data = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}

if (!function_exists('curl_request')) {

    /**
     * curl请求(支持get和post)
     * @param $url 请求地址
     * @param array $data 请求参数
     * @param string $type 请求类型(默认：post)
     * @param bool $https 是否https请求true或false
     * @return bool|string 返回请求结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function curl_request($url, $data = [], $type = 'post', $https = false)
    {
        // 初始化
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        // 设置超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        // 是否要求返回数据
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($https) {
            // 对认证证书来源的检查
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // 从证书中检查SSL加密算法是否存在
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        if (strtolower($type) == 'post') {
            // 设置post方式提交
            curl_setopt($ch, CURLOPT_POST, true);
            // 提交的数据
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } elseif (!empty($data) && is_array($data)) {
            // get网络请求
            $url = $url . '?' . http_build_query($data);
        }
        // 设置抓取的url
        curl_setopt($ch, CURLOPT_URL, $url);
        // 执行命令
        $result = curl_exec($ch);
        if ($result === false) {
            return false;
        }
        // 关闭URL请求(释放句柄)
        curl_close($ch);
        return $result;
    }
}

if (!function_exists('datetime')) {

    /**
     * 时间戳转日期格式
     * @param int $time 时间戳
     * @param string $format 转换格式(默认：Y-m-d h:i:s)
     * @return false|string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function datetime($time, $format = 'Y-m-d H:i:s')
    {
        if (empty($time)) {
            return '--';
        }
        $time = is_numeric($time) ? $time : strtotime($time);
        return date($format, $time);
    }

}

if (!function_exists('data_auth_sign')) {

    /**
     * 数字签名认证
     * @param $data 签名认证数据
     * @return string
     * @author 牧羊人
     * @date 2019/2/1
     */
    function data_auth_sign($data)
    {
        //数据类型检测
        if (!is_array($data)) {
            $data = (array)$data;
        }
        // 排序
        ksort($data);
        // url编码并生成query字符串
        $code = http_build_query($data);
        //生成签名
        $sign = sha1($code);
        return $sign;
    }
}

if (!function_exists('format_bytes')) {

    /**
     * 将字节转换为可读文本
     * @param int $size 字节大小
     * @param string $delimiter 分隔符
     * @return string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function format_bytes($size, $delimiter = '')
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        for ($i = 0; $size >= 1024 && $i < 6; $i++) {
            $size /= 1024;
        }
        return round($size, 2) . $delimiter . $units[$i];
    }

}

if (!function_exists('getter')) {

    /**
     * 获取数组的下标值
     * @param array $data 数据源
     * @param string $field 字段名称
     * @param string $default 默认值
     * @return mixed|string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function getter($data, $field, $default = '')
    {
        $result = $default;
        if (isset($data[$field])) {
            if (is_array($data[$field])) {
                $result = $data[$field];
            } else {
                $result = trim($data[$field]);
            }
        }
        return $result;
    }
}

if (!function_exists('get_random_str')) {

    /**
     * 生成随机字符串
     * @param int $length 生成长度
     * @param int $type 生成类型：0-小写字母+数字，1-小写字母，2-大写字母，3-数字，4-小写+大写字母，5-小写+大写+数字
     * @return string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function get_random_str($length = 8, $type = 0)
    {
        $a = 'abcdefghijklmnopqrstuvwxyz';
        $A = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $n = '0123456789';

        switch ($type) {
            case 1:
                $chars = $a;
                break;
            case 2:
                $chars = $A;
                break;
            case 3:
                $chars = $n;
                break;
            case 4:
                $chars = $a . $A;
                break;
            case 5:
                $chars = $a . $A . $n;
                break;
            default:
                $chars = $a . $n;
        }

        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $str;
    }

}

if (!function_exists('get_random_code')) {

    /**
     * 获取指定位数的随机码
     * @param int $num 随机码长度
     * @return string 返回字符串
     * @author 牧羊人
     * @date 2020-04-21
     */
    function get_random_code($num = 12)
    {
        $codeSeeds = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeSeeds .= "abcdefghijklmnopqrstuvwxyz";
        $codeSeeds .= "0123456789_";
        $len = strlen($codeSeeds);
        $code = "";
        for ($i = 0; $i < $num; $i++) {
            $rand = rand(0, $len - 1);
            $code .= $codeSeeds[$rand];
        }
        return $code;
    }
}

if (!function_exists('get_server_ip')) {

    /**
     * 获取服务端IP地址
     * @return string 返回IP地址
     * @author 牧羊人
     * @date 2020-04-21
     */
    function get_server_ip()
    {
        if (isset($_SERVER)) {
            if ($_SERVER['SERVER_ADDR']) {
                $server_ip = $_SERVER['SERVER_ADDR'];
            } else {
                $server_ip = $_SERVER['LOCAL_ADDR'];
            }
        } else {
            $server_ip = getenv('SERVER_ADDR');
        }
        return $server_ip;
    }
}

if (!function_exists('get_client_ip')) {

    /**
     * 获取客户端IP地址
     * @param int $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @param bool $adv 否进行高级模式获取（有可能被伪装）
     * @return mixed 返回IP
     * @author 牧羊人
     * @date 2020-04-21
     */
    function get_client_ip($type = 0, $adv = false)
    {
        $type = $type ? 1 : 0;
        static $ip = null;
        if ($ip !== null) {
            return $ip[$type];
        }
        if ($adv) {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $pos = array_search('unknown', $arr);
                if (false !== $pos) {
                    unset($arr[$pos]);
                }
                $ip = trim($arr[0]);
            } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u", ip2long($ip));
        $ip = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }

}

if (!function_exists('get_zodiac_sign')) {


    /**
     * 根据月、日获取星座
     *
     * @param unknown $month 月
     * @param unknown $day 日
     * @return boolean|multitype:
     * @author 牧羊人
     * @date 2020-04-21
     */
    function get_zodiac_sign($month, $day)
    {
        // 检查参数有效性
        if ($month < 1 || $month > 12 || $day < 1 || $day > 31) {
            return false;
        }

        // 星座名称以及开始日期
        $signs = array(
            array("20" => "水瓶座"),
            array("19" => "双鱼座"),
            array("21" => "白羊座"),
            array("20" => "金牛座"),
            array("21" => "双子座"),
            array("22" => "巨蟹座"),
            array("23" => "狮子座"),
            array("23" => "处女座"),
            array("23" => "天秤座"),
            array("24" => "天蝎座"),
            array("22" => "射手座"),
            array("22" => "摩羯座")
        );
        list($sign_start, $sign_name) = each($signs[(int)$month - 1]);
        if ($day < $sign_start) {
            list($sign_start, $sign_name) = each($signs[($month - 2 < 0) ? $month = 11 : $month -= 2]);
        }
        return $sign_name;
    }

}

if (!function_exists('get_format_time')) {

    /**
     * 获取格式化显示时间
     * @param int $time 时间戳
     * @return false|string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function get_format_time($time)
    {
        $time = (int)substr($time, 0, 10);
        $int = time() - $time;
        $str = '';
        if ($int <= 2) {
            $str = sprintf('刚刚', $int);
        } elseif ($int < 60) {
            $str = sprintf('%d秒前', $int);
        } elseif ($int < 3600) {
            $str = sprintf('%d分钟前', floor($int / 60));
        } elseif ($int < 86400) {
            $str = sprintf('%d小时前', floor($int / 3600));
        } elseif ($int < 1728000) {
            $str = sprintf('%d天前', floor($int / 86400));
        } else {
            $str = date('Y年m月d日', $time);
        }
        return $str;
    }

}

if (!function_exists('is_empty')) {

    /**
     * 判断是否为空
     * @param $value 参数值
     * @return bool 返回结果true或false
     * @author 牧羊人
     * @date 2020-04-21
     */
    function is_empty($value)
    {
        // 判断是否存在该值
        if (!isset($value)) {
            return true;
        }

        // 判断是否为empty
        if (empty($value)) {
            return true;
        }

        // 判断是否为null
        if ($value === null) {
            return true;
        }

        // 判断是否为空字符串
        if (trim($value) === '') {
            return true;
        }

        // 默认返回false
        return false;
    }
}

if (!function_exists('mkdirs')) {

    /**
     * 递归创建目录
     * @param string $dir 需要创建的目录路径
     * @param int $mode 权限值
     * @return bool 返回结果true或false
     * @author 牧羊人
     * @date 2020-04-21
     */
    function mkdirs($dir, $mode = 0777)
    {
        if (is_dir($dir) || mkdir($dir, $mode, true)) {
            return true;
        }
        if (!mkdirs(dirname($dir), $mode)) {
            return false;
        }
        return mkdir($dir, $mode, true);
    }
}

if (!function_exists('rmdirs')) {

    /**
     * 删除文件夹
     * @param string $dir 文件夹路径
     * @param bool $rmself 是否删除本身true或false
     * @return bool 返回删除结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function rmdirs($dir, $rmself = true)
    {
        if (!is_dir($dir)) {
            return false;
        }
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $file) {
            $todo = ($file->isDir() ? 'rmdir' : 'unlink');
            $todo($file->getRealPath());
        }
        if ($rmself) {
            @rmdir($dir);
        }

        return true;
    }
}

if (!function_exists('copydirs')) {

    /**
     * 复制文件夹
     * @param string $source 原文件夹路径
     * @param string $dest 目的文件夹路径
     * @author 牧羊人
     * @date 2020-04-21
     */
    function copydirs($source, $dest)
    {
        if (!is_dir($dest)) {
            mkdir($dest, 0755, true);
        }
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST
        );
        foreach ($iterator as $item) {
            if ($item->isDir()) {
                $sent_dir = $dest . "/" . $iterator->getSubPathName();
                if (!is_dir($sent_dir)) {
                    mkdir($sent_dir, 0755, true);
                }
            } else {
                copy($item, $dest . "/" . $iterator->getSubPathName());
            }
        }
    }
}

if (!function_exists('mbsubstr')) {
    /**
     * 字符串截取，支持中文和其他编码
     * @param string $str 需要转换的字符串
     * @param int $start 开始位置
     * @param int $length 截取长度
     * @param string $encoding 编码格式
     * @param string $suffix 截断显示字符
     * @return false|mixed|string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function mbsubstr($str, $start = 0, $length = null, $encoding = "utf-8", $suffix = '...')
    {
        if (function_exists("mb_substr")) {
            $slice = mb_substr($str, $start, $length, $encoding);
        } elseif (function_exists('iconv_substr')) {
            $slice = iconv_substr($str, $start, $length, $encoding);
            if (false === $slice) {
                $slice = '';
            }
        } else {
            $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
            $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
            $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
            $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
            preg_match_all($re[$encoding], $str, $match);
            $slice = join("", array_slice($match[0], $start, $length));
        }
        return $suffix ? $slice . $suffix : $slice;
    }
}

if (!function_exists('format_num')) {

    /**
     * 格式化阅读量等数字单位
     * @param $num
     * @return string
     * @author 牧羊人
     * @date 2020-04-21
     */
    function format_num($num)
    {
        if ($num >= 10000) {
            $num = round($num / 10000 * 100) / 100 . 'W';
        } elseif ($num >= 1000) {
            $num = round($num / 1000 * 100) / 100 . 'K';
        } else {
            $num = $num;
        }
        return $num;
    }
}

if (!function_exists('object_array')) {

    /**
     * 对象转数组
     * @param $object 对象
     * @return mixed 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function object_array($object)
    {
        //先编码成json字符串，再解码成数组
        return json_decode(json_encode($object), true);
    }
}

if (!function_exists('parse_attr')) {

    /**
     * 配置值解析成数组
     * @param string $value 参数值
     * @return array 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function parse_attr($value = '')
    {
        if (is_array($value)) {
            return $value;
        }
        $array = preg_split('/[,;\r\n]+/', trim($value, ",;\r\n"));
        if (strpos($value, ':')) {
            $value = array();
            foreach ($array as $val) {
                list($k, $v) = explode(':', $val);
                $value[$k] = $v;
            }
        } else {
            $value = $array;
        }
        return $value;
    }
}


if (!function_exists('cutstr_html')) {

    /**
     * 提取纯文本
     * @param $str 原字符串
     * @return string 过滤后的字符串
     * @author 牧羊人
     * @date 2020-04-21
     */
    function cutstr_html($str)
    {
        $str = trim(strip_tags($str)); //清除字符串两边的空格
        $str = preg_replace("/\t/", "", $str); //使用正则表达式替换内容，如：空格，换行，并将替换为空。
        $str = preg_replace("/\r\n/", "", $str);
        $str = preg_replace("/\r/", "", $str);
        $str = preg_replace("/\n/", "", $str);
        $str = preg_replace("/ /", "", $str);
        $str = preg_replace("/  /", "", $str);  //匹配html中的空格
        return trim($str); //返回字符串
    }
}

if (!function_exists('strip_html_tags')) {

    /**
     * 去除HTML标签、图像等 仅保留文本
     * @param string $str 字符串
     * @param int $length 长度
     * @return string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function strip_html_tags($str, $length = 0)
    {
        // 把一些预定义的 HTML 实体转换为字符
        $str = htmlspecialchars_decode($str);
        // 将空格替换成空
        $str = str_replace("&nbsp;", "", $str);
        // 函数剥去字符串中的 HTML、XML 以及 PHP 的标签,获取纯文本内容
        $str = strip_tags($str);
        $str = str_replace(array("\n", "\r\n", "\r"), ' ', $str);
        $preg = '/<script[\s\S]*?<\/script>/i';
        // 剥离JS代码
        $str = preg_replace($preg, "", $str, -1);
        if ($length == 2) {
            // 返回字符串中的前100字符串长度的字符
            $str = mb_substr($str, 0, $length, "utf-8");
        }
        return $str;
    }

}

if (!function_exists('strip_html_tags2')) {

    /**
     * 去除指定HTML标签
     * @param string $str 字符串
     * @param $tags 指定的标签
     * @param int $content 是否删除标签内的内容 0保留内容 1不保留内容
     * @return string 返回结果
     * 示例：echo strip_html_tags($str, array('a','img'))
     * @author 牧羊人
     * @date 2020-04-21
     */
    function strip_html_tags2($str, $tags, $content = 0)
    {
        if ($content) {
            $html = array();
            foreach ($tags as $tag) {
                $html[] = '/(<' . $tag . '.*?>[\s|\S]*?<\/' . $tag . '>)/';
            }
            $result = preg_replace($html, '', $str);
        } else {
            $html = array();
            foreach ($tags as $tag) {
                $html[] = "/(<(?:\/" . $tag . "|" . $tag . ")[^>]*>)/i";
            }
            $result = preg_replace($html, '', $str);
        }
        return $result;
    }

}

if (!function_exists('sub_str')) {

    /**
     * 字符串截取
     * @param string $str 需要截取的字符串
     * @param int $start 开始位置
     * @param int $length 截取长度
     * @param bool $suffix 截断显示字符
     * @param string $charset 编码格式
     * @return string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function sub_str($str, $start = 0, $length = 10, $suffix = true, $charset = "utf-8")
    {
        if (function_exists("mb_substr")) {
            $slice = mb_substr($str, $start, $length, $charset);
        } elseif (function_exists('iconv_substr')) {
            $slice = iconv_substr($str, $start, $length, $charset);
        } else {
            $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
            $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
            $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
            $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
            preg_match_all($re[$charset], $str, $match);
            $slice = join("", array_slice($match[0], $start, $length));
        }
        $omit = mb_strlen($str) >= $length ? '...' : '';
        return $suffix ? $slice . $omit : $slice;
    }

}

if (!function_exists('cutstr_html')) {

    /**
     * 提取纯文本
     * @param $str 原字符串
     * @return string 过滤后的字符串
     * @author 牧羊人
     * @date 2020-04-21
     */
    function cutstr_html($str)
    {
        $str = trim(strip_tags($str)); //清除字符串两边的空格
        $str = preg_replace("/\t/", "", $str); //使用正则表达式替换内容，如：空格，换行，并将替换为空。
        $str = preg_replace("/\r\n/", "", $str);
        $str = preg_replace("/\r/", "", $str);
        $str = preg_replace("/\n/", "", $str);
        $str = preg_replace("/ /", "", $str);
        $str = preg_replace("/  /", "", $str);  //匹配html中的空格
        return trim($str); //返回字符串
    }
}

if (!function_exists('zip_file')) {

    /**
     * 打包压缩文件及文件夹
     * @param array $files 文件
     * @param string $zipName 压缩包名称
     * @param bool $isDown 压缩后是否下载true或false
     * @return string 返回结果
     * @author 牧羊人
     * @date 2020-04-21
     */
    function zip_file($files = [], $zipName = '', $isDown = true)
    {
        // 文件名为空则生成文件名
        if (empty($zipName)) {
            $zipName = date('YmdHis') . '.zip';
        }

        // 实例化类,使用本类，linux需开启zlib，windows需取消php_zip.dll前的注释
        $zip = new \ZipArchive;
        /*
         * 通过ZipArchive的对象处理zip文件
         * $zip->open这个方法如果对zip文件对象操作成功，$zip->open这个方法会返回TRUE
         * $zip->open这个方法第一个参数表示处理的zip文件名。
         * 这里重点说下第二个参数，它表示处理模式
         * ZipArchive::OVERWRITE 总是以一个新的压缩包开始，此模式下如果已经存在则会被覆盖。
         * ZipArchive::OVERWRITE 不会新建，只有当前存在这个压缩包的时候，它才有效
         * */
        if ($zip->open($zipName, \ZIPARCHIVE::OVERWRITE | \ZIPARCHIVE::CREATE) !== true) {
            exit('无法打开文件，或者文件创建失败');
        }

        // 打包处理
        if (is_string($files)) {
            // 文件夹整体打包
            addFileToZip($files, $zip);
        } else {
            // 文件打包
            foreach ($files as $val) {
                if (file_exists($val)) {
                    // 添加文件
                    $zip->addFile($val, basename($val));
                }
            }
        }
        // 关闭
        $zip->close();

        // 验证文件是否存在
        if (!file_exists($zipName)) {
            exit("文件不存在");
        }

        if ($isDown) {
            // 下载压缩包
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header('Content-disposition: attachment; filename=' . basename($zipName)); //文件名
            header("Content-Type: application/zip"); //zip格式的
            header("Content-Transfer-Encoding: binary"); //告诉浏览器，这是二进制文件
            header('Content-Length: ' . filesize($zipName)); //告诉浏览器，文件大小
            @readfile($zipName);
        } else {
            // 直接返回压缩包地址
            return $zipName;
        }
    }
}

if (!function_exists('addFileToZip')) {

    /**
     * 添加文件至压缩包
     * @param string $path 文件夹路径
     * @param $zip zip对象
     * @author 牧羊人
     * @date 2020-04-21
     */
    function addFileToZip($path, $zip)
    {
        // 打开文件夹
        $handler = opendir($path);
        while (($filename = readdir($handler)) !== false) {
            if ($filename != "." && $filename != "..") {
                // 编码转换
                $filename = iconv('gb2312', 'utf-8', $filename);
                // 文件夹文件名字为'.'和‘..’，不要对他们进行操作
                if (is_dir($path . "/" . $filename)) {
                    // 如果读取的某个对象是文件夹，则递归
                    addFileToZip($path . "/" . $filename, $zip);
                } else {
                    // 将文件加入zip对象
                    $file_path = $path . "/" . $filename;
                    $zip->addFile($file_path, basename($file_path));
                }
            }
        }
        // 关闭文件夹
        @closedir($path);
    }
}

if (!function_exists('unzip_file')) {

    /**
     * 压缩文件解压
     * @param string $file 被解压的文件
     * @param $dirname 解压目录
     * @return bool 返回结果true或false
     * @author 牧羊人
     * @date 2020-04-21
     */
    function unzip_file($file, $dirname)
    {
        if (!file_exists($file)) {
            return false;
        }
        // zip实例化对象
        $zipArc = new ZipArchive();
        // 打开文件
        if (!$zipArc->open($file)) {
            return false;
        }
        // 解压文件
        if (!$zipArc->extractTo($dirname)) {
            // 关闭
            $zipArc->close();
            return false;
        }
        return $zipArc->close();
    }
}

if (!function_exists('checkWords')) {
    /**
     * 检查敏感词
     * @param $list
     * @param $str
     * @return string
     * @author 牧羊人
     * @date 2020-04-21
     */
    function checkWords($list, $str, $flag = false)
    {
        $count = 0; //违规词的个数
        $sensitiveWord = '';  //违规词
        $stringAfter = $str;  //替换后的内容
        $pattern = "/" . implode("|", $list) . "/i"; //定义正则表达式
        if (preg_match_all($pattern, $str, $matches)) { //匹配到了结果
            $patternList = $matches[0];  //匹配到的数组
            $count = count($patternList);
            $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
//            $replaceArray = array_combine($patternList, array_fill(0, count($patternList), '***')); //把匹配到的数组进行合并，替换使用
//            $stringAfter = strtr($str, $replaceArray); //结果替换

            // 临时解决方案
            $itemArr = [];
            if (!empty($patternList)) {
                foreach ($patternList as $val) {
                    if (!$val) {
                        continue;
                    }
                    $itemArr[] = str_pad("", mb_strlen($val), "*", STR_PAD_LEFT);
                }
            }
            $replaceArray = array_combine($patternList, $itemArr); //把匹配到的数组进行合并，替换使用
            $stringAfter = strtr($str, $replaceArray); //结果替换
        }
        $log = "原句为 [ {$str} ]<br/>";
        if ($count == 0) {
            $log .= "暂未匹配到敏感词！";
        } else {
            $log .= "匹配到 [ {$count} ]个敏感词：[ {$sensitiveWord} ]<br/>" .
                "替换后为：[ {$stringAfter} ]";
        }
        if (!$flag) {
            return $stringAfter;
        } else {
            return $count;
        }
    }
}

/**
 * 打印输出数据到文件
 *
 * @param mixed       $data
 * @param bool        $replace
 * @param string|null $pathname
 */
function p($data, $replace = false, $pathname = null) {
	return '抱歉上传失败';
    //is_null($pathname) && $pathname = RUNTIME_PATH . date('Ymd') . '.txt';
    //$str = (is_string($data) ? $data : (is_array($data) || is_object($data)) ? print_r($data, true) : var_export($data, true)) . "\n";
    //$replace ? file_put_contents($pathname, $str) : file_put_contents($pathname, $str, FILE_APPEND);
}

/**
 * 获取mongoDB连接
 *
 * @param string $col   数据库集合
 * @param bool   $force 是否强制连接
 *
 * @return \think\db\Query|\think\mongo\Query
 */
function mongo($col, $force = false) {
    return Db::connect(config('mongo'), $force)->name($col);
}

/**
 * 获取微信操作对象
 *
 * @param string $type
 *
 * @return \Wechat\WechatMedia|\Wechat\WechatMenu|\Wechat\WechatOauth|\Wechat\WechatPay|\Wechat\WechatReceive|\Wechat\WechatScript|\Wechat\WechatUser|\Wechat\WechatExtends|\Wechat\WechatMessage
 * @throws Exception
 */
function &load_wechat($type = '') {
    static $wechat = [];
    $index = md5(strtolower($type));
    if (!isset($wechat[$index])) {
        $config         = [
            'token'          => sysconf('wechat_token'),
            'appid'          => sysconf('wechat_appid'),
            'appsecret'      => sysconf('wechat_appsecret'),
            'encodingaeskey' => sysconf('wechat_encodingaeskey'),
            'mch_id'         => sysconf('wechat_mch_id'),
            'partnerkey'     => sysconf('wechat_partnerkey'),
            'ssl_cer'        => sysconf('wechat_cert_cert'),
            'ssl_key'        => sysconf('wechat_cert_key'),
            'cachepath'      => CACHE_PATH . 'wxpay' . DS,
        ];
        $wechat[$index] = Loader::get($type, $config);
    }
    return $wechat[$index];
}

/**
 * UTF8字符串加密
 *
 * @param string $string
 *
 * @return string
 */
function encode($string) {
    list($chars, $length) = ['', strlen($string = iconv('utf-8', 'gbk', $string))];
    for ($i = 0; $i < $length; $i++) {
        $chars .= str_pad(base_convert(ord($string[$i]), 10, 36), 2, 0, 0);
    }
    return $chars;
}

/**
 * UTF8字符串解密
 *
 * @param string $string
 *
 * @return string
 */
function decode($string) {
    $chars = '';
    foreach (str_split($string, 2) as $char) {
        $chars .= chr(intval(base_convert($char, 36, 10)));
    }
    return iconv('gbk', 'utf-8', $chars);
}

/**
 * 网络图片本地化
 *
 * @param string $url
 *
 * @return string
 */
function local_image($url) {
    if (is_array(($result = FileService::download($url)))) {
        return $result['url'];
    }
    return $url;
}

/**
 * 日期格式化
 *
 * @param string $date   标准日期格式
 * @param string $format 输出格式化date
 *
 * @return false|string
 */
function format_datetime($date, $format = 'Y年m月d日 H:i:s') {
    return empty($date) ? '' : date($format, strtotime($date));
}

/**
 * 设备或配置系统参数
 *
 * @param string $name  参数名称
 * @param bool   $value 默认是null为获取值，否则为更新
 *
 * @return string|bool
 */
function sysconf($name, $value = null) {
    static $config = [];
    if ($value !== null) {
        list($config, $data) = [[], ['name' => $name, 'value' => $value]];
        return DataService::save('SystemConfig', $data, 'name');
    }
    if (empty($config)) {
        $config = Db::name('SystemConfig')->column('name,value');
    }
	// 可删
	//if($name == 'site_domain'){
		//$domain = request()->domain();
		//if(!strstr($domain,$config['site_domain'])){
			//$config['site_domain'] = $domain;
		//}	
	//}	
	// 可删
    return isset($config[$name]) ? $config[$name] : '';
}

/**
 * RBAC节点权限验证
 *
 * @param string $node
 *
 * @return bool
 */
function auth($node) {
    return NodeService::checkAuthNode($node);
}

/**
 * array_column 函数兼容
 */
if (!function_exists("array_column")) {
    function array_column(array &$rows, $column_key, $index_key = null) {
        $data = [];
        foreach ($rows as $row) {
            if (empty($index_key)) {
                $data[] = $row[$column_key];
            } else {
                $data[$row[$index_key]] = $row[$column_key];
            }
        }
        return $data;
    }
}

/**
 * 返回接口数据
 *
 * @param int    $code 状态码
 * @param string $msg  信息
 * @param array  $data 数据
 *
 * @return string json数据
 */
function J($code, $msg = '', $data = [], $url = null) {
    $return = [
        'code'      => $code,
        'msg'       => $msg,
        'data'      => $data,
        'url'       => $url,
        'timestamp' => time(),
    ];
    // 兼容default_ajax_return=json，防止被双重json_encode，添加appliaction/json头
    return json($return);
}

/**
 * 判断是否为合法的身份证号码
 *
 * @param $vStr
 *
 * @return int
 */
function is_idcard_number($vStr) {
    $vCity = array(
        '11', '12', '13', '14', '15', '21', '22',
        '23', '31', '32', '33', '34', '35', '36',
        '37', '41', '42', '43', '44', '45', '46',
        '50', '51', '52', '53', '54', '61', '62',
        '63', '64', '65', '71', '81', '82', '91',
    );
    if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr)) {
        return false;
    }

    if (!in_array(substr($vStr, 0, 2), $vCity)) {
        return false;
    }

    $vStr    = preg_replace('/[xX]$/i', 'a', $vStr);
    $vLength = strlen($vStr);
    if ($vLength == 18) {
        $vBirthday = substr($vStr, 6, 4) . '-' . substr($vStr, 10, 2) . '-' . substr($vStr, 12, 2);
    } else {
        $vBirthday = '19' . substr($vStr, 6, 2) . '-' . substr($vStr, 8, 2) . '-' . substr($vStr, 10, 2);
    }
    if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) {
        return false;
    }

    if ($vLength == 18) {
        $vSum = 0;
        for ($i = 17; $i >= 0; $i--) {
            $vSubStr = substr($vStr, 17 - $i, 1);
            $vSum    += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr, 11));
        }
        if ($vSum % 11 != 1) {
            return false;
        }
    }
    return true;
}

/**
 * 判断是否为正常的手机号
 *
 * @param  string $mobile 手机号
 *
 * @return boolean
 */
function is_mobile_number($mobile) {
    if (!is_numeric($mobile)) {
        return false;
    }

    //return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,1,3,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;

    return preg_match('#^1[3-9]\d{9}$#', $mobile) ? true : false;
}

/**
 * 生成订单号
 *
 * @return string 订单号
 */
function generate_trade_no($flag = 'A', $userid = 0) {
    //订单自定义
    if (sysconf('order_type') == 1) {
        $trade_no = sysconf('user_order_profix') . date('ymdHis') . str_pad(abs($userid - 10000), 4, 0) . str_pad(mt_rand(0, 99), 2, '0');
    } else {
        $trade_no = $flag . date('ymdHis') . str_pad(abs($userid - 10000), 4, 0) . str_pad(mt_rand(0, 99), 2, '0');
    }

    //校验是否有重复订单号
    $res = Db::name('order')->where(['trade_no' => $trade_no])->find();
    if ($res['status']) {
        //需要重新生成订单号
        $trade_no = generate_trade_no($flag, $userid);
    }

    //检查订单号是否唯一
    $res = Db::name('unique_orderno')->insert(['trade_no' => $trade_no]);
    if ($res != 1) {
        //需要重新生成订单号
        $trade_no = generate_trade_no($flag, $userid);
    }


    return $trade_no;
}

/**
 * 友盟推送
 */
function push_umeng($device_tokens, $ticker = '', $title = '', $text = '', $extra = []) {
    $UPush = new \umeng\UPush(sysconf('upush_appkey'), sysconf('upush_appMasterSecret'));
    $res   = $UPush->sendAndroidUnicast($device_tokens, $ticker, $title, $text, $extra);
    return $res;
}

/**
 * 生成随机字符串
 */
function random_str($length = 32) {
    $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    $str   = "";
    for ($i = 0; $i < $length; $i++) {
        $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
}

/**
 * 生成excel
 *
 * @param  array  $title    标题字段
 * @param  array  $data     数据
 * @param  string $filename 文件名
 * @param  string $type     生成类型
 */
function generate_excel($title, $data, $filename, $type = 'file') {
    $file_name = $filename . ".csv";
    if ($type == 'file') {
        $file = fopen(TEMP_PATH . $file_name, "a");
    } else {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $file_name);
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        $file = fopen('php://output', "a");
    }
    $limit = 10000;
    $calc  = 0;
    //列名
    foreach ($title as $v) {
        $tit[] = iconv('UTF-8', 'GB2312//IGNORE', $v);
    }
    //将数据通过fputcsv写到文件句柄
    fputcsv($file, $tit);

    foreach ($data as $v) {
        $calc++;
        if ($limit == $calc) {
            ob_flush();
            flush();
            $calc = 0;
        }
        foreach ($v as $t) {
            $tarr[] = "\t" . iconv('UTF-8', 'GB2312//IGNORE', $t) . '';
        }
        fputcsv($file, $tarr);
        unset($tarr);
    }
    unset($data);
    ob_flush();
    flush();
    fclose($file);
    // return TEMP_PATH . $file_name;
    exit();
}

/**
 * 邮件发送
 *
 * @param  string $to              收件方
 * @param  string $title           标题
 * @param  string $content         内容
 * @param  string $filePath        附件
 * @param  bool   $throwExceptions 是否抛出异常
 *
 * @return bool          发送状态
 */
function sendMail($to, $title, $content, $filePath = '', $throwExceptions = false) {
    //步骤
    //1复制文件到当前项目下的Thinkphp/libary/Org/Util (class.pop3.php class.smtp.php class.phpmailer.php)
    //2.修改类文件的名称
    //3.修改命名空间
    //4.注意在PHPMailer中最后一个继承
    $mail = new Util\Mailer\PHPMailer($throwExceptions);
    // $mail->SMTPDebug  = 1;
    $mail->CharSet = "utf-8"; //设置采用utf8中文编码
    $mail->IsSMTP(); //设置采用SMTP方式发送邮件

    $max   = 2;
    $index = rand(0, 2);

    $tried = explode(',', trim(',', session('last_try_email')));
    if(empty($tried)) $tried = [];

    while (in_array($index, $tried)) {
        $index = rand(0, 2);
    }
    if (empty($index)) {
        $configIndex = '';
    } else {
        $configIndex = $index;
    }

    //$host     = sysconf('email_smtp' . $configIndex);
    //$port     = sysconf('email_port' . $configIndex);
    //$from     = sysconf('email_user' . $configIndex);
    //$password = sysconf('email_pass' . $configIndex);
	
    //$host     = "smtp.qq.com";
    //$port     = "465";
    //$from     = "suzhizhan@qq.com";
    //$password = "zsiajxlsbniebjff";	

    $host     = sysconf('email_smtp');
    $port     = sysconf('email_port');
    $from     = sysconf('email_user');
    $password = sysconf('email_pass');	

    $mail->Host       = $host; //设置邮件服务器的地址
    $mail->Port       = $port; //设置邮件服务器的端口，默认为25
    $mail->From       = $from; //设置发件人的邮箱地址
    $mail->FromName   = sysconf('email_name'); //设置发件人的姓名
    $mail->SMTPAuth   = true; //设置SMTP是否需要密码验证，true表示需要
    $mail->SMTPSecure = "ssl";
    $mail->Username   = $from;
    $mail->Password   = $password;
    $mail->Subject    = $title; //设置邮件的标题
    $mail->AltBody    = "text/html"; // optional, comment out and test
    $mail->Body       = $content;
    $mail->IsHTML(true); //设置内容是否为html类型
    $mail->AddAddress(trim($to), ''); //设置收件的地址
    if (!empty($filePath)) {
        $mail->AddAttachment($filePath);
    }
    $try = session('email_try');
    if (empty($try)) {
        session('email_try', 0);
    }
    if (!$mail->Send()) {
        //发送邮件
        //echo '发送失败:' . $mail->ErrorInfo;
        record_file_log('email_error', 'index: ' . $index . "\r\n" . $mail->ErrorInfo);
        //更新可用的邮箱，保证下一封邮件可以发出
        if ($try >= $max) {
            session('email_try', 0);
            session('last_try_email', '');
            return false;
        } else {
            session('email_try', $try + 1);
            session('last_try_email', session('last_try_email') . ',' . $index);
            return sendMail($to, $title, $content, $filePath, $throwExceptions);
        }
    } else {
        // echo "发送成功";
        session('email_try', 0);
        session('last_try_email', '');
        return true;
    }
}

/**
 * 获取支付类型列表
 *
 * @return array 支付类型列表
 */
function get_paytype_list() {
    $pay_type = null;
    try {
        $pay_type = Db::name('pay_type')->select();

        foreach ($pay_type as $item) {
            if ($item['sub_lists']) {
                $item['sub_lists'] = json_decode($item['sub_lists']);
            }
        }
    } catch (\think\Exception $e) {
        $pay_type = null;
    }
    if (!$pay_type) {
        return config('paytype_config');
    } else {
        return $pay_type;
    }
}

/**
 * 获取支付类型信息
 *
 * @param int $paytype 支付类型
 *
 * @return array 支付类型信息
 */
function get_paytype_info($paytype) {
    $pay_type = null;
    try {
        $pay_type = Db::name('pay_type')->find(['id' => $paytype]);

        if ($pay_type['sub_lists']) {
            $pay_type['sub_lists'] = json_decode($pay_type['sub_lists']);
        }
    } catch (\think\Exception $e) {
        $pay_type = null;
    }
    if (!$pay_type) {
        return isset(config('paytype_config')[$paytype]) ? config('paytype_config')[$paytype] : [];
    } else {
        return $pay_type;
    }
}

/**
 * 获取支付类型名称
 *
 * @param int $paytype 支付类型
 * @param int $type    返回值类型 0：支付产品名 1：渠道名
 *
 * @return string 支付类型名称
 */
function get_paytype_name($paytype, $type = 0) {
    $pay_type = null;

    try {
        $pay_type = Db::name('pay_type')->find(['id' => $paytype]);

        if ($pay_type['sub_lists']) {
            $pay_type['sub_lists'] = json_decode($pay_type['sub_lists']);
        }
    } catch (\think\Exception $e) {
        $pay_type = null;
    }

    if (!$pay_type) {
        $pay_type = isset(config('paytype_config')[$paytype]) ? config('paytype_config')[$paytype] : null;
    }

    if (!$pay_type) {
        return '';
    }

    if ($type == 0) {
        $product_id = $pay_type['product_id'];
        if ($product_id) {
            return isset(config('pay_product')[$product_id]) ? config('pay_product')[$product_id] : '';
        } else {
            return '';
        }
    } else {
        return $pay_type['name'];
    }
}

/**
 * 生成二维码链接
 */
function generate_qrcode_link($url) {
    return 'http://qr.liantu.com/api.php?&w=180&text=' . $url;
}

/**
 * 记录用户金额变动日志
 */
function record_user_money_log($business_type, $user_id, $money, $balance, $reason) {
    $businessTypes = [
        'unfreeze'        => '解冻金额',
        'freeze'          => '冻结金额',
        'sub_sold_rebate' => '下级卖出商品返佣',
        'sub_fee_rebate'  => '下级手续费返佣',
        'cash_notpass'    => '提现未通过',
        'cash_success'    => '提现成功',
        'apply_cash'      => '申请提现',
        'admin_inc'       => '后台操作加钱',
        'admin_dec'       => '后台操作扣钱',
        'fee'             => '手续费',
        'goods_sold'      => '卖出商品',
        'sub_register'    => '推广注册奖励',
    ];
    $tag           = isset($businessTypes[$business_type]) ? "【{$businessTypes[$business_type]}】" : '';
    $res           = Db::name('UserMoneyLog')->insert([
        'business_type' => $business_type,
        'user_id'       => $user_id,
        'money'         => round($money, 3),
        'balance'       => round($balance, 3),
        'reason'        => $tag . $reason,
        'create_at'     => time(),
    ]);
    return $res;
}

/**
 * 获取用户费率
 *
 * @param  int $user_id    用户ID
 * @param  int $channel_id 渠道ID
 *
 * @return double          费率
 */
function get_user_rate($user_id, $channel_id) {
    $rate     = 0;
    $userRate = Db::name('userRate')->where(['user_id' => $user_id, 'channel_id' => $channel_id])->find();
    // 如果用户没有设置费率则走默认充值费率
    if (!$userRate || $userRate['rate'] == 0) {
        $rate = Db::name('channel')->where(['id' => $channel_id])->value('lowrate');
    } else {
        $rate = $userRate['rate'];
    }
    return round($rate, 4);
}

/**
 * 获取用户对应渠道状态
 *
 * @param  int $user_id    用户ID
 * @param  int $channel_id 渠道ID
 *
 * @return double          费率
 */
function get_user_channel_status($user_id, $channel_id) {
    $status      = 1;
    $userChannel = Db::name('userChannel')->where(['user_id' => $user_id, 'channel_id' => $channel_id])->find();
    // 如果用户没有设置则走全局状态
    if (!$userChannel) {
        $rate = Db::name('channel')->where(['id' => $channel_id])->value('status');
    } else {
        $status = $userChannel['status'];
    }
    return $status;
}

/**
 * 获取支付产品的名称
 *
 * @param int $type 产品类型
 *
 * @return string 产品的名称
 */
function get_product_name($type) {
    $product_name = \app\common\model\Product::field('title')->where(['paytype' => $type])->find();
    return isset($product_name['title']) ? $product_name->title : get_paytype_name($type);
}

/**
 * 获取用户渠道信息
 *
 * @param  [type] $user_id [description]
 *
 * @return [type]          [description]
 */
function get_user_channels($user_id) {
    $channels     = \app\common\model\Channel::order('sort desc')->where(['status' => 1])->select();
    //检查是否设置了分组费率
    $rate_group_user = Db::name('rate_group_user')->where('user_id', $user_id)->find();
    $channel_ids = [];
    if(!empty($rate_group_user)) {
        //选择费率分组中开启的渠道
        $channel_ids_temp = Db::name('rate_group_rule')
            ->where(['group_id' => $rate_group_user['group_id'], 'status' => 1])
            ->field('channel_id')
            ->select();
        foreach ($channel_ids_temp as $channel) {
            array_push($channel_ids, $channel['channel_id']);
        }
    }
    $userChannels = [];
    foreach ($channels as &$v) {
        if(!empty($channel_ids)) {
            if(!in_array($v->id, $channel_ids)) {
                //过滤费率分组中已关闭的渠道
                continue;
            }
        }
        $userChannels[] = [
            'user_id'      => $user_id,
            'channel_id'   => $v->id,
            'paytype'      => $v->paytype,
            'title'        => $v->title,
            'show_name'    => $v->show_name,
            'rate'         => get_user_rate($user_id, $v->id),
            'status'       => get_user_channel_status($user_id, $v->id),
            'product_name' => get_product_name($v->paytype), //添加了支付产品名称,
            'is_available' => $v->is_available, //支付产品可用的地方
        ];
    }
    return $userChannels;
}

/**
 * 发送站内信
 */
function sendMessage($from_id, $to_id, $title, $content) {
    $res = Db::name('Message')->insert([
        'from_id'   => $from_id,
        'to_id'     => $to_id,
        'title'     => $title,
        'content'   => $content,
        'status'    => 0,
        'create_at' => $_SERVER['REQUEST_TIME'],
    ]);
    return $res;
}

/**
 * 获取提现手续费
 */
function get_cash_fee($money) {
    $type = (int)sysconf('cash_fee_type');
    $fee  = sysconf('cash_fee');
    switch ($type) {
        case 1: // 固定
            return $fee >= 0 ? round($fee, 2) : 0;
            break;
        case 100: // 百分比
            if ($fee < 0 || $fee > 100) {
                return 0;
            } else {
                return round($fee / 100 * $money, 2);
            }
            break;
        default:
            return 0;
            break;
    }
}

/**
 * 获取提现手续费
 */
function get_auto_cash_fee($money) {
    $type = (int)sysconf('auto_cash_fee_type');
    $fee  = sysconf('auto_cash_fee');
    switch ($type) {
        case 1: // 固定
            return $fee >= 0 ? round($fee, 2) : 0;
            break;
        case 100: // 百分比
            if ($fee < 0 || $fee > 100) {
                return 0;
            } else {
                return round($fee / 100 * $money, 2);
            }
            break;
        default:
            return 0;
            break;
    }
}

/**
 * 获取推广返佣比率
 */
function get_spread_rebate_rate() {
    $rate = sysconf('spread_rebate_rate');
    if ($rate === null || $rate < 0 || $rate > 100) {
        return 0;
    }
    return round($rate / 100, 4);
}

/**
 * 字词过滤
 *
 * @param string $str 待检测字符串
 *
 * @return bool/string FALSE/敏感词汇
 */
function check_wordfilter($str) {
    if ($str !== '' && sysconf('site_wordfilter_status') == 1) {
        $words = explode('|', trim(sysconf('site_wordfilter_danger'), '|'));
        foreach ($words as $word) {
            if ($word) {
                if (strpos($str, $word) !== false) {
                    return $word;
                    break;
                }
            }
        }
    }
    return true;
}

/**
 * 获取短网址
 *
 * @param string $url url地址
 */
function get_short_domain($url, $type = '') {
    if ($type === '') {
        $type = sysconf('site_domain_short');
        if ($type === '') {
            return '';
        }
    }
    $dwz = \app\common\util\DWZ::load($type);
    switch ($type) {
        case 'Baidu':
            $shortDomain = $dwz->create($url);
            break;
        case 'Sina':
            $shortDomain = $dwz->create($url);
            break;
        case 'U6':
            $shortDomain = $dwz->create($url);
            break;
        default:
            return '';
            break;
    }
    if ($shortDomain === false) {
        return '';
    }
    return $shortDomain;
}

/**
 * 获取唯一ID
 */
function get_uniqid($len = 32) {
    return substr(md5(uniqid(md5(microtime(true)), true)), 0, $len);
}

// 检测输入的验证码是否正确，$code为用户输入的验证码字符串，$id多个验证码标识
function verify_code($code, $id = '') {
    $captcha = new \think\captcha\Captcha();
    return $captcha->check($code, $id);
}

/**
 * 记录文件日志
 */
function record_file_log($filename, $content) {
    file_put_contents(LOG_PATH . $filename . '.log', date('【Y-m-d H:i:s】') . "\r\n{$content}\r\n\r\n", FILE_APPEND);
}

/**
 * 获取短信运营成本
 */
function get_sms_cost() {
    switch (sysconf('sms_notify_channel')) {
        case 'alidayu':
            $cost = sysconf('alidayu_cost');
            break;
        case 'smsbao':
            $cost = sysconf('smsbao_cost');
            break;
        case 'yixin': // 弈新
            $cost = sysconf('yixin_sms_cost');
            break;
        case '1cloudsp': //天瑞云
            $cost = sysconf('1cloudsp_cost');
            break;
        case '253sms': //创蓝253
            $cost = sysconf('253sms_cost');
            break;
        default:
            $cost = 0;
            break;
    }
    return round($cost, 2);
}

/**
 * 将秒数转换为时间（年、天、小时、分、秒）
 *
 * @param number $time 秒数
 *
 * @return string
 */
function sec2Time($time) {
    if (is_numeric($time)) {
        $value = array(
            "years"   => 0, "days" => 0, "hours" => 0,
            "minutes" => 0, "seconds" => 0,
        );
        if ($time >= 31556926) {
            $value["years"] = floor($time / 31556926);
            $time           = ($time % 31556926);
        }
        if ($time >= 86400) {
            $value["days"] = floor($time / 86400);
            $time          = ($time % 86400);
        }
        if ($time >= 3600) {
            $value["hours"] = floor($time / 3600);
            $time           = ($time % 3600);
        }
        if ($time >= 60) {
            $value["minutes"] = floor($time / 60);
            $time             = ($time % 60);
        }
        $value["seconds"] = floor($time);
        $t                = '';
        if ($value["years"] > 0) {
            $t .= $value["years"] . "年";
        }
        if ($value["days"] > 0) {
            $t .= $value["days"] . "天";
        }
        if ($value["hours"] > 0) {
            $t .= $value["hours"] . "小时";
        }
        if ($value["minutes"] > 0) {
            $t .= $value["minutes"] . "分";
        }
        if ($value["seconds"] > 0) {
            $t .= $value["seconds"] . "秒";
        }
        return $t;
    } else {
        return (bool)false;
    }
}

//获取系统版本
function getVersion() {
    return \think\Config::get('version.VERSION');
}

//验证是否URL
function validateURL($URL) {
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $URL)) {
        return false;
    }
    return true;
}

//验证邮箱格式
function is_email($email) {
    $regx = "/^([\w-.]+)@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.)|(([\w-]+.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$/";
    if (!preg_match($regx, $email)) {
        return false;
    }
    return true;
}

/**
 * 删除目录下的文件
 *
 * @param [type] $path
 *
 * @return void
 */
function delFileUnderDir($path, $delDir = false) {
    if (is_array($path)) {
        foreach ($path as $subPath) {
            delFileUnderDir($subPath, $delDir);
        }
    } elseif (is_dir($path)) {
        $handle = opendir($path);
        if ($handle) {
            while (false !== ($item = readdir($handle))) {
                if ($item != "." && $item != "..") {
                    is_dir("$path/$item") ? delFileUnderDir("$path/$item", $delDir) : unlink("$path/$item");
                }
            }
            closedir($handle);
            if ($delDir) {
                return rmdir($path);
            }
        }
    } else {
        if (file_exists($path)) {
            return unlink($path);
        } else {
            return false;
        }
    }
}

/**
 * POST 请求
 *
 * @param string $url
 * @param array  $param
 *
 * @return string content
 */
function http_post($url, $param) {
    $oCurl = curl_init();
    if (stripos($url, "https://") !== false) {
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
    }
    if (is_string($param)) {
        $strPOST = $param;
    } else {
        $aPOST = array();
        foreach ($param as $key => $val) {
            $aPOST[] = $key . "=" . urlencode($val);
        }
        $strPOST = join("&", $aPOST);
    }
    curl_setopt($oCurl, CURLOPT_URL, $url);
    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($oCurl, CURLOPT_POST, true);
    curl_setopt($oCurl, CURLOPT_POSTFIELDS, $strPOST);
    $sContent = curl_exec($oCurl);
    $aStatus  = curl_getinfo($oCurl);
    curl_close($oCurl);
    if (intval($aStatus["http_code"]) == 200) {
        return $sContent;
    } else {
        return false;
    }
}

//获取顶级域名
function get_host() {
    $url   = $_SERVER['HTTP_HOST'];
    $data  = explode('.', $url);
    $co_ta = count($data);
    //判断是否是双后缀
    $zi_tow  = true;
    $host_cn = 'com.cn,net.cn,org.cn,gov.cn';
    $host_cn = explode(',', $host_cn);
    foreach ($host_cn as $host) {
        if (strpos($url, $host)) {
            $zi_tow = false;
        }
    }
    //如果是返回FALSE ，如果不是返回true
    if ($zi_tow == true) {
        $host = $data[$co_ta - 2] . '.' . $data[$co_ta - 1];
    } else {
        $host = $data[$co_ta - 3] . '.' . $data[$co_ta - 2] . '.' . $data[$co_ta - 1];
    }
    return $host;
}

//返回：获取代码部署系统中版本列表接口的URL
function get_version_list_url() {
    $versionListUrl = config('deploy_common.version_list_url');
    if (!$versionListUrl) {
        throw new Exception('配置错误');
    }
    return $versionListUrl . DS . config('deploy_unique.project_id') . DS . config('deploy_unique.environment_id');
}

//返回：代码更新URL
function get_version_update_url($versionHash) {
    $versionUpdateUrl = config('deploy_common.version_update_url');
    if (!$versionUpdateUrl) {
        throw new Exception('配置错误');
    }
    return $versionUpdateUrl . DS . $versionHash . DS . config('deploy_unique.environment_id');
}

/**
 * 生成txt
 *
 * @param  array  $title    标题字段
 * @param  array  $data     数据
 * @param  string $filename 文件名
 * @param  string $type     生成类型
 */
function generate_txt($title, $data, $filename, $type = 'file') {
    $file_name = $filename . ".txt";
    if ($type == 'file') {
        $file = fopen(TEMP_PATH . $file_name, "a");
    } else {
        header("Content-type:   application/octet-stream ");
        header("Accept-Ranges:   bytes ");
        header("Content-Disposition:   attachment;   filename=" . $filename . ".txt ");
        header("Expires:   0 ");
        header("Cache-Control:   must-revalidate,   post-check=0,   pre-check=0 ");
        header("Pragma:   public ");
        //列名
        foreach ($title as $v) {
            echo iconv('UTF-8', 'GB2312//IGNORE', trim($v)) . "\t";
        }
        echo "\r\n";
        echo PHP_EOL;
        foreach ($data as $v) {
            foreach ($v as $t) {
                echo iconv('UTF-8', 'GB2312//IGNORE', trim($t)) . "\t";
            }
            echo PHP_EOL;
            echo "\r\n";
        }
        unset($data);
        exit();
    }
}

/**
 * 根据支付产品获取支付类型
 *
 * @param  array $productId 支付产品ID
 * @param  array $paytype   支付类型
 */
function getPaytypeByProductId($productId) {
    $payTypes = null;
    try {
        $payTypes = Db::name('pay_type')->select();

        foreach ($payTypes as $item) {
            if ($item['sub_lists']) {
                $item['sub_lists'] = json_decode($item['sub_lists']);
            }
        }
    } catch (\think\Exception $e) {
        $payTypes = null;
    }

    if (empty($payTypes)) {
        $payTypes = config('paytype_config');
    }

    $paytype = [];
    foreach ($payTypes as $k => $v) {
        if ($v['product_id'] == $productId) {
            array_push($paytype, $k);
        }
    }
    return $paytype;
}

/**
 * 判断是否QQ号
 *
 * @param b
 */
function isQQ($qq) {
    if (preg_match("/^[1-9][0-9]{4,11}$/", $qq)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 数组转换成xml字符串
 *
 * @param $arr
 *
 * @return string
 */
function arrayToXml($arr) {
    $xml = "<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------xml version='1.0' encoding='UTF-8'?><xml>";
    foreach ($arr as $key => $val) {
        $xml .= "<$key><![CDATA[$val]]></$key>";
    }
    $xml .= "</xml>";
    return $xml;
}

/**
 * xml转换成数组
 *
 * @param $xml
 *
 * @return array|mixed|object
 */
function xmlToArray($xml) {
    libxml_disable_entity_loader(true);
    $arr = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    return $arr;
}

/**
 * post 请求
 *
 * @param     $param
 * @param     $url
 * @param int $second
 *
 * @return mixed
 * @throws Exception
 */
function postCurl($url, $param, $second = 30, $refer = '') {
    $ch = curl_init();

    //设置 Refer
    if ($refer) {
        curl_setopt($ch, CURLOPT_REFERER, $refer); //防封域名
    }

    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $header[] = 'ContentType:application/json;charset=UTF-8';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    //设置超时
    curl_setopt($ch, CURLOPT_TIMEOUT, $second);
    curl_setopt($ch, CURLOPT_URL, $url);
    //设置header
    curl_setopt($ch, CURLOPT_HEADER, false);
    //要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //post提交方式
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
    //运行curl
    $data = curl_exec($ch);

    //返回结果
    if ($data) {
        curl_close($ch);
        return $data;
    } else {
        $error = curl_errno($ch);
        curl_close($ch);
        throw new Exception("curl出错，错误码:$error");
    }
}

/**
 * 上传文件
 *
 * @param string $name
 *
 * @return array
 */
function upload($name = 'file') {
    $file = \think\Request::instance()->file($name);
    if ($file) {
        $ext      = pathinfo($file->getInfo('name'), 4);
        $md5      = [uniqid(), uniqid()];
        $filename = join('/', $md5) . ".{$ext}";
        if (strtolower($ext) == 'php' || !in_array(strtolower($ext), explode(',', strtolower(sysconf('storage_local_exts'))))) {
            return ['code' => 'ERROR', 'msg' => '文件上传类型受限'];
        }
		
		// 阿里云文件上传处理
		if(sysconf('storage_type') == 'oss') {
			$site_url =  FileService::oss('file/'.$filename,$file->getInfo('tmp_name'),'uploadFile');
			if ($site_url) {
                return ['site_url' => $site_url['url'], 'code' => 'SUCCESS', 'msg' => '文件上传成功'];
            } else {
                return ['code' => 'ERROR', 'msg' => '获取上传文件' . $filename . '失败'];
            }
		}		
		
        // 本地文件上传处理
        if (($info = $file->move('static' . DS . 'uploads' . DS . $md5[0], $md5[1], true))) {
			$site_url = FileService::getFileUrl($filename, 'local');            
			if ($site_url) {
                return ['site_url' => $site_url, 'code' => 'SUCCESS', 'msg' => '文件上传成功'];
            } else {
                return ['code' => 'ERROR', 'msg' => '获取上传文件' . $filename . '失败'];
            }
        }
    }

    return ['code' => 'ERROR', 'msg' => '文件上传失败'];
}

/**
 * 检查 ip 当日注册次数
 *
 * @param $ip
 *
 * @return bool
 */
function registerIpCheck($ip) {
    $count = Db::name('user')
               ->where('ip', '=', $ip)
               ->whereTime('create_at', 'today')->count('id');

    if ($count >= sysconf('ip_register_limit')) {
        return false;
    } else {
        return true;
    }
}

/**
 * 正确返回
 *
 * @param $data
 * @param $msg
 *
 * @return array
 */
function right($data, $msg) {
    return [
        'status' => true,
        'data'   => $data,
        'msg'    => $msg,
    ];
}

/**
 * 错误返回
 *
 * @param $msg
 * @param $code
 *
 * @return array
 */
function wrong($msg, $code = 500) {
    return [
        'status' => false,
        'code'   => $code,
        'msg'    => $msg,
    ];
}

/**
 * 请求成功
 *
 * @param     $data
 * @param     $msg
 * @param int $code
 *
 * @throws \think\exception\HttpResponseException
 */
function success($data = [], $msg = '提交成功！', $code = 200) {
    ob_clean();

    record_file_log('request_params', 'result :' . json_encode($data));

    $type = \think\Config::get('default_ajax_return');

    $result = [
        'code' => $code,
        'data' => !empty($data) ? $data : new \stdClass(),
        'msg'  => $msg,
    ];

    $response = \think\Response::create($result, $type);

    throw new \think\exception\HttpResponseException($response);
}

/**
 * request error
 *
 * @param int    $code
 * @param string $msg
 *
 * @throws \think\exception\HttpResponseException
 */
function error($code = 500, $msg = '提交失败') {
    ob_clean();

    record_file_log('request_params', 'result :' . $msg);

    $type = \think\Config::get('default_ajax_return');

    $result = [
        'code' => $code,
        'data' => new \stdClass(),
        'msg'  => $msg,
    ];

    $response = \think\Response::create($result, $type);

    throw new \think\exception\HttpResponseException($response);
}

/**
 * 获取上传文件
 *
 * @param      $field
 * @param bool $url
 *
 * @return array
 */
function getUploadFile($field, $url = false, $extison = []) {
    $request = \think\Request::instance();
    $file    = $request->file($field);
    if (!$file) {
        return wrong('上传文件不存在');
    }

    $ext      = pathinfo($file->getInfo('name'), 4);
    $md5      = [uniqid(), uniqid()];
    $filename = join('/', $md5) . ".{$ext}";
    if (!empty($extison)) {
        if (!in_array($ext, $extison)) {
            return wrong('文件上传类型受限');
        }
    } else {
        if ($ext == 'php' || !in_array(strtolower($ext), explode(',', strtolower(sysconf('storage_local_exts'))))) {
            return wrong('文件上传类型受限');
        }
    }

    // 文件上传处理
    if (($info = $file->move('static' . DS . 'upload' . DS . $md5[0], $md5[1], true))) {
        if ($url) {
            $site_url = FileService::getFileUrl($filename, 'local');
            if ($site_url) {
                return right(['file' => $site_url, 'filename' => $filename, 'resource' => $info], '上传成功');
            } else {
                return wrong('获取上传文件' . $filename . '失败');
            }
        } else {
            return right(['file' => ROOT_PATH . '/static/upload/' . $filename, 'filename' => $filename, 'resource' => $info], '上传成功');
        }
    } else {
        return wrong('保存上传文件' . $filename . '失败');
    }
}

/**
 * 二要素校验
 *
 * @param $idcard
 * @param $name
 *
 * @return bool
 */
function idcardAuth($idcard, $name) {
    $path    = sysconf('idcard_auth_path');
    $method  = "GET";
    $appcode = sysconf('idcard_auth_appcode');
    $headers = array();
    array_push($headers, "Authorization:APPCODE " . $appcode);
    $querys = "idCard=$idcard&name=$name";
    $bodys  = "";
    $url    = $path . "?" . $querys;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_FAILONERROR, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    //curl_setopt($curl, CURLOPT_HEADER, true);   如不输出json, 请打开这行代码，打印调试头部状态码。
    //状态码: 200 正常；400 URL无效；401 appCode错误； 403 次数用完； 500 API网管错误
    if (1 == strpos("$" . $path, "https://")) {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    $out_put            = curl_exec($curl);
    $CURLINFO_HTTP_CODE = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    if ($CURLINFO_HTTP_CODE == 200) {
        if ($out_put) {
            $out_put = json_decode($out_put, 1);
            if ($out_put[sysconf('idcard_auth_status_field')] == sysconf('idcard_auth_status_code')) {
                return true;
            }
        }
    } else if ($CURLINFO_HTTP_CODE == 403) {
        record_file_log('idcard_auth', 'result :' . $CURLINFO_HTTP_CODE . '剩余次数不足');
    } else if ($CURLINFO_HTTP_CODE == 400) {
        record_file_log('idcard_auth', 'result :' . $CURLINFO_HTTP_CODE . 'URL无效');
    } else if ($CURLINFO_HTTP_CODE == 401) {
        record_file_log('idcard_auth', 'result :' . $CURLINFO_HTTP_CODE . 'APPCODE错误');
    } else {
        record_file_log('idcard_auth', 'result :' . $CURLINFO_HTTP_CODE . '未知错误');
    }


    return false;
}

function idcardnoMask($idcardno){
    if(strlen($idcardno) == 15) {
        $idcardno = substr_replace($idcardno,"****",6,6);
    } elseif(strlen($idcardno) == 18) {
        $idcardno = substr_replace($idcardno,"********",6,8);
    } else {
        $idcardno = '';
    }
    return $idcardno;
}
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\logic\html;

use think\Db;

class Pview extends Base
{
	// 源码快照
	public function snap($param)
	{
		$down = Db::name('down')->where('ddbh',$param['bh'])->find();
		$data = Db::name($down['type'])->where('bh',$down['codebh'])->find();
		$data['txt'] = txt($data);
		$this->assign('data',$data);		
		return $this->fetch('html/pview/snap/buy');
	}
	
	// 商品审核弹窗
	public function audit($param)
	{
		$data = Db::name($param['type'])->where([
			'bh'  => $param['bh'],
			'ubh' => session('usercode')
		])->find();
		$data['txt']     = txt($data);
		$data['zt_name'] = null;
		if($data['zt'] == 0) {		
			$data['zt_name'] = '审核中';
		}
		if($data['zt'] == 1) {		
			$data['zt_name'] = '出售中';
		}		
		if($data['zt'] == 2) {
			$data['zt_name'] = '审核未通过';
		}
		$this->assign('data',$data);		
		return $this->fetch('html/pview/audit/'.$param['type']);	
	}
}
?>
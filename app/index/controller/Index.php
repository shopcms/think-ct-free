<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Request;
use think\Db;
use thinkct\library\ForeachType;

class Index extends BaseIndex
{
    public function index()
    {	
		$list3 = [];
		$serve_list1 = [];
		$serve_list2 = [];
		// 网站公告
		$gonggao = Db::name('gonggao')->where('fl','in','0,1')->order('id desc')->limit(5)->select();
		// 网站数据
		$operational_data_one = ForeachType::operational_data_one();
		$operational_data     = ForeachType::operational_data();
		// 交易订单
		$orderlist = ForeachType::orderlist(
			Db::name('down')->where('type','code')->order('id desc')->limit(10)->select()
		);
		// 类型列表
		$list_type  = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'源码类型'
		]);		
		$list_brand = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'系统品牌'
		], "img");		
		$list_lang  = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'开发语言'
		]);		
		$list_sql   = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'数据库'
		]);
		// 最新源码
		$code['list'] = ForeachType::list_code(
			Db::name('code')->where('zt',1)->order('id desc')->limit(5)->select()
		);
		// 推荐源码
		$code['tuijian'] = Db::name('tuiguang')->where([
			'dsj'=>['>',sj()],'ads'=>1,'zt'=>1	
		])->order('id desc')->limit(5)->select();
		$list1 = [];
		foreach($code['tuijian'] as $data){
			$sell = db('sell')->where('id|bh',$data['ubh'])->find();
			$row = db('code')->where('bh',$data['pro'])->whereor('id',$data['pro'])->find();
			$data['tit'] = $row['tit'];
			$data['url'] = c_url($row['id'], "code");
			$data['money'] = $row['money'];
            $data['img'] = get_tp($row['bh']);
			$data['sell_tx'] = $sell['tx'];
			$data['sell_name'] = $sell['name'];
			$data['sell_url'] = "/ishop{$sell['id']}/";
			$list1[] = $data;
		}
		$code['tuijian'] = $list1;	

		// 求购源码
		$code['demand'] = ForeachType::list_demand(
			Db::name('demand')->where('type','code')->order('lastgx desc')->limit(10)->select()
		);
		
		//推荐商家
		$code['shop'] = Db::name('tuiguang')->where([
			'dsj' => ['>',sj()],'ads'=>3,'zt'=>1	
		])->order('id desc')->limit(5)->select();	
		
		foreach($code['shop'] as $data){
			
			$sell = db('sell')->where('id|bh',$data['pro'])->find();
			$data['xy'] = xy($sell['rev_m'],"s");
			$data['sell_tx'] = $sell['tx'];
			$data['sell_name'] = $sell['name'];
			$data['sell_url'] = "/ishop{$sell['id']}/";
			// 店铺商品
			$row = Db::name('code')->where('ubh',$sell['bh'])->find();
			$shp_code_count = Db::name('code')->where('ubh',$sell['bh'])->count();
			$data['tit'] = $row['tit'];
			$data['img'] = get_tp($row['bh']);
			$data['money'] = $row['money'];
			$data['url'] = c_url($row['id'], 'code');
			$data['count'] = $shp_code_count; 
			$list3[] = $data;
		}
		$code['shop'] = $list3;			
		
		// 最新服务
		$serve['list'] = ForeachType::list_serve(
			Db::name('serve')->where('zt',1)->order('id desc')->limit(5)->select()
		);
		// 推荐服务 // 目前没有调用
		$serve['tuijian'] = Db::name('tuiguang')->where([
			'dsj'=>['>',sj()],'ads'=>1,'zt'=>1	
		])->limit(5)->select();
		
		foreach($serve['tuijian'] as $data){
			
			$sell = db('sell')->where('bh',$data['ubh'])->find();
			$row = db('serve')->where('bh',$data['pro'])->whereor('id',$data['pro'])->find();
			$data['tit'] = $row['tit'];
			$data['url'] = c_url($row['id'], "serve");
            $data['img'] = get_tp($row['bh']);
			$data['sell_tx'] = $sell['tx'];
			$data['sell_name'] = $sell['name'];
			$data['sell_url'] = "/ishop{$sell['id']}/";
			$serve_list2[] = $data;
		}
		$serve['tuijian'] = $serve_list2;			
		
		
		// 推荐商家 服务待写
		
		
		// 最新网站
		$data['web_list'] = ForeachType::list_web(
			Db::name('web')->where(['zt'=>1])
			->order('id desc')->limit(10)->select()
		);		
		// 网站求购
		$data['web_demand'] = ForeachType::list_demand(
			Db::name('demand')->where(['zt'=>1,'type'=>'web'])
			->order('id desc')->limit(30)->select()
		);
		// 最新单米
		$data['domain_list'] = ForeachType::list_domain(
			Db::name('domain')->where(['zt'=>1,'type'=>0])
			->order('id desc')->limit(15)->select()
		);			
		// 最新多米
		$data['domain_lists'] = ForeachType::list_domain(
			Db::name('domain')->where(['zt'=>1,'type'=>1])
			->order('id desc')->limit(10)->select()
		);			
		// 域名求购
		$data['domain_demand'] = ForeachType::list_demand(
			Db::name('demand')->where(['zt'=>1,'type'=>'domain'])
			->order('id desc')->limit(10)->select()		
		);		
		// 任务大厅
		$data['task_list'] = ForeachType::list_task(
			Db::name('task')->where(['zt'=>1])
			->order('id desc')->limit(6)->select()
		);	
		$this->assign('data',$data);
		// 新闻资讯
		$article['list1'] = Db::name('article')->where(['hotspot'=>1,'status'=>1])->limit(6)->order('top desc,id desc')->select();
		$article['list2'] = Db::name('article')->where('status',1)->limit(5)->order('top desc,id desc')->select();
		$article['list3'] = Db::name('article')->where('status',1)->limit(5,7)->order('views desc,id desc')->select();		

		// 友情链接
		$link['partner'] = Db::name('link')->where('type','partner')->order('sort desc')->select();
		$link['link']    = Db::name('link')->where('type','link')->order('sort desc')->select();
		
		$this->assign('code', $code);
		$this->assign('serve', $serve);
		$this->assign('article', $article);
		$this->assign('link', $link);		
		
		$this->assign('operational_data_one', $operational_data_one);
		$this->assign('gonggao', $gonggao);
		$this->assign('operational_data', $operational_data);
		$this->assign('orderlist', $orderlist);
		$this->assign('list_type', $list_type);
		$this->assign('list_brand', $list_brand);
		$this->assign('list_lang', $list_lang);
		$this->assign('list_sql', $list_sql);		
		return $this->fetch();
    }
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;

class Task extends BaseIndex
{
    public function index($query = [],$page_size = 24,$view = null)
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$screen_on  = $search->screen_on();
		$order      = $search->order();
		$page       = $search->page();
		// 类型列表
		$list['type'] = ForeachType::typelist([
			'admin'=>2,'type'=>'task','name1'=>'类型'
		]);
		// 排序分类
		$sort['order'] = ForeachType::sort_order([
			['name'=>'综合','type'=>''],
			['name'=>'最新','type'=>'time'],
			['name'=>'价格','type'=>'am']
		]);
		// 条件选择
		$sort['checkbox'] = ForeachType::sort_checkbox([
			['name'=>'已成交','type'=>'sold','value'=>1],
			['name'=>'已托管赏金','type'=>'trust','value'=>1]
		]);
		// 推广列表
		$data['tuiguang'] = ForeachType::list_serve(
			Db::name('serve')->where(['zt'=>1])
			->order('djl desc')->limit(5)->select()
		);		
		// 列表数据
		$data['list'] = ForeachType::list_task(
			Db::name('task')->where(['zt'=>1])->where($query)
			->order($order)->page($page,$page_size)->select()
		);
		// 列表数量
		$count['list'] = Db::name('task')->where(['zt'=>1])->where($query)->count();
		// 页数数量
		$count['page'] = (int)($count['list']/$page_size)+1;
		// 页数模板
		$pages = $this->page($count['list'],$page_size,$page);	
		$this->assign('list',$list);
		$this->assign('sort',$sort);
		$this->assign('data',$data);
		$this->assign('screen_on',$screen_on);
		$this->assign('count',$count);
		$this->assign('page',$page);	
		$this->assign('pages',$pages);
		return $this->fetch($view);
    }

	public function goods($id)
	{	
		// 增加点击量
		Db::name('task')->where('id',$id)->setInc('djl');	
		
		$row  = Db::name('task')->where(['id'=>$id,'zt'=>1])->find();
		$buy  = Db::name('buy')->where('bh',$row['ubh'])->find();
		$list = Db::name('task')->where('zt',1)
		->order('djl desc')->limit(10)->select();
		if($row['moneytype']==0){
			$row['type']  = '一口价';
			$row['money'] = '￥'.$row['money'];
		}elseif($row['moneytype']==3){
			$row['type']  = '开放报价';
			$row['money'] = '￥@';
		}else{
			$row['type']  = '范围报价';
			$row['money'] = '￥'.$row['am'].'-'.$row['dm'];
		}					
		
		$this->assign('row',$row);
		$this->assign('buy',$buy);
		$this->assign('list',$list);
		return $this->fetch();
	}


}
?>
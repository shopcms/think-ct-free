<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;

class Serve extends BaseIndex
{
    public function index($where=['zt'=>1],$view=null,$page_size=12)
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$screen_on  = $search->screen_on();
		$order      = $search->order();
		$page       = $search->page();
		// 类型列表
		$list['type'] = ForeachType::typelist([
			'admin'=>2,'type'=>'serve','name1'=>'服务市场'
		]);
		// 排序分类
		$sort['order'] = ForeachType::sort_order([
			['name'=>'综合','type'=>''],
			['name'=>'最新','type'=>'time'],
			['name'=>'销量','type'=>'sales'],
			['name'=>'评分','type'=>'score']
		]);
		// 条件选择
		$sort['checkbox'] = ForeachType::sort_checkbox([
			['name'=>'消保赔付','type'=>'bond','value'=>1]
		]);
		// 推广列表
		$data['tuiguang'] = ForeachType::list_serve(
			Db::name('serve')->where(['zt'=>1])->where($query)
			->order('djl desc')->limit(5)->select()
		);
		// 列表数据
		$data['list'] = ForeachType::list_serve(
			Db::name('serve')->where($where)->where($query)
			->order($order)->page($page,$page_size)->select()
		);
		// 列表数量
		$count['list'] = Db::name('serve')->where($where)->where($query)->count();
		// 页数数量
		$count['page'] = (int)($count['list']/$page_size)+1;
		// 页数模板
		$pages = $this->page($count['list'],$page_size,$page);	
		$this->assign('list',$list);
		$this->assign('sort',$sort);
		$this->assign('data',$data);
		$this->assign('screen_on',$screen_on);
		$this->assign('count',$count);
		$this->assign('page',$page);	
		$this->assign('pages',$pages);
		return $this->fetch($view);
    }

	public function goods($id) 
	{
		// 增加点击量
		Db::name('serve')->where('id',$id)->setInc('djl');	
		
		$row = Db::name('serve')->where(['id'=>$id,'zt'=>1])->find();
		if(!$row) {
			return prompt('serve',$id);
		}
		$row['img'] = get_tp($row['bh']);
        $row['txt'] = txt($row);
		$shop = ForeachType::shop($row);
		$cart = Db::name('cart')->where(['codebh'=>$row['bh'],'ubh'=>session('usercode')])->find();		
		$list = Db::name('serve')->where(['zt'=>1,'ubh'=>$row['ubh']])->order('xsnum desc')->limit(10)->select();
        foreach($list as $data){		
			$data['url'] = c_url($data['id'], 'code');
            $data['img'] = get_tp($data['bh']);
            $list_hot[]=$data;
        }
		// 动态价格
		$moneydata = [];
		$moneydes  = explode(',',$row['moneydes']);
		foreach($moneydes as $v=>$k){
			$allmoney = explode(',',$row['allmoney']);
			$mdata['money'] = $allmoney[$v];
			$mdata['ydes']  = $moneydes[$v];
			$moneydata[] = $mdata;
		}
		if($row['tmoney'] == 1) {
			$row['money'] = $allmoney[0];
		}
		$tmoney = money_tmoney($row);
		$this->assign('shop',$shop);		
		$this->assign('row',$row);
		$this->assign('cart',$cart);
		$this->assign('tmoney',$tmoney);
		$this->assign('moneydata',$moneydata);
		$this->assign('list_hot',$list_hot);
		return $this->fetch();
	}
}
?>
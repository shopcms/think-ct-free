<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;

class Demand extends BaseIndex
{
	public function index($type='code')
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$screen_on  = $search->screen_on();
		$order      = $search->order();
		$page       = $search->page();
		$page_size  = 24;
		// 类型列表
		$list['type'] = ForeachType::typelist([
			'admin'=>2,'type'=>'codebuy','name1'=>'源码类型'
		]);
		// 排序分类
		$sort['order'] = ForeachType::sort_order([
			['name'=>'综合','type'=>''],
			['name'=>'最新','type'=>'time'],
			['name'=>'价格','type'=>'am']
		]);
		// 列表数据
		$data['list'] = $this->data_list(
			Db::name('demand')->where(['zt'=>1,'type'=>$type])->where($query)
			->order($order)->page($page,$page_size)->select()
		);
		// 列表数量
		$count['list'] = Db::name('demand')->where(['zt'=>1,'type'=>$type])->where($query)->count();
		// 页数数量
		$count['page'] = (int)($count['list']/$page_size)+1;
		// 页数模板
		$pages = $this->page($count['list'],$page_size,$page);	
		$this->assign('list',$list);
		$this->assign('sort',$sort);
		$this->assign('data',$data);
		$this->assign('screen_on',$screen_on);
		$this->assign('count',$count);
		$this->assign('page',$page);	
		$this->assign('pages',$pages);
		return $this->fetch('demand/index/'.$type);
	}
	
	public function data_list($data)
	{	
		$list = [];
		foreach($data as $data){
			$buy = Db::name('buy')->where('bh',$data['ubh'])->find();
			$data['url']   = c_url($data['id'],'demand');
			$data['name']  = $buy['name'];
			$data['tx']    = $buy['tx'];
			$list[]        = $data;
		}
		
		return $list;
	}
	
	public function goods($id)
	{
		$data  = Db::name('demand')->where(['id'=>$id,'zt'=>1])->find();
		// 增加点击量
		Db::name('demand')->where('id',$id)->setInc('djl');		
		$buy   = Db::name('buy')->where('bh',$data['ubh'])->find();
		$list = Db::name('demand')->where(['zt'=>1,'type'=>$data['type']])
		->order('djl desc')->limit(10)->select();
		
		$this->assign('data',$data);
		$this->assign('buy',$buy);
		$this->assign('list',$list);
		return $this->fetch('demand/goods/'.$data['type']);
	}	
}
?>
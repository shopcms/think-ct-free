<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;

class Upload extends BaseIndex
{
	public function index()
	{
		$action = input('action',null);
		if(isset($_POST['action'])) {
			return $this->$action();
		}
	}
	
	// 添加图片
	private function add()
	{
		$data = [
			'name'     => $_POST['name'],
			'lastDate' => $_POST['lastDate'],
			'osize'    => $_POST['osize'],
			'number'   => $_POST['number'],
			'action'   => $_POST['action'],
			'type'     => $_POST['type'],
			'maxnum'   => $_POST['maxnum'],
		];		
		$upload = upload('file');
		if($upload['code'] == 'SUCCESS') {
			$dd = Db::name('file')->insert([
				'sj'       => sj(),
				'uip'      => uip(),
				'bh'       => $data['number'],
				'ubh'      => session('usercode'),
				'admin'    => 2,
				'type'     => $data['type'],
				'name'     => $_FILES['file']['name'],
				'tmp_name' => shop_bh().'.png',
				'url'      => $upload['site_url']
			]);
		}else{
			return json(['state'=>'1','info'=>'上传失败！']);
		}			
	}
	
	// 删除图片
	private function del()
	{
		$data = [
			'p'      => $_POST['p'],
			'object' => $_POST['object'],
			'type'   => $_POST['type']
		];
		$row = Db::name($data['type'])->where([
			'bh'=>$data['p'],
			'ubh'=>session('usercode')
		])->find();
		if($row) {
			$psort = str_replace($data['object'],'',$row['psort']);
			Db::name($data['type'])->where('bh',$row['bh'])->update(['psort'=>$psort]);
		}
		$dd = Db::name('file')->where(['bh'=>$data['p'],'tmp_name'=>$data['object']])->delete();
		exit('1');
	}	
}
//{"state":"0","idiv":"<div><p class='imgWrap'><img src='\/\/img.huzhan.com\/file\/u15868334066\/code\/159842366055\/66293842432b868d081c3936162cb410.png!\/fw\/190\/canvas\/190x152a0a0'><span class='icon'>1<\/span><\/p><p class='file-panel' style='height: 0px; overflow: hidden;'><a class='fleft' title='\u524d\u79fb\uff08\u8c03\u6574\u6392\u5e8f\uff09'><span><\/span><i><\/i><\/a><a class='fdel' id='66293842432b868d081c3936162cb410.png' title='\u5220\u9664\uff08\u4e0d\u53ef\u6062\u590d\uff09'><span><\/span><i><\/i><\/a><a class='fright' title='\u540e\u79fb\uff08\u8c03\u6574\u6392\u5e8f\uff09'><span><\/span><i><\/i><\/a><\/p><\/div>"}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;

class Jifen extends BaseIndex
{
	var $page_size = 20;
	
	public function index()
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$screen_on  = $search->screen_on();
		$style_mode = $search->style_mode();
		$order      = $search->order();
		$page       = $search->page();		
		// 推荐列表
		$list['tuijian'] = Db::name('jifen_cms')->where('1=1')
		->order('id desc')->limit(2)->select();	
		// 列表数据
		$list['data'] = Db::name('jifen_cms')->where('1=1')->where($query)
		->order($order)->page($page,$this->page_size)->select();
		// 排序列表
		$sort['order']    = ForeachType::sort_order([
			['name'=>'默认','type'=>''],
			['name'=>'最新','type'=>'time'],
			['name'=>'销量','type'=>'sales'],
			['name'=>'价格','type'=>'am']
		],'','current');		
		// 列表数量	
		$count = Db::name('jifen_cms')->where($query)->count();		
		// 页数数量
		$page_count = (int)($count/$this->page_size)+1;
		// 页数模板
		$pages = $this->page($count,$this->page_size,$page);	
		$this->assign('list',$list);
		$this->assign('sort',$sort);
		$this->assign('pages',$pages);
		$this->assign('screen_on',$screen_on);
		return $this->fetch();
	}	
	
	public function view($id)
	{
		$data = Db::name('jifen_cms')->where(['id'=>$id,'zt'=>1])->find();
		// 推荐列表
		$list = Db::name('jifen_cms')->where('1=1')->order('id desc')->limit(2)->select();			
		
		$this->assign('data',$data);
		$this->assign('list',$list);
		return $this->fetch();	
	}	
}
?>
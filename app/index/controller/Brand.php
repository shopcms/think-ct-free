<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;

class Brand extends BaseIndex
{
	var $page_size = 20;
	
	public function index()
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$screen_on  = $search->screen_on();
		$style_mode = $search->style_mode();
		$order      = $search->order();
		$page       = $search->page();
		// 类型列表
		$list['type'] = ForeachType::typelist([
			'admin'=>2,'type'=>'brand','name1'=>'品牌分类'
		],'list','current');		
		// 推荐列表
		$list['tuijian'] = ForeachType::list_brand(
			Db::name('brand')->where('1=1')
			->order('id desc')->limit(10)->select()
		);	
		// 列表数据
		$list['data'] = ForeachType::list_brand(
			Db::name('brand')->where('1=1')->where($query)
			->order($order)->page($page,$this->page_size)->select()
		);	
		// 列表数量	
		$count = Db::name('brand')->where($query)->count();		
		// 页数数量
		$page_count = (int)($count/$this->page_size)+1;
		// 页数模板
		$pages = $this->page($count,$this->page_size,$page);	
		$this->assign('list',$list);
		$this->assign('pages',$pages);
		$this->assign('screen_on',$screen_on);
		return $this->fetch();
	}	
	
	public function view($name)
	{
		$data = Db::name('brand')->where(['url'=>$name,'zt'=>1])->find();
		if(!$data){return prompt('brand',$data['id']);}
		$data['img'] = get_tp($data['bh']);
		$data['txt'] = txt($data);
		$type        = thinkct()->path($data['url']);
		// 推荐列表
		$list = ForeachType::list_brand(
			Db::name('brand')->where('1=1')
			->order('id desc')->limit(10)->select()
		);		
		$this->assign('data',$data);
		$this->assign('list',$list);
		$this->assign('type',$type);
		return $this->fetch();	
	}	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\index\controller;


use thinkct\controller\BaseIndex;use alipay\Alipay;
use phpqrcode\QRcode;
use think\Controller;
use think\facade\Env;
use lib\AliPayPc;
use think\Request;


class Pay extends Controller
{
    //支付宝扫码付
    public function order()
    {
        //构建订单参数
        $out_trade_no = 32;//订单号写死测试
        $subject='测试';
        $total_fee=0.1;
        $uid=1;
        $username='demo';
        $type=1000;
        $_var_20 = [
            'notify_url' => $_SERVER['REQUEST_SCHEME'] . '://' . "{$_SERVER['HTTP_HOST']}/index/alipay/notify_url",
            'out_trade_no' => $out_trade_no,
            'subject' => $subject.'_'.$uid,//支付显示的名称 可以自己拼接参数 比如：用户ID
            'total_fee' =>$total_fee,
            'body' =>$uid,
        ];

        $_var_21 = new Alipay();
        $_var_22 = $_var_21->qrpay($_var_20);
        if (!$_var_22['code']) {
            return json(['status' => '0', 'data' => $_var_22['msg']]);
        }

        $_var_23['uid'] = $uid;//付款用户ID
        $_var_23['type'] = $type;//区分支付类型和场景
        $_var_23['username'] = $username;//用户表中的账号
        $_var_23['total_amount'] = $total_fee;//订单生成金额
        $_var_23['receipt_amount'] = '';//实际付款金额
        $_var_23['out_trade_no'] = $out_trade_no;//订单号
        $_var_23['trade_no'] = '';//交易号
        $_var_23['subject'] = $subject;//支付名称，自定义
        $_var_23['create_time'] = date('Y-m-d H:i:s',time());//订单创建时间
        $_var_23['trade_status'] = 'WAIT_BUYER_PAY';//默认支付状态，支付成功后在回调中修改状态
        db('order')->insert($_var_23);//写入订单表
        $pay_url=$_var_22['msg'];//支付链接
        $_var_25 = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/index/pay/qr?data=' . $pay_url;

        //为了方便测试，我把参数都在页面显示出来
        return view('index', [ 'alipay_url' => $pay_url, 'alipay_url_qr' => $_var_25, 'out_trade_no' => $out_trade_no,'good_name'=>$username,'money'=>$total_fee,'time'=>date('Y-m-d H:i:s',time()+(5*60)-2)]);
    }

    //生成二维码
    public function qr()
    {
        $path=Env::get('root_path');
        require $path.'extend/phpqrcode/phpqrcode.php';
        $_var_26 = $_GET['data'];
        QRcode::png($_var_26);
    }


    /**
     * 支付宝商户收款异步回调
     */
    public function notify_url()
    {
        $arr = $_POST;
        $result = AliPayPc::setNotify_url($arr);//进行验签
        //验签成功后
        //判断订单号是否存在数据库
        //进行业务处理----------------------
    }
    public function MtAliPay(Request $request)
    {
        $out_trade_no = "19982019121202021000";//订单号
        $subject = "测试商品000";//商品名称
        $total_amount = 0.1;//商品金额 元
        $body = "这是用于测试";//商品描述
        $config_data = [
            'return_url'=>$request->domain().'/index.php/admin/user/return_url',//同步跳转 待写
            'notify_url'=>$request->domain().'/index.php/admin/callback/notify_url',//异步跳转 待写
        ];
        //业务代码，比如生成订单记录

        //因为声明为静态类，所以直接调用
        AliPayPc::setAilPay($out_trade_no,$subject,$total_amount,$body,$config_data = []);
    }

    //微信支付
    public function wx_pay()
    {
        header("Content-type:text/html;charset=utf-8");

        require EXTEND_PATH.'/weixin/WxPay.Api.php'; //引入微信支付
        require EXTEND_PATH.'/phpqrcode/phpqrcode.php'; //引入二维码
        $input = new \WxPayUnifiedOrder();//统一下单
        $config = new \WxPayConfig();//配置参数
        $paymoney = 1; //测试写死
        $out_trade_no = 'WXPAY'.date("YmdHis"); //商户订单号(自定义)
        $goods_name = '扫码支付'.$paymoney.'元'; //商品名称(自定义)
        $input->SetBody($goods_name);
        $input->SetAttach($goods_name);
        $input->SetOut_trade_no($out_trade_no);
        $input->SetTotal_fee($paymoney*100);//金额乘以100
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag("test");
        $input->SetNotify_url("http://www.xxx.com/wxpaynotify"); //回调地址
        $input->SetTrade_type("NATIVE");
        $input->SetProduct_id("123456789");//商品id
        $result = \WxPayApi::unifiedOrder($config, $input);

        if($result['result_code']=='SUCCESS' && $result['return_code']=='SUCCESS') {
            $url = $result["code_url"];
            $this->assign('url',$url);
        }else{
            $this->error('参数错误');
        }
        if (empty($result['code_url'])){
            $qrCode_url = '';
        }else{
            $qrCode_url = $result["code_url"];
        }

        \QRcode::png($qrCode_url);
        exit();

    }

    /**
     * 微信支付 回调逻辑处理
     * @return string
     */

    public function wxpaynotify() {
        // 获取微信回调的数据
        $notifiedData = file_get_contents('php://input');

        //XML格式转换
        $xmlObj = simplexml_load_string($notifiedData, 'SimpleXMLElement', LIBXML_NOCDATA);
        $xmlObj = json_decode(json_encode($xmlObj), true);

        //支付成功
        if ($xmlObj['return_code'] == "SUCCESS" && $xmlObj['result_code'] == "SUCCESS") {
            foreach ($xmlObj as $k => $v) {
                if ($k == 'sign') {
                    $xmlSign = $xmlObj[$k];
                    unset($xmlObj[$k]);
                };
            }
            $sign = $this->WxSign($xmlObj);
            if ($sign === $xmlSign) {
                $trade_no = $xmlObj['out_trade_no']; //商户自定义订单号
                $transaction_id = $xmlObj['transaction_id']; //微信交易单号

                //省略订单处理逻辑...

                //返回成功标识给微信
                return sprintf("<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>");
            }

        }
    }

//微信签名算法
    private function WxSign($param)
    {
        $signkey = 'xxx';//秘钥
        $sign = '';
        foreach ($param as $key => $val) {
            $sign .= $key . '=' . $val . '&';
        }
        $sign .= 'key=' . $signkey;
        $sign = strtoupper(MD5($sign));
        return $sign;
    }
}
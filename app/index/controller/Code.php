<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;

class Code extends BaseIndex
{
    public function index($where=['zt'=>1,'show'=>1],$view=null,$page_size=24)
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$screen_on  = $search->screen_on();
		$style_mode = $search->style_mode();
		$order      = $search->order();
		$page       = $search->page();
		$title      = ForeachType::title($screen_on,"源码集市");// 转移至全局搜类	
		// 求购列表
		$list['demand'] = ForeachType::list_demand(
			Db::name('demand')->where(['zt'=>1,'type'=>'code'])
			->order('sj desc')->limit(20)->select()		
		);
		// 掌柜推荐
		$list['tuijian'] = ForeachType::list_tuiguang(
			Db::name('tuiguang')->where(['dsj'=>['>',sj()],'ads'=>2,'zt'=>1])
			->order('id desc')->limit(7)->select()
		);
		// 类型列表
		$list['type'] = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'源码类型'
		]);		
		$list['brand'] = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'系统品牌'
		],'img');		
		$list['lang']  = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'开发语言'
		]);		
		$list['sql']   = ForeachType::typelist([
			'admin'=>2,'type'=>'code','name1'=>'数据库'
		]);	
		// 排序列表
		$sort['order']    = ForeachType::sort_order([
			['name'=>'综合','type'=>''],
			['name'=>'最新','type'=>'time'],
			['name'=>'销量','type'=>'sales'],
			['name'=>'价格','type'=>'am']
		]);
		// 列表样式
		$sort['option']   = ForeachType::sort_option();
		// 条件选择
		$sort['checkbox'] = ForeachType::sort_checkbox([
			['name'=>'消保赔付','type'=>'bond','value'=>1],
			['name'=>'免费安装','type'=>'install','value'=>1],
			['name'=>'自动发货','type'=>'auto','value'=>1],
			['name'=>'有演示站','type'=>'demo','value'=>1],
			['name'=>'含移动端','type'=>'app','value'=>1],
			['name'=>'积分抵价','type'=>'integro','value'=>1]
		]);		
		// 列表数据
		$list['data'] = ForeachType::list_code(
			Db::name('code')->where($where)->where($query)
			->order($order)->page($page,$page_size)->select()
		);
		// 列表数量	
		$count = Db::name('code')->where($where)->where($query)->count();			
		// 页数数量
		$page_count = (int)($count/$page_size)+1;
		// 页数模板
		$pages = $this->page($count,$page_size,$page);	
		$this->assign('title',$title);
		$this->assign('list',$list);
		$this->assign('sort',$sort);
		$this->assign('style_mode',$style_mode);		
		$this->assign('screen_on',$screen_on);
		$this->assign('count',$count);	
		$this->assign('page',$page);
		$this->assign('page_count',$page_count);
		$this->assign('pages',$pages);
		return $this->fetch($view);
    }

	public function goods($id) 
	{
		$row = Db::name('code')->where(['id'=>$id,'zt'=>1])->find();
		if(!$row) {
			return prompt('code',$id);
		}
		if($row['ubh'] == null) {
			return prompt('code',$id);
		}
		// 浏览记录
		browseck('code_'.$id);
		// 增加点击量
		Db::name('code')->where('id',$id)->setInc('djl');
		$shop = ForeachType::shop($row);
		$row['jfz'] = $row['money']*$row['jfnum']*10;
		$row['jfd'] = $row['jfz']/10;		
		$row['img'] = get_tp($row['bh']);
        $row['txt'] = txt($row);		
		$cart = Db::name('cart')->where([
			'codebh'=>$row['bh'],'ubh'=> session('usercode')
		])->find();		
		$list = Db::name('code')->where([
			'zt'=>1,'ubh'=>$row['ubh']
		])->order('xsnum desc')->limit(10)->select();
        foreach($list as $data){
			$data['url'] = c_url($data['id'], 'code');
            $data['img'] = get_tp($data['bh']);
            $list_hot[]  = $data;
        }	
		$this->assign('shop', $shop);		
		$this->assign('row', $row);
		$this->assign('cart', $cart);
		$this->assign('list_hot', $list_hot);
		return $this->fetch();
	}
}
?>
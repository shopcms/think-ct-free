<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;

class Shop extends BaseIndex
{	
	public function index()
	{
		// 会员活跃榜
/* 		$data_user = Db::name('user')->alias('a')
		->where('zt',1)//->whereTime('a.sj','month')
		->join('shop_anquan b','b.ubh = a.bh')
		->field('a.*,count(b.ubh) as num')->group('ubh')	
		->order('num desc')	
		->limit(10)->select();
		$sql = Db::getLastSql(); */
		$data_user = Db::name('user')->alias('a')->join('shop_sign s','s.ubh = a.bh')
		->where(['qcount'=>['<>',0]])->order('qcount desc')->limit(10)->select();		
		$list_user = [];
		foreach($data_user as $data){
			$user         = Db::name('user')->where('bh',$data['ubh'])->find();
			$uc           = ($user['uc']=='')?'buy':$user['uc'];
			$uc           = Db::name($uc)->where('bh',$user['bh'])->find();
			$data['name'] = $uc['name'];
			$data['tx']   = $uc['tx'];
			$list_user[]  = $data;
		}
		// 推荐商家
		$data_user = Db::name('tuiguang')->where([
			'dsj' => ['>',sj()],'ads'=>3,'zt'=>1	
		])->order('id desc')->limit(6)->select();	
		$list_tjsell = [];
		foreach($data_user as $data){
			$sell = Db::name('sell')->where('bh',$data['ubh'])->find();
			$data['name']    = $sell['name'];
			$data['tx']      = $sell['tx'];
			$data['bh']      = $sell['bh'];
			$data['id']      = $sell['id'];
			$data['goods_n'] = $sell['goods_n']; 
			$data['goods']   = Db::name('code')->where('ubh',$sell['bh'])->order('sj desc')->limit(2)->select();		
			$data['xy']      = xy($sell['rev_m'],"s");
			$data['score']   = explode('|',$sell['score']);
			$list_tjsell[]   = $data;
		}	
		// 商家列表
		$search     = new Search;
		$query      = $search->query('shop');
		$order      = $search->order('shop');
		$page       = $search->page();
		$page_size  = 24;
		// 排序列表
		$sort['order']    = ForeachType::sort_order([
			['name'=>'最新','type'=>''],
			['name'=>'销量','type'=>'sales'],
			['name'=>'宝贝','type'=>'baby'],
			['name'=>'店龄','type'=>'age']
		]);			
		// 列表数据
		$sell = Db::name('sell')->where($query)->where(['goods_n'=>['>',0]])
		->order($order)->page($page,$page_size)->select();
		$list_sell = [];
		foreach($sell as $data){
			$data['goods']  = Db::name('code')->where('ubh',$data['bh'])
			->order('sj desc')->limit(1)->select();
			$data['xy']     = xy($data['rev_m'],"s");
			$list_sell[] = $data;
		}
		$this->assign('list_user',$list_user);
		$this->assign('list_sell',$list_sell);		
		$this->assign('list_tjsell',$list_tjsell);
		// 列表数量	
		$count = Db::name('sell')->where($query)->where(['goods_n'=>['>',0]])->count();			
		// 页数数量
		$page_count = (int)($count/$page_size)+1;
		$pages = $this->page($count,$page_size,$page);
		$this->assign('sort',$sort);		
		$this->assign('count',$count);
		$this->assign('page',$page);
		$this->assign('page_count',$page_count);
		$this->assign('pages',$pages);
		return $this->fetch();
	}	
	
	public function shop($id,$type = null)
	{
		$data = Db::name('sell')->where(['id'=>$id])->find();
		if(!$data) {
			return prompt('shop');
		}
		$shop = ForeachType::shop($data);
		$type = route_name('ishop'.$id);
		if($type != null) {
			$name = ['code','web','serve','domain','evaluation']; 
			if(!in_array($type,$name)){
				return prompt('shop');
			}
			return $this->$type($shop);
		}
		$list['order'] = ForeachType::orderlist(
			Db::name('down')->where([
				'type'=>'code','sell'=>$data['bh']
			])->order('id desc')->limit(10)->select()
		);
		$shop['skill'] = explode(',',$shop['skill']);	
		$code_hot = Db::name('code')->where(['zt'=>1,'ubh'=>$data['bh']])->order('xsnum desc')->limit(10)->select();
		$list['code_hot'] = ForeachType::list_code($code_hot);			
		if($list['code_hot'] == null) {		
			$list['code_hot'] = [];
		}
		// 推荐源码	
		$list['code_tuijian']  = ForeachType::list_code(
			Db::name('code')->where(['zt'=>1,'tj'=>1,'ubh'=>$data['bh']])->order('xsnum desc')->limit(7)->select()
		);
		// 推荐服务
		$list['serve_tuijian'] = ForeachType::list_serve(
			Db::name('serve')->where(['zt'=>1,'tj'=>1,'ubh'=>$data['bh']])->order('xsnum desc')->limit(4)->select()
		);		
		// 幻灯
		$list['slide'] = Db::name('sell_slide')->where(['zt'=>1,'ubh'=>$data['bh']])->select();
		
		$this->assign('shop',$shop);	
		$this->assign('list',$list);		
		return $this->fetch();
	}
	
	public function code($shop)
	{
		$where = ['zt'=>1,'ubh'=>$shop['bh']];
		$code  = new Code;
		$this->assign('shop',$shop);
		return $code->index($where,'code',25);
	}
	
	public function serve($shop)
	{
		$this->success('即将上线！','/code/');
	}

	public function evaluation($shop)
	{
		$this->assign('shop',$shop);	
		return $this->fetch('evaluation');
	}	
}
?>
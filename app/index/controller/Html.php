<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Request;
use think\Db;
use thinkct\library\ForeachType;
use app\common\library\GeetestLib;

class Html extends BaseIndex
{
	// 快捷修改商品弹窗
	public function quickset()
	{
		
		return $this->fetch();
	}
	
	// 网站统计弹窗
	public function stats()
	{
		$web  = Db::name('web')->where(['zt'=>1,'bh'=>input('number')])->find();
		$sell = Db::name('sell')->where('bh',$web['ubh'])->find();		
		// 网站统计截图
		$data  = [];
		$psort = explode(',',$web['psort']);
		foreach($psort as $tmp_name){
			$file = Db::name('file')->where(['admin'=>2,'tmp_name'=>$tmp_name])->find();
			$name['name'] = $file['name'];
			$name['url']  = $file['url'];
			$data[] = $name;
		}	
		$this->assign('web',$web);
		$this->assign('sell',$sell);
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	// 重新选价格
	public function allmoney()
	{
		$serve = Db::name('serve')->where(['zt'=>1,'bh'=>input('number')])->find();		
		// 动态价格
		$moneydata = [];
		$moneydes  = explode(',',$serve['moneydes']);
		foreach($moneydes as $v=>$k){
			$allmoney       = explode(',',$serve['allmoney']);
			$mdata['money'] = $allmoney[$v];
			$mdata['ydes']  = $moneydes[$v];
			$moneydata[]    = $mdata;
		}		
		$this->assign('serve',$serve);
		$this->assign('moneydata',$moneydata);
		return $this->fetch();
	}
	
	// 订单围观
	public function oshare()
	{
		return $this->fetch();
	}

	// 服务商招募
	public function recruit()
	{
		return $this->fetch();
	}

	// 网站代售
	public function entrusts()
	{
		return $this->fetch();
	}
	
	// 新闻详情
	public function article()
	{
		$id    = path('article');
		$data  = Db::name('article')->where('id',$id)->find();
		$list  = Db::name('article')->where('status',1)->order('top desc,id desc')->limit(10)->select();
		$views = Db::name('article')->where('id',$id)->setInc('views');
		$this->assign('data',$data);
		$this->assign('list',$list);
		if(!$data){return '不存在';}		
		return $this->fetch();	
	}
	
	// 商户退款率
	public function expost()
	{
		return $this->fetch();
	}
	
	// 网站验证
	public function webcheck()
	{
		$name = input('name');
		$this->assign('name',$name);
		return $this->fetch();
	}
	
	// 曝光详情
	public function stopinfo()
	{
		$number = input('number');
		
		$data = Db::name('publicity')->where('id',$number)->find();
		$type = Db::name($data['filter'])->where('bh',$data['bh'])->find();
		$shop = Db::name('sell')->where('bh',$type['ubh'])->find();
		// 数据内容
		$data['name'] = hide_name($shop['name']);
		$data['xy']   = xy($shop['rev_m'],'s');
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	// 曝光台
	public function publicity()
	{
		return $this->fetch();
	}	
	
	// 我要认领
	public function claimed()
	{
		return $this->fetch();
	}
	
	// 生成二维码
	public function qrcode()
	{
		require EXTEND_PATH.'/phpqrcode/phpqrcode.php';
		\QRcode::png(input('data'),false,'L',7);exit;	
	}
	
	// 小程序弹窗
	public function wechatapp()
	{
		return '即将上线';
	}
	
	// 提示
	public function prompt()
	{
		$name = route_name('prompt');
		$data['name'] = '抱歉，您访问的信息不存在或审核未通过';
		$data['return'] = null;
		if($name == 'down') {
			$data['name'] ='抱歉，您访问的信息已被发布人关闭（下架）';
			$data['return'] = '[<a href=>返回上一页</a>]';
		}
		if($name == 'shop') {
			$data['name'] ='抱歉，您访问的店铺不存在或审核未通过';
			$data['return'] = '[<a href=/shop>商家专区</a>] ';
		}		
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	// 关于我们
	public function about()
	{
		$path = (path('about')=='')?'index':path('about');
		return $this->fetch('html/about/'.$path);
	}
	
	// 消费保障
	public function protection()
	{
		return $this->fetch();
	}
	
	// 排行榜
	public function top()
	{
		
		return $this->fetch();
	}
	
	// 店铺管理
	public function shopadmin()
	{
		$object = input('object');
		$action = input('action');
		// 自动加载
		return \think\Loader::model('shopadmin')->$object($action);
	}
	
	// 登录白名单设置
	public function whitelist()
	{
		return $this->fetch();
	}
	
	// 列表弹窗
	public function preview()
	{
		// html/preview/code/commend
		$preview          = path('preview');
		$param['type']    = path($preview);
		$param['name']    = path($param['type']);
		// 自动加载
		return \think\Loader::model('preview')->$preview($param);		
	}
	
	// 刷新次数
	public function qrefresh()
	{
		return $this->fetch();
	}
	
	// 建立自助交易
	public function custom()
	{
		return $this->fetch();
	}
	// 验证码
	public function captcha()
	{
		//10>设置session,必须处于脚本最顶部
		//session_start();
		$image = imagecreatetruecolor(100, 30);    //1>设置验证码图片大小的函数
		//5>设置验证码颜色 imagecolorallocate(int im, int red, int green, int blue);
		$bgcolor = imagecolorallocate($image,255,255,255); //#ffffff
		//6>区域填充 int imagefill(int im, int x, int y, int col) (x,y) 所在的区域着色,col 表示欲涂上的颜色
		imagefill($image, 0, 0, $bgcolor);
		//10>设置变量
		$captcha_code = "";
		//7>生成随机的字母和数字
		for($i=0;$i<4;$i++){
		  //设置字体大小
		  $fontsize = 8;    
		  //设置字体颜色，随机颜色
		  $fontcolor = imagecolorallocate($image, rand(0,120),rand(0,120), rand(0,120));      //0-120深颜色
		  //设置需要随机取的值,去掉容易出错的值如0和o
		  $data ='abcdefghigkmnpqrstuvwxy3456789';
		  //取出值，字符串截取方法  strlen获取字符串长度
		  $fontcontent = substr($data, rand(0,strlen($data)),1);
		  //10>.=连续定义变量
		  $captcha_code .= $fontcontent;    
		  //设置坐标
		  $x = ($i*100/4)+rand(5,10);
		  $y = rand(5,10);
		  imagestring($image,$fontsize,$x,$y,$fontcontent,$fontcolor);
		}
		//10>存到session
		//$_SESSION["authcode"] = $captcha_code;
		session('authcode',$captcha_code);
		//8>增加干扰元素，设置雪花点
		for($i=0;$i<200;$i++){
		  //设置点的颜色，50-200颜色比数字浅，不干扰阅读
		  $pointcolor = imagecolorallocate($image,rand(50,200), rand(50,200), rand(50,200));    
		  //imagesetpixel — 画一个单一像素
		  imagesetpixel($image, rand(1,99), rand(1,29), $pointcolor);
		}
		//9>增加干扰元素，设置横线
		for($i=0;$i<4;$i++){
		  //设置线的颜色
		  $linecolor = imagecolorallocate($image,rand(80,220), rand(80,220),rand(80,220));
		  //设置线，两点一线
		  imageline($image,rand(1,99), rand(1,29),rand(1,99), rand(1,29),$linecolor);
		}
		//2>设置头部，image/png
		header('Content-Type: image/png');
		//3>imagepng() 建立png图形函数
		imagepng($image);
		//4>imagedestroy() 结束图形函数 销毁$image
		imagedestroy($image);
	}
	// 提现验证
	public function safever()
	{
		$action = input('post.action');
		if($action != 'cashed'){
			$this->error('非法操作');
		}
		$this->assign('sendtime',sendtime('phone'));
		return $this->fetch();
	}
	// 免审弹窗
	public function noaudit()
	{
		return $this->fetch();
	}
	
	// 审核弹窗
	public function pview()
	{
		$param['pview'] = route_name('pview');
		$param['type']  = route_name($param['pview']);
		$param['bh']    = route_name($param['type']);
		// 自动加载
		return \think\Loader::model('pview','logic\html')->$param['pview']($param);
	}
	
	// 举报商品
	public function report()
	{
		$good   = input('good');
		$number = input('number');
		$data = Db::name($good)->where('bh',$number)->find();
		$data['typename'] = typename($good);
		$data['good'] = $good;
		$this->assign('data',$data);
		return $this->fetch();
	}
	//安装服务弹窗
	public function installing()
	{
		$data_d = input('data_d');
		$data = Db::name('code')->where('id',$data_d)->find();
		$this->assign('data',$data);		
		return $this->fetch();
	}
	
	//联系方式弹窗
	public function contact()
	{
		$str  = input('str');
		$type = input('type');
		$data = unserialize(base64_decode($str));
		
		
		//return var_dump($data);
		
		$this->assign('type',$type);
		$this->assign('data',$data);		
		return $this->fetch();	
	}
	
	//演示弹窗
	public function demo()
	{
		$this->assign('M',input('M',null));
		$this->assign('U',input('U',null));
		return $this->fetch();
	}
	
	// 弹窗登录
	public function login()
	{
		$c   = input('c');
		$get = input('get.');
		$this->assign('get',$get);
		$this->assign('c',$c);
		return $this->fetch();
	}
	
	// 发布弹窗
	public function released()
	{
		$state = state();
		$this->assign('type',input('get.type'));
		$this->assign('state',$state);
		return $this->fetch();
	}
	
	// 网站公告
	public function notice()
	{
		$id    = path('notice');
		$data  = Db::name('gonggao')->where('id',$id)->find();
		$list  = Db::name('gonggao')->where('fl','in','0,1')->limit(10)->select();
		$views = Db::name('gonggao')->where('id',$id)->setInc('djl');
		$this->assign('data',$data);
		$this->assign('list',$list);
		if(!$data){return '不存在';}		
		return $this->fetch();
	}
	
	public function startcaptchaservlet()
	{	
		$conf['CAPTCHA_ID']="b31335edde91b2f98dacd393f6ae6de8";
		$conf['PRIVATE_KEY']="170d2349acef92b7396c7157eb9d8f47";		
		$GtSdk = new GeetestLib($conf['CAPTCHA_ID'], $conf['PRIVATE_KEY']);
		$data=array(
			'user_id' => isset($user)?$user:'public', # 网站用户id
			'client_type' => "web", # web:电脑上的浏览器；h5:手机上的浏览器，包括移动应用内完全内置的web_view；native：通过原生SDK植入APP应用的方式
			'ip_address' => request()->ip() # 请在此处传输用户请求验证时所携带的IP
		);
		$status = $GtSdk->pre_process($data, 1);
		$_SESSION['gtserver'] = $status;
		$_SESSION['user_id'] = isset($pid)?$pid:'public';	
		exit($GtSdk->get_response_str());	
	}
}
?>
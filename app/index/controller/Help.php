<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;

class Help extends BaseIndex
{
	// 首页
	public function index()
	{
		//$this->assign('pages',$pages);
		return $this->fetch();
	}
	
	// 搜索
	public function search()
	{		
		// 搜索列表
		$search = path('search');
		$help = Db::name('help')->where('tit','like','%'.$search.'%')->select();		
		// 常见问题分类
		$helptype = Db::name('helptype')->where('admin',1)->select();	
		$helplist = [];
		foreach($helptype as $data){
			$data['data'] = Db::name('helptype')->where(['admin'=>2,'name1'=>$data['name1']])->select();	
			$helplist[] = $data;
		}
		$count = Db::name('help')->where('tit','like','%'.$search.'%')->count();	
		$this->assign('help',$help);
		$this->assign('search',$search);
		$this->assign('count',$count);
		$this->assign('helplist',$helplist);
		return $this->fetch();
	}
	
	// 列表
	public function list()
	{
		// 常见问题分类
		$helptype = Db::name('helptype')->where('admin',1)->select();	
		$helplist = [];
		foreach($helptype as $data){
			$data['data'] = Db::name('helptype')->where(['admin'=>2,'name1'=>$data['name1']])->select();	
			$helplist[] = $data;
		}
		// 帮助文章列表
		$helpview = Db::name('help')->where(['ty2id'=>path('list')])->select();	
		$this->assign('helplist',$helplist);
		$this->assign('helpview',$helpview);
		return $this->fetch();
	}
	
	// 详情
	public function view()
	{
		// 常见问题分类
		$helptype = Db::name('helptype')->where('admin',1)->select();	
		$helplist = [];
		foreach($helptype as $data){
			$data['data'] = Db::name('helptype')->where(['admin'=>2,'name1'=>$data['name1']])->select();	
			$helplist[] = $data;
		}
		$help = Db::name('help')->where('id',path('view'))->find();
		// 猜你感兴趣的问题
		$tjlist = Db::name('help')->where('ty2id',$help['ty2id'])->limit(5)->select();	
		$this->assign('tjlist',$tjlist);
		$this->assign('helplist',$helplist);
		$this->assign('help',$help);
		return $this->fetch();
	}
	// 购买流程
	public function maps()
	{
		return $this->fetch();
	}	
}
?>
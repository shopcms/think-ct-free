<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;
use thinkct\library\ForeachType;
use thinkct\library\Search;


class Web extends BaseIndex
{
    public function index($query = [],$page_size = 24,$view = null)
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$screen_on  = $search->screen_on();
		$order      = $search->order();
		$page       = $search->page();
		$entrust    = input('entrust');
		// 类型列表			
		$list['type'] = ForeachType::typelist([
			'admin'=>2,'type'=>'web','name1'=>'类型'
		]);	
		$list['money'] = ForeachType::typelist([
			'admin'=>2,'type'=>'web','name1'=>'价格'
		]);	
		$list['ip'] = ForeachType::typelist([
			'admin'=>2,'type'=>'web','name1'=>'流量'
		]);
		$list['br'] = ForeachType::typelist([
			'admin'=>2,'type'=>'web','name1'=>'权重'
		]);			
		// 排序分类
		$sort['order'] = ForeachType::sort_order([
			['name'=>'综合','type'=>''],
			['name'=>'最新','type'=>'time'],
			['name'=>'流量','type'=>'ip'],
			['name'=>'权重','type'=>'br'],
			['name'=>'价格','type'=>'am']
		]);
		// 条件选择
		$sort['checkbox'] = ForeachType::sort_checkbox([
			['name'=>'成交','type'=>'sold','value'=>1],
			['name'=>'含移动端','type'=>'app','value'=>1]
		]);	
		// 求购列表
		$list['demand'] = ForeachType::list_demand(
			Db::name('demand')->where(['zt'=>1,'type'=>'web'])
			->order('sj desc')->limit(20)->select()		
		);		
		// 列表数据
		$data['list'] = ForeachType::list_web(
			Db::name('web')->where(['zt'=>1])->where($query)
			->order($order)->page($page,$page_size)->select()
		);
		// 列表数量
		$count['list'] = Db::name('web')->where(['zt'=>1])->where($query)->count();
		// 页数数量
		$count['page'] = (int)($count['list']/$page_size)+1;
		// 页数模板
		$pages = $this->page($count['list'],$page_size,$page);
		$this->assign('entrust',$entrust);		
		$this->assign('list',$list);
		$this->assign('sort',$sort);
		$this->assign('data',$data);
		$this->assign('screen_on',$screen_on);
		$this->assign('count',$count);
		$this->assign('page',$page);	
		$this->assign('pages',$pages);
		return $this->fetch($view);
    }

	public function goods($id)
	{
		// 增加点击量
		Db::name('web')->where('id',$id)->setInc('djl');	
				
		$row = Db::name('web')->where(['id' => $id,'zt' => "1"])->find();
		$row['img']  = get_tp($row['bh']);
		$row['desc'] = $row['txt'];
		$shop = ForeachType::shop($row);
		$cart = Db::name('cart')->where(['codebh'=>$row['bh'],'ubh'=>session('usercode')])->find();					
		$this->assign('shop',$shop);		
		$this->assign('row',$row);
		$this->assign('cart',$cart);
		return $this->fetch();
	}
}
?>
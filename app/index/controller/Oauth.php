<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\controller;


use thinkct\controller\BaseIndex;
use think\Db;

// 1登录成功 2绑定成功 3注册并且绑定 4绑定失败
class Oauth extends BaseIndex
{
	// 1发起请求
	public function login()
	{
		$oauth_domain = input('oauth_domain');
		$domain       = request()->domain();
		$type         = thinkct()->path('login');
		$site_domain  = plugin_account('qqoauth')['domain'];		
		// 判断域名是否当前站点
		if(!strstr($site_domain,$domain)){
			return thinkct()->url($site_domain.'/oauth/login/'.$type,[
				'oauth_domain' => $domain
			]);
		}
		// 检测是否启用回调地址
		if(!empty($oauth_domain)){
			session('oauth_domain',$oauth_domain);
		}
		return $this->$type('login');
	}
	
	// 2返回回调
	public function _empty($name)
	{
		if($name == 'redirect') {
			$type = thinkct()->path('redirect');
			$data = $this->$type('callback');
			// 判断是否需要返回多域名
			if(session('?oauth_domain')){
				$oauth_domain = session('oauth_domain');
				session('oauth_domain',null);
				return thinkct()->url($oauth_domain.'/oauth/api_redirect',[
					'data' => base64_encode(serialize($data))
				]);
			}
			// 正常返回
			return $this->information($data,$type);
		}else{
			return $this->error('非法操作');
		}
	}
		
	// 3输出模板信息
	public function reload()
	{
		$state = session('oauth_state')[0];
		$this->assign('state',$state);
		return $this->fetch('reload');
	}
	
	// 4处理快捷登录数据 1登录成功 2绑定成功 3注册并且绑定 4绑定失败
	private function information($data,$type)
	{
		// 获取登录信息
		$oauth = Db::name('oauth')->where('code',$data['openid'])->find();
		if($oauth){
			// 存在登录和绑定
			if(session('?usercode')){
				session('oauth_state',[4,'绑定失败，该QQ已经绑定过其他帐号']);
				return '绑定失败，该QQ已经绑定过其他帐号';
			}
			session('oauth_state',[1,'登录成功']);			
			session('usercode',$oauth['ubh']);
			anquan($oauth['ubh'],"login","QQ快捷登陆");
			Db::name('oauth')->where('code',$data['openid'])->update(['login'=>sj(),'uip'=>uip()]);
		}
		if(!$oauth){
			// 绑定登录账号
			if(session('?usercode')){
				Db::name('oauth')->insert(['sj'=>sj(),'uip'=>uip(),'ubh'=>session('usercode'),'name'=>$data['name'],'code'=>$data['openid'],'tx'=>$data['tx'],'login'=>sj(),'type'=>$type,'status'=>1]);	
				session('oauth_state',[2,'QQ帐号绑定成功']);
				return 'QQ帐号绑定成功';
			}
			// 注册并且绑定账号 写入登录缓存
			session('oauth_data',$data);			
			session('oauth_state',[3,'注册并且绑定']);
		}
		return $this->reload();		
	}

	// 多域名回调地址
	public function api_redirect()
	{
		$data = unserialize(base64_decode(input('data')));
		$data = [			
			'openid' => $data['openid'],
			'name'   => $data['name'],
			'tx'     => $data['tx'],
			'type'   => $data['type']
		];
		return $this->information($data,$data['type']);
	}
	
	// QQ登录
	private function qq($type)
	{
		// 发起登录请求
		if($type == 'login') {
			$oauth = new \qq_connect\Oauth();  	
			return $oauth->qq_login();		
		}
		// 返回回调信息
		if($type == 'callback') {
			$oauth       = new \qq_connect\Oauth();
			$accesstoken = $oauth->qq_callback();
			$openid      = $oauth->get_openid();
			// 获取用户信息
			$qc          = new \qq_connect\QC($accesstoken,$openid);
			$userinfo    = $qc->get_user_info();
			// 返回获取到的信息 统一参数
			return [			
				'openid' => $openid,
				'name'   => $userinfo['nickname'],
				'tx'     => $userinfo['figureurl_qq_2'],
				'type'   => 'qq'
			];
		}	
	}
	
	// 微信登录
	private function wechat($type)
	{
		return "微信登录暂未开放-》请使用QQ登录";
	}
	
	// 新浪登录
	private function sina($type)
	{
		return "新浪登录暂未开放-》请使用QQ登录";
	}
	
	// 百度登录
	private function baidu($type)
	{
		return "百度登录暂未开放-》请使用QQ登录";
	}
	
	// 支付宝登录
	private function alipay($type)
	{
		return "支付宝登录暂未开放-》请使用QQ登录";
	}
}
?>
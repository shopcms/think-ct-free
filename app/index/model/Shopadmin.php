<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\model;

use think\Controller;
use think\Db;

class Shopadmin extends Controller
{
	// 客服
	public function contact($action)
	{
		$data   = input('data');
		$action = input('action');
		$good   = input('good');
		$object = input('object');		
		$menu   = Db::name('type')->where(['type'=>'deal','admin'=>2])->select();
		$sell   = Db::name('sell')->where('bh',session('usercode'))->find();
		
		if($action == 'up'){
			preg_match_all('/<'.$good.'-'.$data.'>([^<>]+)<\/'.$good.'-'.$data.'>/',$sell[$object],$result);
			$contact = $result[1][0];
			$iszw = (strstr($contact,':'))?'checked':'';			
			$contact = explode('|',$contact);			
			$result = [];
			foreach($contact as $v){
				$result1['number']  = (strstr($v,':'))?explode(':',$v)[0]:$v;
				$result1['jobs']    = (strstr($v,':'))?explode(':',$v)[1]:'';
				$result1['checked'] = (strstr($v,':'))?'checked':'';
				$result[] = $result1;
			}
			$this->assign('result',$result);
		}
		$this->assign('sell',$sell);
		$this->assign('menu',$menu);		
		return $this->fetch('html/shopadmin/contact');
	}
	
	// 导航
	public function nav($action)
	{	
		$data = input('post.data');
		$row = Db::name('sell_nav')->where('id',$data)->find();
		$action = ($action=='up')?'up':'add';
		$this->assign('row',$row);
		$this->assign('action',$action);
		return $this->fetch('html/shopadmin/nav');
	}

	// 推荐
	public function commend($action)
	{	
		$good = input('good');
		$data = input('data');
		$this->assign('good',$good);
		$this->assign('data',$data);
		$this->assign('action',$action);
		return $this->fetch('html/shopadmin/commend');
	}	

	// 幻灯
	public function slide($action)
	{	
		$data = input('data');
		$row = Db::name('sell_slide')->where('id',$data)->find();
		$action = ($action=='up')?'up':'add';
		$this->assign('row',$row);
		$this->assign('action',$action);
		return $this->fetch('html/shopadmin/slide');
	}
}
?>
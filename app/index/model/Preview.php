<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\index\model;

use think\Controller;
use think\Db;
//use app\member\Base; 缺分页

class Preview extends Controller
{
	private $page      = 1;
	private	$page_size = 10;
	
	// 客服操作记录
	public function contact()
	{
		$data = Db::name('user_log')->where(['ubh'=>session('usercode'),'type'=>'contact'])
		->order('id desc')->page($this->page,$this->page_size)->select();
		$this->assign('data',$data);	
		return $this->fetch('html/preview/contact/undefined');
	}
	
	// 刷新纪录
	public function refresh($param)
	{
		//$this->assign('data',$data);		
		return $this->fetch('html/preview/refresh/undefined');
	}
	
	// 登录白名单日志
	public function login()
	{
		
		return $this->fetch('html/preview/login/protect');	
	}
	
	// 源码弹窗
	public function code($param)
	{		
		// 推荐源码
		if($param['type'] == 'commend'){
			$data = Db::name('code')->where(['ubh'=>session('usercode'),'zt'=>1,'tj'=>0])
			->order('id desc')->page($this->page,$this->page_size)->select();
			$this->assign('data',$data);		
			return $this->fetch('html/preview/code/commend');
		}
	}
	
	// 服务弹窗
	public function serve($param)
	{		
		// 推荐服务
		if($param['type'] == 'commend'){
			$data = Db::name('serve')->where(['ubh'=>session('usercode'),'zt'=>1,'tj'=>0])
			->order('id desc')->page($this->page,$this->page_size)->select();
			$this->assign('data',$data);		
			return $this->fetch('html/preview/serve/commend');
		}
	}
}
?>
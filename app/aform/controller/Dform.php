<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\controller;

use think\Loader;
use think\Db;

class Dform
{
	// 服务下单
	public function serveorder()
	{
		$money = input('money');
		// 没有开发完成
		$user  = Db::name('user')->where('bh',session('usercode'))->find();
		$serve = Db::name('serve')->where(['zt'=>1,'bh'=>input('number')])->find();
		if($serve['ubh'] == $user['bh']){
			return ['state'=>5,'info'=>'亲，不能自己购买自己的商品！'];
		}
		if($money > $user['money']){
			// 这个权限判断可以删除
			return ['state'=>5,'info'=>'<b>服务下单失败</b><br>余额不足，请充值金额大于'.$money.'元','url'=>'/member/onpay/'];
		}		
		return ['state'=>5,'info'=>'系统内部出错！'];
	}
	
	// 商品评价
	public function evaluation()
	{
		$action  = input('action');
		return Loader::model('reviews')->$action();
	}
	
	// 点击取货
    public function takes()
    {
		$post = input('post.');
		return Loader::model('takes','logic\dform')->index($post);
	}
	
	// 订单操作
	public function order()
	{
		$action = input('post.action');
		$post = input('post.');		
		return Loader::model('order','logic\dform')->$action($post);
	}
	
	// 追加
	public function append()
	{
		$action = input('post.action');
		$post = input('post.');		
		return Loader::model('append','logic\dform')->$action($post);
	}
	
	// 订单查询
	public function first_search()
	{
		$input  = input('first_input');
		$select = input('first_select');
		$role   = input('first_role');
		return [
			'state' => 0,
			'info'  => '没有检索到相关交易！'		
		];
	}	
}
?>
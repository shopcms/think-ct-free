<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\controller;

use think\Controller;
use think\Db;
// 回调地址
class NotifyUrl extends Controller
{
	// 支付宝电脑支付回调
	public function alipaypc()
	{
		$alipaypc = new \app\common\pay\Alipaypc();
		return $alipaypc->notify_callback();
	}

	// 支付宝当面付回调
	public function alipaydmf()
	{
		$alipaydmf = new \app\common\pay\Alipaydmf();
		return $alipaydmf->notify_callback();
	}

	// 微信支付回调
	public function wechatpay()
	{
		$wechatpay = new \app\common\pay\Wechatpay();
		return $wechatpay->notify_callback();
	}	

	// 免签约支付回调
	public function epay()
	{
		$Epay = new \app\common\pay\Epay();
		return $Epay->notify_callback();
	}
}
?>
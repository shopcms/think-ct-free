<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\controller;

use app\aform\model\Apage as ApageModel;

class Apage
{
    public function index()
    {
		$list = input('post.list');
		$type = input('post.type');
		$page = input('post.page');
		$page = ($page == 0)? 1 : $page;//当前页
		$aform = new ApageModel();		
		return $aform->logic($list)->index($type,$list,$page);
	}

	public function Order()
	{
		$list = input('post.list');
		$type = input('post.type');
		$page = input('post.page');
		$page = ($page == 0)? 1 : $page;//当前页
		$aform = new ApageModel();		
		return $aform->logic($list)->index($type,$list,$page);		
	}
	
}












?>
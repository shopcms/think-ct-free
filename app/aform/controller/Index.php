<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\controller;

use app\aform\model\Aform as AformModel;
use app\aform\logic\aform\Sendinfo;
use think\Db;
use think\Loader;

class Index
{
	// 发送短信咨询
	public function inquiry()
	{
		
		
		
		return [
			'state' => 6,
			'info'  => '<b>提醒短信，发送成功！</b><br><font color=#666666>系统已向卖家认证手机发送提醒短信</font>',
			'fun'   => 'layer.closeAll();'		
		];
	}
	
	// 网站SEO数据刷新
	public function refresh_webseo()
	{		
		Db::name('web')->where('id',input('id'))->update(['seo_gxsj'=>sj()]);
		return ['state'=>6,'info'=>'获取最新数据成功！','url'=>1];
	}
	
	// 商品举报
	public function report()
	{
		if(input('action') == 'add'){
			Db::name('data_report')->insert([
				'bh'     => input('dispute_bh'),
				'sj'     => sj(),
				'uip'    => uip(),
				'ubh'    => session('usercode'),
				'reason' => input('reason'),
				'txt'    => input('txt'),
				'qq'     => input('qq'),
				'email'  => input('email'),
				'good'   => input('good'),
				'number' => input('number')
			]);
			return [
				'state' => 1,
				'info'  => '<b>举报成功！</b><br>我们会尽快处理您提交的举报！',
				'fun'   => 'layer.closeAll();'
			];
		}
	}
	
	// 清空消息记录
	public function message_empty()
	{
		Db::name('message')->where(['ubh'=>session('usercode'),'zt'=>0])->update(['zt'=>1]);
		return [
			'state' 						=> 1,
			'fixed-message-list #list'      => "<div class=\"info_not\"><div><span><i class=\"iconfont\">&#xe6ae;</i><br>亲，当前没有消息！</span></div></div>",
			'fixed-message-box #count'      => 0,
			'fixed-message-box #total-page' => 0,
			'fixed-message-box #curr-page'  => 1,
			'message-count'                 => ''		
		];
	}
	
	// 清空浏览记录
	public function browse_empty()
	{
		setcookie('browseck','',time()+3600*24*30,"/");
		return [
			'state'                        => 1,
			'fixed-browse-list #list'      => "<div class=\"info_not\"><div><span><i class=\"iconfont\">&#xe6ae;</i><br>亲，当前没有浏览记录！</span></div></div>",
			'fixed-browse-box #count'      => 0,
			'fixed-browse-box #total-page' => 0,
			'fixed-browse-box #curr-page'  => 1		
		];
	}
	
	// 自助交易
	public function nore()
	{
		$role = input('role');
		$user = Db::name('user')->where('id|email|bh',input('str'))->find();
		if(!$user){
			exit('{"state":2,"info":"【对方账号】填写有误，请询问对方正确的账号<br/>（可以是对方的ID号或注册邮箱）","clear":1}');
		}
		if($user['bh'] == session('usercode')){
			exit('{"state":2,"info":"【对方账号】不是填写您自己的账号哦；<br/>填写和你所交易的人账号，不知道可以先问问TA吧","clear":1}');
		}
		// 状态转换
		$role = ($role=='sell')?'buy':'sell';
		$uc   = Db::name($role)->where('bh',$user['bh'])->find();
		exit('{"state":1,"Dinfo":"'.$uc['name'].'<font color=#999999> 【ID:'.$user['id'].'】</font>","clear":1}');
	}
	
	// 实名认证
	public function certification()
	{
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		// 验证安全码
		if(sha1(input('safe')) != $user['safe_code']){
			return ['state'=>-1,'info'=>'安全码错误，请重新输入！','element'=>'input[name=safe]'];
		}
		// 实名认证
		if(input('mode') == 'idcard'){
			$idcard = Db::name('user_certification')->where(['ubh'=>session('usercode'),'card'=>input('num')])->find();
			$allmoney = 2;// 2元认证费用
			if($user['money'] < $allmoney){
				return ['state'=>0,'info'=>'余额不足请进行充值','url'=>'/member/onpay/'.$allmoney];
			}
			// 执行认证扣费
			PointUpdate($user['bh'],$allmoney*(-1),'身份认证核验费'.$allmoney*(-1).'元');			
			// 手机号验证
			if(input('check') == 'phone'){
				$data = think_api('telecom/query',[
					'realname' => urlencode($idcard['name']),
					'idcard'   => $idcard['card'],
					'mobile'   => $user['phone'],
				]);
			}
			// 银行卡验证
			if(input('check') == 'bankid'){
				$data = think_api('verifybankcard3/query',[
					'realname' => urlencode($idcard['name']),
					'idcard'   => $idcard['card'],
					'bankcard' => input('bankid')
				]);	
			}
			// 认证成功
			if($data['data']['res'] == 1){
				Db::name('user')->where('bh',$user['bh'])->update(['idcard'=>1]);
				Db::name('user_certification')->where('id',$idcard['id'])->update(['zt'=>1,'renzheng'=>input('check')]);
				return ['state'=>1,'info'=>'<strong>实名认证</strong>操作成功','url'=>'/member/certification/idcard/'];
			}
			// 认证失败
			if($data['data']['res'] == 2){
				return ['state'=>5,'info'=>(isset($data['data']['resmsg']))?$data['data']['resmsg']:'认证信息不匹配'];
			}
		}
		// 企业认证
		if(input('mode') == 'company'){
			
			
		}		
	}
	
	// 绑定登录
	public function bdreg()
	{
		$bemail    = input('bemail');
		$bpassWord = input('bpassWord');
		$oauth     = input('oauth');
		// 登录信息
		$user = Db::name('user')->where(['email' => $bemail,'pwd'=>sha1($bpassWord)])->find();		
		if(!$user){
			return ['state'=>-1,'info'=>"帐号或密码错误",'element'=>'input[name=bpassWord]'];
		}
		if($user['zt'] == 0){
			return ['state'=>0,'info'=>'<b>该账户正在审核</b><br><font color=#666666>请联系客服证实审核状态</font>','url'=>1];		
		}					
		if($user['zt'] == 2){
			return ['state'=>0,'info'=>'<b>该账户审核不通过</b><br><font color=#666666>'.$user['ztsm'].'</font>','url'=>1];		
		}						
		if($user['regular'] == 0) {
			return ['state' => 0,'info'=>'<b>该账户已关闭常规登录</b><br><font color=#666666>您可以使用手机或第三方快捷登录</font>','url'=>1];
		}
		session('usercode',$user['bh']);
		anquan($user['bh'],"login","绑定并且登录");	
		// 如果为第三方登录
		if(session('?oauth_data')){
			$data = session('oauth_data');
			Db::name('oauth')->insert(['sj'=>sj(),'uip'=>uip(),'ubh'=>$user['bh'],'name'=>$data['name'],'code'=>$data['openid'],'tx'=>$data['tx'],'login'=>sj(),'type'=>$data['type']]);
			// 销毁登录缓存
			session('oauth_data',null);
		}
		return ['state'=>301,'url'=>'/member/']; 
	}
	
	// 解除绑定
	public function Removebind()
	{
		Db::name('oauth')->where('id',input('id'))->delete();
		return ['state'=>1,'info'=>'<b>QQ解绑成功！</b>','url'=>1];
	}
	
	// 网站验证
	public function vsurl()
	{
		$name = input('name');
		$mode = input('mode');		
		// 验证成功
		return ['state'=>-3,'fun'=>"layer.closeAll();$('input[name=verify]').val('{$name}');$('form').submit();"];
		// 验证失败
		return ['state'=>0,'info'=>'<b>网站验证失败！</b><br><font color=#666666>请确保您已正确按要求完成设置</font>'];	
	}
	
	// 撤销提现
	public function cancel_cashed()
	{
		$cashed = Db::name('cashed')->where(['zt'=>0,'ubh'=>session('usercode'),'bh'=>input('bh')])->find();
		if($cashed){
			Db::name('cashed')->where('bh',$cashed['bh'])->update(['zt'=>3,'sm'=>'自行撤销了提现申请！','complete_sj'=>sj()]);
			PointUpdate(session('usercode'),$cashed['money'],"自行撤销了提现申请！");
			return json(['state'=>1,'info'=>'撤销提现操作成功','url'=>1]);
		}
		return json(['state'=>5,'info'=>'撤销提现操作失败','url'=>1]);
	}
	
	// 商品推广
	public function promotion()
	{
		return Loader::model('promotion')->add();
	}
	
	// 品牌点赞
	public function interactive()
	{
		if(!session('?usercode')){
			return ['state'=>-2];
		}
		// 待完善
		// 待完善
		return ['state'=>1,'info'=>'<b>点赞成功！</b>','fun'=>"$('#interactive[data-action=good]').addClass('curr');$('#good-value').html('1');"];
	}
	
	// 首次注册更换邮箱
	public function changemail()
	{
		$new = input('new');
		$old = input('old');
		if($new == $old) {
			return ['state'=>0,'info'=>'两个邮箱一致无需更换，请重试','url'=>1];
		}
		if(Db::name('user')->where('email',$new)->find()) {	
			return json(['state'=>3,'info'=>'此邮箱已被其他帐号绑定了，请返回更换其他邮箱']);
		}		
		Db::name('user')->where('bh',session('usercode'))->update([
			'email_zt' => 0,
			'email'    => $new,
		]);
		// 发送注册邮箱
		$sendinfo = new Sendinfo;		
		$sendinfo->send_email($new);		
		return ['state'=>1,'info'=>'更换成功邮箱，并已发送新的验证码，若一直无法收到验证邮箱请联系管理客服！','url'=>1];
	}
	
	public function u()
	{
		return (session('?usercode'))?['state'=>1]:['state'=>-2];
	}
	
	public function search_seller()
	{
		$value = input('value');
		if(!session('?usercode')) {
			return ['state'=>-2];
		}
		//{"state":301,"url":"https://www.shop.com/ishop24683/"}
		return [
			'state' => 5,
			'info'  => "亲，未搜索到相关联系信息的商家！"
		];
	}
	
	// 搜索框
	public function autocomplete()
	{
		$q     = input('q');
		$scene = input('scene');
		$limit = input('limit',10);
		$where = [];
		if(!empty($q)) {
			$where['skey'] = ['like','%'.$q.'%'];
		}
		if(!empty($scene)) {
			$where['type'] = $scene;
		}
		$list = Db::name('soukey')->where($where)->order('num desc')->limit($limit)->field('skey as value')->select();
		return json(['state' =>1,'list'=>$list]);
	}
	
	public function note()
	{			
		Db::name('down')->where('ddbh',input('bh'))->update(['bz'=>input('txt')]);	
		return ['state'=>1,'clear'=>1];
	}
	
	public function regular_login()
	{
		$aform = new AformModel();		
		return $aform->logic('user')->regular_login();	
	}
	
	public function verify()
	{
		$action = input('post.action');
		$aform = new AformModel();		
		return $aform->logic('verify')->$action();
	}
	
	public function fetch()
	{
		$action = input('post.action');
		$aform = new AformModel();		
		return $aform->logic('fetch')->$action();
	}
	
	public function turnshop()
	{
		$aform = new AformModel();		
		return $aform->logic('user')->turnshop();	
	}
	public function safever()
	{
		$aform = new AformModel();		
		return $aform->logic('user')->safever();	
	}
    public function sendinfo()
	{
		$action = input('post.action');
		$mode = input('post.mode');
		$aform = new AformModel();		
		return $aform->logic('sendinfo')->$action($mode);		
	}
	public function shop_admin()
	{
		$object = input('post.object');
		$aform = new AformModel();		
		return $aform->logic('shop_admin')->$object();		
	}
	
	public function fav()
	{
		$aform = new AformModel();		
		return $aform->logic('fav')->fav();		
	}

	public function safe()
	{
		$aform = new AformModel();		
		return $aform->logic('user')->safe('usercode', null);	
	}

	public function vsafe()
	{
		if(session('?SAFEPWD')) {
			return ['state'=>1];
		}else{
			return ['state'=>0];
		}		
	}
	
	public function cart()
	{
		$aform = new AformModel();
		return $aform->logic('cart')->cart();			
	}

	public function cart_delete()
	{
		$aform = new AformModel();
		return $aform->logic('cart')->cart_delete();				
	}
	
	// 清空购物车
	public function cart_empty()
	{
		Db::name('cart')->where('ubh',session('usercode'))->delete();
		return [
			'state'                      => 1,
			'fixed-cart-list #list'      => "<div class=\"info_not\"><div><span><i class=\"iconfont\">&#xe6ae;</i><br>亲，购物车空空的，您再去逛逛吧！</span></div></div>",
			'cart-count'                 => "",
			'fixed-cart-box #total-page' => 0,
			'fixed-cart-box #curr-page'  => 1
		];	
	}
	
	public function cartadd()
	{
		$aform = new AformModel();
		return $aform->logic('cart')->cartadd();
	}	
	
	public function sign()
	{
		$aform = new AformModel();		
		return $aform->logic('sign')->index(input('post.mode'));	
	}

	public function ajax_logout()
	{
		$aform = new AformModel();		
		return $aform->logic('user')->ajax_logout('usercode', null);
	}

	public function logout()
	{			
		$aform = new AformModel();		
		return $aform->logic('user')->logout('usercode', null);				
	}	

	public function reg()
	{	
		$aform = new AformModel();		
		return $aform->logic('user')->reg();
	}
	
    public function login()
    {
		$aform = new AformModel();		
		return $aform->logic('user')->login();		
    }
	
	public function ajax_login()
	{
		$aform = new AformModel();		
		return $aform->logic('user')->ajax_login();
	}	
	
    public function index()
    {
        return "index";
    }	
}
?>
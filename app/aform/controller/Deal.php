<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\controller;

use think\Controller;
use think\Loader;
use think\Db;

class Deal extends Controller
{
	// 服务订单
	public function addserve()
	{
		$bh    = shop_bh();
		$serve = Db::name('serve')->where(['zt'=>1,'bh'=>input('number')])->find();
		$sell  = Db::name('sell')->where(['bh'=>$serve['ubh']])->find();
		// 订单数据
		$data['piece']   = input('piece');
		$data['edition'] = input('edition');
		$data['money']   = input('money');
		$data['number']  = input('number');
		
		$this->assign('bh',$bh);
		$this->assign('data',$data);
		$this->assign('serve',$serve);
		$this->assign('sell',$sell);
		return $this->fetch();
	}
	
	// 积分兑换
	public function exchange()
	{
		$piece = input('piece',1);
		$user  = Db::name('user')->where('bh',session('usercode'))->find();
		$data  = Db::name('jifen_cms')->where('bh',input('number'))->find();
		$this->assign('user',$user);
		$this->assign('data',$data);
		$this->assign('piece',$piece);
		return $this->fetch();
	}
	
	// 评价订单
	public function evaluation()
	{
		$data = input('post.');
		$this->assign('data',$data);
		return $this->fetch('deal/evaluation/'.$data['object']);
	}
	// 点击取货
    public function takes()
    {
		$post = input('post.');
		return Loader::model('takes','logic\deal')->index($post);
	}
	
	// 订单操作
	public function order()
	{
		$action = input('action');
		$post = input('post.');	

		// 如果是消保免赔
		if($action == 'waiver'){
			
			
			return $this->fetch('deal/order/waiver');
		}
		return Loader::model('order','logic\deal')->$action($post);
	}
	
	// 追加
	public function append()
	{
		$action = input('post.action');
		$post = input('post.');		
		return Loader::model('append','logic\deal')->$action($post);
	}		
}












?>
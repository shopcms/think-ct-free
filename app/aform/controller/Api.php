<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\controller;

use think\Controller;
use think\Db;

class Api extends Controller
{
	// 首页
	public function index()
	{
		return $this->success('欢迎使用ThinkCT系统');
	}
	
	// 刷新源码
	public function lastgx()
	{
		$id = rand(1,15000);
		//$code = Db::name('code')->where('id',$id)->find();
		//if($code) {
			//$baidu = $this->baidu($code['id']);
		//}
		Db::name('code')->where('id',$id)->update(['lastgx'=>sj()]);
		return json(['code'=>'刷新源码ID:'.$id.'更新时间成功']);
	}
	
	// 数据列表
	public function site()
	{
		$domain = sysconf('site_domain');
		$code = Db::name('code')->where('zt',1)
		->order('id desc')->limit(10000)->select();
		$url = null;
		foreach($code as $data){
			$url = $url.$domain.c_url($data['id'],'code')."\n";
		}
		return $url;
	}

	// 数据列表
	public function sitemafabu()
	{
		$domain = sysconf('site_domain');
		$page   = input('page',1);
		$code   = Db::name('code')->where('zt',1)->page($page,5000)->order('id asc')->select();
		$url    = '';
		foreach($code as $data){
			// 更新百度提交状态
			Db::name('code')->where('id',$data['id'])->update(['baidu'=>1]);
			
			$url = $url.$domain.c_url($data['id'],'code')."\n";
		}
		$url = trim($url,"\n");		
		return $url;
	}
	
	// 百度接口提交
	public function baiduapi()
	{
		//https://www.mafabu.com/api/baiduapi/token/CyOKbd9NJXW2b6zo/page/1/limit/100
		$site  = input('site',sysconf('site_domain'));
		$token = input('token','CyOKbd9NJXW2b6zo');
		$page  = input('page',1);
		$limit = input('limit',100);
		$code = Db::name('code')->where('zt',1)->page($page,$limit)->order('id desc')->select();
		$url  = '';
		foreach($code as $data){
			$url = $url.$site.c_url($data['id'],'code')."\n";
		}
		$url = trim($url,"\n");		
		// 提交百度部分代码
		$urls = explode("\n",$url);
		//return var_dump($urls);	
		$api = 'http://data.zz.baidu.com/urls?site=https://www.mafabu.com&token='.$token;
		$ch  = curl_init();
		$options =  array(
			CURLOPT_URL => $api,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => implode("\n", $urls),
			CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
		);
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		return $result;			
	}	
	

	
	// 提交百度接口
	public function baidu($id=500)
	{
		$url = sysconf('site_domain').c_url($id,'code');
		$urls = array($url);
		$api = 'http://data.zz.baidu.com/urls?site=https://www.guoyn.com&token=CyOKbd9NJXW2b6zo';
		$ch = curl_init();
		$options =  array(
			CURLOPT_URL => $api,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => implode("\n", $urls),
			CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
		);
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		return $result;
	}
}
?>
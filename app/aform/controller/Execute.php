<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\controller;

use app\aform\model\Execute as ExecuteModel;
use think\Db;

class Execute
{
    public function routine()
    {
		$batch = input('batch',0);
		if($batch == 'uptime' or $batch == 'updown' or $batch == 'del') {
			$list = input('post.list');
			$batch = input('post.batch');
			$type = input('post.type');
			$C1 = $_POST['C1'];
			$execute = new ExecuteModel();		
			return $execute->logic('goods')->$batch($C1,$type);
		}
		return $this->$batch();
	}
	
	// 消息操作
	public function message_read()
	{
		$id = input('id');
		Db::name('message')->where(['ubh'=>session('usercode'),'zt'=>0,'id'=>$id])->update(['zt'=>1]);
		return ['state'=>1,'clear'=>1];
	}
	
	// 消息
	public function message()
	{
		$action = input('action');
		$C1     = input('C1');
		$all    = input('all');
		// 选中设为已读
		if($action == 'read'){
			if($all == 1) {
				Db::name('message')->where(['ubh'=>session('usercode'),'zt'=>0])->update(['zt'=>1]);
				return ["state"=>1,"info"=>"<b>操作成功！</strong>","url"=>1,"fun"=>"$(\".MemberMessageList\").empty();$(\".MemberTopMessageList\").html('<cite class=\"MessageUnreadNot show\"><p class=\"iconfont\">&#xe6ae;<\/p>亲，当前没有新消息！<\/cite>');$(\".MessageUnreadTips\").remove();$(\".message_num_0\").html('0');$(\".MessageAllRead\").html('没有未读的消息');if(typeof $.blinkTitle.clear === \"function\") $.blinkTitle.clear();"];
			}
			Db::name('message')->where(['ubh'=>session('usercode'),'zt'=>0,'id'=>$C1])->update(['zt'=>1]);
			return ['state'=>1,'info'=>'<b>已读成功！</strong>','url'=>1];		
		}
		// 删除消息
		if($action == 'del') {
			if($all == 1) {
				Db::name('message')->where(['ubh'=>session('usercode'),'zt'=>1])->delete();
			}
			Db::name('message')->where(['ubh'=>session('usercode'),'zt'=>1,'id'=>$C1])->delete();
			return ['state'=>1,'info'=>'<b>删除成功！</strong>','url'=>1];
		}
	}
}
?>
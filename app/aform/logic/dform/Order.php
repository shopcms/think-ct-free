<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\dform;

use think\Loader;
use think\Db;

class Order extends Base
{
	var $number = 0;
	var $yy     = 0;
	var $zt     = 0;
	var $row    = 0;
	
	public function _initialize()
	{
		// 获取交易信息
		$row = Db::name('down')->where([
			'ddbh'     => input('post.number'),
			'buy|sell' => session('usercode')
		])->find();
		if(!$row) {
			return ['state'=>5,'info'=>'<strong>交易不存在</strong><br>可能已被发起人删除'];
		}
		// 获取交易内容
		if($row['type'] == 'custom' || $row['type'] == 'task' || $row['type'] == 'serve'){
			$row1 = Db::name('custom')->where([
				'ddbh'     => input('post.number'),
				'fqbh|fqbh' => session('usercode')
			])->find();
			if(!$row1) {
				return ['state'=>5,'info'=>'<strong>交易不存在</strong><br>可能已被发起人删除'];
			}
		}
		// 安全码验证
		// 安全码验证
		
		// 主交易状态
		$this->zt = $row['ddzt'];
		$this->yy = input('post.main_reason')."|".input('post.txt');
		$this->number = $row['ddbh'];
		$this->row = $row;		
	}
	
	// 消保免赔
	public function waiver()
	{
		$sj        = sj();
		$waiver_sj = date('Y-m-d H:i:s',strtotime($sj)+10800);	
		Db::name('moneyback')->where('ddbh',input('number'))->update(['waiver'=>1,'waiver_sj'=>$sj,'waiver_oksj'=>$waiver_sj]);
		return ['state'=>1,'info'=>'<strong>消保免赔</strong>操作成功<br>此交易已进入消保免赔付状态','url'=>1];
	}
	
	// 确定收货
	public function confirm($data)
	{
		$row = $this->row;
		// 判断状态合法性
		if($this->zt != 'back' && $this->zt != 'backerr' && $this->zt != 'db') {
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}		
		// 获取担保信息
		$row2 = Db::name('moneydb')->where(['buy'=>session('usercode'),'ddbh'=>$this->number])->find();
		if(!$row2) {
			$row2 = Db::name('moneyback')->where(['buy'=>session('usercode'),'ddbh'=>$this->number])->find();		
			if(!$row2) {
				return ['state'=>5,'info'=>'<strong>交易不存在或无权操作</strong>','url'=>1];
			}
		}
		// 自助交易 删除未完成追加记录	
		if($row['type'] == 'custom') {
			// 更新交易结束时间
			Db::name('custom')->where(['ddbh'=>$this->number,'txttype'=>['<>','main'],'t'=>['<>','success']])->delete();
			Db::name('custom')->where('bh',$this->number)->update(['etime' => sj()]);
		}
		// 执行确认收货
		e_order($this->number,'success',sj());
		if($row['type']=='task') {
			// 修改任务状态为结束
			Db::name('task')->where('bh',$row['codebh'])->update(['zt' => 'success']);
		}elseif($row['type']=='web' || $row['type']=='domain') {
			// 修改网站和玉米状态为已出售
			Db::name($row['type'])->where('bh',$row['codebh'])->update(['zt' => 3]);		
		}
		return ['state'=>1,'info'=>'<strong>确认收货</strong> 操作成功<br>此交易已完结，款项已转入卖方帐户','url'=>1];
	}
	
	// 申请退款
	public function apply_back($data)
	{	
		return Loader::model('order')->apply_back();
	}
	
	// 延长时间
	public function delay($data)
	{
		return Loader::model('order')->delay();
	}
	
	// 同意退款
	public function agree_back($data)
	{
		$row = $this->row;
		$adminbz = null;
		$row2 = Db::name('moneyback')->where(['ddbh'=>$this->number,'sell'=>session('usercode')])->find();
		if(!$row2) {
			return ['state'=>5,'info'=>'<strong>退款不存在或无权操作</strong>','url'=>1];			
		}
		// 判断状态合法性
		if($this->zt!='back' && $this->zt!='backerr') {
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}
		// 有问题待查询
		$superadmin = (session('usercode')=='yes')? 0:1;
		// 判断是否为强制退款
		if($superadmin == 0) {			
			$backtype = 'forback';
			$adminbz = sj()."|".$this->yy;
			$fortktxt = "<br>处理结果：强制退款 <br>处理原因：".$this->yy." 故商业源码交易网对此交易予以强制退款处理。";
			$tktxt = "退款申请(申诉)商业源码交易网已做出了处理。".$fortktxt."<br>注：交易款项已退回至您的会员账户。";
		}else{
			$backtype = 'backsuc';
			$tktxt = "退款申请卖家已做出了处理。<br>处理结果：同意退款。<br>注：交易款项已退回至您的会员账户。";
		}
		// 自助交易 删除未完成追加记录
		if($row['type'] == 'custom') {
			// 更新交易结束时间
			Db::name('custom')->where(['ddbh'=>$this->number,'txttype'=>['<>','main'],'t'=>['<>','success']])->delete();
			Db::name('custom')->where('bh',$this->number)->update(['etime'=>sj()]);
		}
		// 执行退款
		e_order($this->number,$backtype,sj(),$adminbz);
		if($row['type'] == 'task') {
			// 修改任务状态为结束
			Db::name('task')->where('bh',$row['codebh'])->update(['zt'=>'success']);
		}elseif($row['type']=='web' || $row['type']=='domain') {
			// 修改网站和玉米状态为正常
			Db::name($row['type'])->where('bh',$row['codebh'])->update(['zt'=>1]);
		}
		Db::name('down')->where('ddbh',input('number'))->update(['confirm'=>1]);
		return ['state'=>1,'info'=>'<strong>同意退款</strong>　操作成功<br />交易款已退回买方帐户，交易结束','url'=>1];
	}
	
	// 拒绝退款
	public function refuse_back($data)
	{
		return Loader::model('order')->refuse_back();
	}
	
	// 取消退款/确认收货
	public function afresh_confirm()
	{
		$row = $this->row;
		//判断状态合法性
		if($this->zt!='back' && $this->zt!='backerr' && $this->zt!='db') {
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}		
		$row2 = Db::name('moneydb')->where(['buy'=>session('usercode'),'ddbh'=>$this->number])->find();
		if(!$row2) {
			$row2 = Db::name('moneyback')->where(['buy'=>session('usercode'),'ddbh'=>$this->number])->find();
			if(!$row2) {
				return ['state'=>5,'info'=>'<strong>交易不存在或无权操作</strong>','url'=>1];
			}
		}
		if($row['type'] == 'custom') {
			//自助交易 删除未完成追加记录
			Db::name('custom')->where(['ddbh'=>$this->number,'txttype'=>['<>','main'],'t'=>['<>','success']])->delete();	
			//更新交易结束时间
			Db::name('custom')->where('bh',$this->number)->update(['etime'=>sj()]);					
		}
		// 执行确认收货
		e_order($this->number,'success',sj());
		if($row['type'] == 'task') {
			// 修改任务状态为结束
			Db::name('task')->where('bh',$row['codebh'])->update(['zt'=>'success']);
		}elseif($row['type']=='web' || $row['type']=='domain') {
			// 修改网站和玉米状态为已出售
			Db::name($row['type'])->where('bh',$row['codebh'])->update(['zt'=>3]);		
		}
		return ['state'=>1,'info'=>'<strong>确认收货</strong> 操作成功<br>此交易已完结，款项已转入卖方帐户','url'=>1];
	}
	
	// 重新处理退款/同意退款
	public function afresh_agree_back()
	{
		$row = $this->row;
		$adminbz = null;
		$row2 = Db::name('moneyback')->where(['sell'=>session('usercode'),'ddbh'=>$this->number])->find();
		if(!$row2) {
			return ['state'=>5,'info'=>'<strong>退款不存在或无权操作</strong>','url'=>1];
		}
		// 判断状态合法性
		if($this->zt!='back' && $this->zt!='backerr') {
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}
		// 有问题待查询
		if(session('coerceuser') == 'yes11') {
			$superadmin = 0;
		}else{
			$superadmin = 1;
		}
		// 判断是否为强制退款
		if($superadmin == 0) {
			$backtype = "forback";
			$adminbz = sj()."|".$this->yy;
			$fortktxt = "<br>处理结果：强制退款 <br>处理原因：".$this->yy." 故商业源码交易网对此交易予以强制退款处理。";
			$tktxt = "退款申请(申诉)商业源码交易网已做出了处理。".$fortktxt."<br>注：交易款项已退回至您的会员账户。";
		}else{
			$backtype = "backsuc";
			$tktxt = "退款申请卖家已做出了处理。<br>处理结果：同意退款。<br>注：交易款项已退回至您的会员账户。";
		}
		if($row['type'] == 'custom') {
			//自助交易 删除未完成追加记录
			Db::name('custom')->where(['ddbh'=>$this->number,'txttype'=>['<>','main'],'t'=>['<>','success']])->delete();	
			//更新交易结束时间
			Db::name('custom')->where('bh',$this->number)->update(['etime'=>sj()]);					
		}
		// 执行退款
		e_order($this->number,$backtype,sj(),$adminbz);
		if($row['type'] == 'task') {
			// 修改任务状态为结束
			Db::name('task')->where('bh',$row['codebh'])->update(['zt'=>'success']);
		}elseif($row['type']=='web' || $row['type']=='domain') {
			// 修改网站和玉米状态为已出售
			Db::name($row['type'])->where('bh',$row['codebh'])->update(['zt'=>3]);		
		}		
		Db::name('down')->where('ddbh',input('number'))->update(['confirm'=>1]);
		return ['state'=>1,'info'=>'<strong>同意退款</strong>　操作成功<br />交易款已退回买方帐户，交易结束','url'=>1];
	}
		
	// 购买安装费
	public function buy_install()
	{
		$row = $this->row;
		$code = Db::name('code')->where('bh',$row['codebh'])->find();
		$dingdang = Db::name('code')->where(['bh'=>$this->number,'type'=>'install'])->find();
		if(!$dingdang) {
			// 删除以前的订单
			Db::name('dingdang')->where(['bh'=>$this->number,'ubh'=>session('usercode')])->delete();
			// 增加订单数据
			$number = $this->number;
			Db::name('dingdang')->insert([
				'bh'     => $this->number,
				'ddbh'   => $number.'|'.$user,
				'ubh'    => session('usercode'),
				'sj'     => sj(),
				'uip'    => uip(),
				'money1' => $code['azmoney'],
				'money2' => $code['azmoney'],
				'ddzt'   => '等待买家付款',
				'type'   => 'install'	
			]);
		}
		return ['state'=>1,'info'=>'创建订单成功，请付款','url'=>'/member/cashier/'.$this->number];	
	}
		
	// 卖家同意交易
	public function agree_deal()
	{
		$row = order(input('number'));
		$this->number = $row['ddbh'];
		$this->zt     = $row['ddzt'];
		// 判断状态合法性
		if($this->zt!='fqing' && $this->zt!='cfing' && $this->zt!='waiting'){
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}
		//if($row1[jsbh]!=$_SESSION['usercode']){
			//exit('{"state":5,"info":"<strong>仅【接收方】可操作此项！</strong>'.$user.$row1['id'].'"}');
		//}
		// 交易周期
		$hday = $_POST['hday'];
		if($hday != null){
			$update['cyc'] = $hday;
		}
		$update['htime'] = sj();
		$number = $this->number;
		// 更新回复时间和接受人联系方式
		Db::name('custom')->where('ddbh',$this->number)->update($update);
		//updatetable("shop_custom",$hday."htime='".$sj."' where ddbh='".$number."'");
		// 更新交易状态
		Db::name('down')->where('ddbh',$this->number)->update(['ddzt'=>'fking']);
		// m1总金额 m2交易金额 
		$ddbh = $number.'|'.$row['buy'];
		// 买家手续费
		$fees = fees($row['money1'],$row['fees'],'buy');
		Db::name('dingdang')->insert([
			'bh'     => $number,
			'ddbh'   => $ddbh,
			'ubh'    => $row['buy'],
			'sj'     => sj(),
			'uip'    => uip(),
			'money1' => $row['money1']+$fees,
			'money2' => $row['money1'],
			'ddzt'   => '等待买家付款',
			'type'   => $row['type']	
		]);
		return ['state'=>1,'info'=>'<strong>同意交易</strong> 操作成功<br>开始正式交易，进入一下步：<font color=\'#ff6600\'>买方支付交易款项</font>','url'=>1];
	}
	
	// 卖家拒绝交易
	public function refuse_deal()
	{
		$row = order(input('number'));
		$this->number = $row['ddbh'];
		$this->zt     = $row['ddzt'];		
		// 更新回复时间和拒绝原因
		Db::name('custom')->where('ddbh',$this->number)->update(['sm'=>input('txt'),'htime'=>sj()]);
		// 更新交易状态
		Db::name('down')->where('ddbh',$this->number)->update(['ddzt'=>'noing']);
		return ['state'=>1,'info'=>'<strong>拒绝交易</strong> 操作成功<br>可联系对方<font color=\'#ff6600\'>修改交易内容后重发请求</font>或<font color=\'red\'>删除交易</font>','url'=>1];
	}
		
	// 取消交易
	public function cancel()
	{
		$row = $this->row;
		// 判断状态合法性
		if($this->zt!='fking'){
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}		
		if($row['buy'] != session('usercode')){
			return ['state'=>5,'info'=>'<strong>仅【买方】可取消交易！</strong>','url'=>1];
		}
		// 删除订单信息
		Db::name('dingdang')->where(['ubh'=>$row['buy'],'bh'=>$row['ddbh']])->delete();
		// 更新内容信息
		Db::name('custom')->where('ddbh',$this->number)->update(['sm'=>input('txt'),'htime'=>sj()]);
		// 更新交易状态
		Db::name('down')->where('ddbh',$this->number)->update(['ddzt'=>'qxing']);
		return ['state'=>1,'info'=>'<strong>取消交易</strong> 操作成功<br>发起方可<font color=\'#ff6600\'>修改交易后重发请求</font>或<font color=\'red\'>删除交易</font>','url'=>1];
	}
		
	// 重发交易请求
	public function resend()
	{
		$row = $this->row;
		// 判断状态合法性
		if($this->zt!='noing' && $this->zt!='qxing'){
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}
		if($row['fqbh'] != session('usercode')){
			return ['state'=>5,'info'=>'<strong>仅【发起方】可重发交易！</strong>','url'=>1];
		}
		Db::name('down')->where('ddbh',$this->number)->update(['ddzt'=>'cfing']);
		return ['state'=>1,'info'=>'<strong>重发交易</strong> 操作成功<br>可联系对方重新查看操作','url'=>1];
	}
		
	// 删除交易
	public function delete()
	{
		$row = $this->row;
		// 判断状态合法性
		if($this->zt!='noing' && $this->zt!='qxing' && $this->zt!='close'){
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项！</strong>','url'=>1];
		}
		// 若是任务修改状态
		if($row['type'] == 'task'){
			Db::name('task')->where('bh',$this->number)->update(['zt'=>2]);
		}
		if($row['buy'] == session('usercode')){
			$m_url = 'buy';
		}else{
			$m_url = 'sell';
		}
		Db::name('dingdang')->where('ddbh',$this->number)->delete();
		Db::name('custom')->where('ddbh',$this->number)->delete();
		Db::name('down')->where('ddbh',$this->number)->delete();
		return ['state'=>1,'info'=>'<strong>交易删除成功</strong>','url'=>1];
	}
		
	// 我已发货
	public function delivery()
	{
		$row = $this->row;
		// 删除正在交易信息
		Db::name('moneyback')->where(['sell'=>session('usercode'),'ddbh'=>$this->number])->delete();
		// 更改状态
		Db::name('down')->where(['sell'=>session('usercode'),'ddbh'=>$this->number])->update(['ddzt'=>'db']);
		$rowcontrol['tksj'] = sysconf('time_refund');
		$oksj = date("Y-m-d H:i:s",strtotime("+".$rowcontrol['tksj']." day"));
		Db::name('moneydb')->insert([
			'bh' => time(),
			'money1' => $row['money1'],
			'money2' => $row['money2'],
			'sj'     => sj(),
			'sell'   => $row['sell'],
			'buy'    => $row['buy'],
			'oksj'   => $oksj,
			'codebh' => $row['codebh'],
			'tit'    => $row['tit'],
			'ddbh'   => $row['ddbh'],
			'jf'     => $row['jf'],
			'type'   => $row['type']
		]);
		return ['state'=>1,'info'=>'<strong>手动发货</strong> 操作成功<br>等待买家验收','url'=>1];
	}
		
	// 卖家主动退款
	public function active_back()
	{
		
		return ['state'=>1,'info'=>'<strong>主动退款</strong>　操作成功<br />交易款已退回买方帐户，交易结束','url'=>1];
	}
		
	// 申请客服介入
	public function service()
	{
		return Loader::model('order')->service();			
	}
}
?>
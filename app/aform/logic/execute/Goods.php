<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\execute;

use think\Db;

class Goods extends Base
{	
	
	// 刷新商品
	public function uptime($C1,$type)
	{
		// 刷新数量
		$shig['shua'] = 5;
		$aa = 1;
		$x = 1;		
		$C1 = (!is_array($C1))? array($C1):$C1;
		$shu = sizeof($C1);
		$log_count =Db::name('user_log')->where(['ubh'=>session('usercode'),'type'=>'uptime'])->whereTime('sj','today')->count();
		
		foreach($C1 as $bh){
			
			$log = Db::name('user_log')->where(['ubh'=>session('usercode'),'type'=>'uptime'])->whereTime('sj','today')->count()+1;
			if($log <= $shig['shua']){
				
				$aa= 2;
				$a = $x++;
				Db::name($type)->where(['ubh'=>session('usercode'),'bh'=>$bh])->update(['lastgx' => sj()]);	
				Db::name('user_log')->insert([
					'sj' => sj(),
					'uip' => uip(),
					'ubh' => session('usercode'),
					'tit' => '基础刷新',
					'txt' => '商品标题',
					'type' => 'uptime'		
				]);	
			}
		}
		if($aa == 2){
			if($shu<=$a){
				$green=$shig['shua']-$log;
				$green="剩余刷新<b class=\"blue\">{$green}</b>次";
			}else{
				$green=$shu-$a;
				$green="刷新失败<b class=\"red\">{$green}</b>个";
			}
			$info="<b>操作成功</b><br>刷新成功<b class=\"green\">{$a}</b>次，{$green}<br>";
			$icon="6";
		}else{
			$info="<b>刷新失败</b><br>您的刷新次数已用完，中午12点后再来吧<br>";
			$icon="0";
		}
		return [
			'state' => -2,
			'icon' => $icon,
			'info' => $info."<a href=\"/member/bond/index/\" style=\"text-decoration:underline\" target=\"_blank\">缴纳保证金</a>后每日可刷新<b class=\"green\"> 10 </b>次",
			'btn1' => '我的刷新',
			'btn2' => '我知道了',
			'btn1fun' => "$('.qrefresh').click();",
			'fun' => "list('R');"
		];
	}

	// 下架商品
	public function updown($C1,$type)
	{	
		$data = Db::name($type)->where($this->bh($C1))->select();
		foreach($data as $v){
			$cz = ($v['zt'] == 1)? 2:1;
			Db::name($type)->where('bh',$v['bh'])->update(['zt' =>$cz]);	
		}
		return [
			'state' => 1,
			'info' => '<strong>操作成功！</strong>',
			'fun' => "list('R');"	
		];
	}
	
	// 删除商品
	public function del($C1,$type)
	{
		Db::name($type)->where($this->bh($C1))->update(['zt'=>4]);	
		return [
			'state' => 1,
			'info' => '<strong>删除操作成功！</strong>',
			'fun' => "list('R');"
		];
	}
	
	// 商品编号
	public function bh($C1,$type = null)
	{
		if(is_array($C1)) {	
			$where['bh'] = array('in',implode(',',$C1));
		}else{
			$where['bh'] = $C1;
		}
		return $where;
	}
}
?>
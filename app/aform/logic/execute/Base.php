<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\aform\logic\execute;

use think\Controller;
use think\Db;
use think\Request;


class Base extends Controller
{
    public function _initialize()
    {
        parent::_initialize();
		$authcode = authcode();
		//判断会员是否登录
		$user = null;
		$uc = null;
		if(session('?usercode')) {
			$user = Db::name('user')->where('bh',session('usercode'))->find();
			$uc = Db::name($user['uc'])->where('bh',session('usercode'))->find();
			$uc['id'] = null;			
			$user['qiandao'] = qiandao($user['bh'],'1');
			
		}
		$this->assign('user', $user);
		$this->assign('uc', $uc);


		//基本参数
		$this->assign('site_domain', sysconf('site_domain'));
		$this->assign('site_name', sysconf('site_name'));

        //注册平台
        if ($this->request->isMobile()) {
            //$this->view->config('view_platform', 'mobile');
        } else {
           // $this->view->config('view_platform', 'pc');
        }
		$this->view->config('view_platform', 'pc');

        //注册主题
        $this->view->config('view_theme', sysconf('index_theme') . DS);

        // 站点关闭
        if (sysconf('site_status') === '0') {
            die(sysconf('site_close_tips'));
        }
       //$nav = Db::name('nav')->where('status=1')->order('sort DESC')->select();
		
		//foreach($nav as $data){
			//$data['class'] = "bold";
			//$nav_list[] = $data;
		//}
        //$this->assign('nav_list', $nav_list);
    }
	
	
	
	public function page($count="30", $page_size="24", $page="1") {

		$count = $count;//总数
		$page_size = $page_size;//每页显示
		$page = (!isset($page))? 1 : $page;//当前页
		
		$url = rule_url("page", $page);
		//总页
		if($count % $page_size){
			$page_count=(int)($count/$page_size)+1;
		} else {
			$page_count=$count/$page_size;
		}
	
		if($page_count>1){
			
			//首页
			if($page!=1){				
				$page_i = ($page>=5)? "..." : "";
				$page_url = rule_url("page", 1);
				$page_home = "<li><a href=\"{$page_url}\" data-page=\"1\">1 {$page_i}</a></li>";				
			}else{
				$page_home = "";
			}
			
			$si=3;
			if($page==1){
				$si=4;
			}elseif($page==2){
				$si=3;
			}
			
			//上页
			$page_upper ="";
			for($i=$page-$si;$i<$page;$i++){
				if($i>1){
					$page_url = rule_url("page", $i);
					$page_upper .= "<li><a href=\"{$page_url}\" data-page=\"{$i}\">{$i}</a></li>";
				}	
			}
			
			//中间
			$page_middle = "<li class=\"ohave\">{$page}</li>";
			
			//下页
			$page_lower ="";
		    for($i=$page+1;$i<=$page+$si;$i++){
				if($i<$page_count){
					$page_url = rule_url("page", $i);
					$page_lower .= "<li><a href=\"{$page_url}\" data-page=\"{$i}\">{$i}</a></li>";
				}		
			}
			
			//尾页
			if($page<$page_count){
				$page_i = ($i<$page_count)? "..." : "";	
				$page_url = rule_url("page", $page_count);
				$page_Last = "<a href=\"{$page_url}\" data-page=\"{$page_count}\">{$page_i} {$page_count}</a>";			
			}else{
				$page_Last = "";
			}
			
			return "<div id=\"page\" total=\"{$page_count}\"><ul>{$page_home}{$page_upper}{$page_middle}{$page_lower}{$page_Last}</ul></div>";
		
		}
	}	
}

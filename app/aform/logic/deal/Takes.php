<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\deal;

use think\Db;

class Takes extends Base
{
	public function index($post)
	{
		$number = $post['number'];
		$role = $post['role'];
		
		$down = Db::name('down')->where(['ddbh'=>$number,$role=>session('usercode')])->find();
		if($down) {
			$data = Db::name($down['type'])->where(['bh'=>$down['codebh'],'ubh'=>$down['sell']])->find();
			$shop = Db::name('sell')->where('bh',$down['sell'])->find();
			$data['shopid'] = $shop['id'];
			$data['name'] = $shop['name'];
			$data['xy'] = xy($shop['rev_m'],'s');
			$data['ddbh'] = $number;
			$sjc = DateDiff(sj(),$down['sj']);
			if($sjc <= 30){
				$data['downnum'] = 1;
			}else{
				$data['downnum'] = 2;
			}
		}else{
			$this->error('非法操作！');
		}
		$this->assign('data',$data);		
		return $this->fetch();
	}
	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\deal;

use think\Db;

class Order extends Base
{
	protected $number;
	

	// 恢复消保赔付
	public function cancel_waiver($data)
	{
		$moneyback = Db::name('moneyback')->where('ddbh',input('number'))->find();
		//$waiver_sj = $moneyback['waiver_oksj'];		
		//$endTime   = date('Y-m-d H:i:s',strtotime($waiver_sj)+10800);	
		$endTime   = $moneyback['waiver_oksj'];		
		$this->assign('endTime',$endTime);
		return $this->fetch('deal/order/cancel_waiver');
	}
	
	// 订单数据
	private function order($number)
	{
		$row = Db::name('down')->where('ddbh',$number)->find();
		$this->number = $row['ddbh'];
		return $row;
	}

	// 确定收货
	public function confirm($data)
	{
		$this->assign('data',$data);
		return $this->fetch('deal/order/confirm');
	}
	
	// 申请退款
	public function apply_back($data)
	{		
		$row = $this->order($data['number']);
		$moneyback  = Db::name('moneyback')->where(['ddbh'=>$row['ddbh'],'buy'=>session('usercode')])->find();
		$apply_back = (!$moneyback)?'apply_back':'apply_back_bc';
		// 可补充数量
		$count = Db::name('order_dispute')->where(['ddbh'=>$data['number'],'ubh'=>session('usercode')])->count();	
		$supplement = 10-$count;
		$this->assign('supplement',$supplement);
		$this->assign('count',$count);		
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/'.$apply_back);
	}
	
	// 发起追加
	public function append($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/append');
	}
	
	// 延长时间
	public function delay($data)
	{
		$order   = $this->order($data['number']);
		$moneydb = Db::name('moneydb')->where(['ddbh'=>$order['ddbh'],'buy'=>session('usercode')])->find();	
		$extend  = sj($moneydb['oksj'],sysconf('time_extend'));
		$data    = ['order'=>$order,'moneydb'=>$moneydb,'extend'=>$extend];
		$this->assign('data',$data);		
		return $this->fetch('deal/order/delay');
	}
	
	// 延迟担保详情
	public function delay_info($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/delay_info');
	}
	
	// 同意退款
	public function agree_back($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/agree_back');
	}
	// 拒绝退款
	public function refuse_back($data)
	{
		$row = $this->order($data['number']);
		$moneyback   = Db::name('moneyback')->where(['ddbh'=>$row['ddbh'],'sell'=>session('usercode')])->find();
		$refuse_back = (!$moneyback)?'refuse_back':'refuse_back_bc';
		// 可补充数量
		$count = Db::name('order_dispute')->where(['ddbh'=>$data['number'],'ubh'=>session('usercode')])->count();	
		$supplement = 10-$count;
		$this->assign('supplement',$supplement);
		$this->assign('count',$count);		
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/'.$refuse_back);
	}
	
	// 取消退款
	public function afresh_confirm($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/afresh_confirm');
	}
	
	// 重新处理退款
	public function afresh_agree_back($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/afresh_agree_back');
	}	
	
	// 申请客服介入
	public function service($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/service');
	}
	
	// 购买安装费
	public function buy_install($data)
	{
		$row = $this->order($data['number']);
		$code = Db::name('code')->where('bh',$row['codebh'])->find();		
		$this->assign('row',$row);
		$this->assign('code',$code);
		$this->assign('data',$data);
		return $this->fetch('deal/order/buy_install');
	}
	
	// 取消交易
	public function cancel($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/cancel');
	}
	
	// 我已发货
	public function delivery($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/delivery');
	}
	
	// 主动退款
	public function active_back($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/active_back');
	}	
	
	// 同意交易
	public function agree_deal($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/agree_deal');
	}
	
	// 拒绝交易
	public function refuse_deal($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/refuse_deal');
	}	
	
	// 退款历史记录
	public function back_history($data)
	{
		$order   = Db::name('down')->where('ddbh',input('number'))->find();
		$dispute = Db::name('order_dispute')->where('ddbh',$order['ddbh'])->select();		
		$this->assign('order',$order);
		$this->assign('dispute',$dispute);
		return $this->fetch('deal/order/back_history');
	}	
	
	// 订单围观
	public function oshare($data)
	{
		$row = $this->order($data['number']);
		$this->assign('row',$row);
		$this->assign('data',$data);
		return $this->fetch('deal/order/oshare');
	}	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\apage;

use think\Db;
use thinkct\library\Search;

class Seva extends Base
{
	public function index()
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$page       = $search->page();
		$page_size  = 20;
		// 查询条件
		$query['sell']   = input('bh');
		$query['ddzt']   = 'success';		
		//$query['filter'] = [];
		
		$datas = Db::name('down')->where($query)->limit($page_size)->page($page)->order('id desc')->select();
		$list  = [];
		foreach($datas as $data){
			$buy   = Db::name('buy')->where('bh',$data['buy'])->find();
			$rev   = Db::name('reviews')->where(['ddbh'=>$data['ddbh'],'type'=>'code'])->find();
			$goods = Db::name($data['type'])->where('bh',$data['codebh'])->find();
			$data['eval']   = $this->eva($rev['buyimp']);
			$data['rev_zt'] = $rev['zt'];
			$data['money']  = $data['money2'];
			$data['sj']     = xssj($rev['buytime'],'Y年m月d日 H:i');
			if($rev['zt'] == 1){
				$data['buycom'] = ($rev['buycom'] == null)? '交易完成，3天未评价，系统自动好评！':$rev['buycom'];
			}
			$data['typename'] = typename($data['type']);
			$data['url']      = c_url($goods['id'],$data['type']);
			$data['name']     = hide_name($buy['name']);
			$data['tx']       = $buy['tx'];
			$data['xy']       = xy($buy['rev_m'],"b");;
			$list[]           = $data;
		}		
		
		$total = Db::name('down')->where($query)->count();
		$this->assign('list',$list);	
		return [
			'eList' => $this->fetch('seva'),
			'total' => $total,
			'upage' => $this->upage($total,$page_size,$page)
		];
	}

	public function upage($count="30", $page_size="24", $page="1") {

		$count     = $count;//总数
		$page_size = $page_size;//每页显示
		$page      = ($page==0)? 1 : $page;//当前页
		//总页
		if($count % $page_size){
			$page_count=(int)($count/$page_size)+1;
		} else {
			$page_count=$count/$page_size;
		}
	
		if($page_count>1){

			//首页
			if($page!=1){				
				$page_i = ($page>=5)? "..." : '';
				$page_home = "<li><a href=\"javascript:selist(1);\" data-page=\"1\">1 {$page_i}</a></li>";
			}else{
				$page_home = "";
			}
			
			$si=2;
			if($page==1){
				$si=3;
			}elseif($page==2){
				$si=2;
			}
			
			//上页
			$page_upper ="";
			for($i=$page-$si;$i<$page;$i++){
				if($i>1){
					$page_upper .= "<li><a href=\"javascript:selist({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}	
			}		
			//中间
			$page_middle ="<li class=\"ohave\">{$page}</li>";
			
			//下页
			$page_lower ="";
		    for($i=$page+1;$i<=$page+$si;$i++){
				if($i<$page_count){
					$page_lower .= "<li><a href=\"javascript:selist({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}		
			}
			
			//尾页
			if($page<$page_count){
				$page_i = ($i<$page_count)? "..." : "";	
				$page_Last = "<a href=\"javascript:selist({$page_count});\" data-page=\"{$page_count}\">{$page_i} {$page_count}</a>";
			}else{
				$page_Last = "";
			}
			return "<div id=\"page\" total=\"{$page_count}\"><ul>{$page_home}{$page_upper}{$page_middle}{$page_lower}{$page_Last}</ul></div>";
		}
	}
}
?>
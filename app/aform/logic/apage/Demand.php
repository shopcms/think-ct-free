<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\apage;

use think\Db;
use thinkct\library\Search;

class Demand extends Base
{
	var $page_size = 10;
	var $table     = 'demand';
	
	public function index($type,$list,$page)
	{
		$mode      = input('mode');
		$key       = input('key');
		// 通过
		if($mode == "ing"){$zt = 1;}
		// 结束
		if($mode == "over"){$zt = 2;}
		// 审核
		if($mode == "audit"){$zt = ['in','0,3'];}
		// 条件搜索
		$search        = new Search;
		$query         = $search->query();
		$query['ubh']  = session('usercode');
		$query['zt']   = $zt;
		if($type=='task'){$this->table='task';}else{$query['type']=$type;}
		// 数据列表
		$data  = Db::name($this->table)->where($query)->order('lastgx desc')->page($page,$this->page_size)->select();
		$total = Db::name($this->table)->where($query)->count();
		$this->assign('data',$data);
		$this->assign('type',$type);
		return [
			'mode'    => $mode,
			'type'    => $type,
			'list'    => $list,
			'page'    => $page,
			'if_list' => $this->fetch('demand'),
			'total'   => $total,
			'upage'   => $this->upage($total,$this->page_size,$page)
		];
	}
	
	public function upage($count="30",$page_size="24",$page="1") {

		$count = $count;//总数
		$page_size = $page_size;//每页显示
		$page = (!isset($page))? 1 : $page;//当前页
		
		$url = rule_url("page", $page);
		//总页
		if($count % $page_size){
			$page_count=(int)($count/$page_size)+1;
		} else {
			$page_count=$count/$page_size;
		}
	
		if($page_count>1){
			
			//首页
			if($page!=1){				
				$page_i = ($page>=5)? "..." : "";
				$page_home = "<li><a href=\"javascript:list(1);\" data-page=\"1\">1 {$page_i}</a></li>";
			}else{
				$page_home = "";
			}
			
			$si=2;
			if($page==1){
				$si=3;
			}elseif($page==2){
				$si=2;
			}
			
			//上页
			$page_upper ="";
			for($i=$page-$si;$i<$page;$i++){
				if($i>1){
					$page_upper .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}	
			}
			
			//中间
			$page_middle ="<li class=\"ohave\">{$page}</li>";
			
			//下页
			$page_lower ="";
		    for($i=$page+1;$i<=$page+$si;$i++){
				if($i<$page_count){
					$page_lower .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}		
			}
			
			//尾页
			if($page<$page_count){
				$page_i = ($i<$page_count)? "..." : "";	
				$page_Last = "<a href=\"javascript:list({$page_count});\" data-page=\"{$page_count}\">{$page_i} {$page_count}</a>";			
			}else{
				$page_Last = "";
			}
			
			if($page_count > 10) {
				$jump = "<i class=\"iconfont jump_page\" title=\"跳转翻页\">&#xe6d0;</i><div class=\"jumpbox Search-box\"><div>跳转到第 <input type=\"text\" name=\"\" class=\"jump_inp Search-inp\"> 页 <button onclick=\"jump_page($('.jump_inp').val());\" type=\"button\" class=\"Search-btn\">确定</button></div></div>";
			}else{
				$jump = null;
			}
			
			return "{$jump}<div id=\"page\" total=\"{$page_count}\"><ul>{$page_home}{$page_upper}{$page_middle}{$page_lower}{$page_Last}</ul></div>";
		}
	}	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\apage;

use think\Db;
use thinkct\library\Search;

class Publicity extends Base
{
	var $page_size = 20;
	
	public function index()
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$page       = $search->page();
		
		//[];
		
		$list = Db::name('publicity')->where('1=1')->limit($this->page_size)->page($page)->order('id desc')->select();	
		foreach($list as $data){
			$type         = Db::name($data['filter'])->where('bh',$data['bh'])->find();
			$shop         = Db::name('sell')->where('bh',$type['ubh'])->find();
			$shop['name'] = hide_name($shop['name']);
			$shop['xy']   = xy($shop['rev_m'],'s');
			$this->assign('data',$data);
			$this->assign('type',$type);
			$this->assign('shop',$shop);
			$html = $this->fetch('publicity');
			array_push($list,$html);
		}
		$total = Db::name('publicity')->where($query)->count();
		return [
			'list'  => $list,
			'total' => $total,
			'upage' => null
		];
	}
}
?>
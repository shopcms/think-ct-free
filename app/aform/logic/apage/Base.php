<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\aform\logic\apage;

use think\Controller;
use think\Db;
use think\Request;


class Base extends Controller
{
    public function _initialize()
    {
        parent::_initialize();
		$authcode = authcode();
		//判断会员是否登录
		$user = null;
		$uc = null;
		if(session('?usercode')) {
			$user = Db::name('user')->where('bh',session('usercode'))->find();
			$uc = Db::name($user['uc'])->where('bh',session('usercode'))->find();
			$uc['id'] = null;			
			$user['qiandao'] = qiandao($user['bh'],'1');
			
		}
		$this->assign('user', $user);
		$this->assign('uc', $uc);


		//基本参数
		$this->assign('site_domain', sysconf('site_domain'));
		$this->assign('site_name', sysconf('site_name'));

        //注册平台
        if ($this->request->isMobile()) {
            //$this->view->config('view_platform', 'mobile');
        } else {
           // $this->view->config('view_platform', 'pc');
        }
		$this->view->config('view_platform', 'pc');

        //注册主题
        $this->view->config('view_theme', sysconf('index_theme') . DS);

        // 站点关闭
        if (sysconf('site_status') === '0') {
            die(sysconf('site_close_tips'));
        }
       //$nav = Db::name('nav')->where('status=1')->order('sort DESC')->select();
		
		//foreach($nav as $data){
			//$data['class'] = "bold";
			//$nav_list[] = $data;
		//}
        //$this->assign('nav_list', $nav_list);
    }
	
	public function eva($num){
		$eva = "good";
		if($num=="2"){
			//好评
			$eva="good";
		}elseif($num=="0"){	
			//中评
			$eva="normal";
		}elseif($num=="-1"){	
			//差评
			$eva="bad";	   	
		}	
		return $eva;
	}	
	
	public function upage($count="30", $page_size="24", $page="1") {

		$count = $count;//总数
		$page_size = $page_size;//每页显示
		$page = (!isset($page))? 1 : $page;//当前页
		//总页
		if($count % $page_size){
			$page_count=(int)($count/$page_size)+1;
		} else {
			$page_count=$count/$page_size;
		}
	
		if($page_count>1){
			
			//首页
			if($page!=1){				
				$page_i = ($page>=5)? "..." : "";
				$page_home = "<li><a href=\"javascript:list(1);\" data-page=\"1\">1 {$page_i}</a></li>";
			}else{
				$page_home = "";
			}
			
			$si=2;
			if($page==1){
				$si=3;
			}elseif($page==2){
				$si=2;
			}
			
			//上页
			$page_upper ="";
			for($i=$page-$si;$i<$page;$i++){
				if($i>1){
					$page_upper .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}	
			}
			
			//中间
			$page_middle ="<li class=\"ohave\">{$page}</li>";
			
			//下页
			$page_lower ="";
		    for($i=$page+1;$i<=$page+$si;$i++){
				if($i<$page_count){
					$page_lower .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}		
			}
			
			//尾页
			if($page<$page_count){
				$page_i = ($i<$page_count)? "..." : "";	
				$page_Last = "<a href=\"javascript:list({$page_count});\" data-page=\"{$page_count}\">{$page_i} {$page_count}</a>";			
			}else{
				$page_Last = "";
			}
			
			if($page_count > 10) {
				$jump = "<i class=\"iconfont jump_page\" title=\"跳转翻页\">&#xe6d0;</i><div class=\"jumpbox Search-box\"><div>跳转到第 <input type=\"text\" name=\"\" class=\"jump_inp Search-inp\"> 页 <button onclick=\"jump_page($('.jump_inp').val());\" type=\"button\" class=\"Search-btn\">确定</button></div></div>";
			}else{
				$jump = null;
			}
			
			return "{$jump}<div id=\"page\" total=\"{$page_count}\"><ul>{$page_home}{$page_upper}{$page_middle}{$page_lower}{$page_Last}</ul></div>";
		}
	}	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\apage;

use think\Db;

class FixedTool extends Base
{	
	public function index($type,$list,$page)
	{
		$post = input('post.');
		$action = input('post.action');	
		if(!session('?usercode')) {
			if($action == 'browse') {
				
				return $this->browse($page);
			}
			return ['state'=>'-2'];
		}
		// 自动加载
		if(session('?usercode')) {
		
			return $this->$action($page);
		}
	}

	// 浏览足迹
	private function browse($page)
	{
		$browseck    = cookie('browseck');
		//$browseck    = 'code_36,code_36,code_36';
		$his         = explode(',',$browseck);
		$pagenum     = count($his);
		$pagesize    = 20;
		$start       =($page-1)*$pagesize;// 偏移量，当前页-1乘以每页显示条数
		$his         = array_slice($his,$start,$pagesize);	
		$browse_list = [];
		if(!empty($browseck)){
			foreach($his as $v){
				$v     = explode('_',$v);
				$type  = $v[0];// 类型
				$id    = $v[1];// id	
				$goods = Db::name($type)->where('id',$id)->find();
				$sell  = Db::name('sell')->where('bh',$goods['ubh'])->find();
				$data['xy']   = xy($sell['rev_m'],'s');
				$data['sid']  = $sell['id'];
				$data['tx']   = $sell['tx'];
				$data['name'] = $sell['name'];	
				$data['tit']  = $goods['tit'];
				$data['url']  = c_url($id,$type);
				$data['type'] = typename($type);
				
				$browse_list[] = $data;
			}
		}
		$this->assign('browse_list',$browse_list);
		$list = $this->fetch('browse');	
		return [
			'curr-page'  => $page,
			'count'      => $pagenum,
			'total-page' => (int)($pagenum/$pagesize)+1,
			'list'       => $list,
			'total'      => $pagenum,
			'upage'      => null			
		];
	}
	
	// 个人信息
	private function my($page)
	{
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		$uc = Db::name($user['uc'])->where('bh',session('usercode'))->find();
		$user['qiandao'] = 	qiandao($user['bh'],'2');
		// 类型数量
		$sale_deal = Db::name('down')->where('sell',$user['bh'])->count();
		$sale_info = Db::name('code')->where('ubh',$user['bh'])->count();
		$buy_deal  = Db::name('down')->where('buy',$user['bh'])->count();
		$buy_info  = Db::name('task')->where('ubh',$user['bh'])->count();
		return [
			'avatar'           => "<img src='{$uc['tx']}'>",
			'name'             => $uc['name'],
			'money'			   => $user['money'],
			'jifen' 		   => $user['jifen'],
			'sale-info-number' => $sale_info,
			'sale-deal-number' => $sale_deal,
			'buy-deal-number'  => $buy_deal,
			'buy-info-number'  => $buy_info,
			'sign'             => "<a {$user['qiandao']}></a>",
			'total'            => 0,
			'upage'            => null		
		];	
	}
	
	// 消息列表
	private function message($page)
	{	
		$page_size = 20;
		$query = ['ubh'=>session('usercode'),'zt'=>0];		
		$message_list = Db::name('message')->where($query)
		->order('id desc')->limit($page_size)->page($page)->select();		
		$message_count = Db::name('message')->where($query)->count();	
		$this->assign('message_list',$message_list);
		$list = $this->fetch('message');		
		if($message_count % $page_size){
			$page_count = (int)($message_count/$page_size)+1;
		} else {
			$page_count = $message_count/$page_size;
		}		
		return [
			'curr-page' => $page,
			'count' => $message_count,
			'total-page' => ($page_count == 0)? 1:$page_count,
			'.message-count' => ($message_count < 1)? null:"<em>{$message_count}</em>",
			'list' => $list,
			'total' => $message_count,
			'upage' => null		
		];
	}	
	
	// 购物车
	private function cart($page)
	{
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		
		$page_size = 20;
		$query = ['ubh'=>session('usercode')];		
		$cart = Db::name('cart')->where($query)
		->order('id desc')->limit($page_size)->page($page)->select();	
		$cart_count = Db::name('cart')->where($query)->count();
		
		$cart_list = [];
		foreach($cart as $data){
			$row  = Db::name($data['type'])->where('bh',$data['codebh'])->find();
			$shop = Db::name('sell')->where('bh',$row['ubh'])->find();
			$data['shop_id']   = $shop['id'];
			$data['name']      = $shop['name'];
			$data['shop_url']  = "/ishop{$shop['id']}/";
			$data['contact']   = contact($shop['bh'],"sell",$data['type'],"p","span");;
			$data['bh']        = $row['bh'];
			$data['note_icon'] = note_icon($row,$data['type']);;
			$data['tit']       = $row['tit'];
			$data['img']       = get_tp($row['bh']);
			$data['mode']      = (isset($row['ifwp']))?$row['ifwp']:0;
			$data['url']       = c_url($row['id'],$data['type']);
			$data['typename']  = typename($data['type']);
			$data['money']     = $row['money'];
			$data['ifaz']      = (isset($row['ifaz']))?$row['ifaz']:0;
			$data['azmoney']   = (isset($row['azmoney']))?$row['azmoney']:0;
			$data['jfnum']     =  (isset($row['jfnum']))?$row['jfnum']:0;
			$data['jfs']       = (isset($row['jfnum']))?$row['money']*$row['jfnum']*10:0;
			$data['jfa']       =(isset($row['jfnum']))? $row['money']*$row['jfnum']:0;			
			$cart_list[]       = $data;
		}
		
		$this->assign('cart_list',$cart_list);
		$list = $this->fetch('cart');	

		if($cart_count % $page_size){
			$page_count = (int)($cart_count/$page_size)+1;
		} else {
			$page_count = $cart_count/$page_size;
		}		
		return [
			'myjifen' => $user['jifen'],//用户积分
			'jq' => "/static/thinkct/js/cart.js",
			'curr-page' => $page,
			'count' => $cart_count,
			'total-page' => $page_count,
			'.cart-count' => ($cart_count < 1)? null:"<em>{$cart_count}</em>",
			'list' => $list,
			'total' => $cart_count,
			'upage' => null			
		];
	}
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\apage;

use think\Db;
use thinkct\library\Search;

class Goods extends Base
{
	
	public function index($type,$list,$page)
	{
		// 商品数量同步
		goods_count(session('usercode'));
		
		$mode      = input('post.mode');
		$key       = input('key');
		$page_size = 10;
		$dd        = null;
		$fabu      = null;
		$html      = null;
		$uptime    = null;
		$au        = null;
		$updown    = null;
		$content   = null;
	
		//通过
		if($mode == "sale"){
			$zt = 1;
		}
		//下架
		if($mode == "down"){
			$zt = 2;
		}
		//审核
		if($mode == "audit"){
			$zt = ['in','0,3'];
		}
		// 条件搜索
		$search       = new Search;
		$query        = $search->query();
		$query['ubh'] = session('usercode');
		$query['zt']  = $zt;
		
		$type_list = Db::name($type)->where($query)->order('lastgx desc')->page($page,$page_size)->select();
		$total = Db::name($type)->where($query)->count();
		
		foreach($type_list as $row){
			//通过
			if($mode=="sale"){
				//刷新时间
				$sj="<span>刷新时间：".ifsj($row['lastgx'])."</span><span>人气：{$row['djl']}</span>";
				//链接地址
				$lianjie=c_url($row['id'],$type);$lianjie="href=\"{$lianjie}\"";
				//商品推广
				$au="<a class=\"\" href=\"/member/promotion/{$type}/{$row['id']}\" target=_blank><i class=\"va-2 iconfont\">&#xe6bd;</i> 推广</a>";
				//刷新商品
				$uptime="<a class=\"single_operate\" action=\"uptime\"><i>&#xe6be;</i> 刷新</a>";
				//下架商品
				$updown="<a class=\"single_operate\" action=\"updown\"><i>&#xe6c5;</i> 下架</a>";
			}
			//仓库
			if($mode=="down"){
				//刷新时间
				$sj="<span>刷新时间：".ifsj($row['lastgx'])."</span><span>人气：{$row['djl']}</span>";		
				//上架商品
				$updown="<a class=\"single_operate\" action=\"updown\"><i>&#xe904;</i> 上架</a>";
				//预览商品
				$lianjie="href=\"javascript:void(0);\" title=\"预览源码\" data=\info/{$type}/{$row['id']}\" onclick=\"pview(this);\"";		
			}
			//审核
			if($mode=="audit"){
				$fabu="<font color=\"red\">【发布】</font>";
				$sj="<span>最后提交时间：".ifsj($row['sj'])."</span>";
				$lianjie="href=\"javascript:void(0);\" title=\"预览源码\" data=\"info/{$type}/{$row['id']}\" onclick=\"pview(this);\"";
				$caozuo="";		
			}	
			//商品标题
			$tit=bianse($key,$row['tit']);
			//商品保障
			$note_icon=note_icon($row,$type);
			//商品封面
			$tp=get_tp($row['bh']);
			$img="<li class=\"l1\"><a {$lianjie} target=\"_blank\"><img src='{$tp}' width=\"90\" height=\"72\"></a></li>";
			//查询类型
			switch($type){
				case "code"://源码
				//演示站
				if($row['ysweb']!=""){$ysweb="<a href=\"{$row['ysweb']}\" target=\"_blank\" title=\"{$row['ysweb']}\" class=\"a1\">查看</a>";}else{$ysweb="无";}
				//移动端
				$app=spsx($row['app'],"2","移动端");
				if($row['app']!=""){$app="<a title=\"{$app}\" class=\"a1\">有</a>";}else{$app="无";}
				//加密
				$encrypt=spsx($row['encrypt'],"typename");
				//授权
				$auth=spsx($row['auth']);
				//规格
				$spec=spsx($row['spec']);
				$content="<font class=\"orange\">￥<cite class=\"C_money b\">{$row['money']}</cite></font>&nbsp;演示：<cite class=\"C_demo\">{$ysweb}</cite>&nbsp;移动端：<cite class=\"C_app\">{$app}</cite><a title=\"快捷修改常规参数\" class=\"quickset\" >大小：<cite class=\"C_sizes\">{$row['sizes']} MB</cite><br>加密：<cite class=\"C_encrypt\">{$encrypt}</cite><br>授权：<cite class=\"C_auth\">{$auth}</cite><br>规格：<cite class=\"C_spec\">{$spec}</cite></a>";
				break;	
				case "serve"://服务
				//是否计件
				if($row['piece']=="1"){$jijian="<a title=\"数量：{$row['piece']}\" class=\"a1\">有</a>";}else{$jijian="否";}
				//判断价格
				if($row['tmoney']=="1"){
					$money=money_tmoney($row);
					$money="价格范围：{$money}元";
				}else{
					$money="一口价：{$row['money']}元";
				}		
				$content="{$money}&nbsp;&nbsp;&nbsp;计件：{$jijian}";
				break;
				case "web"://网站
				$content="售价：{$row['money']}元&nbsp;&nbsp;&nbsp;域名：<a href=\"//{$row['name']}\" target=\"_blank\">查看</a>";
				break;
				case "domain"://域名
				$dd="class=\"demand\"";
				$note_icon="";$img="";
				break;		
			}	
			//查询状态
			switch($row['zt']){
				case "0"://审核
				$zt="<span class=blue><i>&#xe668;</i>审核中</span>";
				break;	
				case "1"://通过			
				$zt="<span><i>&#xe668;</i>出售中</span>";	
				break;
				case "2"://下架
				$zt="<span class=gray><i>&#xe668;</i>已下架</span>";
				break;
				case "3"://不通过
				case "4"://禁止销售
				$zt="<a href=\"/member/appeal/{$type}/{$row['bh']}/audit\" target=_blank><span class=red><i>&#xe668;</i>未通过</span> 详情</a>";
				break;		
			}
			//商品操作
			$del="<a class=\"single_operate\"  action=\"del\" data={$row['bh']}><i>&#xe647;</i> 删除</a>";
			$up="<a href=\"/member/form/{$type}/{$row['bh']}\" target=\"_blank\"><i>&#xe6aa;</i> 编辑</a>";
			$caozuo=$del.$up.$updown.$uptime;
			//html列表
			$html.="<dl><dt><input name=\"C1\" type=\"checkbox\" value=\"{$row['bh']}\" />{$sj}<span class='au'>{$au}</span></dt><dd {$dd}>{$img}<li class=\"l2\"><a class=\"tit\" {$lianjie} target=\"_blank\">{$fabu}<em class=\"C_tit\">{$tit}</em></a>{$content}</li><li class=\"l3\"><div class=\"gstate\">{$zt}</div><div class=\"note_icon\">{$note_icon}</div></li><li class=\"l4\">{$caozuo}</li></dd></dl>";

		}		
		
		return [
			'mode' => $mode,
			'type' => $type,
			'list' => $list,
			'page' => $page,
			'if_list' => $html,
			'total' => $total,
			'upage' => $this->upage($total,$page_size,$page)
		];
	}





	public function upage($count="30", $page_size="24", $page="1") {

		$count = $count;//总数
		$page_size = $page_size;//每页显示
		$page = (!isset($page))? 1 : $page;//当前页
		
		$url = rule_url("page", $page);
		//总页
		if($count % $page_size){
			$page_count=(int)($count/$page_size)+1;
		} else {
			$page_count=$count/$page_size;
		}
	
		if($page_count>1){
			
			//首页
			if($page!=1){				
				$page_i = ($page>=5)? "..." : "";
				$page_home = "<li><a href=\"javascript:list(1);\" data-page=\"1\">1 {$page_i}</a></li>";
			}else{
				$page_home = "";
			}
			
			$si=2;
			if($page==1){
				$si=3;
			}elseif($page==2){
				$si=2;
			}
			
			//上页
			$page_upper ="";
			for($i=$page-$si;$i<$page;$i++){
				if($i>1){
					$page_upper .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}	
			}
			
			//中间
			$page_middle ="<li class=\"ohave\">{$page}</li>";
			
			//下页
			$page_lower ="";
		    for($i=$page+1;$i<=$page+$si;$i++){
				if($i<$page_count){
					$page_lower .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}		
			}
			
			//尾页
			if($page<$page_count){
				$page_i = ($i<$page_count)? "..." : "";	
				$page_Last = "<a href=\"javascript:list({$page_count});\" data-page=\"{$page_count}\">{$page_i} {$page_count}</a>";			
			}else{
				$page_Last = "";
			}
			
			if($page_count > 10) {
				$jump = "<i class=\"iconfont jump_page\" title=\"跳转翻页\">&#xe6d0;</i><div class=\"jumpbox Search-box\"><div>跳转到第 <input type=\"text\" name=\"\" class=\"jump_inp Search-inp\"> 页 <button onclick=\"jump_page($('.jump_inp').val());\" type=\"button\" class=\"Search-btn\">确定</button></div></div>";
			}else{
				$jump = null;
			}
			
			return "{$jump}<div id=\"page\" total=\"{$page_count}\"><ul>{$page_home}{$page_upper}{$page_middle}{$page_lower}{$page_Last}</ul></div>";
		}
	}
}
?>
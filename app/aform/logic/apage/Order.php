<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\apage;

use think\Db;
use thinkct\library\Search;

class Order extends Base
{
	
	public function index($type,$list,$page)
	{
		$tisp="";
		$cz="";
		$fuzhu = null;
		$jf = null;
		$tisp_color='';
		
		$mode = input('post.mode');
		$page_size = 10;
		$if_list = null;
		
		// 条件搜索
		$search       = new Search;
		$query        = $search->query();
		$query[$mode] = session('usercode');

		$type_list = Db::name('down')->where($query)->order('sj desc')->page($page,$page_size)->select();
		
		//exit(var_dump($type_list));
		
		$total = Db::name('down')->where($query)->count();
		
		foreach($type_list as $order){		
		
			if($mode=='buy'){$dfRole='sell';$dfjs="卖家";}elseif($mode=='sell'){$dfRole='buy';$dfjs="买家";}		
			
			//exit(var_dump($order));	
			
			$ddbh=$order['ddbh'];//编号	
			$zt=$order['ddzt'];//状态	
			//$myorder_links="/member/myorder_".$ddbh.".html";//订单链接
			$myorder_links="/member/myorder/".$ddbh;//订单链接
			$jysj=ifsj($order['sj']);//交易时间	
			$g_link="http://".$order['type'].".test.com/".$order['mid']."/";//订单详情连接
			/*对方联系信息 开始*/
			$contact=contact($order[$dfRole],$dfRole,$order['type'],"a","span");
			$dfinfo=explode("||",user($order[$dfRole],'name|qq|phone|tx',$dfRole,$order['type']));
			//if(!empty($dfinfo[2])){$phone="<div class=\"phone\" title=\"联系电话\"><a>{$dfinfo[2]}</a></div>";}		
			$tit=$order['tit'];//标题
			$typename=typename($order['type']);//类型名称
			$url=c_url($order['codebh'],$order['type']);
			//新区判断
			$iff = null;
			$df = null;
			$fuzhu="<div class=\"note\"><b>备注:</b><a number=\"{$ddbh}\" class=\"order_note\">[编辑]</a><span title=\"{$order['bz']}\">{$order['bz']}</span></div>";
			if($mode=='buy') {
				$fuzhu="";
			}			
			if($order['type']=='code'){
				//源码交易
				$row1 = Db::name($order['type'])->where('bh',$order['codebh'])->find();
				//判断发货方式
				if($order['type']=="code"and$mode=="buy"){
					//买家的发货方式
					if($row1['ifwp']=="1" && $row1['wpurl']!=""){$fh="<a class=\"ah order_info\" action=\"takes\" number=\"{$ddbh}\">取货</a>";}else{$fh="<a class=\"ah0 order_info\" action=\"takes\" number=\"{$ddbh}\">手动</a>";}
					//买家的安装方式
					if($row1['azmoney']=="0"){
						$az="<a title=\"免费安装,点击查看详细要求\" class=\"installing az1\" data_s=\"1\" data_u=\"1\" data_d=\"{$row1['id']}\" data_m=\"0\" data_t=\"{$row1['tit']}\" data_i=\"{$row1['azsxid']}\" data_b=\"\">安装</a>";
					}else{
						$az="<a title=\"收费安装({$row1['azmoney']}元),点击查看详细要求\" class=\"installing az0\" data_s=\"0\" data_u=\"0\" data_d=\"{$row1['id']}\" data_m=\"1\" data_t=\"{$row1['tit']}\" data_i=\"{$row1['azsxid']}\" data_b=\"请注意各类源码的环境要求，还请需要协助安装的提前电话预约时间！\">安装</a>";
					}			
					$icon="<p class=\"icon buy\">".$fh.$az."</p>";
				}elseif($order['type']=="code"and$mode=="sell"){
					//卖家的发货方式				
					$fh="<a title=\"免费安装,点击查看详细要求\" class=\"installing az1\" data_d=\"{$row1['id']}\">免费安装</a>";
					$az="<a class=\"ah ah1\" href=\"/help/list/26\" target=\"_blank\">自动发货</a>";
					$icon="<p class=\"icon sell\">".$fh.$az."</p>";
				}				
				//积分低价
				if($order['jf']>"0"){$jf="<p>+<br>{$order['jf']}积分</p>";}else{$jf="";}
				$info="{$dfinfo[0]}|{$dfinfo['3']}";
				$tp=get_tp($row1['bh']);
				$imgs="<img border=0 src='{$tp}' width=50 height=50 />";
				$g_link=c_url($row1['id'],$order['type']);
			}else{
				//其他交易
				if($order['type']=='custom') {
					//若是自助
					$g_link=$myorder_links;
				}
				$name[$mode]="<font color='#247fbd'>您</font>";
				$name[$dfRole]="<font color='#ff0000'>".$df['name']."对方</font>";
				$imgs="<font color='#3366CC'>{$typename}</font>";
				if($order['fqbh']==session('usercode')) {
					//生成发起人和接受人
					$fqr=$name[$mode];
					$jsr=$name[$dfRole];
					$iff='fq';
				} else {
					$fqr=$name[$dfRole];
					$jsr=$name[$mode];
					$iff='js';
				}
			}

			$btn[$mode]="";
			$btn[$iff]="";


			switch($zt) {
				case 'wait': //等待发货
				case 'waiting': //发起交易
				case 'cfing': //重发交易
				case 'noing': //拒绝交易
				case 'qxing': //取消交易
				case 'fking': //等待付款
				case 'close': //永久拒绝交易
				$orbtn="交易详情";
				if($zt=='waiting'){
					$tisp="<font color=\"blue\">发起交易</font><p>等待 {$jsr} 的操作回应</p>";
				}elseif($zt=='wait'){
					//$restk=mysql_query("select tkly,cljg,jjsj,oksj from shop_moneyback where ddbh='".$ddbh."' and type='".$order[type]."'");$rowtk=mysql_fetch_array($restk);
					$rowtk = Db::name('moneyback')->where(['ddbh'=>$ddbh,'type'=>$order['type']])->find();
					$tisp="<font color=\"blue\">等待卖家发货</font><p>自动退款时间:<br><span class=\"ttime\" endtime=\"{$rowtk['oksj']}\"></span></p>";								
				} elseif($zt=='cfing') {
					$tisp="<span style='color:green'>重发交易</span><p>等待 $jsr 的再次操作回应</p>";
				} elseif($zt=='noing') {
					$tisp="<span class='red'>拒绝交易</span><p>$fqr 可以修改、重发或删除交易</p>";
				} elseif($zt=='qxing') {
					//$tisp="<span class='red'>取消交易</span><p>$fqr 可以修改、重发或删除交易</p>";
					$tisp="<font color=\"#999999\">取消交易</font><p>{$fqr} 可以修改、重发或删除交易</p>";
				} elseif($zt=='fking') {
					$tisp="<font color=\"#ff6600\">等待付款</font><p>{$jsr} 同意交易，等待 {$fqr} 付款中</p>";
					if($order['buy'] == session('usercode')) {
						//$btn[buy]="<br><a href='carpay.html?bh=$ddbh' target='_blank'>付款</a>";
						$btn['buy']="<a href=\"/member/cashier/{$ddbh}\" class=\"b\" target=\"_blank\" style=\"color:#f60\">付款</a>";
					}	
				} else {
					$tisp="<span class='red'>永久拒绝交易</span><p>双方只能对交易进行删除处理</p>";
					$cz="<a i-bh='$ddbh' onclick=\"dirsc('<strong>确定要<font color=#ff0000>删除交易</font>吗?</strong><br>删除后不可恢复，慎重操作','del',this)\"/>删除</a>";
				}
				break;
				default:
				switch($zt){				
					case 'db': //正在交易
					$orbtn="交易详情";
					//$resdb=mysql_query("select money2,oksj from shop_moneydb where ddbh='".$ddbh."' and type='".$order['type']."'");
					$rowdb = Db::name('moneydb')->where(['ddbh'=>$ddbh,'type'=>$order['type']])->find();
					//exit(var_dump($rowdb['oksj']));
					if($rowdb) {
						//$tisp="<span class='blue'>正在交易</span><p>自动完结时间:<br /><span class='ttime' endTime=''></span></p>";
						$tisp="<font color=\"blue\">卖家已发货</font><p>交易剩余时间:<br><span class=\"ttime\" endtime=\"{$rowdb['oksj']}\"></span></p>";
					}
					$order['money']=$rowdb['money2'];
					$btn['buy']="<br><a data=\"confirm\" class=\"order_handle\" number=\"$ddbh\">确认</a><br><a data=\"apply_back\" class=\"order_handle\" number=\"$ddbh\">退款</a>";
					break;
					case 'back': //买方申请退款
					case 'backerr': //卖方拒绝退款
					$rowtk = Db::name('moneyback')->where(['ddbh'=>$ddbh,'type'=>$order['type']])->find();
					if($zt=='back') {
						$tisp="<font color=\"red\">退款申请</font><p>自动退款时间:<br><span class=\"ttime\" endtime=\"{$rowtk['oksj']}\"></span></p>";
					} else {
						//拒绝退款
						$tisp="<font color=\"red\">拒绝退款</font><p>自动结束时间:<br><span class=\"ttime\" endtime=\"{$rowtk['oksj']}\"></span></p>";
					}
					$orbtn="<strong>退款详情</strong>";
					break;
					default:
					$orbtn="订单详情";
					if($zt=='success') {
						//交易成功
						$tisp="<br><a target=\"_blank\" href=\"/member/evaluation/{$ddbh}\">等待评价</a>";
						$btn[$mode]="<a target='_blank' href='/member/evaluation/{$ddbh}'>评价</a>";
						//$reviews=mysql_query("select * from shop_reviews where ddbh='".$ddbh."'");
						$reviews = Db::name('reviews')->where('ddbh',$ddbh)->find();
						if($reviews) {
							//查询评价
							if($reviews['buyimp']!='' && $reviews['sellimp']!='') {
								$tisp='<font class="gray">双方已评</font>';
								$btn[$mode]="";
							} elseif($reviews[$mode."imp"]!='') {
								$tisp='<font class="gray">我已评价</font>';
								$btn[$mode]="";
							} elseif($reviews[$dfRole."imp"]!='') {
								$tisp='<font style="color:#f60">对方已评</font>';
							}
							if($reviews[$mode."tisp"]==1) {
								//若有新内容加色显示
								$tisp_color='style="color:#f60"';
							}
							$tisp="<br /><a target='_blank' href='/member/evaluation/".$ddbh."' $tisp_color>".$tisp."</a>";
						}
						$tisp="<span class='green'>交易成功</span>".$tisp;
					} elseif($zt=='backsuc') {
						//退款成功
						$tisp="<span class='hui'>同意退款</span>";
					} elseif($zt=='forback') {
						//强制退款
						$tisp="<span class='hui'>强制退款</span>";
					}
					break;
					break;
				}
				break;
			}
			//新区判断
			if($order['type']!='code'){$icon="";}
			$if_list.="<dl><dt><span>订单号：{$ddbh}<font style=\"padding-left:10px;\">交易时间：</font>{$jysj} &nbsp;&nbsp;{$dfjs}：{$dfinfo[0]}</span>&nbsp;&nbsp;{$contact}</dt><dd><li class=\"l1\"><b>{$imgs}</b></li><li class=\"l2\"><a class=\"tit\" href=\"{$g_link}\" target=\"_blank\">[{$typename}]{$tit}</a>{$fuzhu}{$icon}</li><li class=\"l3\"><em>{$order['money1']}</em>{$jf}</li><li class=\"l4\">{$tisp}</li><li class=\"l5\"><a href=\"{$myorder_links}\" class=\"oxq\" target=\"_blank\">{$orbtn}</a>{$btn[$mode]}{$btn[$iff]}</li></dd></dl>";
		}		

		//总页
		if($total % $page_size){
			$page_count=(int)($total/$page_size)+1;
		} else {
			$page_count=$total/$page_size;
		}
		
		return [
			'if_list' =>$if_list,
			'total' => $total,
			'curPage' => $page,
			'totalPage' => $page_count,
			'upage' => $this->upage($total,$page_size,$page)
		];
	}
	
	public function upage($count="30", $page_size="24", $page="1") {

		$count = $count;//总数
		$page_size = $page_size;//每页显示
		$page = (!isset($page))? 1 : $page;//当前页
		
		$url = rule_url("page", $page);
		//总页
		if($count % $page_size){
			$page_count=(int)($count/$page_size)+1;
		} else {
			$page_count=$count/$page_size;
		}
	
		if($page_count>1){
			
			//首页
			if($page!=1){				
				$page_i = ($page>=5)? "..." : "";
				$page_home = "<li><a href=\"javascript:list(1);\" data-page=\"1\">1 {$page_i}</a></li>";
			}else{
				$page_home = "";
			}
			
			$si=2;
			if($page==1){
				$si=3;
			}elseif($page==2){
				$si=2;
			}
			
			//上页
			$page_upper ="";
			for($i=$page-$si;$i<$page;$i++){
				if($i>1){
					$page_upper .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}	
			}
			
			//中间
			$page_middle ="<li class=\"ohave\">{$page}</li>";
			
			//下页
			$page_lower ="";
		    for($i=$page+1;$i<=$page+$si;$i++){
				if($i<$page_count){
					$page_lower .= "<li><a href=\"javascript:list({$i});\" data-page=\"{$i}\">{$i}</a></li>";
				}		
			}
			
			//尾页
			if($page<$page_count){
				$page_i = ($i<$page_count)? "..." : "";	
				$page_Last = "<a href=\"javascript:list({$page_count});\" data-page=\"{$page_count}\">{$page_i} {$page_count}</a>";			
			}else{
				$page_Last = "";
			}
			
			if($page_count > 10) {
				$jump = "<i class=\"iconfont jump_page\" title=\"跳转翻页\">&#xe6d0;</i><div class=\"jumpbox Search-box\"><div>跳转到第 <input type=\"text\" name=\"\" class=\"jump_inp Search-inp\"> 页 <button onclick=\"jump_page($('.jump_inp').val());\" type=\"button\" class=\"Search-btn\">确定</button></div></div>";
			}else{
				$jump = null;
			}
			
			return "{$jump}<div id=\"page\" total=\"{$page_count}\"><ul>{$page_home}{$page_upper}{$page_middle}{$page_lower}{$page_Last}</ul></div>";
		}
	}	

}
?>
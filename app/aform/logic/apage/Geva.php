<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\apage;

use think\Db;
use thinkct\library\Search;

class Geva extends Base
{
	public function index($type,$list,$page)
	{
		// 条件查询
		$search     = new Search;
		$query      = $search->query();
		$page       = $search->page();
		$page_size  = 10;		
		// 查询条件
		$query['type']   = input('good');
		$query['codebh'] = input('pro');
		$query['ddzt']   = ['<>','backsuc'];
		
		$datas = Db::name('down')->where($query)->limit($page_size)->page($page)->order('id desc')->select();		
		$list  = [];
		foreach($datas as $data){
			$buy = Db::name('buy')->where('bh',$data['buy'])->find();
			$rev = Db::name('reviews')->where(['ddbh'=>$data['ddbh'],'type'=>'code'])->find(); 
			$data['eval']   = $this->eva($rev['buyimp']);
			$data['rev_zt'] = $rev['zt'];
			$data['sj']     = xssj($rev['buytime'],'Y年m月d日 H:i');
			$data['zjcom']  = $rev['zjcom'];
			$data['zjtime'] = $rev['zjtime'];			
			$data['hfcom']  = $rev['hfcom'];
			$data['hftime'] = $rev['hftime'];
			if($rev['zt'] == 1){
				$data['buycom'] = ($rev['buycom'] == null)? '交易完成，3天未评价，系统自动好评！':$rev['buycom'];
			}
			$data['name']   = hide_name($buy['name']);
			$data['tx']     = $buy['tx'];
			$data['xy']     = xy($buy['rev_m'],"b");;
			$list[]         = $data;
		}		
		$total = Db::name('down')->where($query)->count();
		//总页
		if($total % $page_size){
			$page_count = (int)($total/$page_size)+1;
		} else {
			$page_count = $total/$page_size;
		}
		$this->assign('list',$list);
		return [		
			'total_tips' => ($total>0)?"共 {$total} 笔":null,
			'curPage'    => $page,
			'totalPage'  => ($page_count < 1)? 1:$page_count,
			'c_r_rev'    => $this->fetch('geva'),
			'total'      => $total,
			'upage'      => $this->upage($total,$page_size,$page)			
		];
	}
}
?>
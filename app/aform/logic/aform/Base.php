<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------php
namespace app\aform\logic\aform;

use think\Controller;
use think\Db;
use think\Request;


class Base extends Controller
{
    public function _initialize()
    {
        parent::_initialize();
		$authcode = authcode();
		//判断会员是否登录
		$user = null;
		$uc = null;
		if(session('?usercode')) {
			$user = Db::name('user')->where('bh',session('usercode'))->find();
			$uc = Db::name($user['uc'])->where('bh',session('usercode'))->find();
			$uc['id'] = null;			
			$user['qiandao'] = qiandao($user['bh'],'1');
			
		}
		$this->assign('user', $user);
		$this->assign('uc', $uc);
		
		// 验证安全码
		//$safe = input('post.safe');
		//$user = Db::name('user')->where('bh',session('usercode'))->find();
		//if(sha1($safe) != $user['safe_code']){
			//return [
				//'state' => -1,
				//'info' => '安全码错误，请重新输入！',
				//'element' => 'input[name=safe]'
			//];
		//}
	}	
}
?>
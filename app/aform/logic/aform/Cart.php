<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Db;

class Cart extends Base
{
	
	public function cart()
	{
		// 删除没有支付的订单
		Db::name('dingdang')->where(['ddzt'=>'等待买家付款','ubh'=>session('usercode')])->delete();
		
		$id = input('post.id');
		$jf = input('post.jf');
		$ty = input('post.ty');
		$az = input('post.az');
		$authcode = authcode();
		if(!empty($id)) {
			$myjf = user(session('usercode'),'jifen','buy');
			$bh = shop_bh();
			$dingbh = $bh."|".session('usercode');
			$bharr = explode("|",$id);
			$jfarr = explode("|",$jf);
			$tyarr = explode("|",$ty);
			$azarr = explode("|",$az);
			
			$info = null;
			$ymoney_count = null;
			$xmoney_count = null;
			$jifen_count = null;
			
			for($i=0;$i<count($bharr);$i++){
				
				$row = Db::name($tyarr[$i])->where(['bh'=>$bharr[$i],'zt'=>1])->find();
				
				if($row){
					$gbh = $bharr[$i];//编号
					$money = $row['money'];//价格
					$ymoney_count = $ymoney_count+$money;//计算总原价 ，便于后面判断合法性
					$jifen = 0;//积分
					$type = $tyarr[$i];//类型
					//是源码、支持积分、使用积分则执行
					$anzhuang = 0;
					if($type == 'code'){
						if($row['jfnum']>0 && $jfarr[$i]>0){
							$jfmoney = $jfarr[$i]*0.1;//计算所用积分可抵价
							$zgj = $money*$row['jfnum'];//计算当前商品最高可让抵多少钱、
							//若使用超过可用即结束执行
							if($jfmoney>$zgj){exit('{"state":1,"info":"非法提交"}');}
							$money = $money-$jfmoney;//计算抵价后的金额
							$jifen = $jfarr[$i];//计算最终使用抵价
							$jifen_count = $jifen_count+$jifen;//支出总积分，目的同上
						}
						//积分抵价处理结束
						//增加安装费
						$azif=$azarr[$i];			
						if($azif == "1"){
							$anzhuang = $row['azmoney'];
						}else{
							$anzhuang = "0";				
						}
						$money = $money+$anzhuang;						
					}
					//总现价，目的同上
					$xmoney_count = $xmoney_count+$money;
					if($info!=''){$info.="|";}
								
					$info.=$type.",".$gbh.",".$money.",".$jifen.",".$anzhuang;
					//循环操作结束
				}				
			}
			
			//金额超半或者积分超支则返回值
			if($xmoney_count<($ymoney_count*0.5) || $jifen_count>$myjf){
				return json(['state'=>1,'info'=>'<b>金额超半或者积分超支</b>']);
			}else{
				// 删除之前存在的订单
				Db::name('dingdang')->where('bh',$bh)->delete();
				Db::name('dingdang')->insert([
					'bh'     => $bh,
					'ddbh'   => $dingbh,
					'ubh'    => session('usercode'),
					'sj'     => date("Y-m-d H:i:s"),
					'uip'    => $_SERVER["REMOTE_ADDR"],
					'money1' => $xmoney_count,
					'money2' => $xmoney_count,
					'ddzt'   => '等待买家付款',
					'info'   => $info,
					'type'   => 'cashier'
				]);
				return json(['state'=>301,'url'=>'/member/cashier/'.$bh]);
			}				
		}	
	}
	
	public function cart_delete()
	{
		$number = input('post.number');
		$mode = input('post.mode');
		if($mode=="1"){
			Db::name('cart')->where([
				'ubh' => session('usercode'),
				'codebh' => $number			
			])->delete();

			return json([
				'clear' => 1,
				'cart-count' => '<em>1</em>'	
			]);
		}		
	}
	
	public function cartadd()
	{
		$action = input('post.action');
		$info = input('post.info');
		$info = explode("|",$info);
		$type = $info[0];
		$bh = $info[1];
		
		if(!session('?usercode')) {
			return json(['state'=>'-2']);
		}
		
		$row = Db::name($type)->where('bh',$bh)->find();
		$cart = Db::name('cart')->where([
			'ubh'=>session('usercode'),
			'codebh'=>$bh
		])->find();
		
		if($row['ubh'] == session('usercode')){			
			return json(['state'=>'5','info'=>'<b>亲，不能自己购买自己的商品！</b>']);
		}
		
		// 购买商品
		if($action=="buy"){
			
			if(!$cart){
				Db::name('cart')->insert([
					'bh'	  => time(),
					'codebh'  => $bh,
					'ubh'	  => session('usercode'),
					'sj'      => date("Y-m-d H:i:s"),
					'money'   => $row['money'],
					'type'    => $type
				]);
			}
			return json(['state'=>301,'url'=>'/member/cart/']);
		}
		
		// 添加购物车
		if($action == "add"){
			
			if(!$cart){
				Db::name('cart')->insert([
					'bh'	  => time(),
					'codebh'  => $bh,
					'ubh'	  => session('usercode'),
					'sj'      => date("Y-m-d H:i:s"),
					'money'   => $row['money'],
					'type'    => $type
				]);
			}
			return json(['state'=>1]);
		}
	}

}
?>
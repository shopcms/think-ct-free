<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Model;
use think\Db;

class Sign extends Model
{
	public function index($mode)
	{	
		$jg = null;
		$jia = null;
		if($mode=="0"){
			$usercode = session('usercode');
			$authcode = authcode();
			if($usercode == null){echo "0";exit;}
			
			$row = Db::name('sign')->where('ubh',$usercode)->find();
			//var_dump($row);exit;
			// 判断是否是第一次签到 是则创建记录			
			if(!$row){
				Db::name('sign')->insert([
					'ubh' => $usercode,
					'lasttime' => date("Y-m-d H:i:s"),
					'qcount' => 1,
					'uip' => $_SERVER["REMOTE_ADDR"]
				]);
				$qjf=1;
				$qday=1;
				update_jifen($usercode, $qjf, "连续1天签到获得{$qjf}积分");
				$jg=1;
				$mtjf=2;				
			}else{
				if($row['lasttime']!=date('Y-m-d')){ 
				if($row['lasttime']==date('Y-m-d',strtotime('-1 day'))){
						 if($row['qcount']<6){
						 $qjf=$row['qcount']+1;
						 $qday=$row['qcount']+1;
						 }else{
						 $qjf=10;
						 $qday=7;
						 $jia="以上";
						 }
				 }else{
						 $qday=1;
						 $qjf=1;
				}
				Db::name('sign')->where('ubh',$usercode)->update([
					'lasttime' => date("Y-m-d H:i:s"),
					'qcount'   => $qday,
					'uip'      => $_SERVER["REMOTE_ADDR"]
				]);
				
				update_jifen($usercode, $qjf, "连续{$qday}天{$jia}签到获得{$qjf}积分");
				if($qday<6){$mtjf=$qday+1;}else{$mtjf="<b>10</b>";}
				$jg=1; 
				}
			}

			if($jg==1){

				return [
					'state' => "6",
					'info' => '<b>签到成功，您获得了<font color=red>'.$qjf.'</font>积分</b><br>已连续签到<font color=#ff6600>'.$qday.'</font>天，明天签到可获<font color=blue>'.$mtjf.'</font>积分',
					'fun' => "($('input.sign').length>0)?$('input.sign').replaceWith(\"<input type='button' class='signsuc' value='已签'>\"):$('a.sign').remove();layer.closeAll();",
				];
			}else{
				
				return [
					'state' => 0,
					'info' => "您今天已签到，明天再来！",
					'fun' => "($('input.sign').length>0)?$('input.sign').replaceWith(\"<input type='button' class='signsuc' value='已签'>\"):$('a.sign').remove();layer.closeAll();"				
				];
				//exit('{"state":5,"info":"您今天已签到，明天再来！"}');
			}
		}			
	}
}


?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Db;
use app\aform\logic\aform\Sendinfo;

class Verify extends Base
{
	public function _initialize()
	{
		if(session('?usercode')) {
			//$this->success('已经登录，请勿重复操作！', '/member/');
			$authcode = authcode();
		}	
	}

	// 绑定手机
	public function phone()
	{
		$flow = input('post.flow');
		$vcode = input('post.vcode');
		$uyzm = explode('|',session('uyzm'))[1];
		if($uyzm == $vcode) {
			// 首次绑定
			if($flow == 'into') {
				Db::name('user')->where('bh',session('usercode'))->update(['phone_zt'=>1]);
				return [
					'state' => 1,
					'info' => '<strong>操作成功</strong><br>手机已绑定成功！',
					'url' => 1
				];
			}
			// 解除绑定
			if($flow == 'check') {
				Db::name('user')->where('bh',session('usercode'))->update(['phone_zt'=>0]);
				session('verify_phone','ok');
				return [
					'state' => 301,
					'url' => '/member/verify/phone/setup'
				];	
			}
			// 绑定新手机
			if($flow == 'setup') {
				$phone = input('post.phone');
				if(session('verify_phone') != 'ok') {
					return [
						'state' => 0,
						'info' => '请先完成上一步操作！'
					];
				}				
				Db::name('user')->where('bh',session('usercode'))->update(['phone'=>$phone,'phone_zt'=>1]);
				session('verify_phone',null);
				return [
					'state' => 1,
					'info' => '<strong>操作成功</strong><br>新手机已绑定成功！',
					'url' => '/member/verify/phone'
				];
			}
		}else{
			return [
				'state' => -1,
				'info' => '验证码错误，请重新输入！',
				'element' => 'input[name=vcode]'		
			];
		}
	}
	
	// 绑定邮箱
	public function email()
	{
		$flow  = input('flow',0);
		$vcode = input('vcode');
		$eyzm  = explode('|',session('eyzm'))[1];
		if($eyzm == $vcode) {
			$action = input('action');
			// 注册验证邮箱
			if($flow == 0) {
				Db::name('user')->where('bh',session('usercode'))->update(['email_zt'=>1]);				
				return ['state'=>301,'url'=>'/member/'];
			}			
			// 首次绑定
			if($flow == 'into') {
				Db::name('user')->where('bh',session('usercode'))->update(['email_zt'=>1]);
				return ['state'=>1,'info'=>'<strong>操作成功</strong><br>邮箱已绑定成功！','url'=>1];
			}
			// 解除绑定
			if($flow == 'check') {
				Db::name('user')->where('bh',session('usercode'))->update(['email_zt'=>0]);
				session('verify_email','ok');
				return ['state'=>301,'url'=>'/member/verify/email/setup'];	
			}
			// 绑定新邮箱
			if($flow == 'setup') {
				$email = input('post.email');
				if(session('verify_email') != 'ok') {
					return ['state'=>0,'info'=>'请先完成上一步操作！'];
				}
				Db::name('user')->where('bh',session('usercode'))->update(['email'=>$email,'email_zt'=>1]);
				session('verify_email',null);
				return ['state'=>1,'info'=>'<strong>操作成功</strong><br>新邮箱已绑定成功！','url'=>'/member/verify/email'];
			}
		}else{
			return ['state'=>-1,'info'=>'验证码错误，请重新输入！','element'=>'input[name=vcode]'		];
		}
	}
	
	// 找回密码
	public function pass()
	{
		$vcode = input('post.vcode');
		$eyzm = explode('|',session('eyzm'))[1];
		if($eyzm == $vcode) {
			session('pass_setup','ok');
			return [
				'state' => 301,
				'url' => '/member/fetch/pass/setup'
			];
		}else{
			return [
				'state' => -1,
				'info' => '验证码错误，请重新输入！',
				'element' => 'input[name=vcode]'		
			];
		}		
	}	
	
	// 找回安全码
	public function safe()
	{
		$vcode = input('post.vcode');
		$uyzm = explode('|',session('uyzm'))[1];
		if($uyzm == $vcode) {
			session('safe_setup','ok');
			return [
				'state' => 301,
				'url' => '/member/fetch/safe/setup'
			];
		}else{
			return [
				'state' => -1,
				'info' => '验证码错误，请重新输入！',
				'element' => 'input[name=vcode]'		
			];
		}		
	}	
}
?>
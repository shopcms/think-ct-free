<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Model;
use think\Db;

class ShopAdmin extends Model
{
	// 客服
	public function contact()
	{	
 		$action  = input('action');
		$data    = input('data');
		$scene   = input('scene');
		$number  = (input('action')=='del')?'':$_POST['number'];
		$jobs    = (input('action')=='del')?'':$_POST['jobs'];
		$good    = input('good');
		$jobs_io = input('jobs_io');
		
		$sell    = Db::name('sell')->where('bh',session('usercode'))->find();
		$contact = $sell['contact'];		
		$data     = ($data=='')?$scene:$data;
		$typename = ($data=='i')?'全局':typename($data);
		// 获取到的客服内容
		preg_match_all('/<'.$good.'-'.$data.'>([^<>]+)<\/'.$good.'-'.$data.'>/',$contact,$result);
		preg_match_all('/<'.$good.'-'.$data.'>([^<>]+)<\/'.$good.'-'.$data.'>/',$contact,$data_result);
		// 删除客服
		if(input('action') == 'del'){
			$data_result = $data_result[1][0];
			$kefu        = "<{$good}-{$data}>{$data_result}</{$good}-{$data}>";	
			$data_result = str_replace($kefu,'',$contact);
			// 更改数据库
			Db::name('sell')->where('bh',session('usercode'))->update(['contact'=>$data_result]);
			// 写入操作记录	
			Db::name('user_log')->insert([
				'sj'   => sj(),
				'uip'  => uip(),
				'bh'   => shop_bh(),
				'ubh'  => session('usercode'),
				'tit'  => '删除',
				'txt'  => "删除店铺{$typename}QQ客服( {$data_result} )",
				'type' => 'contact'
			]);			
			return ['state'=>1,'info'=>'<b>删除成功</b>','url'=>1];
		}
		// 循环输出
		$i = 0;
		foreach($number as $v){
			$kfname = '';
			if($v != ''){
				$kfname.=$v.":".$jobs[$i];
			}
			$dataname[] = $kfname;
			$i++;
		}
		$data_name   = implode('|',$dataname);
		$data_number = implode(',',$number);
		// 新增客服
		if(input('data') == ''){			
			$kefu = "<{$good}-{$data}>{$data_name}</{$good}-{$data}>";			
		}	
		// 修改客服
		if(input('data') != ''){			
			$data_result = $data_result[1][0];
			$kefu = "<{$good}-{$data}>{$data_result}</{$good}-{$data}>";			
		}
		$data_result = str_replace($kefu,'',$contact);
		// 判断是否全局客服
		if($data == 'i'){
			$kfname = "<{$good}-{$data}>{$data_name}</{$good}-{$data}>".$data_result;
		}else{
			$kfname = $data_result."<{$good}-{$data}>{$data_name}</{$good}-{$data}>";
		}		
		// 更新联系客服
		Db::name('sell')->where('bh',session('usercode'))->update(['contact'=>$kfname]);
		// 写入操作记录
		Db::name('user_log')->insert(['sj'=>sj(),'uip'=>uip(),'bh'=>shop_bh(),'ubh'=>session('usercode'),'tit'=>'新增','txt'=>"新增店铺{$typename}QQ客服( {$data_number} )",'type'=>'contact']);		
		return ['state'=>1,'info'=>'<b>操作成功</b>','url'=>1];
	}
	
	// 导航
	public function nav()
	{
		$action = input('post.action');
		$data = input('post.data');
		$id = input('post.id');
		if($data == 'add') {
			Db::name('sell_nav')->insert([
				'sj'   => sj(),
				'uip'  => uip(),
				'ubh'  => session('usercode'),
				'tit'  => input('post.title'),
				'link' => input('post.link')			
			]);	
			return ['state'=>1,'info'=>'<b>操作成功</b>','url'=>1];
		}
		if($data == 'up') {
			Db::name('sell_nav')->where(['ubh'=>session('usercode'),'id'=>$id])
			->update(['tit'=>input('post.title'),'link'=>input('post.link')]);
			return ['state'=>1,'info'=>'<b>修改成功</b>','url'=>1];return ['state'=>1,'info'=>'<b>删除成功</b>','url'=>1];
		}
		if($action == 'sort') {
			return ['state'=>1,'info'=>'<b>操作成功</b>','url'=>1];
		}
		if($action == 'del') {
			Db::name('sell_nav')->where(['ubh'=>session('usercode'),'id'=>$data])->delete();
			return ['state'=>1,'info'=>'<b>删除成功</b>','url'=>1];		
		}		
	}
	
	// 技能
	public function skill()
	{	
		Db::name('sell')->where('bh',session('usercode'))->update(['skill'=>input('post.data')]);
		return ['state'=>1,'info'=>'<b>操作成功</b>','url'=>1];
	}
	
	// 推荐
	public function commend()
	{
		$action = input('action');
		$data   = input('data');		
		$good   = input('good');	
		$gid    = input('gid');
		// 新增推荐
		if($data == 'add'){
			// 判断是否添加上限
			$count = Db::name($good)->where(['tj'=>1,'ubh'=>session('usercode')])->count();
			$tyshu  = ($good=='code')?7:4;
			if($count >= $tyshu){
				return ['state'=>0,'info'=>'添加推荐'.typename($good).'失败已经上限'.$tyshu.'个','url'=>1];
			}
			// 修改推荐状态
			Db::name($good)->where(['ubh'=>session('usercode'),'id'=>$gid])
			->update(['tj'=>1]);
			return ['state'=>1,'info'=>'<b>操作成功</b>','url'=>1];
		}
		// 修改推荐
		if($data == 'up'){
			
			return ['state'=>1,'info'=>'<b>操作成功</b>','url'=>1];
		}
		if($action == 'del'){
			Db::name($good)->where(['ubh'=>session('usercode'),'id'=>$data])
			->update(['tj'=>0]);			
			return ['state'=>1,'info'=>'<b>删除成功</b>','url'=>1];		
		}
	}
	
	// 幻灯
	public function slide()
	{
		$action = input('post.action');
		$data   = input('post.data');			
		if($action == 'del'){
			Db::name('sell_slide')->where(['ubh'=>session('usercode'),'id'=>$data])->delete();
			return ['state'=>1,'info'=>'<b>删除成功</b>','url'=>1];		
		}		
	}
}
?>
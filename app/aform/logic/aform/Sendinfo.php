<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Db;
use app\common\util\Email;
use app\common\util\Sms;

class Sendinfo extends Base
{
	// 绑定邮箱
	public function email()
	{
		$email = input('post.email');
		return $this->send_email($email);
	}
	
	// 绑定手机
	public function phone()
	{
		$phone = input('post.phone');
		return $this->send_phone($phone);
	}
	
	// 找回安全码
	public function safe()
	{
		$phone = input('post.phone');
		return $this->send_phone($phone);
	}
	
	// 找回密码
	public function pass()
	{
		$email = input('post.email');
		return $this->send_email($email);
	}
	
	// 登录验证
	public function login($mode)
	{	
		$login_phone = input('post.login_phone');
		$user = Db::name('user')->where('phone',$login_phone)->find();
		if(!$user) {
			return ['state'=>'-1','info'=>'手机号不存在，请重试！','element'=>'input[name=login_phone]'];
		}
		return $this->send_phone($login_phone);
	}
	// 提现验证
	public function cashed($mode)
	{
		$phone = input('post.phone');
		return $this->send_phone($phone);
	}
	
	// 发送手机验证码
	public function send_phone($phone)
	{
		$uyzm_time = session('uyzm_time');
		$xtime = 90-(strtotime('now')-$uyzm_time);
		// 判断是否开启验证码
		// 判断是否开启验证码
		if(!isset($uyzm_time) || strtotime('now')-$uyzm_time>90){
			$rand = rand(1,9).rand(0,9).rand(0,9).rand(0,9);
			// 发送短信
			$screen = "您的验证码为：{$rand}，该验证码5分钟内有效，请勿泄露他人。";
			$sms = new Sms();
			$res = $sms->sendCode($phone,$screen,$rand);
			if($res === false) {
				return ['state'=>0,'info'=>$sms->getError()];
			}			
			session('uyzm',"{$phone}|{$rand}");
			session('uyzm_time',strtotime('now'));
			return [
				'state' => -1,
				'info' => '手机登陆验证发送成功！请在此输入验证码！',
				'color' => '#78BA32',
				'element' => 'input[name=vcode]'
			];
		}else{		
			return [
				'state' => 5,
				'info' => '90秒内只能获取一次验证码,请等待'.$xtime.'秒刷新本页再试！'
			];	
		}			
	}
	
	// 邮箱发送
	public function send_email($email)
	{
		$eyzm_time = session('eyzm_time');
		$xtime = 60-(strtotime('now')-$eyzm_time);
		// 判断是否开启验证码
		// 判断是否开启验证码
		if(!isset($eyzm_time) || strtotime('now')-$eyzm_time>60){
			$rand = rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
			// 发送邮箱
			$sms = new Email();
			$screen = '发布成功！！';
			$res = $sms->sendCode($email,$screen,$rand);
			if($res === false) {
				return ['state'=>0,'info'=>$sms->getError()];
			}
			session('eyzm',"{$email}|{$rand}");
			session('eyzm_time',strtotime('now'));				
			return [
				'state' => -1,
				'info' => '发送成功！请在此输入【验证码】，若一直无法收到验证短信请联系管理客服！',
				'color' => '#78BA32',
				'element' => 'input[name=vcode]'
			];			
		}else{
			return [
				'state' => 5,
				'info'  => '90秒内只能获取一次验证码,请等待'.$xtime.'秒刷新本页再试！'
			];		
		}	
	}
}	
?>
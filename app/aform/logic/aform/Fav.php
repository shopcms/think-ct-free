<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Model;
use think\Db;

class Fav extends Model
{
	public function fav()
	{
		$info = input('post.info');		
		$info_arr = explode("|",$info);	
		$type = $info_arr[0];
		$bh = $info_arr[1];

		if(!session('?usercode')) {
			
			return ['state'=>'-2','info'=>'请登录后再操作'];
		}
		
		if($type != "shop") {
			$row = Db::name($type)->where(['bh'=>$bh,'ubh'=>session('usercode')])->find();
			if($row){
				
				return ['state'=>2,'info'=>'不能收藏自己的商品'];
			}			
		}
			
		if(session('usercode') == $bh){
			
			return ['state'=>2,'info'=>'不能收藏自己的店铺'];
		}
		$fav = Db::name('fav')->where(['favbh'=>$bh,'ubh'=>session('usercode')])->find();
		if(!$fav){

			Db::name('fav')->insert([
				'sj' => date("Y-m-d H:i:s"),
				'bh' => time(),
				'favbh' => $bh,
				'ubh' => session('usercode'),
				'type' => $type
			]);
			
			return ['state'=>1,'info'=>'<b>成功收藏该'.typename($type).'！</b><br><a href=/member/lists/fav/type/'.$type.'/><font color=#0b72c4>查看我的收藏</font></a>'];
		}else{
			return ['state'=>0,'info'=>'<b>您已收藏过该'.typename($type).'了<br>不信？去<a href=/member/lists/fav/type/'.$type.'><font color=#0b72c4>看看</font></a>！</b>'];
		}		
		
		
	}
}


?>
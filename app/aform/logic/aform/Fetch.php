<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Db;
use app\aform\logic\aform\Sendinfo;

class Fetch extends Base
{
	public function _initialize()
	{
		if(session('?usercode')) {
			//$this->success('已经登录，请勿重复操作！', '/member/');
			$authcode = authcode();
		}	
	}
	
	// 找回安全码
	public function safe()
	{
		$flow = input('post.flow');
		// 开始验证
		if($flow == 'into') {
			$passWord     = input('post.passWord');
			$captcha_code = input('post.captcha_code');
			if(session('authcode') == $captcha_code) {
				$user = Db::name('user')->where(['bh'=>session('usercode'),'pwd'=>sha1($passWord)])->find();
				if(!$user) {
					return [
						'state' =>  -1,
						'info' => '登录密码错误，请重试！',
						'element' => 'input[name=passWord]'		
					];				
				}	
				// 发送手机验证码
				$sendinfo = new Sendinfo();
				$sendinfo->send_phone($user['phone']);
				session('phone',$user['phone']);
				session('safe_verify','ok');
				return [
					'state' => 301,
					'url' => '/member/fetch/safe/verify'
				];					
			}else{
				return [
					'state' => -1,
					'info' => '验证码错误，请重新输入！',
					'element' => 'input[name=captcha_code]'
				];
			}
		}

		// 验证完成，修改安全码
		if($flow == 'setup') {
			$fnew         = input('post.fnew');
			$rfnew        = input('post.rfnew');
			$captcha_code = input('post.captcha_code');
			$eyzm  = explode('|',session('eyzm'))[0];
			if(session('safe_setup') != 'ok') {
				return [
					'state' => 0,
					'info' => '请先完成上一步操作！',
				];
			}
			
			if(session('authcode') != $captcha_code) {
				return [
					'state' => -1,
					'info' => '验证码错误，请重新输入！',
					'element' => 'input[name=captcha_code]'
				];
			}
			
			if($fnew != $rfnew) {
				return [
					'state' => -1,
					'info' => '两次安全码不一致，请重新输入！',
					'element' => 'input[name=rfnew]'
				];
			}
			Db::name('user')->where('bh',session('usercode'))->update(['safe_code'=>sha1($fnew)]);
			session('safe_verify',null);
			session('safe_setup',null);
			return [
				'state' => 1,
				'info' => '账号安全码重置成功！',
				'url' => '/member/'
			];
		}
	}
	
	// 找回密码
	public function pass()
	{		
		$flow = input('post.flow');
		// 开始验证
		if($flow == 'into') {
			$backemail    = input('post.backemail');
			$backphone    = input('post.backphone');
			$captcha_code = input('post.captcha_code');		
			$user = Db::name('user')->where(['email'=>$backemail,'phone'=>$backphone])->find();
			if(session('authcode') == $captcha_code) {
				if(!$user) {
					return [
						'state' => 0,
						'info' => '找不到匹配该邮箱、手机的帐号，请重试！',
						'url' => 1			
					];				
				}
				// 发送邮件验证码
				$sendinfo = new Sendinfo();
				$sendinfo->send_email($user['email']);
				session('email',$user['email']);
				session('pass_verify','ok');
				return [
					'state' => 301,
					'url' => '/member/fetch/pass/verify'
				];			
			}else{
				return [
					'state' => -1,
					'info' => '验证码错误，请重新输入！',
					'element' => 'input[name=captcha_code]'
				];
			}
		}

		// 验证完成，修改密码
		if($flow == 'setup') {
			$fnew  = input('post.fnew');
			$rfnew = input('post.rfnew');
			$eyzm  = explode('|',session('eyzm'))[0];
			if(session('pass_setup') != 'ok') {
				return [
					'state' => 0,
					'info' => '请先完成上一步操作！',
				];
			}
			
			if($fnew != $rfnew) {
				return [
					'state' => -1,
					'info' => '两次密码不一致，请重新输入！',
					'element' => 'input[name=rfnew]'
				];
			}
			Db::name('user')->where('email',$eyzm)->update(['pwd'=>sha1($fnew)]);
			session('pass_verify',null);
			session('pass_setup',null);
			return [
				'state' => 1,
				'info' => '账号密码重置成功！',
				'url' => '/member/login'
			];
		}		
	}
}
?>
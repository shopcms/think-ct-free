<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\logic\aform;

use think\Db;
use app\aform\logic\aform\Sendinfo;

class User extends Base
{
	// 常规登录
	public function regular_login()
	{
		$id = input('post.id');	
		// 关闭常规登录
		if($id == 0) {
			Db::name('user')->where('bh',session('usercode'))->update([
				'regular' => $id
			]);
			return [
				'state' => 1,
				'info' => '<b>常规登录关闭成功！</b>',
				'url' => 1
			];
		}
		// 开启常规登录
		if($id == 1) {
			$safe = input('post.safe');
			$user = Db::name('user')->where('bh',session('usercode'))->find();
			if($user['safe_code'] != sha1($safe)) {
				return [
					'success' => 1,
					'state' => 1,
					'info' => '安全码错误，请重新输入！',
					'element' => 'input[name=safe]'
				];
			}
			Db::name('user')->where('bh',session('usercode'))->update([
				'regular' => $id
			]);
			return [
				'state' => 1,
				'info' => '<b>常规登录开启成功！</b>',
				'url' => 1
			];
		}
	}
	
	// 转型商家
	public function turnshop()
	{
		$first_input  = input('post.first_input');
		$first_select = input('post.first_select');
		$first_role   = input('post.first_role');
		$shopname     = input('post.shopname');
		$user = Db::name('user')->where(['uc'=>'buy','bh'=>session('usercode')])->find();
		$buy  = Db::name('buy')->where('bh',$user['bh'])->find();
		if($user){
			$sell = Db::name('sell')->where('name',$shopname)->find();
			if($sell){
				return [
					'state' => -1,
					'info' => '店铺名称经被其他用户使用，请重试！',
					'element' => 'input[name=shopname]'	
				];
			}
			// 转型身份
			Db::name('user')->where('bh',$user['bh'])->update(['uc' => 'sell']);
			$info = explode(' ',anquaninfo($user['bh'],'city','0'));
			$diqu = ($info[0] == null)? '中国地区':$info[0];			
			// 新建卖家表
			Db::name('sell')->insert([
				'sj'          => sj(),				
				'bh'          => $user['bh'],
				'name'        => $shopname,
				'tx'          => $buy['tx'],
				'address'     => $diqu,
				'score'       => '0|5.00|5.00|5.00|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0',
				'goods_count' => '{"code":0,"serve":0,"web":0,"domain":0,"total":0}',
				'contact'     => "<qq-i>{$buy['contact']}</qq-i>"			
			]);		
		}else{
			return ['state'=>0,'info'=>'您当前已是【商家】，无需转型！'];
		}	
		return [
			'state' => 1,
			'info'  => '<strong>店铺</strong> 开通成功<br>请尽快完善店铺资料吧！',
			'url'   => '/member/setup/shop'
		];
	}
	
	// 提现验证
    public function safever()
	{
		$safe  = input('post.safe');
		$phone = input('post.phone');
		$vcode = input('post.vcode');	
		$user  = Db::name('user')->where('bh',session('usercode'))->find();
		$uyzm  = explode('|',session('uyzm'));
		if(!isset($uyzm[1])){return ['state'=> -1,'info'=>'验证码错误，请重新输入！','element'=>'input[name=vcode]'];}		
		if($vcode == $uyzm[1]){
			// 条件验证
			if(sysconf('cash_verify')==2){
				if($user['idcard']==0){
					return json(['state'=>0,'info'=>'请先完成实名认证，在执行此操作！','url'=>'/member/certification/idcard']);
				}
			}			
			if(sha1($safe) != $user['safe_code']){
				return ['state'=> -1,'info'=>'安全码错误，请重新输入！','element'=>'input[name=safe]'];
			}	
			return ['state'=>1,'info'=>'验证成功执行提现！'];
		}else{
			return ['state'=> -1,'info'=>'验证码错误，请重新输入！','element'=>'input[name=vcode]'];
		}
	}
	public function safe()
	{
		//SafeTipsNo=1
		//safetytips=1
		$str  = input('post.str');	
		$user = Db::name('user')->where('bh',session('usercode'))->find();
		if($user['safe_code'] == sha1($str)) {
			
			session('SAFEPWD',$str);
			return ['success'=>1,'state'=>1,'info'=>'安全码验证成功！'];
		}else{
			
			return ['state'=>false,'info'=>'安全码错误，请重新输入！'];
		}
	}
	
	// 会员中心退出登录
	public function logout($name, $type)
	{
		session($name, $type);
		return json(['state'=>301,'url'=>'/']);
	}	

	// 登录返回
	public function ajax_login()
	{
		return [
			'state' => 1,
			'top_box' =>$this->fetch('index@/top_box'),
			'ruser#ruser' => $this->fetch('index@/ruser')
		];
	}
	
	// 退出返回
	public function ajax_logout($name, $type)
	{
		session($name, $type);
		return [
		    'state' => 1,
			'top_box' =>$this->fetch('index@/top_box'),
			'ruser#ruser' => $this->fetch('index@/ruser')
		];
	}
	
	// 注册账号
	public function reg()
	{
		$post = input('post.');		
		if($post['passWord'] != $post['passWord_enter']){
			return json(['state'=>3,'info'=>'两次输入的密码不一致']);
		}
		
		if(Db::name('buy')->where('name',$post['nick'])->find()) {	
			return json(['state'=>3,'info'=>'该昵称已被使用，返回修改昵称']);
		}
		
		if(Db::name('user')->where('email',$post['email'])->find()) {	
			return json(['state'=>3,'info'=>'此邮箱已被其他帐号绑定了，请返回更换其他邮箱']);
		}		

		if(Db::name('user')->where('phone',$post['phone'])->find()) {	
			return json(['state'=>3,'info'=>'此手机号已被其他帐号绑定了，请返回更换其他邮箱']);
		}		
		// 新建账号	
		$bh = 'u'.time();
		$tx = '//suzhizhan.oss-cn-beijing.aliyuncs.com/images/none.jpg';
		// 如果为第三方注册
		if(session('?oauth_data')){
			$data = session('oauth_data');
			Db::name('oauth')->insert(['sj'=>sj(),'uip'=>uip(),'ubh'=>$bh,'name'=>$data['name'],'code'=>$data['openid'],'tx'=>$data['tx'],'login'=>sj(),'type'=>$data['type'],'status'=>1]);
			$tx = $data['tx'];
			// 销毁登录缓存
			session('oauth_data',null);
		}
		$user_data = [
			'sj'        => sj(),
			'gxtime'    => sj(),
			'uip'       => uip(),
			'id'        => newuid(),
			'bh'        => $bh,
			'uc'        => 'buy',
			'email'     => $post['email'],
			'phone'     => $post['phone'],
			'pwd'       => sha1($post['passWord']),
			'safe_code' => sha1($post['safe'])
		];
		$user = Db::name('user')->insert($user_data);	
		$buy_data = [
			'bh'      => $user_data['bh'],
			'name'    => $post['nick'],
			'tx'      => $tx,
			'contact' => $post['qq']
		];	
		$buy = Db::name('buy')->insert($buy_data);
		session('usercode', $bh);
		anquan($bh,"login","注册登陆");
		// 发送注册邮箱
		$sendinfo = new Sendinfo;
		$sendinfo->send_email($post['email']);
		return ['state'=>301,'info'=>'注册成功！','url'=>'/member/reg/verify/'];
	}
	
	// 登录账号
	public function login()
	{	
		$post = input('post.');
		$ureturn = input('ureturn','/member/');
		// 常规登录
		if($post['login'] == 'name') {
			$user = Db::name('user')->where([
				'email|phone' => $post['login_name'],
				'pwd' 		  => sha1($post['login_pass'])
			])->find();

			if($user) {
				// 账号禁用检测
				if($user['zt'] == 0){
					return ['state'=>0,'info'=>'<b>该账户正在审核</b><br><font color=#666666>请联系客服证实审核状态</font>','url'=>1];		
				}					
				if($user['zt'] == 2){
					return ['state'=>0,'info'=>'<b>该账户审核不通过</b><br><font color=#666666>'.$user['ztsm'].'</font>','url'=>1];		
				}			
				// 检测常规登录				
				if($user['regular'] == 0) {
					return ['state' => 0,'info'=>'<b>该账户已关闭常规登录</b><br><font color=#666666>您可以使用手机或第三方快捷登录</font>','url'=>1];
				}				
				session('usercode', $user['bh']);
				anquan($user['bh'],"login","常规登陆");		
			}else{
				return ['state'=>-1,'info'=>"帐号或密码错误",'element'=>'input[name=login_pass]'];
			}
		}
		// 手机快捷登录
		if($post['login'] == 'phone') {			
			$user = Db::name('user')->where('phone',$post['login_phone'])->find();
			if($user) {
				$uyzm = explode('|',session('uyzm'));
				if($post['vcode'] == $uyzm['1']) {
					session('usercode', $user['bh']);
					anquan($user['bh'],"login","手机快捷登陆");	
				}else{
					return ['state'=>'-1','info'=>'手机验证码错误，请重新输入！','element'=>'input[name=vcode]'];
				}
			}else{
				return ['state'=>'-1','info'=>'手机号不存在，请重试！','element'=>'input[name=login_phone]'];				
			}			
		}
		// 最后登录时间
		Db::name('user')->where('bh',$user['bh'])->update(['gxtime'=>sj()]);
		// 返回登录信息
		if(isset($post['scene'])) {			
			return ['state'=>1];
		}else{
			return ['state'=>301,'url'=>$ureturn]; 
		}
	}
}
?>
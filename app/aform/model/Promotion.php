<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\model;

use think\Model;
use think\Db;

class Promotion extends Model
{
	// 新增推广
	public function add()
	{
		$ads     = input('ads');
		$paytype = input('paytype');
		$mid     = input('mid');
		$good    = input('good');
		$sj      = sj();
		$xfjifen = 0;
		$xfmoney = 0;
		$user    = Db::name('user')->where('bh',session('usercode'))->find();
		$jifen   = $user['jifen'];
		$money   = $user['money'];
		// 查询数据
		$data = Db::name('ads')->where('id',$ads)->find();
		if(!$data){
			return ['state'=>0,'info'=>'<b>购买失败！</b>广告不存在或已下架',];
		}
		if($paytype=='jifen' && $jifen>0){
			// 积分支付且积分大于0
			if($jifen >= $data['jifen']) {
				// 拥有积分大于所需积分
				$xfjifen = $data['jifen'];
				// 则消费积分=所需积分
			}else{
				// 若小于所需积分 则用余额补差
				$xfmoney = ($data['jifen']-$jifen)*0.1;
				// 否则计算还需补多少余额
				if($money < $xfmoney) {
					// 若余额不够补则提示充值
					return ['state'=>0,'info'=>'<b>购买失败！</b>您的账户可用余额不足，请先充值'];
				}
				$xfjifen = $jifen;
				// 把拥有积分全部消费掉
				PointUpdate(session('usercode'),$xfmoney*(-1),"购买推广广告",$mid);
				// 扣钱
			}
			PointUpdatejf(session('usercode'),"使用".$xfjifen."积分兑换推广广告",$xfjifen*(-1),$mid);
			// 执行扣分
		}else{		
			// 使用全余额支付
			if($money >= $data['money']) {
				// 拥有余额大于等于所需余额
				$xfmoney = $data['money'];
				// 则消费余额=所需余额
				PointUpdate(session('usercode'),$xfmoney*(-1),"购买推广广告",$mid);
				// 扣钱
			}else{
				return ['state'=>0,'info'=>'<b>购买失败！</b>您的账户可用余额不足，请先充值'];
			}
		}
		// 判断是否满位
		if(adnum($data['type2']) == 0) {
			$adzt = 2;//无位
		}else{
			$adzt = 1;//有位
		}
		// 执行购买
		$oksj = date("Y-m-d H:i:s",strtotime("+".$data['day']." day"));
		// 到期时间
		$tg = Db::name('tuiguang')->where(['pro'=>$mid,'ads'=>$data['type2']])->find();
		if($tg){
			// 有存在
			$tips = "1|<b>续期成功！</b>广告时长增加".$data['day']."天";
			$ses  = "dsj='".$oksj."',jifen='".$xfjifen."',money='".$xfmoney."',cyc='".$data['day']."',zt='".$adzt."'";
			// 用于已存在但已过期的更新
			if($tg['zt']==2) {
				//存在 等待中
				$tips .= "，目前正在等待中";
				$ses   = "";
			}elseif(strtotime($tg['dsj'])>=strtotime($sj)){
				//存在 展示中
				$tips .= "，目前正在展示中";
				$ses   = "";
			}elseif(strtotime($tg['dsj'])<strtotime($sj)){
				//存在 但已过期
				if($adzt == 1) {
					$tips = '<b>购买成功！</b>广告开始展示中';
				} else {
					$tips = '<b>购买成功！</b>正在排队等待展示中';
				}
			}
			if($ses == "") {
				$ses = "dsj=DATE_ADD(dsj,INTERVAL ".$data['day']." DAY),jifen=jifen+{$xfjifen},money=money+{$xfmoney},cyc=cyc+{$data['day']},zt=".$tg['zt'];
			}
			updatetable('shop_tuiguang',"asj='".$sj."',".$ses." where id='".$tg['id']."'");
		}else{
			// 没存在
			intotable('shop_tuiguang',"ubh,asj,dsj,pro,ads,jifen,money,cyc,zt","'".session('usercode')."','".$sj."','".$oksj."','".$mid."','".$data['type2']."','".$xfjifen."','".$xfmoney."','".$data['day']."','".$adzt."'");
			// 新建
			if($adzt==1) {
				$tips = '<b>购买成功！</b>广告开始展示中';
			}else{
				$tips = '<b>购买成功！</b>正在排队等待展示中';
			}
		}
		return ['state'=>1,'info'=>'<strong>'.typename($good).'推广</strong> 操作成功当前状态'.$tips,'url'=>'/member/lists/'.$paytype];
	}
}
?>
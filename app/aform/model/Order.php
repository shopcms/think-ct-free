<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\model;

use think\Model;
use think\Db;

// 订单处理操作
class Order extends Model
{
	// 申请客服介入
	public function service()
	{
		$order = Db::name('down')->where(['ddbh'=>input('number'),'buy|sell'=>session('usercode')])->find();
		if($order['service'] == 1){
			return ['state'=>5,'info'=>'<strong>当前交易已经执行此操作</strong>','url'=>1];
		}
		if($order['ddbh'] != input('number')){
			return ['state'=>5,'info'=>'<strong>交易不存在或无权操作</strong>','url'=>1];
		}
		// 新增交易纠纷
		Db::name('order_dispute')->insert([
			'sj'   => sj(),
			'uip'  => uip(),
			'bh'   => time(),
			'ubh'  => session('usercode'),
			'ddbh' => $order['ddbh'],
			'txt'  => '申请'.sysconf('site_name').'人工客服介入该笔交易纠纷。',
			'role' => 'message'
		]);
		Db::name('down')->where(['ddbh'=>input('number'),'buy|sell'=>session('usercode')])->update(['service'=>1]);
		// 交易申请客服介入通知
		noticeemail()->service($order['ddbh']);			
		return ['state'=>1,'info'=>'<strong>申请客服介入</strong> 操作成功<br>请您耐心等待，客服人员会尽快介入该交易','url'=>1];
	}
	
	// 延长时间
	public function delay()
	{
		$order = Db::name('down')->where(['ddbh'=>input('number'),'buy|sell'=>session('usercode')])->find();
		if($order['ddbh'] != input('number')){
			return ['state'=>5,'info'=>'<strong>交易不存在或无权操作</strong>','url'=>1];
		}
		if($order['ddzt'] != 'db') {
			return ['state'=>5,'info'=>'<strong>当前交易状态不允许操作此项</strong>','url'=>1];
		}
		if($order['ifyc'] == 1) {
			return ['state'=>5,'info'=>'<strong>只允许延长1次担保时间</strong>','url'=>1];
		}
		$moneydb = Db::name('moneydb')->where(['ddbh'=>$order['ddbh'],'buy'=>session('usercode')])->find();		
		if($moneydb['ddbh'] != $order['ddbh']){
			return ['state'=>5,'info'=>'<strong>交易不存在或无权操作</strong>','url'=>1];
		}
		$extend  = sysconf('time_extend');
		$newoksj = date("Y-m-d H:i:s",strtotime("+".$extend." day",strtotime($moneydb['oksj'])));
		Db::name('moneydb')->where('ddbh',$order['ddbh'])->update(['oksj'=>$newoksj]);
		// 订单字段设为已延长
		Db::name('down')->where('ddbh',$order['ddbh'])->update(['ifyc'=>1]);	
		// 交易延长担保时间通知
		noticeemail()->delay($order['ddbh']);		
		return ['state'=>1,'info'=>'<strong>延长担保时间</strong> 操作成功','url'=>1];
	}
	
	// 申请退款
	public function apply_back()
	{
		$order = Db::name('down')->where(['ddbh'=>input('number'),'buy|sell'=>session('usercode')])->find();
		if($order['ddbh'] != input('number')){
			return ['state'=>5,'info'=>'<strong>交易不存在或无权操作</strong>','url'=>1];
		}
		// 判断是否为部分退款
		if(input('backtype') == 'part'){
			if(input('backmoney') > $order['money1']){
				return ['state'=>0,'info'=>'部分退款金额不能大于当前订单金额','url'=>1];
			}
			// 订单改为部分退款
			Db::name('down')->where('ddbh',input('number'))->update(['backmoney'=>input('backmoney')]);
		}
		// 判断是否存在担保
		$moneydb   = Db::name('moneydb')->where(['ddbh'=>$order['ddbh'],'buy'=>session('usercode')])->find();
		// 若没有担保记录则创建补充
		if(!$moneydb){
			$dispute = order_dispute($order['ddbh'],input('txt'));
			if($dispute == 1){
				return ['state'=>1,'info'=>'<strong>补充退款</strong>操作成功！','url'=>1];
			}
			return ['state'=>0,'info'=>$dispute,'url'=>1];	
		}
		// 判断是否存在退款
		$moneyback = Db::name('moneyback')->where(['ddbh'=>$order['ddbh'],'buy'=>session('usercode')])->find();
		// 若没有退款记录则创建退款
		if(!$moneyback){
			// 创建退款原因
			$dispute = order_dispute($order['ddbh'],input('txt'),input('main_reason'));
			if($dispute != 1){
				return ['state'=>0,'info'=>$dispute,'url'=>1];	
			}			
			$refund  = sysconf('time_refund');
			$oksj    = date("Y-m-d H:i:s",strtotime("+".$refund." day"));
			Db::name('moneyback')->insert([
				'bh'     => time(),
				'money1' => $moneydb['money1'],
				'money2' => $moneydb['money2'],
				'sj'     => sj(),
				'oksj'   => $oksj,
				'sell'   => $order['sell'],
				'buy'    => $order['buy'],
				'tkly'   => input('txt'),
				'cljg'   => '等待卖家处理',
				'zt'     => 0,
				'ddbh'   => $order['ddbh'],
				'jf'     => $moneydb['jf'],
				'type'   => $order['type']
			]);	
		}
		// 删除正在交易信息
		Db::name('moneydb')->where(['buy'=>$order['buy'],'ddbh'=>$order['ddbh']])->delete();
		// 更新订单状态
		Db::name('down')->where(['buy'=>session('usercode'),'ddbh' =>$order['ddbh']])->update(['ddzt'=>'back']);
		if($order['type'] == 'custom') {
			// 删除未完成追加记录
			Db::name('custom')->where([
				'ddbh'    => $order['ddbh'],
				'txttype' => ['<>','main'],
				't'       => ['<>','success']
			])->delete();
		}
		// 订单退款邮箱通知
		noticeemail()->tuikuan($order['ddbh'],input('txt'));
		return ['state'=>1,'info'=>'<strong>申请退款</strong>操作成功<br>等待卖家处理退款中','url'=>1];
	}	
	
	// 拒绝退款
	public function refuse_back()
	{
		$order = Db::name('down')->where(['ddbh'=>input('number'),'buy|sell'=>session('usercode')])->find();
		if($order['ddbh'] != input('number')){
			return ['state'=>5,'info'=>'<strong>交易不存在或无权操作</strong>','url'=>1];
		}
		$moneyback = Db::name('moneyback')->where(['ddbh'=>$order['ddbh'],'sell'=>session('usercode')])->find();
		if(!$moneyback){
			return ['state'=>5,'info'=>'<strong>退款不存在或无权操作</strong>','url'=>1];				
		}	
		// 若交易状态拒绝退款则创建补充
		if($order['ddzt'] == 'backerr'){
			// 补充退款原因
			$dispute = order_dispute($order['ddbh'],input('txt'));
			if($dispute == 1){
				return ['state'=>1,'info'=>'<strong>补充拒绝</strong>操作成功！','url'=>1];
			}
			return ['state'=>0,'info'=>$dispute,'url'=>1];		
		}
		// 创建拒绝原因
		$dispute = order_dispute($order['ddbh'],input('txt'));
		if($dispute != 1){
			return ['state'=>0,'info'=>$dispute,'url'=>1];	
		}		
		// 更新订单状态
		Db::name('down')->where(['ddbh'=>$order['ddbh'],'sell'=>session('usercode')])->update(['ddzt'=>'backerr']);
		// 更新退款状态
		$refund = sysconf('time_refund');
		$oksj   = date("Y-m-d H:i:s",strtotime("+".$refund." day"));
		if($order['type'] == 'custom') {
			// 自助交易 删除未完成追加记录
			Db::name('custom')->where(['ddbh'=>$order['ddbh'],'txttype'=>['<>','main'],'t'=>['<>','success']])->delete();
		}
		Db::name('moneyback')->where(['sell'=>session('usercode'),'ddbh'=>$order['ddbh']])->update(['zt'=>1,'cljg'=>input('txt'),'jjsj'=>sj(),'oksj'=>$oksj]);
		// 订单拒绝邮箱通知
		noticeemail()->jujue($order['ddbh'],input('txt'));		
		return ['state'=>1,'info'=>'<strong>拒绝退款</strong>　操作成功<br />此交易已冻结，进入客服人工处理阶段','url'=>1];			
	}







	
}
?>
<?
// +----------------------------------------------------------------------
// | ThinkCt框架[基于ThinkPHP6.0开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.thinkct.net
// +----------------------------------------------------------------------
// | ThinkCt承诺基础框架商业版永久免费技术支持，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 时光网络 <66983239@qq.com>，开发者QQ群：61162800 付费QQ群：250059867
// +----------------------------------------------------------------------
namespace app\aform\model;

use think\Db;

class Reviews 
{
	// 评价订单
	public function first()
	{
		$imp     = input('imp');
		$bh      = input('bh');
		$role    = input('role');
		$order   = Db::name('down')->where(['ddbh'=>$bh,$role=>session('usercode')])->find();
		$reviews = Db::name('reviews')->where('ddbh',$bh)->find();		
		if($imp == 0){
			$imp = 2;
			$com = '好评';
		}elseif($imp == 1){
			$imp = 2;
			$com = '中评';
		}elseif($imp == 2){
			$imp = '-1';
			$com = '差评';
		}		
		$name = "对交易作出首次评价（{$com}）";
		if($reviews[$role.'imp'] != null){
			return ['state'=>5,'info'=>'您已评价过了'];
		}
		// 若没查到则新建
		if(!$reviews){
			Db::name('reviews')->insert([
				'ddbh' => $bh,
				'pro'  => $order['codebh'],
				'type' => $order['type'],
				'buy'  => $order['buy'],
				'sell' => $order['sell']
			]);
		}
		// 首次评价处理
		$name = "对交易作出首次评价（{$com}）";
		// 更新评价订单状态
		Db::name('down')->where('ddbh',$bh)->setInc('rev_zt',1);
		$ses = [
			$role.'com'  => input('txt','好评！'),
			$role.'imp'  => $imp,
			$role.'time' => sj()
		];	
		if($role == 'buy'){
			$ses['Attitude']   = input('attitude');
			$ses['efficiency'] = input('efficiency');
			$ses['quality']    = input('quality');
		}
		// 更新评价表
		Db::name('reviews')->where(['ddbh'=>$bh,$role=>session('usercode')])->update($ses);
		uprev($order['sell'],$order['buy']);
		// 更新双方评分评价值
		// 2019-08-10 23:39:40	买家	撤销了中评（已改为双方5分好评）
		// 新建操作记录
		Db::name('user_log')->insert([
			'sj'   => sj(),
			'uip'  => uip(),
			'bh'   => $bh,
			'ubh'  => session('usercode'),
			'tit'  => '订单评价',
			'txt'  => $name,
			'role' => $role,
			'type' => 'evaluation'
		]);
		return ['state'=>1,'info'=>'评价成功','url'=>1];
		//return ['state'=>-3,'fun'=>'layer.closeAll();layer.alert(\"<b>评价成功</b>\",{icon:1}, function(index){$(\'.Evalist\').length>0?list(\'R\'):location.reload();layer.close(index);});'];
	}

	// 买家追评
	public function append()
	{
		$name = '对交易作出追加评价';
		$bh   = input('bh');
		$role = input('role');
		$reviews = Db::name('reviews')->where(['ddbh'=>$bh,$role=>session('usercode')])->find();		
		if($reviews['buy'] != session('usercode')){
			return ['state'=>5,'info'=>'您非买家，无权操作'];			
		}		
		Db::name('reviews')->where('id',$reviews['id'])->update([
			'zjcom'  => input('txt'),
			'zjtime' => sj()
		]);
		// 新建操作记录
		Db::name('user_log')->insert([
			'sj'   => sj(),
			'uip'  => uip(),
			'bh'   => $bh,
			'ubh'  => session('usercode'),
			'tit'  => '订单评价',
			'txt'  => $name,
			'role' => $role,
			'type' => 'evaluation'
		]);		
		return ['state'=>1,'info'=>'追加评价成功','url'=>1];
	}
	
	// 卖家回复
	public function reply()
	{
		$name = '回复了买家的首次评价';		
		$bh   = input('bh');
		$role = input('role');
		$reviews = Db::name('reviews')->where(['ddbh'=>$bh,$role=>session('usercode')])->find();		
		if($reviews['sell'] != session('usercode')){
			return ['state'=>5,'info'=>'您非卖家，无权操作'];			
		}		
		Db::name('reviews')->where('id',$reviews['id'])->update([
			'hfcom'  => input('txt'),
			'hftime' => sj()
		]);
		// 新建操作记录
		Db::name('user_log')->insert([
			'sj'   => sj(),
			'uip'  => uip(),
			'bh'   => $bh,
			'ubh'  => session('usercode'),
			'tit'  => '订单评价',
			'txt'  => $name,
			'role' => $role,
			'type' => 'evaluation'
		]);
		return ['state'=>1,'info'=>'回复评价成功','url'=>1];
	}
}
?>